package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import praktyki.core.entities.UzytkownicyEntity;
import praktyki.core.service.Interface.iUzytkownicyService;

import java.util.List;

/**
 * Created by dawid on 25.03.15.
 */

/**
 * UsersController<br>
 * Offers URL requests for working with Users in DB/Testing purpose
 */
@Controller
@RequestMapping("/uzytkownicy")
public class UzytkownicyController {

    private iUzytkownicyService iUzytkownicyService;

    @Autowired
    public UzytkownicyController(iUzytkownicyService iUzytkownicyService) {
        this.iUzytkownicyService = iUzytkownicyService;
    }

    /**
     * Finds every User in DB
     * @return list of every User
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<UzytkownicyEntity>> findAll() {
        List list = iUzytkownicyService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<UzytkownicyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<UzytkownicyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets User with given ID
     * @param idUzytkownika ID of User
     * @return single User
     */
    @RequestMapping(value = "/{idUzytkownika}", method = RequestMethod.GET)
    public ResponseEntity<UzytkownicyEntity> getUser(@PathVariable int idUzytkownika) {
        UzytkownicyEntity user = iUzytkownicyService.getUser(idUzytkownika);
        if (user !=null) {
            return new ResponseEntity<UzytkownicyEntity>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<UzytkownicyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Searches Users for one with given Login
     * @see praktyki.core.entities.UzytkownicyEntity
     * @param searchString search by Login
     * @param sortType ASC or DESC
     * @return list of serached Users
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<List<UzytkownicyEntity>> getUserByLogin(@RequestParam String searchString, @RequestParam String sortType) {
        List list = iUzytkownicyService.getUserByLogin(searchString, sortType);
        if (list !=null) {
            return new ResponseEntity<List<UzytkownicyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<UzytkownicyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Changed Blockage of User with given ID
     * @param idUzytkownika ID of User
     * @param blokada Blockage status - true OR false
     * @return Blockage Status of given User
     */
    @RequestMapping(value = "/{idUzytkownika}/blokada/{blokada}", method = RequestMethod.PATCH)
    public ResponseEntity<UzytkownicyEntity> editBlockade(@PathVariable int idUzytkownika, @PathVariable Boolean blokada) {
        UzytkownicyEntity blockage = iUzytkownicyService.editBlockade(idUzytkownika, blokada);
        if (blockage !=null) {
            return new ResponseEntity<UzytkownicyEntity>(blockage, HttpStatus.OK);
        } else {
            return new ResponseEntity<UzytkownicyEntity>(HttpStatus.NOT_FOUND);
        }
    }
}
