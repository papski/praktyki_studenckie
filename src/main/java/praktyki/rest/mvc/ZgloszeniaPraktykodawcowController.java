package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.ZgloszeniaPraktykodawcowEntity;
import praktyki.core.service.Interface.iZgloszeniaPraktykodawcowService;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.List;

/**
 * Created by dawid on 11.03.15.
 */

/**
 * EmployerApplicationsController<br>
 * Offers URL requests for working with Employer's Application in DB.<br>
 */
@Controller
@RequestMapping("/zgloszeniapraktykodawcow")
public class ZgloszeniaPraktykodawcowController {

    private iZgloszeniaPraktykodawcowService izgloszeniaPraktykodawcowService;

    @Autowired
    public ZgloszeniaPraktykodawcowController(iZgloszeniaPraktykodawcowService izgloszeniaPraktykodawcowService) {
        this.izgloszeniaPraktykodawcowService = izgloszeniaPraktykodawcowService;
    }

    /**
     * Adds Employer's Application into DB
     * @see praktyki.core.entities.ZgloszeniaPraktykodawcowEntity
     * @param zgloszeniaPraktykodawcowEntity Employer's Application to be persisted
     * @return persisted Application
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ZgloszeniaPraktykodawcowEntity> addEmployerApplication(@RequestBody ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity) {
        ZgloszeniaPraktykodawcowEntity addApplication = izgloszeniaPraktykodawcowService.addEmployerApplication(zgloszeniaPraktykodawcowEntity);
        if (addApplication !=null) {
            return new ResponseEntity<ZgloszeniaPraktykodawcowEntity>(addApplication, HttpStatus.OK);
        } else {
            return new ResponseEntity<ZgloszeniaPraktykodawcowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Employer's Application in DB.
     * @return list of every Employer's Application
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ZgloszeniaPraktykodawcowEntity>> findAll() {
        List list = izgloszeniaPraktykodawcowService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<ZgloszeniaPraktykodawcowEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<ZgloszeniaPraktykodawcowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Application of Employer by Course or/and Academic Year.
     * If we want to ignore Course or Academic Year we put 0 in
     * URL request.
     * @param idPraktykodawcy ID of Employer
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return result of searched Employer's Applications
     */
    @RequestMapping(value = "/praktykodawca/{idPraktykodawcy}/kierunek/{idKierunku}/rok/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<ZgloszeniaPraktykodawcowEntity>> getAllApplicationsOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idKierunku, @PathVariable int idRokuAkademickiego) {
        List list = izgloszeniaPraktykodawcowService.getAllApplicationsOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<ZgloszeniaPraktykodawcowEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<ZgloszeniaPraktykodawcowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Employer's Application Info from multiple entities and
     * shows them in one JSON.
     * @see praktyki.rest.mvc.helpers.ZgloszenieInfo
     * @param idZgloszeniaPraktykodawcy ID of Application
     * @return Helper class with info about Application
     */
    @RequestMapping(value = "/{idZgloszeniaPraktykodawcy}", method = RequestMethod.GET)
    public ResponseEntity<ZgloszenieInfo> getApplication(@PathVariable Long idZgloszeniaPraktykodawcy) {
         ZgloszenieInfo info = izgloszeniaPraktykodawcowService.getApplication(idZgloszeniaPraktykodawcy);
        if (info !=null) {
            return new ResponseEntity<ZgloszenieInfo>(info, HttpStatus.OK);
        } else {
            return new ResponseEntity<ZgloszenieInfo>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Decision of Employer's Application
     * @param idZgloszeniaPraktykodawcy ID of Application
     * @param zgloszeniaPraktykodawcowEntity updated Employer's Application (just decision attribute)
     * @return updated Employer's Application
     */
    @RequestMapping(value = "/{idZgloszeniaPraktykodawcy}", method = RequestMethod.PATCH)
    public ResponseEntity<ZgloszeniaPraktykodawcowEntity> editApplication(@PathVariable Long idZgloszeniaPraktykodawcy, @RequestBody ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity) {
        ZgloszeniaPraktykodawcowEntity update = izgloszeniaPraktykodawcowService.editApplication(idZgloszeniaPraktykodawcy, zgloszeniaPraktykodawcowEntity);
        if (update !=null) {
            return new ResponseEntity<ZgloszeniaPraktykodawcowEntity>(update, HttpStatus.OK);
        } else {
            return new ResponseEntity<ZgloszeniaPraktykodawcowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Employer's Application. To ensure proper deletion
     * method MUST be used at least 2 times (or until it returns
     * null).
     * @param idZgloszeniaPraktykodawcy ID of Application
     * @return deleted Employer's Application
     */
    @RequestMapping(value = "/{idZgloszeniaPraktykodawcy}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteApplication(@PathVariable Long idZgloszeniaPraktykodawcy) {
        Long delteApplication = izgloszeniaPraktykodawcowService.deleteApplication(idZgloszeniaPraktykodawcy);
        if (delteApplication !=null) {
            return new ResponseEntity<Long>(delteApplication, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }


}
