package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import praktyki.core.entities.*;
import praktyki.core.service.Interface.iKierunkiStudiowService;
import praktyki.rest.mvc.helpers.KierunekDodatkoweInfo;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */

/**
 * FieldOfStudiesController (CoursesController)<br>
 * Offers URL requests for working with FieldOfStudies in DB
 */
@Controller
@RequestMapping(value = "/kierunkistudiow")
public class KierunkiStudiowController {

    private iKierunkiStudiowService ikierunkiStudiowService;

    @Autowired
    KierunkiStudiowController(iKierunkiStudiowService ikierunkiStudiowService) {
        this.ikierunkiStudiowService = ikierunkiStudiowService;
    }

    /**
     * Adds new Field of Study into DB.
     * @see praktyki.core.entities.KierunkiStudiowEntity
     * @param kierunkiStudiowEntity Field of Study to be persisted
     * @param idTytuluZawodowego ID of Course's Degree
     * @param idStopniaStudiow ID of Course's Grade
     * @param idTrybuStudiow ID of Course's Type
     * @return persisted address
     */
    @RequestMapping(value = "/tytul/{idTytuluZawodowego}/stopien/{idStopniaStudiow}/tryb/{idTrybuStudiow}", method = RequestMethod.POST)
    public ResponseEntity<KierunkiStudiowEntity> addCourse(@RequestBody KierunkiStudiowEntity kierunkiStudiowEntity, @PathVariable int idTytuluZawodowego, @PathVariable int idStopniaStudiow, @PathVariable int idTrybuStudiow) {
        KierunkiStudiowEntity newCourse = ikierunkiStudiowService.addCourse(kierunkiStudiowEntity, idTytuluZawodowego, idStopniaStudiow, idTrybuStudiow);
        if (newCourse !=null) {
            return new ResponseEntity<KierunkiStudiowEntity>(newCourse, HttpStatus.OK);
        } else {
            return new ResponseEntity<KierunkiStudiowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Field of Study in DB
     * @return list of every Field of Study
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<KierunkiStudiowEntity>> getAllCourses() {
        List<KierunkiStudiowEntity> list = ikierunkiStudiowService.getAllCourses();
        return new ResponseEntity<List<KierunkiStudiowEntity>>(list, HttpStatus.OK);
    }

    /**
     * Finds Field of Study with given ID
     * @param idKierunku ID of Course
     * @return single course
     */
    @RequestMapping(value = "/{idKierunku}", method = RequestMethod.GET)
    public ResponseEntity<List<KierunkiStudiowEntity>> getCourse(@PathVariable int idKierunku) {
        List list = ikierunkiStudiowService.getCourseInfo(idKierunku);
        if(list !=null) {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Searches Field of Studies with given parameter and returns them.
     * @see praktyki.core.entities.KierunkiStudiowEntity
     * @param searchString search by Name or Speciality of Course
     * @param sortBy sort by Field of Studies' attributes, preferably ones in searchString
     * @param sortType ASC or DESC
     * @return list of searched courses
     */
    @RequestMapping(value = "search", method = RequestMethod.GET)
    public ResponseEntity<List<KierunkiStudiowEntity>> getCourseByNameSpeciality(@RequestParam String searchString, @RequestParam String sortBy, @RequestParam String sortType) {
        List list = ikierunkiStudiowService.getCourseByNameSpeciality(searchString, sortBy, sortType);
        if (list !=null) {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Field of Study from DB.
     * @param idKierunku ID of course
     * @return deleted course
     */
    @RequestMapping(value ="/{idKierunku}", method = RequestMethod.DELETE)
    public ResponseEntity<KierunkiStudiowEntity> deleteCourse(@PathVariable int idKierunku) {
        KierunkiStudiowEntity deletedCourse = ikierunkiStudiowService.deleteCourse(idKierunku);
        if(deletedCourse !=null) {
            return new ResponseEntity<KierunkiStudiowEntity>(deletedCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KierunkiStudiowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Field Of Study with given ID and updated entity. We can update
     * name, speciality and number of semesters. If we want to update just
     * one attribute we should but it into updated entity, rest will not be
     * updated.
     * @see praktyki.core.entities.KierunkiStudiowEntity
     * @param kierunkiStudiowEntity updated entity
     * @param idKierunku ID of Course
     * @return updated course
     */
    @RequestMapping(value = "/{idKierunku}", method = RequestMethod.PATCH)
    public ResponseEntity<KierunkiStudiowEntity> updateCourse (@RequestBody KierunkiStudiowEntity kierunkiStudiowEntity,@PathVariable int idKierunku) {
        KierunkiStudiowEntity updateCourse = ikierunkiStudiowService.updateCourse(kierunkiStudiowEntity, idKierunku);
        if(updateCourse !=null) {
            return new ResponseEntity<KierunkiStudiowEntity>(updateCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KierunkiStudiowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * TO BE DELETED
     * @param idKierunku ID of Course
     * @return
     */
    @RequestMapping(value = "/{idKierunku}/info", method = RequestMethod.GET)
    public ResponseEntity<KierunekDodatkoweInfo> getTypeDegreeTitle(@PathVariable int idKierunku) {
        KierunekDodatkoweInfo info = ikierunkiStudiowService.getTypeDegreeTitle(idKierunku);
        if (info !=null) {
            return new ResponseEntity<KierunekDodatkoweInfo>(info, HttpStatus.OK);
        } else {
            return new ResponseEntity<KierunekDodatkoweInfo>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Academic Year to Course in CourseYearbookEntity (join table).
     * @see praktyki.core.entities.RocznikiStudiowEntity
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return persisted row in join-table CourseYearbooksEntity
     */
    @RequestMapping(value = "/{idKierunku}/roczniki/{idRokuAkademickiego}", method = RequestMethod.POST)
    public ResponseEntity<RocznikiStudiowEntity> addAcademicYearToCourse(@PathVariable Integer idKierunku, @PathVariable Integer idRokuAkademickiego) {
        RocznikiStudiowEntity addYear = ikierunkiStudiowService.addAcademicYearToCourse(idKierunku, idRokuAkademickiego);
        if (addYear !=null) {
            return new ResponseEntity<RocznikiStudiowEntity>(addYear, HttpStatus.OK);
        } else {
            return new ResponseEntity<RocznikiStudiowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Academic Year of Field of Study.
     * @param idKierunku ID of Course
     * @return list of every academic year of Course
     */
    @RequestMapping(value = "/{idKierunku}/roczniki", method = RequestMethod.GET)
    public ResponseEntity<List<LataAkademickieEntity>> findEveryAcademicYearOfCourse(@PathVariable Integer idKierunku){
        List list = ikierunkiStudiowService.findEveryYearOfCourse(idKierunku);
        if (list !=null) {
            return new ResponseEntity<List<LataAkademickieEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<LataAkademickieEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Academic Year of Field of Study with given ID of Academic Year.
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return academic year of course
     */
    @RequestMapping(value = "/{idKierunku}/roczniki/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<LataAkademickieEntity>> getYearOfCourse(@PathVariable Integer idKierunku, @PathVariable Integer idRokuAkademickiego){
        List list = ikierunkiStudiowService.getYearOfCourse(idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<LataAkademickieEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<LataAkademickieEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Academic Year of Field of Study with given ID of Academic Year
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKierunku}/roczniki/{idRokuAkademickiego}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteYearFromCourse (@PathVariable Integer idKierunku, @PathVariable Integer idRokuAkademickiego) {
        Long deleted = ikierunkiStudiowService.deleteYearFromCourse(idKierunku, idRokuAkademickiego);
        if (deleted !=null) {
            return new ResponseEntity<Long>(deleted, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Student assigned to given Field of Study.
     * @param idKierunku ID of Course
     * @return list of students
     */
    @RequestMapping(value = "/{idKierunku}/studenci", method = RequestMethod.GET)
    public ResponseEntity<List<StudenciEntity>> findstudentsOfCourse(@PathVariable int idKierunku) {
        List<StudenciEntity> findstudentsOfCourse = ikierunkiStudiowService.findStudentsOfCourse(idKierunku);
        if(findstudentsOfCourse !=null) {
            return new ResponseEntity<List<StudenciEntity>>(findstudentsOfCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<StudenciEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Every Student assigned to given Field of Study
     * @param idKierunku ID of Course
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKierunku}/studenci", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteEveryStudentFromCourse (@PathVariable int idKierunku) {
        Long deleteEveryStudentFromCourse = ikierunkiStudiowService.deleteEveryStudentFromCourse(idKierunku);
        if (deleteEveryStudentFromCourse !=null) {
            return new ResponseEntity<Long>(deleteEveryStudentFromCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Student of Field of Study with given ID
     * @param nrAlbumu (ID) Student's Number
     * @param idKierunku ID of Course
     * @return student
     */
    @RequestMapping(value = "/{idKierunku}/studenci/{nrAlbumu}", method = RequestMethod.GET)
    public ResponseEntity<List<StudenciEntity>> getStudentOfCourse(@PathVariable Long nrAlbumu, @PathVariable int idKierunku) {
        List<StudenciEntity> list = ikierunkiStudiowService.getStudentOfCourse(nrAlbumu, idKierunku);
        if (list !=null) {
            return new ResponseEntity<List<StudenciEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<StudenciEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Student with given ID from Field of Study.
     * @param idKierunku ID of Course
     * @param nrAlbumu (ID) Student's Number
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKierunku}/studenci/{nrAlbumu}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteStudentFromCourse(@PathVariable int idKierunku, @PathVariable Long nrAlbumu) {
        Long deleteStudentFromCourse = ikierunkiStudiowService.deleteStudentFromCourse(nrAlbumu, idKierunku);
        if (deleteStudentFromCourse != -1) {
            return new ResponseEntity<Long>(deleteStudentFromCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Coordinator to given Field of Study.
     * @see praktyki.core.entities.KoordynatorzyKierunkowiEntity
     * @param idKierunku ID fo Course
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return persisted row in join-table - CourseCoordinatorsEntity
     */
    @RequestMapping(value = "/{idKierunku}/koordynatorzy/{idKoordynatoraPraktyk}", method = RequestMethod.POST)
    public ResponseEntity<KoordynatorzyKierunkowiEntity> addCoordinatorToCourse(@PathVariable int idKierunku, @PathVariable int idKoordynatoraPraktyk) {
        KoordynatorzyKierunkowiEntity addCoordinatorToCourse = ikierunkiStudiowService.addCoordinatorToCourse(idKierunku, idKoordynatoraPraktyk);
        if (addCoordinatorToCourse !=null) {
            return new ResponseEntity<KoordynatorzyKierunkowiEntity>(addCoordinatorToCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KoordynatorzyKierunkowiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Coordinators assigned to given Field of Study
     * @param idKierunku ID of Course
     * @return list of Coordinators
     */
    @RequestMapping(value = "/{idKierunku}/koordynatorzy", method = RequestMethod.GET)
    public ResponseEntity<List<KoordynatorzyPraktykEntity>> findCoordinatorOfCourse(@PathVariable int idKierunku) {
        List<KoordynatorzyPraktykEntity> list = ikierunkiStudiowService.findCoordinatorOfCourse(idKierunku);
        if (list !=null) {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Every Coordinator assigned to given Field of Study
     * @param idKierunku ID of Course
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKierunku}/koordynatorzy", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteEveryCoordinatorFromCourse(@PathVariable int idKierunku) {
        Long deleteEveryCoordinatorFromCourse = ikierunkiStudiowService.deleteEveryCoordinatorFromCourse(idKierunku);
        if (deleteEveryCoordinatorFromCourse != -1) {
            return new ResponseEntity<Long>(deleteEveryCoordinatorFromCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }


    /**
     * Gets Coordinator with given ID from Field of Study.
     * @param idKierunku ID of COurse
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return coordinator
     */
    @RequestMapping(value = "/{idKierunku}/koordynatorzy/{idKoordynatoraPraktyk}", method = RequestMethod.GET)
    public ResponseEntity<List<KoordynatorzyPraktykEntity>> getCoordinatorOdCourse(@PathVariable int idKierunku, @PathVariable int idKoordynatoraPraktyk) {
        List<KoordynatorzyPraktykEntity> list = ikierunkiStudiowService.getCoordinatorOfCourse(idKierunku, idKoordynatoraPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Coordinator from given Field of Study.
     * @param idKierunku ID of Course
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKierunku}/koordynatorzy/{idKoordynatoraPraktyk}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteCoordinatorFromCourse(@PathVariable int idKierunku, @PathVariable int idKoordynatoraPraktyk) {
        Long deleteCoordinatorFromCourse = ikierunkiStudiowService.deleteCoordinatorFromCourse(idKierunku, idKoordynatoraPraktyk);
        if (deleteCoordinatorFromCourse != -1) {
            return new ResponseEntity<Long>(deleteCoordinatorFromCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }
}
