package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.service.Interface.iOsobyService;

import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */

/**
 * PersonsController<br>
 * Offers URL requests for working with Persons in DB.<br>
 *
 */
@Controller
@RequestMapping("/osoby")
public class OsobyController {

    private iOsobyService iosobyservice;

    @Autowired
    public OsobyController(iOsobyService iosobyservice) {
        this.iosobyservice = iosobyservice;
    }

    /**
     * Searches Person by name and/or surname.
     * @see praktyki.core.entities.OsobyEntity
     * @param searchString search by name or/and surname
     * @param sortBy sort by attributes from PersonsEntity, preferably ones in searchString
     * @param sortType ASC or DESC
     * @return search results
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<List<OsobyEntity>> getUserByNameSurname (@RequestParam String searchString, @RequestParam String sortBy, @RequestParam String sortType) {
        List list = iosobyservice.getUserByNameSurname(searchString, sortBy, sortType);
        if (list !=null) {
            return new ResponseEntity<List<OsobyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<OsobyEntity>>(HttpStatus.NOT_FOUND);
        }
    }
/*    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<OsobyEntity> addPerson(@RequestBody OsobyEntity osobyEntity) {
        OsobyEntity addPerson = iosobyservice.addPerson(osobyEntity);
        if (addPerson !=null) {
            return new ResponseEntity<OsobyEntity>(addPerson, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<OsobyEntity>> findAll() {
        List<OsobyEntity> list = iosobyservice.findAll();
        if (list !=null) {
            return new ResponseEntity<List<OsobyEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<OsobyEntity>>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/{idOsoby}", method = RequestMethod.PATCH)
    public ResponseEntity<OsobyEntity> updatePerson(@PathVariable Integer idOsoby, @RequestBody OsobyEntity osobyEntity) {
        OsobyEntity updatePerson = iosobyservice.updatePerson(idOsoby, osobyEntity);
        if (updatePerson !=null) {
            return new ResponseEntity<OsobyEntity>(updatePerson, HttpStatus.OK);
        }
        else {
            return null;
        }
    }

    @RequestMapping(value = "/{idOsoby}", method = RequestMethod.GET)
    public ResponseEntity<OsobyEntity> getPersonByIdOsoby(@PathVariable Integer idOsoby) {
        OsobyEntity getPersonByIdOsoby = iosobyservice.getPersonByIdOsoby(idOsoby);
        if (getPersonByIdOsoby !=null) {
            return new ResponseEntity<OsobyEntity>(getPersonByIdOsoby, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idOsoby}", method = RequestMethod.DELETE)
    public ResponseEntity<OsobyEntity> deletePerson(@PathVariable Integer idOsoby) {
        OsobyEntity deletePerson = iosobyservice.deletePerson(idOsoby);
        if (deletePerson !=null) {
            return new ResponseEntity<OsobyEntity>(deletePerson, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }*/

}
