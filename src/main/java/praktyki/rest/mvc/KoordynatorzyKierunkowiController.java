package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.KoordynatorzyKierunkowiEntity;
import praktyki.core.service.Interface.iKoordynatorzyKierunkowiService;

import java.util.List;

/**
 * Created by dawid on 22.01.15.
 */

/**
 * CourseCoordinatorsController<br>
 * For Testing
 */
@Controller
@RequestMapping("/koordynatorzykierunkowi")
public class KoordynatorzyKierunkowiController {

    private iKoordynatorzyKierunkowiService ikoordynatorzyKierunkowiService;

    @Autowired
    public KoordynatorzyKierunkowiController (iKoordynatorzyKierunkowiService ikoordynatorzyKierunkowiService) {
        this.ikoordynatorzyKierunkowiService = ikoordynatorzyKierunkowiService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<KoordynatorzyKierunkowiEntity> addRelation(@RequestBody KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity) {
        KoordynatorzyKierunkowiEntity addRelation = ikoordynatorzyKierunkowiService.addRelation(koordynatorzyKierunkowiEntity);
        if (addRelation !=null) {
            return new ResponseEntity<KoordynatorzyKierunkowiEntity>(addRelation, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KoordynatorzyKierunkowiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<KoordynatorzyKierunkowiEntity>> findAll() {
        List<KoordynatorzyKierunkowiEntity> list = ikoordynatorzyKierunkowiService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<KoordynatorzyKierunkowiEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KoordynatorzyKierunkowiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

}
