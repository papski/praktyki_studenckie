package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.OpiekunowiePraktykEntity;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.entities.UzytkownicyEntity;
import praktyki.core.service.Interface.iOpiekunowiePraktykService;

import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */

/**
 * TutorsController<br>
 * Offers URL requests for working with Tutors in DB.<br>
 * Login and Email MUST BE CHECKED before persisitng new
 * Tutor into DB. After checking we need to use 3 methods
 * one after another addTutor, addPerson and addUser to ensure
 * proper persisting the Tutor.
 * @see praktyki.rest.mvc.OpiekunowiePraktykController#addTutor
 * @see praktyki.rest.mvc.OpiekunowiePraktykController#addPerson
 * @see praktyki.rest.mvc.OpiekunowiePraktykController#addUser
 */
@Controller
@RequestMapping("/opiekunowie")
public class OpiekunowiePraktykController {

    private iOpiekunowiePraktykService iopiekunowiePraktykService;

    @Autowired
    public OpiekunowiePraktykController (iOpiekunowiePraktykService iopiekunowiePraktykService) {
        this.iopiekunowiePraktykService = iopiekunowiePraktykService;
    }

    /**
     * Function for checking if Email is already used by someone.
     * If it is used, user MUST change his/hers emil.
     * @param email email to be checked
     * @return 1 when email is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/email/{email:.+}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkEmail(@PathVariable String email) {
        Long checkEmail = iopiekunowiePraktykService.checkEmail(email);
        if (checkEmail ==1 ) {
            return new ResponseEntity<Long>(checkEmail, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Function for checking if Login is already used by someone.
     * If it is used user MUST change his/hers login.
     * @param login login to be checked
     * @return 1 when login is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/login/{login}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkLogin(@PathVariable String login) {
        Long checkLogin = iopiekunowiePraktykService.checkLogin(login);
        if (checkLogin ==1 ) {
            return new ResponseEntity<Long>(checkLogin, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds new Tutor into DB
     * @see praktyki.core.entities.OpiekunowiePraktykEntity
     * @param opiekunowiePraktykEntity Tutor to be persisted
     * @return persisted Tutor
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<OpiekunowiePraktykEntity> addTutor(@RequestBody OpiekunowiePraktykEntity opiekunowiePraktykEntity) {
        OpiekunowiePraktykEntity addTutor = iopiekunowiePraktykService.addTutor(opiekunowiePraktykEntity);
        if (addTutor !=null) {
            return new ResponseEntity<OpiekunowiePraktykEntity>(addTutor, HttpStatus.OK);
        } else {
            return new ResponseEntity<OpiekunowiePraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Tutor in DB.
     * @return list of every Tutor
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<OpiekunowiePraktykEntity>> findAll() {
        List list = iopiekunowiePraktykService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<OpiekunowiePraktykEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<OpiekunowiePraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Tutor with given ID.
     * @param idOpiekunaPraktyk ID of Tutor
     * @return single Tutor
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}", method = RequestMethod.GET)
    public ResponseEntity<OpiekunowiePraktykEntity> getTutor(@PathVariable Integer idOpiekunaPraktyk) {
        OpiekunowiePraktykEntity getTutor = iopiekunowiePraktykService.getTutor(idOpiekunaPraktyk);
        if (getTutor !=null) {
            return new ResponseEntity<OpiekunowiePraktykEntity>(getTutor, HttpStatus.OK);
        } else {
            return new ResponseEntity<OpiekunowiePraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Tutor. We can only change Tutor's position.
     * Position need to be read from current one, otherwise
     * if we persist it empty it will be set to NULL.
     * @see praktyki.core.entities.OpiekunowiePraktykEntity
     * @param idOpiekunaPraktyk ID of Tutor
     * @param opiekunowiePraktykEntity updated Tutor
     * @return updated Tutor
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}", method = RequestMethod.PATCH)
    public ResponseEntity<OpiekunowiePraktykEntity> updateTutor(@PathVariable Integer idOpiekunaPraktyk, @RequestBody OpiekunowiePraktykEntity opiekunowiePraktykEntity) {
        OpiekunowiePraktykEntity updateTutor = iopiekunowiePraktykService.updateTutor(idOpiekunaPraktyk, opiekunowiePraktykEntity);
        if (updateTutor !=null) {
            return new ResponseEntity<OpiekunowiePraktykEntity>(updateTutor, HttpStatus.OK);
        } else {
            return new ResponseEntity<OpiekunowiePraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Tutor. Need to be used AT LEAST 2 times to
     * ensure proper deletion (untill method returns empty JSON)
     * @param idOpiekunaPraktyk ID of Tutor
     * @return deleted Tutor
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}", method = RequestMethod.DELETE)
    public ResponseEntity<OpiekunowiePraktykEntity> deleteTutor(@PathVariable Integer idOpiekunaPraktyk) {
        OpiekunowiePraktykEntity deleteTutor = iopiekunowiePraktykService.deleteTutor(idOpiekunaPraktyk);
        if (deleteTutor !=null) {
            return new ResponseEntity<OpiekunowiePraktykEntity>(deleteTutor, HttpStatus.OK);
        } else {
            return new ResponseEntity<OpiekunowiePraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Personal Data to Tutor. Before we can add this data
     * we need to persist new Tutor with method addTutor.
     * @see praktyki.core.entities.OsobyEntity
     * @see praktyki.rest.mvc.OpiekunowiePraktykController#addTutor(OpiekunowiePraktykEntity opiekunowiePraktykEntity)
     * @param idOpiekunaPraktyk ID of Tutor
     * @param osobyEntity Tutor's personal data
     * @return Tutor's persoanl data
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}/dane", method = RequestMethod.POST)
    public ResponseEntity<OsobyEntity> addPerson(@PathVariable Integer idOpiekunaPraktyk, @RequestBody OsobyEntity osobyEntity) {
        OsobyEntity addPerson = iopiekunowiePraktykService.addPerson(idOpiekunaPraktyk, osobyEntity);
        if (addPerson !=null) {
            return new ResponseEntity<OsobyEntity>(addPerson, HttpStatus.OK);
        } else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Tutor's Personal Data. We can change his/hers Degree,
     * Name, Surname, Cellphone number and Landline number.
     * @see praktyki.core.entities.OsobyEntity
     * @param idOpiekunaPraktyk ID of Tutor
     * @param osobyEntity updated Tutor's personal data
     * @return updated Tutor's personal data
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}/dane", method = RequestMethod.PATCH)
    public ResponseEntity<OsobyEntity> updatePerson(@PathVariable Integer idOpiekunaPraktyk, @RequestBody OsobyEntity osobyEntity) {
        OsobyEntity updatePerson = iopiekunowiePraktykService.updatePerson(idOpiekunaPraktyk, osobyEntity);
        if (updatePerson !=null) {
            return new ResponseEntity<OsobyEntity>(updatePerson, HttpStatus.OK);
        } else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Tutor's Personal Data.
     * @param idOpiekunaPraktyk ID of Tutor
     * @return Tutor's Personal Data
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}/dane", method = RequestMethod.GET)
    public ResponseEntity<List<OsobyEntity>> getInfoOfTutor(@PathVariable Integer idOpiekunaPraktyk) {
        List list = iopiekunowiePraktykService.getInfoOfTutor(idOpiekunaPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<OsobyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<OsobyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Tutor's User Data into DB. Before this we need to
     * persist his/hers Personal data with method addPerson
     * @see praktyki.core.entities.UzytkownicyEntity
     * @see praktyki.rest.mvc.OpiekunowiePraktykController#addPerson(Integer idOpiekunaPraktyk, OsobyEntity osobyEntity)
     * @param idOpiekunaPraktyk ID of Tutor
     * @param uzytkownicyEntity Tutor's User Data
     * @return Tutor's User Data
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}/user", method = RequestMethod.POST)
    public ResponseEntity<UzytkownicyEntity> addUser(@PathVariable Integer idOpiekunaPraktyk, @RequestBody UzytkownicyEntity uzytkownicyEntity) {
        UzytkownicyEntity addUser = iopiekunowiePraktykService.addUser(idOpiekunaPraktyk, uzytkownicyEntity);
        if (addUser !=null) {
            return new ResponseEntity<UzytkownicyEntity>(addUser, HttpStatus.OK);
        } else {
            return new ResponseEntity<UzytkownicyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Tutor's User Data.
     * @param idOpiekunaPraktyk ID of Tutor
     * @return Tutor's User Data
     */
    @RequestMapping(value = "/{idOpiekunaPraktyk}/user", method = RequestMethod.GET)
    public ResponseEntity<List<UzytkownicyEntity>> getUser(@PathVariable Integer idOpiekunaPraktyk) {
        List list = iopiekunowiePraktykService.getUser(idOpiekunaPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<UzytkownicyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<UzytkownicyEntity>>(HttpStatus.NOT_FOUND);
        }
    }
}
