package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.TrybyStudiowEntity;
import praktyki.core.service.Interface.iTrybyStudiowService;

import java.util.List;

/**
 * Created by dawid on 14.02.15.
 */

/**
 * CourseModeController<br>
 * Unecessary
 */
@Controller
@RequestMapping("/trybystudiow")
public class TrybyStudiowController {

    private iTrybyStudiowService itrybyStudiowService;

    @Autowired
    public TrybyStudiowController(iTrybyStudiowService itrybyStudiowService) {
        this.itrybyStudiowService = itrybyStudiowService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TrybyStudiowEntity>> findAll() {
        List list = itrybyStudiowService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<TrybyStudiowEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<TrybyStudiowEntity>>(HttpStatus.NOT_FOUND);
        }
    }
}
