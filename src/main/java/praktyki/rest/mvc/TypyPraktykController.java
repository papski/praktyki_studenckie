package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.TypyPraktykEntity;
import praktyki.core.service.Interface.iTypyPraktykService;

import java.util.List;

/**
 * Created by dawid on 05.02.15.
 */

/**
 * PracticeTypesController
 * Unecessary
 */
@Controller
@RequestMapping("/typypraktyk")
public class TypyPraktykController {

    private iTypyPraktykService itypyPraktykService;

    @Autowired
    public TypyPraktykController (iTypyPraktykService itypyPraktykService) {
        this.itypyPraktykService = itypyPraktykService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TypyPraktykEntity> addTypeOfTraineeship(@RequestBody TypyPraktykEntity typyPraktykEntity) {
        TypyPraktykEntity addTypeOfTraineeship = itypyPraktykService.addTypeOfTraineeship(typyPraktykEntity);
        if (addTypeOfTraineeship !=null) {
            return new ResponseEntity<TypyPraktykEntity>(addTypeOfTraineeship, HttpStatus.OK);
        } else {
            return new ResponseEntity<TypyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TypyPraktykEntity>> findAll() {
        List list = itypyPraktykService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<TypyPraktykEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<TypyPraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idTypuPraktyki}", method = RequestMethod.PATCH)
    public ResponseEntity<TypyPraktykEntity> updateTypeOfTraineeship(@PathVariable Integer idTypuPraktyki, @RequestBody TypyPraktykEntity typyPraktykEntity) {
        TypyPraktykEntity updateTypeOfTraineeship = itypyPraktykService.updateTypeOfTraineeship(idTypuPraktyki, typyPraktykEntity);
        if (updateTypeOfTraineeship !=null) {
            return new ResponseEntity<TypyPraktykEntity>(updateTypeOfTraineeship, HttpStatus.OK);
        } else {
            return new ResponseEntity<TypyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idTypuPraktyki}", method = RequestMethod.GET)
    public ResponseEntity<TypyPraktykEntity> getTypeOfTraineeship(@PathVariable Integer idTypuPraktyki) {
        TypyPraktykEntity getTypeOfTraineeship = itypyPraktykService.getTypeOfTraineeship(idTypuPraktyki);
        if (getTypeOfTraineeship !=null) {
            return new ResponseEntity<TypyPraktykEntity>(getTypeOfTraineeship, HttpStatus.OK);
        } else {
            return new ResponseEntity<TypyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idTypuPraktyki}", method = RequestMethod.DELETE)
    public ResponseEntity<TypyPraktykEntity> deleteTypeOfTraineeship(@PathVariable Integer idTypuPraktyki) {
        TypyPraktykEntity deleteTypeOfTraineeship = itypyPraktykService.deleteTypeOfTraineeship(idTypuPraktyki);
        if (deleteTypeOfTraineeship !=null) {
            return new ResponseEntity<TypyPraktykEntity>(deleteTypeOfTraineeship, HttpStatus.OK);
        } else {
            return new ResponseEntity<TypyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

}
