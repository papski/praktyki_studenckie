package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.service.utilities.Interface.iMailService;
import praktyki.rest.mvc.helpers.MailHelper;

import java.util.List;

/**
 * Created by dawid on 19.04.15.
 */

/**
 * Offers URL requests for working with sending emails through platform
 */
@RequestMapping("mail")
public class MailController {

    private iMailService iMailService;

    @Autowired
    public MailController(@Qualifier("mailService") iMailService iMailService) {
        this.iMailService = iMailService;
    }

    /**
     * (Not yet tested) Sends emails to students. Helper class does
     * contain list of students, from who, subject and message.
     * We need to get list of students using method in EmployersController
     * named getStudentsOfEmployer and put it into helper class as
     * studentsToBeMailed.
     * @see praktyki.rest.mvc.helpers.MailHelper
     * @see praktyki.rest.mvc.PraktykodawcyController#getStudentsOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego)
     * @param mailHelper helper class
     * @return list of students (their emails)
     */
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ResponseEntity<List> sendMailsToStudents(@RequestBody MailHelper mailHelper) {
        List list = iMailService.sendMailsToStudents(mailHelper);
        if (list !=null) {
            return new ResponseEntity<List>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        }
    }

}
