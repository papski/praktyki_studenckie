package praktyki.rest.mvc.helpers;

/**
 * Created by dawid on 26.03.15.
 */

/**
 * Heper class which contains user's login and password.
 * Used in validating user and changing password.
 */
public class LoginInfo {
    String login;
    String haslo;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }
}
