package praktyki.rest.mvc.helpers;

import praktyki.core.entities.KategorieKwalifikacjiEntity;

import java.util.List;

/**
 * Created by dawid on 01.05.15.
 */

/**
 * Helper Class for Qualification's categories. It does contain category
 * with its categories.<br><br>
 * List<KategorieKwalifikacjiEntity> nazwaKategorii;   //name of Category<br>
 * List<KategorieKwalifikacjiEntity> subkategorie;     //list of Subcategoreis<br>
 */
public class CategoryInfo {
    List<KategorieKwalifikacjiEntity> nazwaKategorii;   //name of Category
    List<KategorieKwalifikacjiEntity> subkategorie;     //list of Subcategoreis

    public List<KategorieKwalifikacjiEntity> getNazwaKategorii() {
        return nazwaKategorii;
    }

    public void setNazwaKategorii(List<KategorieKwalifikacjiEntity> nazwaKategorii) {
        this.nazwaKategorii = nazwaKategorii;
    }

    public List<KategorieKwalifikacjiEntity> getSubkategorie() {
        return subkategorie;
    }

    public void setSubkategorie(List<KategorieKwalifikacjiEntity> subkategorie) {
        this.subkategorie = subkategorie;
    }
}
