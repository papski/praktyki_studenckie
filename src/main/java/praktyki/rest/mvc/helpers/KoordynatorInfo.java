package praktyki.rest.mvc.helpers;

import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.entities.PraktykodawcyEntity;
import praktyki.core.entities.UzytkownicyEntity;

import java.util.List;

/**
 * Created by dawid on 15.03.15.
 */

/**
 * CoordinatorInfo<br>
 * Helper Class containing info about Coordinator from
 * entities and shows them at once.<br><br>
 * List<OsobyEntity> daneKoordynatora;                  //Coordinator's personal data<br>
 * List<UzytkownicyEntity> loginKoordynatora;           //Coordinator's user data<br>
 * List<KierunkiStudiowEntity> kierunkiKoordynatora;    //Coordinator's courses<br>
 * List<PraktykodawcyEntity> praktykodawcy;             //Coordinator's employers<br>
 */
public class KoordynatorInfo {
    public List<OsobyEntity> daneKoordynatora;                  //Coordinator's personal data
    public List<UzytkownicyEntity> loginKoordynatora;           //Coordinator's user data
    public List<KierunkiStudiowEntity> kierunkiKoordynatora;    //Coordinator's courses
    public List<PraktykodawcyEntity> praktykodawcy;             //Coordinator's employers


    public List<OsobyEntity> getDaneKoordynatora() {
        return daneKoordynatora;
    }

    public void setDaneKoordynatora(List<OsobyEntity> daneKoordynatora) {
        this.daneKoordynatora = daneKoordynatora;
    }

    public List<UzytkownicyEntity> getLoginKoordynatora() {
        return loginKoordynatora;
    }

    public void setLoginKoordynatora(List<UzytkownicyEntity> loginKoordynatora) {
        this.loginKoordynatora = loginKoordynatora;
    }

    public List<KierunkiStudiowEntity> getKierunkiKoordynatora() {
        return kierunkiKoordynatora;
    }

    public void setKierunkiKoordynatora(List<KierunkiStudiowEntity> kierunkiKoordynatora) {
        this.kierunkiKoordynatora = kierunkiKoordynatora;
    }

    public List<PraktykodawcyEntity> getPraktykodawcy() {
        return praktykodawcy;
    }

    public void setPraktykodawcy(List<PraktykodawcyEntity> praktykodawcy) {
        this.praktykodawcy = praktykodawcy;
    }
}
