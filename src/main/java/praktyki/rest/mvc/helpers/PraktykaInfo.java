package praktyki.rest.mvc.helpers;

import praktyki.core.entities.*;

import java.util.List;


/**
 * Created by dawid on 20.03.15.
 */

/**
 * PracticeInfo<br>
 * Helper Class which contains all info about practice
 * from multiple entities and shows them at once<br><br>
 * Long idPraktykiStudenckiej;                              //ID of Practice<br>
 * SzablonyPraktykEntity szablonPraktyki;                   //Practice's Template<br>
 * PraktykodawcyEntity praktykodawca;                       //Practice's Employer<br>
 * StatusyEntity statusPraktyki;                            //Practice's Status<br>
 * List<KierunkiStudiowEntity> kierunek;                    //Practice's Course<br>
 * TypyPraktykEntity typPraktyki;                           //Practice's Type<br>
 * LataAkademickieEntity rokAkademicki;                     //Practice's Academic Year<br>
 * List<StudenciEntity> student;                            //Practice's Student<br>
 * List<OpiekunowiePraktykEntity> opiekunPraktyki;          //Practice's Tutor<br>
 * List<KoordynatorzyPraktykEntity> koordynatorPraktyki;    //Practice's Coordinator<br>
 * AdresyEntity adresPracodawcy;                            //Practice Employer's Address<br>
 * PorozumieniaEntity porozumienie;                         //Practice's Agreement<br>
 */
public class PraktykaInfo {

    public Long idPraktykiStudenckiej;                              //ID of Practice
    public SzablonyPraktykEntity szablonPraktyki;                   //Practice's Template
    public PraktykodawcyEntity praktykodawca;                       //Practice's Employer
    public StatusyEntity statusPraktyki;                            //Practice's Status
    public List<KierunkiStudiowEntity> kierunek;                    //Practice's Course
    public TypyPraktykEntity typPraktyki;                           //Practice's Type
    public LataAkademickieEntity rokAkademicki;                     //Practice's Academic Year
    public List<StudenciEntity> student;                            //Practice's Student
    public List<OpiekunowiePraktykEntity> opiekunPraktyki;          //Practice's Tutor
    public List<KoordynatorzyPraktykEntity> koordynatorPraktyki;    //Practice's Coordinator
    public AdresyEntity adresPracodawcy;                            //Practice's Employer Address
    public PorozumieniaEntity porozumienie;                         //Practice's Agreement

    public Long getIdPraktykiStudenckiej() {
        return idPraktykiStudenckiej;
    }

    public void setIdPraktykiStudenckiej(Long idPraktykiStudenckiej) {
        this.idPraktykiStudenckiej = idPraktykiStudenckiej;
    }

    public SzablonyPraktykEntity getSzablonPraktyki() {
        return szablonPraktyki;
    }

    public void setSzablonPraktyki(SzablonyPraktykEntity szablonPraktyki) {
        this.szablonPraktyki = szablonPraktyki;
    }

    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    public StatusyEntity getStatusPraktyki() {
        return statusPraktyki;
    }

    public void setStatusPraktyki(StatusyEntity statusPraktyki) {
        this.statusPraktyki = statusPraktyki;
    }

    public List<KierunkiStudiowEntity> getKierunek() {
        return kierunek;
    }

    public void setKierunek(List<KierunkiStudiowEntity> kierunek) {
        this.kierunek = kierunek;
    }

    public TypyPraktykEntity getTypPraktyki() {
        return typPraktyki;
    }

    public void setTypPraktyki(TypyPraktykEntity typPraktyki) {
        this.typPraktyki = typPraktyki;
    }

    public LataAkademickieEntity getRokAkademicki() {
        return rokAkademicki;
    }

    public void setRokAkademicki(LataAkademickieEntity rokAkademicki) {
        this.rokAkademicki = rokAkademicki;
    }

    public List<StudenciEntity> getStudent() {
        return student;
    }

    public void setStudent(List<StudenciEntity> student) {
        this.student = student;
    }

    public List<OpiekunowiePraktykEntity> getOpiekunPraktyki() {
        return opiekunPraktyki;
    }

    public void setOpiekunPraktyki(List<OpiekunowiePraktykEntity> opiekunPraktyki) {
        this.opiekunPraktyki = opiekunPraktyki;
    }

    public List<KoordynatorzyPraktykEntity> getKoordynatorPraktyki() {
        return koordynatorPraktyki;
    }

    public void setKoordynatorPraktyki(List<KoordynatorzyPraktykEntity> koordynatorPraktyki) {
        this.koordynatorPraktyki = koordynatorPraktyki;
    }

    public AdresyEntity getAdresPracodawcy() {
        return adresPracodawcy;
    }

    public void setAdresPracodawcy(AdresyEntity adresPracodawcy) {
        this.adresPracodawcy = adresPracodawcy;
    }

    public PorozumieniaEntity getPorozumienie() {
        return porozumienie;
    }

    public void setPorozumienie(PorozumieniaEntity porozumienie) {
        this.porozumienie = porozumienie;
    }
}
