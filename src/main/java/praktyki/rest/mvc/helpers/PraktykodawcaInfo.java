package praktyki.rest.mvc.helpers;

import praktyki.core.entities.*;

import java.util.List;

/**
 * Created by dawid on 15.03.15.
 */

/**
 * EmployerInfo<br>
 * Helper Class contating info about Employer from multiple
 * entites and shows them at once.<br><br>
 * PraktykodawcyEntity praktykodawca;                   //Employer<br>
 * List<AdresyEntity> adresy;                           //Employer's Addresses<br>
 * List<ProfilEntity> profil;                           //Employer's Profiles<br>
 * List<OpiekunowiePraktykEntity> opiekunowie;          //Employer's Tutors<br>
 * List<PorozumieniaEntity> porozumieniaPraktykodawcy;  //Employer's Agreements<br>
 */
public class PraktykodawcaInfo {
    public PraktykodawcyEntity praktykodawca;                   //Employer
    public List<AdresyEntity> adresy;                           //Employer's Addresses
    public List<ProfilEntity> profil;                           //Employer's Profiles
    public List<OpiekunowiePraktykEntity> opiekunowie;          //Employer's Tutors
    public List<PorozumieniaEntity> porozumieniaPraktykodawcy;  //Employer's Agreements


    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    public List<AdresyEntity> getAdresy() {
        return adresy;
    }

    public void setAdresy(List<AdresyEntity> adresy) {
        this.adresy = adresy;
    }

    public List<ProfilEntity> getProfil() {
        return profil;
    }

    public void setProfil(List<ProfilEntity> profil) {
        this.profil = profil;
    }

    public List<OpiekunowiePraktykEntity> getOpiekunowie() {
        return opiekunowie;
    }

    public void setOpiekunowie(List<OpiekunowiePraktykEntity> opiekunowie) {
        this.opiekunowie = opiekunowie;
    }

    public List<PorozumieniaEntity> getPorozumieniaPraktykodawcy() {
        return porozumieniaPraktykodawcy;
    }

    public void setPorozumieniaPraktykodawcy(List<PorozumieniaEntity> porozumieniaPraktykodawcy) {
        this.porozumieniaPraktykodawcy = porozumieniaPraktykodawcy;
    }
}