package praktyki.rest.mvc.helpers;

/**
 * Created by dawid on 21.02.15.
 */

/**
 * NEEDS TO BE DELETED<br>
 * FieldOfStudyAdditionalInfo<br>
 * Helper class for Field of Studies. Contains information about
 * Course's Degree, Grade and Mode.<br><br>
  String tytulZawodowy;       //Course's Degree<br>
  String stopienStudiow;      //Course's Grade<br>
  String trybStudiow;         //Course's Mode<br>
 */
public class KierunekDodatkoweInfo {
    private String tytulZawodowy;       //Course's Degree
    private String stopienStudiow;      //Course's Grade
    private String trybStudiow;         //Course's Mode

    public String getTytulZawodowy() {
        return tytulZawodowy;
    }

    public void setTytulZawodowy(String tytulZawodowy) {
        this.tytulZawodowy = tytulZawodowy;
    }

    public String getStopienStudiow() {
        return stopienStudiow;
    }

    public void setStopienStudiow(String stopienStudiow) {
        this.stopienStudiow = stopienStudiow;
    }

    public String getTrybStudiow() {
        return trybStudiow;
    }

    public void setTrybStudiow(String trybStudiow) {
        this.trybStudiow = trybStudiow;
    }
}
