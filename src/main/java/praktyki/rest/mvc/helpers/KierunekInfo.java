package praktyki.rest.mvc.helpers;

import praktyki.core.entities.KierunkiStudiowEntity;

/**
 * Created by dawid on 15.03.15.
 */

/**
 * NEEDS TO BE DELETED
 */
public class KierunekInfo {
    public KierunkiStudiowEntity kierunek;
    public KierunekDodatkoweInfo dodatkoweInfo;

    public KierunkiStudiowEntity getKierunek() {
        return kierunek;
    }

    public void setKierunek(KierunkiStudiowEntity kierunek) {
        this.kierunek = kierunek;
    }

    public KierunekDodatkoweInfo getDodatkoweInfo() {
        return dodatkoweInfo;
    }

    public void setDodatkoweInfo(KierunekDodatkoweInfo dodatkoweInfo) {
        this.dodatkoweInfo = dodatkoweInfo;
    }
}
