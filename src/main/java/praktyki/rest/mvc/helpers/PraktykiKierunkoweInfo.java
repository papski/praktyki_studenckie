package praktyki.rest.mvc.helpers;

import praktyki.core.entities.JednostkiKalendarzoweEntity;
import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.core.entities.LataAkademickieEntity;
import praktyki.core.entities.TypyPraktykEntity;

import java.sql.Date;
import java.util.List;

/**
 * Created by dawid on 22.03.15.
 */

/**
 * CoursePracticesInfo<br>
 * Helper Class containg info about Course's Practices.<br><br>
 * List<KierunkiStudiowEntity> kierunekStudiow;        //Field of Study(Course)<br>
 * TypyPraktykEntity typPraktyki;                      //Type of Course<br>
 * LataAkademickieEntity rokAkademicki;                //Academic Year<br>
 * Date minDataRozpoczecia;                            //Minimum Begining Date of Practice<br>
 * Date maxDataZakonczenia;                            //Maximum End Date of Practice<br>
 * int liczbaGodzin;                                   //Practice's Number of Hours<br>
 * int czasTrwania;                                    //Duration TIme<br>
 * JednostkiKalendarzoweEntity jednostkaKalendarzowa;  //Calendar Unit<br>
 */
public class PraktykiKierunkoweInfo {
    private List<KierunkiStudiowEntity> kierunekStudiow;        //Field of Study(Course)
    private TypyPraktykEntity typPraktyki;                      //Type of Course
    private LataAkademickieEntity rokAkademicki;                //Academic Year
    private Date minDataRozpoczecia;                            //Minimum Begining Date of Practice
    private Date maxDataZakonczenia;                            //Maximum End Date of Practice
    private int liczbaGodzin;                                   //Practice's Number of Hours
    private int czasTrwania;                                    //Duration TIme
    private JednostkiKalendarzoweEntity jednostkaKalendarzowa;  //Calendar Unit

    public List<KierunkiStudiowEntity> getKierunekStudiow() {
        return kierunekStudiow;
    }

    public void setKierunekStudiow(List<KierunkiStudiowEntity> kierunekStudiow) {
        this.kierunekStudiow = kierunekStudiow;
    }

    public TypyPraktykEntity getTypPraktyki() {
        return typPraktyki;
    }

    public void setTypPraktyki(TypyPraktykEntity typPraktyki) {
        this.typPraktyki = typPraktyki;
    }

    public LataAkademickieEntity getRokAkademicki() {
        return rokAkademicki;
    }

    public void setRokAkademicki(LataAkademickieEntity rokAkademicki) {
        this.rokAkademicki = rokAkademicki;
    }

    public Date getMinDataRozpoczecia() {
        return minDataRozpoczecia;
    }

    public void setMinDataRozpoczecia(Date minDataRozpoczecia) {
        this.minDataRozpoczecia = minDataRozpoczecia;
    }

    public Date getMaxDataZakonczenia() {
        return maxDataZakonczenia;
    }

    public void setMaxDataZakonczenia(Date maxDataZakonczenia) {
        this.maxDataZakonczenia = maxDataZakonczenia;
    }

    public int getLiczbaGodzin() {
        return liczbaGodzin;
    }

    public void setLiczbaGodzin(int liczbaGodzin) {
        this.liczbaGodzin = liczbaGodzin;
    }

    public int getCzasTrwania() {
        return czasTrwania;
    }

    public void setCzasTrwania(int czasTrwania) {
        this.czasTrwania = czasTrwania;
    }

    public JednostkiKalendarzoweEntity getJednostkaKalendarzowa() {
        return jednostkaKalendarzowa;
    }

    public void setJednostkaKalendarzowa(JednostkiKalendarzoweEntity jednostkaKalendarzowa) {
        this.jednostkaKalendarzowa = jednostkaKalendarzowa;
    }
}
