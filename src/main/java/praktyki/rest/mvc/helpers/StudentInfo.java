package praktyki.rest.mvc.helpers;

import praktyki.core.entities.*;

import java.util.List;

/**
 * Created by dawid on 15.03.15.
 */

/**
Helper class containing info about Student from
multiple entities and shows them at once.<br><br>
List<AdresyEntity> adresStudenta;                //Student's Address<br>
List<OsobyEntity> daneStudenta;                  //Student's personal data<br>
List<UzytkownicyEntity> loginStudenta;           //Student's user data<br>
List<KierunkiStudiowEntity> kierunkiStudenta;    //Student's Field of Studies<br>
List<PorozumieniaEntity> porozumieniaStudenta;   //Student's Agreements<br>
 */
public class StudentInfo {
    public List<AdresyEntity> adresStudenta;                //Student's Address
    public List<OsobyEntity> daneStudenta;                  //Student's personal data
    public List<UzytkownicyEntity> loginStudenta;           //Student's user data
    public List<KierunkiStudiowEntity> kierunkiStudenta;    //Student's Field of Studies
    public List<PorozumieniaEntity> porozumieniaStudenta;   //Student's Agreements

    public List<AdresyEntity> getAdresStudenta() {
        return adresStudenta;
    }

    public void setAdresStudenta(List<AdresyEntity> adresStudenta) {
        this.adresStudenta = adresStudenta;
    }

    public List<OsobyEntity> getDaneStudenta() {
        return daneStudenta;
    }

    public void setDaneStudenta(List<OsobyEntity> daneStudenta) {
        this.daneStudenta = daneStudenta;
    }

    public List<UzytkownicyEntity> getLoginStudenta() {
        return loginStudenta;
    }

    public void setLoginStudenta(List<UzytkownicyEntity> loginStudenta) {
        this.loginStudenta = loginStudenta;
    }

    public List<KierunkiStudiowEntity> getKierunkiStudenta() {
        return kierunkiStudenta;
    }

    public void setKierunkiStudenta(List<KierunkiStudiowEntity> kierunkiStudenta) {
        this.kierunkiStudenta = kierunkiStudenta;
    }

    public List<PorozumieniaEntity> getPorozumieniaStudenta() {
        return porozumieniaStudenta;
    }

    public void setPorozumieniaStudenta(List<PorozumieniaEntity> porozumieniaStudenta) {
        this.porozumieniaStudenta = porozumieniaStudenta;
    }
}
