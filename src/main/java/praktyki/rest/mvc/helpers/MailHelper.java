package praktyki.rest.mvc.helpers;

import praktyki.core.entities.StudenciEntity;

import java.util.List;

/**
 * Created by dawid on 19.04.15.
 */

/**
 * Helper Class for creating email with list of receivers (students)<br><br>
 * List<StudenciEntity> studentsToBeMailed;<br>
 * String from;<br>
 * String subject;<br>
 * String msg;<br>
 */
public class MailHelper {

    public List<StudenciEntity> studentsToBeMailed;
    public String from;
    public String subject;
    public String msg;

    public List<StudenciEntity> getStudentsToBeMailed() {
        return studentsToBeMailed;
    }

    public void setStudentsToBeMailed(List<StudenciEntity> studentsToBeMailed) {
        this.studentsToBeMailed = studentsToBeMailed;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
