package praktyki.rest.mvc.helpers;

import praktyki.core.entities.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by dawid on 21.03.15.
 */

/**
 * ApplicationInfo<br>
 * Helper Class which containts info about Employer's Application
 * from multiple entities and shows them at once.<br><br>
 * Long idZgloszeniaPraktykodawcy;         //ID of Application<br>
 * Timestamp dataZgloszenia;               //Date of Application<br>
 * String oswiadczeniePraktykodawcy;       //Employer's Statement<br>
 * Date dataRozpoczecia;                   //Begining Date of Practice<br>
 * Date dataZakonczenia;                   //End Date of Practice<br>
 * Boolean decyzja;                        //Decision<br>
 * PraktykodawcyEntity praktykodawca;      //Employer's Info<br>
 * List<StudenciEntity> student;           //Student's Info<br>
 * TypyZgloszenEntity typZgloszenia;       //Type of Application<br>
 * List<KierunkiStudiowEntity> kierunek;   //Application's Field of Study (for which Course it was issued)<br>
 * LataAkademickieEntity rok;              //Academic Year<br>
 */
public class ZgloszenieInfo {
    private Long idZgloszeniaPraktykodawcy;         //ID of Application
    private Timestamp dataZgloszenia;               //Date of Application
    private String oswiadczeniePraktykodawcy;       //Employer's Statement
    private Date dataRozpoczecia;                   //Begining Date of Practice
    private Date dataZakonczenia;                   //End Date of Practice
    private Boolean decyzja;                        //Decision
    private PraktykodawcyEntity praktykodawca;      //Employer's Info
    private List<StudenciEntity> student;           //Student's Info
    private TypyZgloszenEntity typZgloszenia;       //Type of Application
    private List<KierunkiStudiowEntity> kierunek;   //Application's Field of Study (for which Course it was issued)
    private LataAkademickieEntity rok;              //Academic Year

    public Long getIdZgloszeniaPraktykodawcy() {
        return idZgloszeniaPraktykodawcy;
    }

    public void setIdZgloszeniaPraktykodawcy(Long idZgloszeniaPraktykodawcy) {
        this.idZgloszeniaPraktykodawcy = idZgloszeniaPraktykodawcy;
    }

    public Timestamp getDataZgloszenia() {
        return dataZgloszenia;
    }

    public void setDataZgloszenia(Timestamp dataZgloszenia) {
        this.dataZgloszenia = dataZgloszenia;
    }

    public String getOswiadczeniePraktykodawcy() {
        return oswiadczeniePraktykodawcy;
    }

    public void setOswiadczeniePraktykodawcy(String oswiadczeniePraktykodawcy) {
        this.oswiadczeniePraktykodawcy = oswiadczeniePraktykodawcy;
    }

    public Date getDataRozpoczecia() {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(Date dataRozpoczecia) {
        this.dataRozpoczecia = dataRozpoczecia;
    }

    public Date getDataZakonczenia() {
        return dataZakonczenia;
    }

    public void setDataZakonczenia(Date dataZakonczenia) {
        this.dataZakonczenia = dataZakonczenia;
    }

    public Boolean getDecyzja() {
        return decyzja;
    }

    public void setDecyzja(Boolean decyzja) {
        this.decyzja = decyzja;
    }

    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    public List<StudenciEntity> getStudent() {
        return student;
    }

    public void setStudent(List<StudenciEntity> student) {
        this.student = student;
    }

    public TypyZgloszenEntity getTypZgloszenia() {
        return typZgloszenia;
    }

    public void setTypZgloszenia(TypyZgloszenEntity typZgloszenia) {
        this.typZgloszenia = typZgloszenia;
    }

    public List<KierunkiStudiowEntity> getKierunek() {
        return kierunek;
    }

    public void setKierunek(List<KierunkiStudiowEntity> kierunek) {
        this.kierunek = kierunek;
    }

    public LataAkademickieEntity getRok() {
        return rok;
    }

    public void setRok(LataAkademickieEntity rok) {
        this.rok = rok;
    }
}
