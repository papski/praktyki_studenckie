package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.TypyZgloszenEntity;
import praktyki.core.service.Interface.iTypyZgloszenService;

import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */

/**
 * ApplicationTypesController<br>
 * Unecessary/testing purpose
 */
@Controller
@RequestMapping("/typyzgloszen")
public class TypyZgloszenController {

    private iTypyZgloszenService itypZgloszeniaService;

    @Autowired
    public TypyZgloszenController(iTypyZgloszenService itypZgloszeniaService) {
        this.itypZgloszeniaService = itypZgloszeniaService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TypyZgloszenEntity> addRequest(@RequestBody TypyZgloszenEntity typyZgloszenEntity) {
        TypyZgloszenEntity addRequest = itypZgloszeniaService.addRequest(typyZgloszenEntity);
        if (addRequest !=null) {
            return new ResponseEntity<TypyZgloszenEntity>(addRequest, HttpStatus.OK);
        } else {
            return new ResponseEntity<TypyZgloszenEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TypyZgloszenEntity>> findAll() {
        List list = itypZgloszeniaService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<TypyZgloszenEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<TypyZgloszenEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idTypuZgloszenia}", method = RequestMethod.GET)
    public ResponseEntity<TypyZgloszenEntity> getRequest(@PathVariable Integer idTypuZgloszenia) {
        TypyZgloszenEntity getRequest = itypZgloszeniaService.getRequest(idTypuZgloszenia);
        if (getRequest !=null) {
            return new ResponseEntity<TypyZgloszenEntity>(getRequest, HttpStatus.OK);
        } else {
            return new ResponseEntity<TypyZgloszenEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idTypuZgloszenia}", method = RequestMethod.PATCH)
    public ResponseEntity<TypyZgloszenEntity> updateRequest(@PathVariable Integer idTypuZgloszenia, TypyZgloszenEntity typyZgloszenEntity) {
        TypyZgloszenEntity updateRequest = itypZgloszeniaService.updateRequest(idTypuZgloszenia, typyZgloszenEntity);
        if (updateRequest !=null) {
            return new ResponseEntity<TypyZgloszenEntity>(updateRequest, HttpStatus.OK);
        } else {
            return new ResponseEntity<TypyZgloszenEntity>(HttpStatus.NOT_FOUND);
        }
    }

}
