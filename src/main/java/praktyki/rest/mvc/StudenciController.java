package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.*;
import praktyki.core.service.Interface.iStudenciService;
import praktyki.rest.mvc.helpers.LoginInfo;
import praktyki.rest.mvc.helpers.PraktykaInfo;
import praktyki.rest.mvc.helpers.StudentInfo;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.List;

/**
 * Created by dawid on 12.12.14.
 */

/**
 * StudentsController<br>
 * Offers URL requests for working with Students in DB.<br>
 * Student's Number, Login and Email MUST BE CHECKED before persisitng new
 * Student into DB. After checking we need to use 3 methods
 * one after another addStudent, addPerson and addUser to ensure
 * proper persisting of Student.
 * @see praktyki.rest.mvc.StudenciController#addStudent
 * @see praktyki.rest.mvc.StudenciController#addPerson
 * @see praktyki.rest.mvc.StudenciController#addUser
 */
@Controller
@RequestMapping("/studenci")
public class StudenciController {

    private iStudenciService istudenciService;

    @Autowired
    public StudenciController(iStudenciService istudenciService) {
        this.istudenciService = istudenciService;
    }

    /*
    Functions for checking Student Number, Email,
    Login, CellPhone and HomeNumbers before
    we persist Student to StudentsEntity,
    PersonsEntity and UsersEntity
     */

    /**
     * Function for checking if Student Number is already used by someone.
     * If it is used, user MUST change his/hers emil.
     * @param nrAlbumu Student Number to be checked
     * @return 1 when Student Number is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/nralbumu/{nrAlbumu}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkNrAlbumu(@PathVariable Long nrAlbumu) {
        Long checkNrAlbumu = istudenciService.checkNrAlbumu(nrAlbumu);
        if (checkNrAlbumu !=null) {
            return new ResponseEntity<Long>(checkNrAlbumu, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Function for checking if Email is already used by someone.
     * If it is used, user MUST change his/hers emil.
     * @param email email to be checked
     * @return 1 when email is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/email/{email:.+}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkEmail(@PathVariable String email) {
        Long checkEmail = istudenciService.checkEmail(email);
        if (checkEmail ==1 ) {
            return new ResponseEntity<Long>(checkEmail, HttpStatus.OK);
        }/* else {
            throw new EmailUsedException("Email is already used");
        }*/
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Function for checking if Login is already used by someone.
     * If it is used user MUST change his/hers login.
     * @param login login to be checked
     * @return 1 when login is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/login/{login}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkLogin(@PathVariable String login) {
        Long checkLogin = istudenciService.checkLogin(login);
        if (checkLogin ==1 ) {
            return new ResponseEntity<Long>(checkLogin, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Function for checking if CellPhoneNumber is already used by someone
     * @param telefonKomorkowy CellPhoneNumber to be checked
     * @return 1 when CellPhoneNumber is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/telefonkomorkowy/{telefonKomorkowy}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkTelefonKomorkowy(@PathVariable String telefonKomorkowy) {
        Long checkTelefonKomorkowy = istudenciService.checkTelefonKomorkowy(telefonKomorkowy);
        if (checkTelefonKomorkowy !=null) {
            return new ResponseEntity<Long>(checkTelefonKomorkowy, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Function for checking if HomePhoneNumber is already used by someone
     * @param telefonStacjonarny HomePhoneNumber to be checked
     * @return 1 when CellPhoneNumber is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/telefonstacjonarny/{telefonStacjonarny}", method = RequestMethod.GET)
    public ResponseEntity<Long> checktelefonStacjonarny(@PathVariable String telefonStacjonarny) {
        Long checktelefonStacjonarny = istudenciService.checkTelefonStacjonarny(telefonStacjonarny);
        if (checktelefonStacjonarny !=null) {
            return new ResponseEntity<Long>(checktelefonStacjonarny, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /*
    end of checking functions
     */


    /**
     * Adds new Student into DB.
     * @see praktyki.core.entities.StudenciEntity
     * @param studenciEntity Student to be persisted
     * @return persisted Student
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<StudenciEntity> addStudent(@RequestBody StudenciEntity studenciEntity) {
        StudenciEntity addStudent = istudenciService.addStudent(studenciEntity);
        return new ResponseEntity<StudenciEntity>(addStudent, HttpStatus.CREATED);
    }

    /**
     * Finds Every Student in DB
     * @return list of every Student
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<StudenciEntity>> findEveryStudent() {
        List<StudenciEntity> list = istudenciService.findEveryStudent();
        if (list !=null) {
            return new ResponseEntity<List<StudenciEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<StudenciEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Login testing function. We send helper class with login and password,
     * if login and login's password are correct user info is being sent back.
     * @see praktyki.rest.mvc.helpers.LoginInfo
     * @param login Helper Class with login and password
     * @return user info
     */
    @RequestMapping(value = "/login/test", method = RequestMethod.POST)
    public ResponseEntity<UzytkownicyEntity> loginTEST(@RequestBody LoginInfo login) {
        UzytkownicyEntity loginTEST = istudenciService.loginTEST(login);
        if (loginTEST !=null) {
            return new ResponseEntity<UzytkownicyEntity>(loginTEST, HttpStatus.OK);
        } else {
            return new ResponseEntity<UzytkownicyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Password changing testing function. We send Helper Class with login
     * and NEW password. Old password of user is being overwritten by new one.
     * @see praktyki.rest.mvc.helpers.LoginInfo
     * @param login Helper Class with login and NEW password
     * @return user info
     */
    @RequestMapping(value = "/login/haslo", method = RequestMethod.POST)
    public ResponseEntity<UzytkownicyEntity> passwordChangeTEST(@RequestBody LoginInfo login) {
        UzytkownicyEntity loginTEST = istudenciService.passwordChangeTEST(login);
        if (loginTEST !=null) {
            return new ResponseEntity<UzytkownicyEntity>(loginTEST, HttpStatus.OK);
        } else {
            return new ResponseEntity<UzytkownicyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Student with given ID.
     * @param nrAlbumu (ID) Student's Number
     * @return single Studnet
     */
    @RequestMapping(value = "/{nrAlbumu}", method = RequestMethod.GET)
    public ResponseEntity<StudenciEntity> getStudent(@PathVariable Long nrAlbumu) {
        StudenciEntity getStudent = istudenciService.findByNrAlbumu(nrAlbumu);
        if(getStudent !=null) {
            return new ResponseEntity<StudenciEntity>(getStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<StudenciEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /*
    TRZEBA JEJ UŻYĆ 2 RAZY ABY USUNĄĆ WSZYSTKO
    CO ZWIĄZANE ZE STUDENTEM, DO SPRAWDZENIA!
     */

    /**
     * Deletes Student from DB. To ensure proper deletion we MUST use
     * this function at least 2 times (or until it returns null/empty entity)
     * @param nrAlbumu (ID) Student's Number
     * @return deleted Student
     */
    @RequestMapping(value = "/{nrAlbumu}", method = RequestMethod.DELETE)
    public ResponseEntity<StudenciEntity> deleteStudent(@PathVariable Long nrAlbumu) {
        StudenciEntity deleteStudent = istudenciService.deleteStudent(nrAlbumu);
        if (deleteStudent !=null) {
            return new ResponseEntity<StudenciEntity>(deleteStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<StudenciEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds info about our Student from multiple entites - Address,
     * Personal Data, User Data, Field of Studies and Agreements.
     * @see praktyki.rest.mvc.helpers.StudentInfo
     * @param nrAlbumu (ID) Student's Number
     * @return Helper Class with Student's info
     */
    @RequestMapping(value = "/{nrAlbumu}/info", method = RequestMethod.GET)
    public ResponseEntity<StudentInfo> getAllDataOfStudent (@PathVariable Long nrAlbumu) {
        StudentInfo getAll = istudenciService.getAllDataOfStudent(nrAlbumu);
        if (getAll !=null) {
            return new ResponseEntity<StudentInfo>(getAll, HttpStatus.OK);
        } else {
            return new ResponseEntity<StudentInfo>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Returns info about Practices of Student.
     * @see praktyki.rest.mvc.helpers.PraktykaInfo
     * @param nrAlbumu (ID) Student's Number
     * @return Helper Class with Student's Practices info
     */
    @RequestMapping(value = "/{nrAlbumu}/praktyka", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykaInfo>> getPracticeOfStudent(@PathVariable Long nrAlbumu) {
        List list = istudenciService.getPracticeOfStudent(nrAlbumu);
        if (list !=null) {
            return new ResponseEntity<List<PraktykaInfo>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykaInfo>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Returns info about Applications of Student.
     * @see praktyki.rest.mvc.helpers.ZgloszenieInfo
     * @param nrAlbumu (ID) Student's Number
     * @return Helper Class with Student's Applications
     */
    @RequestMapping(value = "/{nrAlbumu}/zgloszenie", method = RequestMethod.GET)
    public ResponseEntity<List<ZgloszenieInfo>> getApplicationOfStudent(@PathVariable Long nrAlbumu) {
        List list = istudenciService.getApplicationOfStudent(nrAlbumu);
        if (list !=null) {
            return new ResponseEntity<List<ZgloszenieInfo>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<ZgloszenieInfo>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Personal Data to Student. Before we can add this data
     * we need to persist new Student with method addStudent.
     * @see praktyki.rest.mvc.StudenciController#addStudent(StudenciEntity studenciEntity)
     * @see praktyki.core.entities.OsobyEntity
     * @param nrAlbumu
     * @param osobyEntity
     * @return
     */
    @RequestMapping(value = "/{nrAlbumu}/dane", method = RequestMethod.POST)
    public ResponseEntity<OsobyEntity> addPerson(@PathVariable Long nrAlbumu, @RequestBody OsobyEntity osobyEntity) {
        OsobyEntity addPerson = istudenciService.addPerson(nrAlbumu, osobyEntity);
        if (addPerson !=null) {
            return new ResponseEntity<OsobyEntity>(addPerson, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Personal Data of Student. We can change Name, Surname,
     * CellPhone Number, Landline Number and Degree.
     * @see praktyki.core.entities.OsobyEntity
     * @param nrAlbumu (ID) Student's Number
     * @param osobyEntity updated Student's Personal Data
     * @return updated Student
     */
    @RequestMapping(value = "/{nrAlbumu}/dane", method = RequestMethod.PATCH)
    public ResponseEntity<OsobyEntity> editInfoOfStudent(@PathVariable Long nrAlbumu, @RequestBody OsobyEntity osobyEntity) {
        OsobyEntity editInfoOfStudent = istudenciService.editInfoOfStudent(nrAlbumu, osobyEntity);
        if (editInfoOfStudent !=null) {
            return new ResponseEntity<OsobyEntity>(editInfoOfStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Student's Personal Data.
     * @param nrAlbumu (ID) Student's Number
     * @return Student's Personal Data
     */
    @RequestMapping(value = "/{nrAlbumu}/dane", method = RequestMethod.GET)
    public ResponseEntity<List<OsobyEntity>> getInfoOfStudent(@PathVariable Long nrAlbumu) {
        List<OsobyEntity> getInfoOfStudent = istudenciService.getInfoOfStudent(nrAlbumu);
        if (getInfoOfStudent !=null) {
            return new ResponseEntity<List<OsobyEntity>>(getInfoOfStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<OsobyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Student's User Data into DB. Before this we need to
     * persist his/hers Personal data with method addPerson.
     * @see praktyki.rest.mvc.StudenciController#addPerson(Long nrAlbumu, OsobyEntity osobyEntity)
     * @see praktyki.core.entities.UzytkownicyEntity
     * @param nrAlbumu (ID) Student's Number
     * @param uzytkownicyEntity Student's User Data
     * @return Student's User Data
     */
    @RequestMapping(value = "/{nrAlbumu}/user", method = RequestMethod.POST)
    public ResponseEntity<UzytkownicyEntity> addUser(@PathVariable Long nrAlbumu, @RequestBody UzytkownicyEntity uzytkownicyEntity) {
        UzytkownicyEntity addUser = istudenciService.addUser(uzytkownicyEntity, nrAlbumu);
        if (addUser !=null) {
            return new ResponseEntity<UzytkownicyEntity>(addUser, HttpStatus.OK);
        } else {
            return new ResponseEntity<UzytkownicyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets User Data of Student with given ID.
     * @param nrAlbumu (ID) Student's Number
     * @return Student's User Data
     */
    @RequestMapping(value = "/{nrAlbumu}/user", method = RequestMethod.GET)
    public ResponseEntity<List<UzytkownicyEntity>> getUserOfStudent(@PathVariable Long nrAlbumu) {
        List list = istudenciService.getUserOfStudent(nrAlbumu);
        if (list !=null) {
            return new ResponseEntity<List<UzytkownicyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<UzytkownicyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Address to our Student.
     * @see praktyki.core.entities.AdresyEntity
     * @param nrAlbumu (ID) Student's Number
     * @param adresyEntity Address to be persisted
     * @return persisted Address
     */
    @RequestMapping(value = "{nrAlbumu}/adres", method = RequestMethod.POST)
    public ResponseEntity<AdresyEntity> addAddress(@PathVariable Long nrAlbumu, @RequestBody AdresyEntity adresyEntity) {
        AdresyEntity addAddress = istudenciService.addAddress(nrAlbumu, adresyEntity);
        if (addAddress !=null) {
            return new ResponseEntity<AdresyEntity>(addAddress, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<AdresyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Address of Student with given ID. We cant change
     * Street, Building No, City, ZipCode, Country and Fax. All
     * fields (attributes) should be filled with the ones in DB
     * when showed in browser. Without this it will merge changes
     * with null value (overwrite).
     * @param nrAlbumu (ID) Student's Number
     * @param adresyEntity updated Student's Address
     * @return updated Student's Address
     */
    @RequestMapping(value = "{nrAlbumu}/adres", method = RequestMethod.PATCH)
    public ResponseEntity<AdresyEntity> editAddressOfStudent(@PathVariable Long nrAlbumu, @RequestBody AdresyEntity adresyEntity) {
        AdresyEntity editAddressOfStudent = istudenciService.editAddressOfStudent(nrAlbumu, adresyEntity);
        if (editAddressOfStudent !=null) {
            return new ResponseEntity<AdresyEntity>(editAddressOfStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<AdresyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Address of Student
     * @param nrAlbumu (ID) Student's Number
     * @return Student's Address
     */
    @RequestMapping(value = "/{nrAlbumu}/adres", method = RequestMethod.GET)
    public ResponseEntity<List<AdresyEntity>> getAddressOfStudent(@PathVariable Long nrAlbumu) {
        List<AdresyEntity> getAddressOfStudent = istudenciService.getAddressOfStudent(nrAlbumu);
        if(getAddressOfStudent !=null) {
            return new ResponseEntity<List<AdresyEntity>>(getAddressOfStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<AdresyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Field of Study to Student.
     * @see praktyki.core.entities.StudenciKierunkowEntity
     * @param nrAlbumu (ID) Student's Number
     * @param studenciKierunkowEntity Entity with Studen't Number, Course and Cademic Year
     * @return persisted relation
     */
    @RequestMapping(value = "/{nrAlbumu}/kierunek", method = RequestMethod.POST)
    public ResponseEntity<StudenciKierunkowEntity> addCourseToStudent(@PathVariable Long nrAlbumu, @RequestBody StudenciKierunkowEntity studenciKierunkowEntity) {
        StudenciKierunkowEntity addCourseToStudent = istudenciService.addCourseToStudent(studenciKierunkowEntity);
        if (addCourseToStudent !=null) {
            return new ResponseEntity<StudenciKierunkowEntity>(addCourseToStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<StudenciKierunkowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Field of Studies of Student.
     * @param nrAlbumu (ID) Student's Number
     * @return list of Student's Courses
     */
    @RequestMapping(value = "/{nrAlbumu}/kierunek", method = RequestMethod.GET)
    public ResponseEntity<List<KierunkiStudiowEntity>> findCourseOfStudent(@PathVariable Long nrAlbumu) {
        List<KierunkiStudiowEntity> CourseOfStudent = istudenciService.findCourseOfStudent(nrAlbumu);
        if(CourseOfStudent !=null) {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(CourseOfStudent, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets info of Student's Course with given ID
     * @param nrAlbumu (ID) Student's Number
     * @param idKierunku ID of Field of Study(Course)
     * @return Student's Course
     */
    @RequestMapping(value = "/{nrAlbumu}/kierunek/{idKierunku}", method = RequestMethod.GET)
    public ResponseEntity<List<KierunkiStudiowEntity>> getCourseOfStudent(@PathVariable Long nrAlbumu, @PathVariable int idKierunku) {
        List<KierunkiStudiowEntity> list = istudenciService.getCourseOfStudent(nrAlbumu, idKierunku);
        if (list !=null) {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Student from Course.
     * @param nrAlbumu (ID) Student's Number
     * @param idKierunku ID of Field of Study(Course)
     * @return Deleted Course
     */
    @RequestMapping(value = "/{nrAlbumu}/kierunek/{idKierunku}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteStudentFromCourse(@PathVariable Long nrAlbumu, @PathVariable int idKierunku) {
        Long deleteStudentFromCourse = istudenciService.deleteStudentFromCourse(nrAlbumu, idKierunku);
        if (deleteStudentFromCourse != -1) {
            return new ResponseEntity<Long>(deleteStudentFromCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

}

