package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import praktyki.core.entities.*;
import praktyki.core.service.Interface.iPraktykodawcyService;
import praktyki.rest.mvc.helpers.PraktykaInfo;
import praktyki.rest.mvc.helpers.PraktykodawcaInfo;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.List;

/**
 * Created by dawid on 06.01.15.
 */

/**
 * EmployersController<br>
 * Offers URL requests for working with Employers in DB<br>
 * Before adding Employer into DB we should use name checker
 * to be sure there is no such name assigned to other Employer.
 * @see praktyki.rest.mvc.PraktykodawcyController#checkNazwa
 */
@Controller
@RequestMapping(value = "/praktykodawcy")
public class PraktykodawcyController {

    private iPraktykodawcyService ipraktykodawcyService;

    @Autowired
    PraktykodawcyController(iPraktykodawcyService ipraktykodawcyService) {
        this.ipraktykodawcyService = ipraktykodawcyService;
    }

    /**
     * Checks name of Employer in DB
     * @param nazwa name of Company
     * @return true for occupied name, false for free name
     */
    @RequestMapping(value = "/sprawdz/nazwa/{nazwa:.+}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> checkNazwa(@PathVariable String nazwa) {
        Boolean checkNazwa = ipraktykodawcyService.checkNazwa(nazwa);
        if (checkNazwa !=null) {
            return new ResponseEntity<Boolean>(checkNazwa, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Boolean>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Employer to DB.
     * @see praktyki.core.entities.PraktykiEntity
     * @param praktykodawcyEntity Employer to be persisted
     * @return persited Employer
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PraktykodawcyEntity> addEmployeer (@RequestBody PraktykodawcyEntity praktykodawcyEntity) throws Exception {
        try {
            PraktykodawcyEntity newEmployeer = ipraktykodawcyService.addEmployeer(praktykodawcyEntity);
            return new ResponseEntity<PraktykodawcyEntity>(newEmployeer, HttpStatus.CREATED);
        }
        catch (Exception e){
            throw new Exception(e);
        }
    }

    /**
     * Finds every Employer in DB
     * @return list of every Employer
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PraktykodawcyEntity>> findEveryEmployeer () {
        List<PraktykodawcyEntity> list = ipraktykodawcyService.findEveryEmployeer();
        return new ResponseEntity<List<PraktykodawcyEntity>>(list, HttpStatus.OK);
    }

    /**
     * Gets Employer with given ID
     * @param idPraktykodawcy ID of Employer
     * @return single Employer
     */
    @RequestMapping(value = "/{idPraktykodawcy}", method = RequestMethod.GET)
    public ResponseEntity<PraktykodawcyEntity> findByIdPraktykodawcy(@PathVariable int idPraktykodawcy) {
        PraktykodawcyEntity praktykodawcyEntity = ipraktykodawcyService.findEmployeer(idPraktykodawcy);
        if(praktykodawcyEntity !=null) {
            return new ResponseEntity<PraktykodawcyEntity>(praktykodawcyEntity, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<PraktykodawcyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Employer by its name. If we set searchString to 0
     * it will be ignored in searching
     * @param searchString search by Company's name
     * @param sortType ASC or DESC
     * @return list of searched Employers
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    private ResponseEntity<List<PraktykodawcyEntity>> getEmployerByName(@RequestParam String searchString, @RequestParam String sortType) {
        List list = ipraktykodawcyService.getEmployerByName(searchString, sortType);
        if (list !=null) {
            return new ResponseEntity<List<PraktykodawcyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykodawcyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Edits trusted status of Employer.
     * @param idPraktykodawcy ID of Employer
     * @param zaufany Trusted Status
     * @return single Employer
     */
    @RequestMapping(value = "/{idPraktykodawcy}/zaufany/{zaufany}", method = RequestMethod.PATCH)
    public ResponseEntity<PraktykodawcyEntity> editTrustedStatus(@PathVariable int idPraktykodawcy, @PathVariable Boolean zaufany) {
        PraktykodawcyEntity editTrustedStatus = ipraktykodawcyService.editTrustedStatus(idPraktykodawcy, zaufany);
        if (editTrustedStatus !=null) {
            return new ResponseEntity<PraktykodawcyEntity>(editTrustedStatus, HttpStatus.OK);
        } else {
            return new ResponseEntity<PraktykodawcyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets info from multiple Entites about Employers
     * and shows them in single JSON.
     * @see praktyki.rest.mvc.helpers.PraktykaInfo
     * @param idPraktykodawcy
     * @return
     */
    @RequestMapping(value = "/{idPraktykodawcy}/info", method = RequestMethod.GET)
    public ResponseEntity<PraktykodawcaInfo> getAllDataOfEmployeer(@PathVariable int idPraktykodawcy) {
        PraktykodawcaInfo getData = ipraktykodawcyService.getAllDataOfEmployeer(idPraktykodawcy);
        if (getData !=null) {
            return new ResponseEntity<PraktykodawcaInfo>(getData, HttpStatus.OK);
        } else {
            return new ResponseEntity<PraktykodawcaInfo>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Students of Employer by Course and/or Academic Year.
     * If we want to ignore Course or Academic Year we put 0 in
     * URL request.
     * @param idPraktykodawcy ID of Employer
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return Employer's Students
     */
    @RequestMapping(value = "/{idPraktykodawcy}/students/course/{idKierunku}/year/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<StudenciEntity>> getStudentsOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idKierunku, @PathVariable int idRokuAkademickiego) {
        List list = ipraktykodawcyService.getStudentsOfEmployerByCourseAcademicYear(idPraktykodawcy, idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<StudenciEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<StudenciEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Practice of Employer and returns them
     * as helper class with all info about Practice in it.
     * If we want to ignore Course and/or Academic Year we
     * should put 0 in the request.
     * @see praktyki.rest.mvc.helpers.PraktykaInfo
     * @param idPraktykodawcy ID of Employer
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return
     */
    @RequestMapping(value = "/{idPraktykodawcy}/praktyki/kierunek/{idKierunku}/rok/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykaInfo>> getAllPracticesOfEmployer (@PathVariable int idPraktykodawcy, @PathVariable int idKierunku, @PathVariable int idRokuAkademickiego) {
        List list =ipraktykodawcyService.getAllPracticesOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<PraktykaInfo>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykaInfo>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Application of Employer and returns them
     * as helper class with all info about it in it. If we
     * want to ignore Course and/or Academic Year we should
     * put 0 in the request.
     * @see praktyki.rest.mvc.helpers.ZgloszenieInfo
     * @param idPraktykodawcy ID of Employer
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return list of Employer's Application
     */
    @RequestMapping(value = "/{idPraktykodawcy}/zgloszenia/kierunek/{idKierunku}/rok/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<ZgloszenieInfo>> getAllApplicationsOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idKierunku, @PathVariable int idRokuAkademickiego) {
        List list = ipraktykodawcyService.getAllApplicationsOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<ZgloszenieInfo>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<ZgloszenieInfo>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Practice of Employer by Tutor and
     * returns them as helper class with all info about it
     * in it.
     * @see praktyki.rest.mvc.helpers.PraktykaInfo
     * @param idPraktykodawcy ID of Employer
     * @param idOpiekunaPraktyk ID of Tutor
     * @return Employer's Practices by Tutor
     */
    @RequestMapping(value = "/{idPraktykodawcy}/praktyki/opiekun/{idOpiekunaPraktyk}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykaInfo>> getAllPracticesOfEmployerByTutor(@PathVariable int idPraktykodawcy, @PathVariable int idOpiekunaPraktyk) {
        List list = ipraktykodawcyService.getAllPracticesOfEmployerByTutor(idPraktykodawcy, idOpiekunaPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<PraktykaInfo>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykaInfo>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Employer. We can change name and full name of company.
     * @param praktykodawcyEntity updated Employer
     * @param idPraktykodawcy ID of EMployer
     * @return updated Employer
     */
    @RequestMapping(value = "/{idPraktykodawcy}", method = RequestMethod.PATCH)
    public ResponseEntity<PraktykodawcyEntity> updateEmployeer (@RequestBody PraktykodawcyEntity praktykodawcyEntity, @PathVariable int idPraktykodawcy) {
        PraktykodawcyEntity updateEmployeer = ipraktykodawcyService.updateEmployeer(praktykodawcyEntity, idPraktykodawcy);
        if (updateEmployeer !=null) {
            return new ResponseEntity<PraktykodawcyEntity>(updateEmployeer, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<PraktykodawcyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Employer from DB. To ensure proper deletion
     * this method MUST be usead AT LEAST 2 times (until it
     * shows empty entity in returned JSON)
     * @param idPraktykodawcy ID of Employer
     * @return deleton Employer
     */
    @RequestMapping(value = "/{idPraktykodawcy}", method = RequestMethod.DELETE)
    public ResponseEntity<PraktykodawcyEntity> deleteEmployeer(@PathVariable int idPraktykodawcy) {
        PraktykodawcyEntity praktykodawcyEntity = ipraktykodawcyService.deleteEmployeer(idPraktykodawcy);
        if(praktykodawcyEntity !=null) {
            return new ResponseEntity<PraktykodawcyEntity>(praktykodawcyEntity, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<PraktykodawcyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Address of Employer with branch status.
     * @see praktyki.core.entities.AdresyEntity
     * @see praktyki.core.entities.AdresyPraktykodawcyEntity
     * @param idPraktykodawcy ID of Employer
     * @param oddzial branch office status
     * @param adresyEntity Address to be persisted and assigned to Employer
     * @return persisted Address
     */
    @RequestMapping(value = "/{idPraktykodawcy}/adres/oddzial/{oddzial}", method = RequestMethod.POST)
    public ResponseEntity<AdresyEntity> addAddress(@PathVariable int idPraktykodawcy, @PathVariable Boolean oddzial, @RequestBody AdresyEntity adresyEntity) {
        AdresyEntity addAddress = ipraktykodawcyService.addAddress(idPraktykodawcy, oddzial, adresyEntity);
        if (addAddress !=null) {
            return new ResponseEntity<AdresyEntity>(addAddress, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<AdresyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Address of Employer.
     * @param idPraktykodawcy ID of Employer
     * @return list of Employer's Addresses
     */
    @RequestMapping(value = "/{idPraktykodawcy}/adres", method = RequestMethod.GET)
    public ResponseEntity<List<AdresyEntity>> findAddressesOfEmployer(@PathVariable int idPraktykodawcy) {
        List list = ipraktykodawcyService.findAddresses(idPraktykodawcy);
        if (list !=null) {
            return new ResponseEntity<List<AdresyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<AdresyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Every Address of Employer
     * @param idPraktykodawcy ID of Employer
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idPraktykodawcy}/adres", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteEveryAddressOfEmployer(@PathVariable int idPraktykodawcy) {
        Long deleteEveryAddressOfEmployer = ipraktykodawcyService.deleteEveryAddressOfEmployer(idPraktykodawcy);
        if (deleteEveryAddressOfEmployer !=null) {
            return new ResponseEntity<Long>(deleteEveryAddressOfEmployer, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets info about Address of Employer.
     * @param idPraktykodawcy ID of Employer
     * @param idAdresu ID of Address
     * @return Employer's Address
     */
    @RequestMapping(value = "/{idPraktykodawcy}/adres/{idAdresu}", method = RequestMethod.GET)
    public ResponseEntity<List<AdresyEntity>> getAddressOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idAdresu) {
        List list = ipraktykodawcyService.getAddress(idPraktykodawcy, idAdresu);
        if (list !=null) {
            return new ResponseEntity<List<AdresyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<AdresyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Employer's Address with given ID.
     * @param idPraktykodawcy ID of Employer
     * @param idAdresu ID of Address
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idPraktykodawcy}/adres/{idAdresu}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteAddressOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idAdresu) {
        Long deleteAddressOfEmployer = ipraktykodawcyService.deleteAddressOfEmployer(idPraktykodawcy, idAdresu);
        if (deleteAddressOfEmployer !=null) {
            return new ResponseEntity<Long>(deleteAddressOfEmployer, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Testing purpouse/to be deleted/unecessary
     * @param idPraktykodawcy
     * @param idAdresu
     * @return
     */
    @RequestMapping(value = "/{idPraktykodawcy}/adres/{idAdresu}/oddzial", method = RequestMethod.GET)
    public ResponseEntity<Long> checkBranch(@PathVariable int idPraktykodawcy, @PathVariable int idAdresu) {
        Long checkBranch = ipraktykodawcyService.checkBranch(idPraktykodawcy, idAdresu);
        if (checkBranch !=null) {
            return new ResponseEntity<Long>(checkBranch, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Profile to Employer.
     * @see praktyki.core.entities.ProfilEntity
     * @param idPraktykodawcy ID of Employer
     * @param profilEntity Profile to be persisted and assigned to Employer
     * @return persisted Profile
     */
    @RequestMapping(value = "/{idPraktykodawcy}/profile", method = RequestMethod.POST)
    public ResponseEntity<ProfilEntity> addProfile(@PathVariable int idPraktykodawcy, @RequestBody ProfilEntity profilEntity) {
        ProfilEntity addProfile = ipraktykodawcyService.addProfile(idPraktykodawcy, profilEntity);
        if (addProfile !=null) {
            return new ResponseEntity<ProfilEntity>(addProfile, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProfilEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Profile of Employer
     * @param idPraktykodawcy ID of Employer
     * @return list of Employer's Profiles
     */
    @RequestMapping(value = "/{idPraktykodawcy}/profile", method = RequestMethod.GET)
    public ResponseEntity<List<ProfilEntity>> findProfilesOfEmployeer(@PathVariable int idPraktykodawcy) {
        List list = ipraktykodawcyService.findProfilesOfEmployeer(idPraktykodawcy);
        if (list !=null) {
            return new ResponseEntity<List<ProfilEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<ProfilEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Unecessary/To be deleted
     * @param idPraktykodawcy
     * @param idProfilu
     * @param profilEntity
     * @return
     */
    @RequestMapping(value = "/{idPraktykodawcy}/profile/{idProfilu}", method = RequestMethod.PATCH)
    public ResponseEntity<ProfilEntity> updateProfile(@PathVariable int idPraktykodawcy, @PathVariable int idProfilu, @RequestBody ProfilEntity profilEntity) {
        ProfilEntity updateProfile = ipraktykodawcyService.updateProfile(idPraktykodawcy,idProfilu, profilEntity);
        if (updateProfile !=null) {
            return new ResponseEntity<ProfilEntity>(updateProfile, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProfilEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Profile of Employer.
     * @param idPraktykodawcy ID of Employer
     * @param idProfilu ID of Profile
     * @return Employer's Profile
     */
    @RequestMapping(value = "/{idPraktykodawcy}/profile/{idProfilu}", method = RequestMethod.GET)
    public ResponseEntity<List<ProfilEntity>> getProfileOfEmployeer(@PathVariable int idPraktykodawcy, @PathVariable int idProfilu) {
        List list = ipraktykodawcyService.getProfileOfEmployeer(idProfilu, idPraktykodawcy);
        if (list !=null) {
            return new ResponseEntity<List<ProfilEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<ProfilEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes every Profile of Employer
     * @param idPraktykodawcy ID of Employer
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idPraktykodawcy}/profile", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteEveryProfileOfEmployeer(@PathVariable int idPraktykodawcy) {
        Long deleteEveryProfileOfEmployeer = ipraktykodawcyService.deleteEveryProfileOfEmployeer(idPraktykodawcy);
        if (deleteEveryProfileOfEmployeer !=null) {
            return new ResponseEntity<Long>(deleteEveryProfileOfEmployeer, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Employer's Profile with given ID.
     * @param idPraktykodawcy ID of Employer
     * @param idProfilu ID of Profile
     * @return number of deleted wors
     */
    @RequestMapping(value = "/{idPraktykodawcy}/profile/{idProfilu}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteProfileOfEmployeer(@PathVariable int idPraktykodawcy, @PathVariable int idProfilu) {
        Long deleteProfileOfEmployeer = ipraktykodawcyService.deleteProfileOfEmployeer(idPraktykodawcy, idProfilu);
        if (deleteProfileOfEmployeer != null) {
            return new ResponseEntity<Long>(deleteProfileOfEmployeer, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Coordinator of Employer.
     * @param idPraktykodawcy ID of Employer
     * @return list of Employer's Coordinators
     */
    @RequestMapping(value = "/{idPraktykodawcy}/koordynatorzy", method = RequestMethod.GET)
    public ResponseEntity<List<KoordynatorzyPraktykEntity>> findEveryCoordinatorOfEmployer(@PathVariable int idPraktykodawcy) {
        List list = ipraktykodawcyService.findEveryCoordinatorOfEmployer(idPraktykodawcy);
        if (list !=null) {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets personal data of Employer's Coordinator.
     * @see praktyki.core.entities.OsobyEntity
     * @param idPraktykodawcy ID of Employer
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return Coordinator's Employer
     */
    @RequestMapping(value = "/{idPraktykodawcy}/koordynatorzy/{idKoordynatoraPraktyk}", method = RequestMethod.GET)
    public ResponseEntity<List<OsobyEntity>> getCoordinatorOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idKoordynatoraPraktyk) {
        List list = ipraktykodawcyService.getCoordinatorOfEmployer(idPraktykodawcy, idKoordynatoraPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<OsobyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<OsobyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds relation between Employer, Tutor and Field of Study
     * @see praktyki.core.entities.OpiekunowieKierunkowiEntity
     * @param opiekunowieKierunkowiEntity relation with proper IDs
     * @return persisted relation
     */
    @RequestMapping(value = "/{idPraktykodawcy}/opiekunkierunku", method = RequestMethod.POST)
    public ResponseEntity<OpiekunowieKierunkowiEntity> addTutorCourse (@RequestBody OpiekunowieKierunkowiEntity opiekunowieKierunkowiEntity) {
        OpiekunowieKierunkowiEntity relation = ipraktykodawcyService.addTutorCourse(opiekunowieKierunkowiEntity);
        if (relation !=null) {
            return new ResponseEntity<OpiekunowieKierunkowiEntity>(relation, HttpStatus.OK);
        } else {
            return new ResponseEntity<OpiekunowieKierunkowiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Tutor of Employer.
     * @param idPraktykodawcy ID of Employer
     * @return list of Employer's Tutors
     */
    @RequestMapping(value = "/{idPraktykodawcy}/opiekunkierunku", method = RequestMethod.GET)
    public ResponseEntity<List<OpiekunowiePraktykEntity>> getEveryTutorOfEmployer(@PathVariable int idPraktykodawcy) {
        List list = ipraktykodawcyService.getAllTutorsOfEmployee(idPraktykodawcy);
        if (list !=null) {
            return new ResponseEntity<List<OpiekunowiePraktykEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<OpiekunowiePraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Tutor of Employer
     * @param idPraktykodawcy ID of Employer
     * @param idOpiekunaPraktyk ID of Tutor
     * @return Employer's Tutor
     */
    @RequestMapping(value = "/{idPraktykodawcy}/opiekunkierunku/{idOpiekunaPraktyk}", method = RequestMethod.GET)
    public ResponseEntity<List<OpiekunowiePraktykEntity>> getTutorOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idOpiekunaPraktyk) {
        List list = ipraktykodawcyService.getTutorOfEmployer(idPraktykodawcy, idOpiekunaPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<OpiekunowiePraktykEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<OpiekunowiePraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Employer's Tutor with given ID (deletes realtion
     * between Tutor and Employer)
     * @param idPraktykodawcy ID of Employer
     * @param idOpiekunaPraktyk ID of Tutor
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idPraktykodawcy}/opiekunkierunku/{idOpiekunaPraktyk}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteTutorOfEmployer(@PathVariable int idPraktykodawcy, @PathVariable int idOpiekunaPraktyk) {
        Long deleteTutor = ipraktykodawcyService.deleteTutorOfEmployer(idPraktykodawcy, idOpiekunaPraktyk);
        if (deleteTutor !=null) {
            return new ResponseEntity<Long>(deleteTutor, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }
}
