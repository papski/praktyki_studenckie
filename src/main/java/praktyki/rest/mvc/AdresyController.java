package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import praktyki.core.entities.AdresyEntity;
import praktyki.core.service.Interface.iAdresyService;

import java.util.List;

/**
 * Created by dawid on 29.12.14.
 */

/**
 * AddressesControler<br>
 * Offers URL requests for working with Addresses in DB/Testing purpose
 */
@Controller
@RequestMapping("/adresy")
public class AdresyController {

    private iAdresyService iadresyService;

    @Autowired
    public AdresyController(iAdresyService idresyService) {
        this.iadresyService = idresyService;
    }

    /**
     * Adds address into DB. Should not be used to add addresses of anyone,
     * testing feature
     * @see praktyki.core.entities.AdresyEntity
     * @param addAddress address to be persisted
     * @return persisted address
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AdresyEntity> addAddress(@RequestBody AdresyEntity addAddress) throws Exception {
        try {
            AdresyEntity createdAdresyEntity = iadresyService.addAddress(addAddress);
            return new ResponseEntity<AdresyEntity>(createdAdresyEntity, HttpStatus.OK);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * Finds every address in DB
     * @return list of every address
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<AdresyEntity>> findEveryAddress() {
        List<AdresyEntity> list = iadresyService.findEveryAddress();
        return new ResponseEntity<List<AdresyEntity>>(list, HttpStatus.OK);
    }

    /**
     * Finds address in DB with given ID.
     * @param idAdresu ID of address
     * @return single address
     */
    @RequestMapping(value = "/{idAdresu}", method = RequestMethod.GET)
    public ResponseEntity<AdresyEntity> getAddress(@PathVariable int idAdresu) {
        AdresyEntity adresyEntity = iadresyService.findByIdAdresu(idAdresu);
        if(adresyEntity !=null) {
            return new ResponseEntity<AdresyEntity>(adresyEntity, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<AdresyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Searches addresses with given parameter and returns them.
     * @see praktyki.core.entities.AdresyEntity
     * @param searchString search by Street Name, City or Zip Code
     * @param sortBy sort by address attributes, preferably ones in searchString
     * @param sortType ASC or DESC
     * @return list of searched addresses
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<List<AdresyEntity>> getAddressByStreetCityZipCode (@RequestParam String searchString, @RequestParam String sortBy, @RequestParam String sortType) {
        List list = iadresyService.getAddressByStreetCityZipCode(searchString, sortBy, sortType);
        if (list !=null) {
            return new ResponseEntity<List<AdresyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<AdresyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates address with given ID and updated entity. If we want to update
     * just one atttribute we should read old values and put them into updated
     * entity that needs to be merged. Otherwise values will be replaced with NULL.
     * ID of address SHOULD NOT be changed.
     * @see praktyki.core.entities.AdresyEntity
     * @param adresyEntity updated address
     * @param idAdresu ID of address
     * @return updated address
     */
    @RequestMapping(value = "/{idAdresu}", method = RequestMethod.PATCH)
    public ResponseEntity<AdresyEntity> updateAddress (@RequestBody AdresyEntity adresyEntity, @PathVariable int idAdresu) {
        AdresyEntity updateAddress = iadresyService.updateAddress(adresyEntity, idAdresu);
        if (updateAddress !=null) {
            return new ResponseEntity<AdresyEntity>(updateAddress, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<AdresyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes address with given ID. To ensure proper deletion
     * this method should be used AT LEAST 2 times (untill method returns empty JSON)
     * @param idAdresu ID of address
     * @return deleted address
     */
    @RequestMapping(value = "/{idAdresu}", method = RequestMethod.DELETE)
    public ResponseEntity<AdresyEntity> deleteAddress(@PathVariable int idAdresu) {
        AdresyEntity adresyEntity = iadresyService.deleteAddress(idAdresu);
        if(adresyEntity !=null) {
            return new ResponseEntity<AdresyEntity>(adresyEntity, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<AdresyEntity>(HttpStatus.NOT_FOUND);
        }
    }

}
