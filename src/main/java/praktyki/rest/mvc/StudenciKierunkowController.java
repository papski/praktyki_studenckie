package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.StudenciKierunkowEntity;
import praktyki.core.service.Interface.iStudenciKierunkowService;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */

/**
 * to be deleted/unecessary
 */
@Controller
@RequestMapping(value = "/studencikierunkow")
public class StudenciKierunkowController {

    private iStudenciKierunkowService istudencikierunkowService;

    @Autowired
    StudenciKierunkowController(iStudenciKierunkowService istudencikierunkowService) {
        this.istudencikierunkowService = istudencikierunkowService;
    }

    /*@RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<StudenciKierunkowEntity> addRelation(@RequestBody StudenciKierunkowEntity studenciKierunkowEntity) {
        StudenciKierunkowEntity addRelation = istudencikierunkowService.addRelation(studenciKierunkowEntity);
        if (addRelation !=null) {
            return new ResponseEntity<StudenciKierunkowEntity>(addRelation, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<StudenciKierunkowEntity>(HttpStatus.NOT_FOUND);
        }
    }*/

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<StudenciKierunkowEntity>> getAll () {
        List<StudenciKierunkowEntity> list = istudencikierunkowService.getAll();
        if(list !=null) {
            return new ResponseEntity<List<StudenciKierunkowEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<StudenciKierunkowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{nrAlbumu}", method = RequestMethod.GET)
    public ResponseEntity<List<StudenciKierunkowEntity>> getByNrAlbumu(@PathVariable Long nrAlbumu) {
        List<StudenciKierunkowEntity> list = istudencikierunkowService.getByNrAlbumu(nrAlbumu);
        if(list !=null) {
            return new ResponseEntity<List<StudenciKierunkowEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<StudenciKierunkowEntity>>(HttpStatus.NOT_FOUND);
        }
    }
}
