package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.*;
import praktyki.core.service.Interface.iKoordynatorzyPraktykService;
import praktyki.rest.mvc.helpers.KoordynatorInfo;
import praktyki.rest.mvc.helpers.KoordynatorzyDate;
import praktyki.rest.mvc.helpers.PraktykaInfo;

import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */

/**
 * CoordinatorsController<br>
 * Offers URL requests for working with Coordinators in DB.<br>
 * Login and Email MUST BE CHECKED before persisitng new
 * Tutor into DB. After checking we need to use 3 methods
 * one after another addTutor, addPerson and addUser to ensure
 * proper persisting the Tutor.
 * @see praktyki.rest.mvc.KoordynatorzyPraktykController#addCoordinator
 * @see praktyki.rest.mvc.KoordynatorzyPraktykController#addPerson
 * @see praktyki.rest.mvc.KoordynatorzyPraktykController#addUser
 */
@Controller
@RequestMapping("/koordynatorzy")
public class KoordynatorzyPraktykController {

    private iKoordynatorzyPraktykService ikoordynatorzyPraktykService;

    @Autowired
    public KoordynatorzyPraktykController(iKoordynatorzyPraktykService ikoordynatorzyPraktykService) {
        this.ikoordynatorzyPraktykService = ikoordynatorzyPraktykService;
    }

    /**
     * Function for checking if Email is already used by someone.
     * If it is used, user MUST change his/hers emil.
     * @param email email to be checked
     * @return 1 when email is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/email/{email:.+}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkEmail(@PathVariable String email) {
        Long checkEmail = ikoordynatorzyPraktykService.checkEmail(email);
        if (checkEmail ==1 ) {
            return new ResponseEntity<Long>(checkEmail, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Function for checking if Login is already used by someone.
     * If it is used user MUST change his/hers login.
     * @param login login to be checked
     * @return 1 when login is used, -1 when it is not
     */
    @RequestMapping(value = "/sprawdz/login/{login}", method = RequestMethod.GET)
    public ResponseEntity<Long> checkLogin(@PathVariable String login) {
        Long checkLogin = ikoordynatorzyPraktykService.checkLogin(login);
        if (checkLogin ==1 ) {
            return new ResponseEntity<Long>(checkLogin, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds new Coordinator into DB.
     * @param koordynatorzyPraktykEntity Coordinator to be persisted
     * @return persisted Coordinator
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<KoordynatorzyPraktykEntity> addCoordinator(@RequestBody KoordynatorzyPraktykEntity koordynatorzyPraktykEntity) {
        KoordynatorzyPraktykEntity addCoordinator = ikoordynatorzyPraktykService.addCoordinator(koordynatorzyPraktykEntity);
        if (addCoordinator !=null) {
            return new ResponseEntity<KoordynatorzyPraktykEntity>(addCoordinator, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KoordynatorzyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Coordinator in DB
     * @return list of every coordinator
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<KoordynatorzyPraktykEntity>> getAll() {
        List<KoordynatorzyPraktykEntity> list = ikoordynatorzyPraktykService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KoordynatorzyPraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Coordinator with given ID.
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return single coordinator
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}", method = RequestMethod.GET)
    public ResponseEntity<KoordynatorzyPraktykEntity> getCoordinator(@PathVariable int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity getCoordinator = ikoordynatorzyPraktykService.getCoordinator(idKoordynatoraPraktyk);
        if (getCoordinator !=null) {
            return new ResponseEntity<KoordynatorzyPraktykEntity>(getCoordinator, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KoordynatorzyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds info about Coordinator - personal data, user's data,
     * courses and coordinato'r employers.
     * @see praktyki.rest.mvc.helpers.KoordynatorInfo
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return Coordinator's info
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/info", method = RequestMethod.GET)
    public ResponseEntity<KoordynatorInfo> getAllDataOfCoordinator (@PathVariable int idKoordynatoraPraktyk) {
        KoordynatorInfo getData = ikoordynatorzyPraktykService.getAllDataOfCoordinator(idKoordynatoraPraktyk);
        if (getData !=null) {
            return new ResponseEntity<KoordynatorInfo>(getData, HttpStatus.OK);
        } else {
            return new ResponseEntity<KoordynatorInfo>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds info about Coordinator's Practices with all their data -
     * Practice's ID, Template, Employer, Status, Course, Type, Academic
     * Year, Student, Tutor, Coordinator, Employer's Address and Agreement.
     * @see praktyki.rest.mvc.helpers.PraktykaInfo
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return Coordinator's Practices info
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/praktyki", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykaInfo>> getAllPracticesOfCoordinator (@PathVariable int idKoordynatoraPraktyk) {
        List list = ikoordynatorzyPraktykService.getAllPracticesOfCoordinator(idKoordynatoraPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<PraktykaInfo>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykaInfo>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Coordinator. To ensure proper deletion it must be
     * used AT LEAST 2 times(untill method returns empty JSON)
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return deleted Coordinator
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}", method = RequestMethod.DELETE)
    public ResponseEntity<KoordynatorzyPraktykEntity> deleteCoordinator(@PathVariable int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity deleteCoordinator = ikoordynatorzyPraktykService.deleteCoordinator(idKoordynatoraPraktyk);
        if (deleteCoordinator !=null) {
            return new ResponseEntity<KoordynatorzyPraktykEntity>(deleteCoordinator, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KoordynatorzyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Personal Data to Coordinator. Before we can add this data
     * we need to persist new Coordinator with method addCoordinator
     * @see praktyki.core.entities.OsobyEntity
     * @see praktyki.rest.mvc.KoordynatorzyPraktykController#addCoordinator(KoordynatorzyPraktykEntity koordynatorzyPraktykEntity)
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param osobyEntity Coordinator's Personal Data
     * @return Coordinator's Personal Data
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/dane", method = RequestMethod.POST)
    public ResponseEntity<OsobyEntity> addPerson(@PathVariable int idKoordynatoraPraktyk, @RequestBody OsobyEntity osobyEntity) {
        OsobyEntity addPerson = ikoordynatorzyPraktykService.addPerson(idKoordynatoraPraktyk, osobyEntity);
        if (addPerson !=null) {
            return new ResponseEntity<OsobyEntity>(addPerson, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Coordinator's Personal Data. We can change his/hers Degree,
     * Name, Surname, Cellphone number and Landline number.
     * @see praktyki.core.entities.OsobyEntity
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param osobyEntity updated Coordinator's Personal Data
     * @return updated Coordinator's Personal Data
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/dane", method = RequestMethod.PATCH)
    public ResponseEntity<OsobyEntity> editInfoOfCoordinator(@PathVariable int idKoordynatoraPraktyk, @RequestBody OsobyEntity osobyEntity) {
        KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykService.getCoordinator(idKoordynatoraPraktyk);
        Integer idOsoby = coordinator.getIdOsoby();
        OsobyEntity editInfoOfCoordinator = ikoordynatorzyPraktykService.editInfoOfCoordinator(idOsoby, idKoordynatoraPraktyk, osobyEntity);
        if (editInfoOfCoordinator !=null) {
            return new ResponseEntity<OsobyEntity>(editInfoOfCoordinator, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<OsobyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Coordinator's Personal Data
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return Coordinator's Personal Data
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/dane", method = RequestMethod.GET)
    public ResponseEntity<List<OsobyEntity>> getInfoOfCoordinator(@PathVariable int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykService.getCoordinator(idKoordynatoraPraktyk);
        Integer idOsoby = coordinator.getIdOsoby();
        List<OsobyEntity> getInfoOfCoordinator = ikoordynatorzyPraktykService.getInfoOfCoordinator(idOsoby, idKoordynatoraPraktyk);
        if (getInfoOfCoordinator !=null) {
            return new ResponseEntity<List<OsobyEntity>>(getInfoOfCoordinator, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<OsobyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Coordinator's User Data into DB. Before this we need to
     * persist his/hers Personal data with method addPerson
     * @see praktyki.core.entities.UzytkownicyEntity
     * @see praktyki.rest.mvc.KoordynatorzyPraktykController#addPerson(int idKoordynatoraPraktyk, OsobyEntity osobyEntity)
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param uzytkownicyEntity Coordinator's User Data
     * @return Coordinator's User Data
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/user", method = RequestMethod.POST)
    public ResponseEntity<UzytkownicyEntity> addUser(@PathVariable int idKoordynatoraPraktyk, @RequestBody UzytkownicyEntity uzytkownicyEntity) {
        UzytkownicyEntity addUser = ikoordynatorzyPraktykService.addUser(idKoordynatoraPraktyk, uzytkownicyEntity);
        if (addUser !=null) {
            return new ResponseEntity<UzytkownicyEntity>(addUser, HttpStatus.OK);
        } else {
            return new ResponseEntity<UzytkownicyEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Coordinator's User Data.
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return Coordinator's User Data
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/user", method = RequestMethod.GET)
    public ResponseEntity<List<UzytkownicyEntity>> getUser(@PathVariable int idKoordynatoraPraktyk) {
        List list = ikoordynatorzyPraktykService.getUser(idKoordynatoraPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<UzytkownicyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<UzytkownicyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Assigns Field of Study to Coordinator.
     * @see praktyki.core.entities.KoordynatorzyKierunkowiEntity
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param idKierunku ID of Course
     * @return join table row - CourseCoordinatorsEntity
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/kierunek/{idKierunku}", method = RequestMethod.POST)
    public ResponseEntity<KoordynatorzyKierunkowiEntity> addCourseToCoordinator(@PathVariable int idKoordynatoraPraktyk, @PathVariable int idKierunku) {
        KoordynatorzyKierunkowiEntity addCourseToCoordinator = ikoordynatorzyPraktykService.addCourseToCoordinator(idKoordynatoraPraktyk, idKierunku);
        if (addCourseToCoordinator !=null) {
            return new ResponseEntity<KoordynatorzyKierunkowiEntity>(addCourseToCoordinator, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<KoordynatorzyKierunkowiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Field of Studies assigned to Coordinator.
     * @see praktyki.core.entities.KoordynatorzyKierunkowiEntity
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return list of Field of Studies
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/kierunek", method = RequestMethod.GET)
    public ResponseEntity<List<KierunkiStudiowEntity>> findCourseOfCoordinator(@PathVariable int idKoordynatoraPraktyk) {
        List<KierunkiStudiowEntity> list = ikoordynatorzyPraktykService.findCourseOfCoordinator(idKoordynatoraPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes every Field of Studies assigned to Coordinator
     * @see praktyki.core.entities.KoordynatorzyKierunkowiEntity
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/kierunek", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteCoordinatorFromEveryCourse(@PathVariable int idKoordynatoraPraktyk) {
        Long deleteCoordinatorFromCourse = ikoordynatorzyPraktykService.deleteCoordinatorFromEveryCourse(idKoordynatoraPraktyk);
        if (deleteCoordinatorFromCourse != -1) {
            return new ResponseEntity<Long>(deleteCoordinatorFromCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Course of Coordinator.
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param idKierunku ID of Course
     * @return Coordinator's Course
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/kierunek/{idKierunku}", method = RequestMethod.GET)
    public ResponseEntity<List<KierunkiStudiowEntity>> getCourseOfCoordinator(@PathVariable int idKoordynatoraPraktyk, @PathVariable int idKierunku) {
        List<KierunkiStudiowEntity> list = ikoordynatorzyPraktykService.getCourseOfCoordinator(idKoordynatoraPraktyk, idKierunku);
        if (list !=null) {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<KierunkiStudiowEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * To be delted
     * @param idKoordynatoraPraktyk
     * @param idKierunku
     * @return
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/kierunek/{idKierunku}/data", method = RequestMethod.GET)
    public ResponseEntity<KoordynatorzyDate> getDate(@PathVariable int idKoordynatoraPraktyk, @PathVariable int idKierunku) {
        KoordynatorzyDate getDate = ikoordynatorzyPraktykService.getDate(idKoordynatoraPraktyk, idKierunku);
        if (getDate !=null) {
            return new ResponseEntity<KoordynatorzyDate>(getDate, HttpStatus.OK);
        } else {
            return new ResponseEntity<KoordynatorzyDate>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idKoordynatoraPraktyk}/kierunek/{idKierunku}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteCoordinatorFromCourse(@PathVariable int idKoordynatoraPraktyk, @PathVariable int idKierunku) {
        Long deleteCoordinatorFromCourse = ikoordynatorzyPraktykService.deleteCoordinatorFromCourse(idKoordynatoraPraktyk, idKierunku);
        if (deleteCoordinatorFromCourse != -1) {
            return new ResponseEntity<Long>(deleteCoordinatorFromCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Assigns Employer to Coordinator with correct Field of Study.
     * @see praktyki.core.entities.KoordynatorzyKierunkowiPraktykodawcowEntity
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param koordynatorzyKierunkowiPraktykodawcowEntity join-table entity
     * @return join-table row EmployersCourseCoordinatorsEntity
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/praktykodawca", method = RequestMethod.POST)
    public ResponseEntity<KoordynatorzyKierunkowiPraktykodawcowEntity> addEmployerToCoordinator(@PathVariable int idKoordynatoraPraktyk, @RequestBody KoordynatorzyKierunkowiPraktykodawcowEntity koordynatorzyKierunkowiPraktykodawcowEntity){
        KoordynatorzyKierunkowiPraktykodawcowEntity addRelation = ikoordynatorzyPraktykService.addEmployerToCoordinator(idKoordynatoraPraktyk, koordynatorzyKierunkowiPraktykodawcowEntity);
        if (addRelation !=null) {
            return new ResponseEntity<KoordynatorzyKierunkowiPraktykodawcowEntity>(addRelation, HttpStatus.OK);
        } else {
            return new ResponseEntity<KoordynatorzyKierunkowiPraktykodawcowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Employers of Coordinator
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return list of Coordinator's Employers
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/praktykodawca", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykodawcyEntity>> findEmployersOfCoordinator(@PathVariable int idKoordynatoraPraktyk) {
        List list = ikoordynatorzyPraktykService.findEmployersOfCoordinator(idKoordynatoraPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<PraktykodawcyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykodawcyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Every Employer of Coordinator
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/praktykodawca", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteEveryEmployerFromCoordinator (@PathVariable int idKoordynatoraPraktyk) {
        Long delete = ikoordynatorzyPraktykService.deleteEveryEmployerFromCoordinator(idKoordynatoraPraktyk);
        if (delete !=null) {
            return new ResponseEntity<Long>(delete, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Employer of Coordinator
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param idPraktykodawcy ID of Employer
     * @return Coordinator's Employer
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/praktykodawca/{idPraktykodawcy}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykodawcyEntity>> getEmployerOfCoordinator(@PathVariable int idKoordynatoraPraktyk, @PathVariable int idPraktykodawcy) {
        List list = ikoordynatorzyPraktykService.getEmployerOfCoordinator(idKoordynatoraPraktyk, idPraktykodawcy);
        if (list !=null) {
            return new ResponseEntity<List<PraktykodawcyEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykodawcyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Employer from Coordinator.
     * @see praktyki.core.entities.KoordynatorzyKierunkowiPraktykodawcowEntity
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param idPraktykodawcy ID of Employer
     * @return number of deleted rows
     */
    @RequestMapping(value = "/{idKoordynatoraPraktyk}/praktykodawca/{idPraktykodawcy}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deletEmployerFromCoordinator (@PathVariable int idKoordynatoraPraktyk, @PathVariable int idPraktykodawcy) {
        Long delete = ikoordynatorzyPraktykService.deleteEmployerFromCoordinator(idKoordynatoraPraktyk, idPraktykodawcy);
        if (delete !=null) {
            return new ResponseEntity<Long>(delete, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }


}
