package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.PraktykiKierunkoweEntity;
import praktyki.core.service.Interface.iPraktykiKierunkoweService;
import praktyki.rest.mvc.helpers.PraktykiKierunkoweInfo;

import java.util.List;

/**
 * Created by dawid on 08.03.15.
 */

/**
 * CoursePracticesController<br>
 * Offers URL requests for working with Course's Practices in DB<br>
 * Field of Study (Course) needs row in this entity before it can
 * add any Practices for that Course.
 */
@Controller
@RequestMapping("/praktykikierunkowe")
public class PraktykiKierunkoweController {

    private iPraktykiKierunkoweService ipraktykiKierunkoweService;

    @Autowired
    public PraktykiKierunkoweController (iPraktykiKierunkoweService ipraktykiKierunkoweService) {
        this.ipraktykiKierunkoweService = ipraktykiKierunkoweService;
    }

    /**
     * Adds Course's Practice into DB
     * @param praktykiKierunkoweEntity Course's Practice to be persisted
     * @return persisted Course's Practice
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PraktykiKierunkoweEntity> addRelation(@RequestBody PraktykiKierunkoweEntity praktykiKierunkoweEntity){
        PraktykiKierunkoweEntity addRelation = ipraktykiKierunkoweService.addRelation(praktykiKierunkoweEntity);
        if (addRelation !=null) {
            return new ResponseEntity<PraktykiKierunkoweEntity>(addRelation, HttpStatus.OK);
        } else {
            return new ResponseEntity<PraktykiKierunkoweEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Course's Practices in DB.
     * @return list of every Course's Practices
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiKierunkoweEntity>> findAll() {
        List list = ipraktykiKierunkoweService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<PraktykiKierunkoweEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiKierunkoweEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Course's Practice by 3 parameters. If any parameter is set
     * to 0 it will be ignored in searching.
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @param idTypuPraktyki ID of Practice's Type
     * @return list of search resaults
     */
    @RequestMapping(value = "/kierunek/{idKierunku}/rok/{idRokuAkademickiego}/typ/{idTypuPraktyki}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiKierunkoweInfo>> getRelation(@PathVariable int idKierunku, @PathVariable int idRokuAkademickiego, @PathVariable int idTypuPraktyki) {
        List list = ipraktykiKierunkoweService.getRelation(idKierunku, idRokuAkademickiego, idTypuPraktyki);
        if (list !=null) {
            return new ResponseEntity<List<PraktykiKierunkoweInfo>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiKierunkoweInfo>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Course's Practice. We can change MinDateOfBegining, MaxDateOfEnd,
     * number of Hours and Duration time of Course's Practices
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @param idTypuPraktyki ID of Practice's Type
     * @param praktykiKierunkoweEntity updated Course's Practice
     * @return updated Course's Practice
     */
    @RequestMapping(value = "/kierunek/{idKierunku}/rok/{idRokuAkademickiego}/typ/{idTypuPraktyki}", method = RequestMethod.PATCH)
    public ResponseEntity<PraktykiKierunkoweEntity> editRelation(@PathVariable int idKierunku, @PathVariable int idRokuAkademickiego, @PathVariable int idTypuPraktyki, @RequestBody PraktykiKierunkoweEntity praktykiKierunkoweEntity) {
        PraktykiKierunkoweEntity edit = ipraktykiKierunkoweService.editRelation(idKierunku, idTypuPraktyki, idRokuAkademickiego, praktykiKierunkoweEntity);
        if (edit !=null) {
            return new ResponseEntity<PraktykiKierunkoweEntity>(edit, HttpStatus.OK);
        } else {
            return new ResponseEntity<PraktykiKierunkoweEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Course's Practice.
     * @param idKierunku ID of COurse
     * @param idRokuAkademickiego ID of Academic Year
     * @param idTypuPraktyki ID of Practice's Type
     * @return
     */
    @RequestMapping(value = "/kierunek/{idKierunku}/rok/{idRokuAkademickiego}/typ/{idTypuPraktyki}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteRelation(@PathVariable int idKierunku, @PathVariable int idRokuAkademickiego, @PathVariable int idTypuPraktyki) {
        Long delete = ipraktykiKierunkoweService.deleteRelation(idKierunku, idRokuAkademickiego, idTypuPraktyki);
        if (delete !=null) {
            return new ResponseEntity<Long>(delete, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }
}
