package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.*;
import praktyki.core.service.Interface.iSzablonyPraktykService;
import praktyki.rest.mvc.helpers.CategoryInfo;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */

/**
 * TemplatesController<br>
 * Offers URL request for working with Practice's Templates
 * and Offered/Required Qualifications in DB.<br>
 * Template needs to be persisted along with Practice's Agreement
 * before Practice is being added into DB.
 * @see praktyki.rest.mvc.PraktykiController
 * @see praktyki.rest.mvc.PorozumieniaController
 */
@Controller
@RequestMapping("/szablonypraktyk")
public class SzablonyPraktykController {

    private iSzablonyPraktykService iszablonyPraktykService;

    @Autowired
    public SzablonyPraktykController(iSzablonyPraktykService iszablonyPraktykService) {
        this.iszablonyPraktykService = iszablonyPraktykService;
    }

    /**
     * Adds Template into DB
     * @see praktyki.core.entities.SzablonyPraktykEntity
     * @param SzablonyPraktykEntity Template to be persisted
     * @return persisted Template
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<SzablonyPraktykEntity> addTemplate(@RequestBody SzablonyPraktykEntity SzablonyPraktykEntity) {
        SzablonyPraktykEntity addTemplate = iszablonyPraktykService.addTemplate(SzablonyPraktykEntity);
        if (addTemplate !=null) {
            return new ResponseEntity<SzablonyPraktykEntity>(addTemplate, HttpStatus.OK);
        } else {
            return new ResponseEntity<SzablonyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Every Template in DB.
     * @return list of every Template
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<SzablonyPraktykEntity>> findAll() {
        List list = iszablonyPraktykService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<SzablonyPraktykEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<SzablonyPraktykEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Template with given ID.
     * @param idSzablonu ID of Template
     * @return single Template
     */
    @RequestMapping(value = "/{idSzablonu}", method = RequestMethod.GET)
    public ResponseEntity<SzablonyPraktykEntity> getTemplate(@PathVariable int idSzablonu) {
        SzablonyPraktykEntity getTemplate = iszablonyPraktykService.getTemplate(idSzablonu);
        if (getTemplate !=null) {
            return new ResponseEntity<SzablonyPraktykEntity>(getTemplate, HttpStatus.OK);
        } else {
            return new ResponseEntity<SzablonyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Template from DB. To ensure proper deletion
     * method MUST be used at least 2 times (or until it
     * returns null)
     * @param idSzablonu ID of Template
     * @return deleted Template
     */
    @RequestMapping(value = "/{idSzablonu}", method = RequestMethod.DELETE)
    public ResponseEntity<SzablonyPraktykEntity> deleteTemplate(@PathVariable int idSzablonu) {
        SzablonyPraktykEntity deleteTemplate = iszablonyPraktykService.deleteTemplate(idSzablonu);
        if (deleteTemplate !=null) {
            return new ResponseEntity<SzablonyPraktykEntity>(deleteTemplate, HttpStatus.OK);
        } else {
            return new ResponseEntity<SzablonyPraktykEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Qualification of Template
     * @param idSzablonu ID of Template
     * @return list of Template's every Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/kwalifikacje", method = RequestMethod.GET)
    public ResponseEntity<List<KwalifikacjeEntity>> getQualificationsOfTemplate(@PathVariable int idSzablonu) {
        List list = iszablonyPraktykService.getQualificationsOfTemplate(idSzablonu);
        if (list !=null) {
            return new ResponseEntity<List<KwalifikacjeEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KwalifikacjeEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Required Qualification to Template.
     * @see praktyki.core.entities.WymaganeKwalifikacjeEntity
     * @see praktyki.core.entities.KwalifikacjeEntity
     * @param idSzablonu ID of Template
     * @param kwalifikacjeEntity Qualification to be persisted
     * @return persisted Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/wymaganekwalifikacje", method = RequestMethod.POST)
    public ResponseEntity<WymaganeKwalifikacjeEntity> addRequiredQualification(@PathVariable int idSzablonu, @RequestBody KwalifikacjeEntity kwalifikacjeEntity){
        WymaganeKwalifikacjeEntity addReqQuali = iszablonyPraktykService.addRequiredQualification(idSzablonu, kwalifikacjeEntity);
        if (addReqQuali !=null) {
            return new ResponseEntity<WymaganeKwalifikacjeEntity>(addReqQuali, HttpStatus.OK);
        } else {
            return new ResponseEntity<WymaganeKwalifikacjeEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Required Qualification of Template.
     * @param idSzablonu ID of Template
     * @return list of Template's every Required Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/wymaganekwalifikacje", method = RequestMethod.GET)
    public ResponseEntity<List<KwalifikacjeEntity>> findEveryRequiredQualification(@PathVariable int idSzablonu) {
        List list = iszablonyPraktykService.findEveryRequiredQualifcation(idSzablonu);
        if (list !=null) {
            return new ResponseEntity<List<KwalifikacjeEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KwalifikacjeEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Required Qualificaiton of Template with given ID.
     * @param idSzablonu ID of Template
     * @param idKwalifikacji ID of Required Qualification
     * @return single Required Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/wymaganekwalifikacje/{idKwalifikacji}", method = RequestMethod.GET)
    public ResponseEntity<List<KwalifikacjeEntity>> getRequiredQualification(@PathVariable int idSzablonu, @PathVariable int idKwalifikacji) {
        List list = iszablonyPraktykService.getRequiredQualification(idSzablonu, idKwalifikacji);
        if (list !=null) {
            return new ResponseEntity<List<KwalifikacjeEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KwalifikacjeEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Category of Required Qualification of Template.
     * @see praktyki.core.entities.KategorieKwalifikacjiEntity
     * @param idKwalifikacji ID of Qualificaiton
     * @param kategorieKwalifikacjiEntity Category of Required Qualification to be persited
     * @return persisted Category of Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/wymaganekwalifikacje/{idKwalifikacji}/kategoria", method = RequestMethod.POST)
    public ResponseEntity<KategorieKwalifikacjiEntity> addRequiredCategory(@PathVariable int idKwalifikacji, @RequestBody KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity) {
        KategorieKwalifikacjiEntity addCategory = iszablonyPraktykService.addCategoryOfRequiredQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        if (addCategory !=null ) {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(addCategory, HttpStatus.OK);
        } else {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds SubCategory to Template's Required Qualification Category
     * @see praktyki.core.entities.KategorieKwalifikacjiEntity
     * @param kategorieKwalifikacjiEntity SubCategory of Required Qualification to be persisted
     * @param idKategoriiKwalifikacji ID of Qualification's Category
     * @param idKwalifikacji ID of Qualification
     * @return
     */
    @RequestMapping(value = "/{idSzablonu}/wymaganekwalifikacje/{idKwalifikacji}/kategoria/{idKategoriiKwalifikacji}/subkategoria", method = RequestMethod.POST)
    public ResponseEntity<KategorieKwalifikacjiEntity> addRequiredSubCategory(@RequestBody KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, @PathVariable int idKategoriiKwalifikacji, @PathVariable int idKwalifikacji) {
        kategorieKwalifikacjiEntity.setIdKategoriiNadrzednej(idKategoriiKwalifikacji);
        KategorieKwalifikacjiEntity addSubCategory = iszablonyPraktykService.addSubCategoryOfRequiredQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        if (addSubCategory !=null) {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(addSubCategory, HttpStatus.OK);
        } else {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Offered Qualification to Template.
     * @see praktyki.core.entities.OferowaneKwalifikacjeEntity
     * @see praktyki.core.entities.KwalifikacjeEntity
     * @param idSzablonu ID of Template
     * @param kwalifikacjeEntity Qualification to be persisted
     * @return persisted Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/oferowanekwalifikacje", method = RequestMethod.POST)
    public ResponseEntity<OferowaneKwalifikacjeEntity> addOfferedQualification(@PathVariable int idSzablonu, @RequestBody KwalifikacjeEntity kwalifikacjeEntity) {
        OferowaneKwalifikacjeEntity addOffQuali = iszablonyPraktykService.addOfferedQualification(idSzablonu, kwalifikacjeEntity);
        if (addOffQuali !=null) {
            return new ResponseEntity<OferowaneKwalifikacjeEntity>(addOffQuali, HttpStatus.OK);
        } else {
            return new ResponseEntity<OferowaneKwalifikacjeEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Offered Qualification of Template.
     * @param idSzablonu ID of Template
     * @return list of Template's every Offered Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/oferowanekwalifikacje", method = RequestMethod.GET)
    public ResponseEntity<List<KwalifikacjeEntity>> findEveryOfferedQualification(@PathVariable int idSzablonu) {
        List list = iszablonyPraktykService.findEveryOfferedQualification(idSzablonu);
        if (list !=null) {
            return new ResponseEntity<List<KwalifikacjeEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KwalifikacjeEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Offered Qualificaiton of Template with given ID.
     * @param idSzablonu ID of Template
     * @param idKwalifikacji ID of Offered Qualification
     * @return single Offered Qualification
     */
    @RequestMapping(value = "{idSzablonu}/oferowanekwalifikacje/{idKwalifikacji}", method = RequestMethod.GET)
    public ResponseEntity<List<KwalifikacjeEntity>> getOfferedQualification(@PathVariable int idSzablonu, @PathVariable int idKwalifikacji) {
        List list = iszablonyPraktykService.getOfferedQualification(idSzablonu, idKwalifikacji);
        if (list !=null) {
            return new ResponseEntity<List<KwalifikacjeEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KwalifikacjeEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds Category of Offered Qualification of Template.
     * @see praktyki.core.entities.KategorieKwalifikacjiEntity
     * @param idKwalifikacji ID of Qualificaiton
     * @param kategorieKwalifikacjiEntity Category of Offered Qualification to be persited
     * @return persisted Category of Qualification
     */
    @RequestMapping(value = "/{idSzablonu}/oferowanekwalifikacje/{idKwalifikacji}/kategoria", method = RequestMethod.POST)
    public ResponseEntity<KategorieKwalifikacjiEntity> addOfferedCategory(@PathVariable int idKwalifikacji, @RequestBody KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity) {
        KategorieKwalifikacjiEntity addCategory = iszablonyPraktykService.addCategoryOfOfferdQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        if (addCategory !=null ) {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(addCategory, HttpStatus.OK);
        } else {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds SubCategory to Template's Offered Qualification Category
     * @see praktyki.core.entities.KategorieKwalifikacjiEntity
     * @param kategorieKwalifikacjiEntity SubCategory of Offered Qualification to be persisted
     * @param idKategoriiKwalifikacji ID of Qualification's Category
     * @param idKwalifikacji ID of Qualification
     * @return
     */
    @RequestMapping(value = "/{idSzablonu}/oferowanekwalifikacje/{idKwalifikacji}/kategoria/{idKategoriiKwalifikacji}/subkategoria", method = RequestMethod.POST)
    public ResponseEntity<KategorieKwalifikacjiEntity> addOfferedSubCategory(@RequestBody KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, @PathVariable int idKategoriiKwalifikacji, @PathVariable int idKwalifikacji) {
        kategorieKwalifikacjiEntity.setIdKategoriiNadrzednej(idKategoriiKwalifikacji);
        KategorieKwalifikacjiEntity addSubCategory = iszablonyPraktykService.addSubCategoryOfOfferedQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        if (addSubCategory !=null) {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(addSubCategory, HttpStatus.OK);
        } else {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Category.
     * @return list of every category
     */
    @RequestMapping(value = "kwalifikacje/kategoria", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> GetAllCategories() {
        List list = iszablonyPraktykService.findCategory();
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Subcategory
     * @return list of every subcategory
     */
    @RequestMapping(value = "kwalifikacje/subkategoria", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getAllSubCategories() {
        List list = iszablonyPraktykService.findSubCategory();
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Category/Subcategory by given ID
     * @param idKategoriiKwalifikacji ID of Category/Subcategory
     * @return
     */
    @RequestMapping(value = "kwalifikacje/kategoria/{idKategoriiKwalifikacji}", method = RequestMethod.GET)
    public ResponseEntity<CategoryInfo> getCategorOrSubCategoryyById(@PathVariable int idKategoriiKwalifikacji) {
        CategoryInfo info = iszablonyPraktykService.getInfoOfCategory(idKategoriiKwalifikacji);
        if (info !=null) {
            return new ResponseEntity<CategoryInfo>(info, HttpStatus.OK);
        } else {
            return new ResponseEntity<CategoryInfo>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Category. To ensure proper deletion it MUST be used
     * at least 2 times (or until it returns null)
     * @param idKategoriiKwalifikacji ID of Category
     * @return deleted Category
     */
    @RequestMapping(value = "kwalifikacje/kategoria/{idKategoriiKwalifikacji}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteCategory(@PathVariable int idKategoriiKwalifikacji) {
        Long delete = iszablonyPraktykService.deleteCategory(idKategoriiKwalifikacji);
        if (delete !=null) {
            return new ResponseEntity<Long>(delete, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Category by Name
     * @param nazwaKategorii Name of Category
     * @return list of search results
     */
    @RequestMapping(value = "kwalifikacje/kategoria/nazwa/{nazwaKategorii}", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getCategoryByName(@PathVariable String nazwaKategorii) {
        List list = iszablonyPraktykService.findCategoryByName(nazwaKategorii);
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Subcategories of Category
     * @param idKategoriiKwalifikacji ID of Category
     * @return list of Category's Subcategories
     */
    @RequestMapping(value = "kwalifikacje/kategoria/{idKategoriiKwalifikacji}/subkategoria", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getSubCategoriesOfCategory(@PathVariable int idKategoriiKwalifikacji) {
        List list = iszablonyPraktykService.findSubCategoryOfCategory(idKategoriiKwalifikacji);
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Subcategory by Name
     * @param nazwaKategorii Name of Subcategory
     * @return list of serach result
     */
    @RequestMapping(value = "kwalifikacje/subkategoria/nazwa/{nazwaKategorii}", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getSubCategoriesByName(@PathVariable String nazwaKategorii) {
        List list = iszablonyPraktykService.findSubCategoryByName(nazwaKategorii);
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Subcategory. To ensure proper deletion it MUST be used
     * at least 2 times (or until it returns null)
     * @param idKategoriiKwalifikacji ID of Category
     * @return deleted Subcategory
     */
    @RequestMapping(value = "kwalifikacje/subkategoria/{idKategoriiKwalifikacji}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deleteSubCategory(@PathVariable int idKategoriiKwalifikacji) {
        Long delete = iszablonyPraktykService.deleteSubCategory(idKategoriiKwalifikacji);
        if (delete !=null) {
            return new ResponseEntity<Long>(delete, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }

}
