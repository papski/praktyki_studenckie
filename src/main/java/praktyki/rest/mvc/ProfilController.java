package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.PraktykodawcyEntity;
import praktyki.core.entities.ProfilEntity;
import praktyki.core.entities.ProfilePraktykodawcowEntity;
import praktyki.core.service.Interface.iProfilService;

import java.util.List;

/**
 * Created by dawid on 25.01.15.
 */

/**
 * ProfilesController
 * Unecessary
 */
@Controller
@RequestMapping("/profile")
public class ProfilController {

    private iProfilService iprofilService;

    @Autowired
    public ProfilController(iProfilService iprofilService) {
        this.iprofilService = iprofilService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ProfilEntity> addProfile(@RequestBody ProfilEntity profilEntity) {
        ProfilEntity addProfile = iprofilService.addProfile(profilEntity);
        if (addProfile !=null) {
            return new ResponseEntity<ProfilEntity>(addProfile, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProfilEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ProfilEntity>> finAll() {
        List list = iprofilService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<ProfilEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<ProfilEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idProfilu}", method = RequestMethod.PATCH)
    public ResponseEntity<ProfilEntity> updateProfile(@PathVariable Integer idProfilu, @RequestBody ProfilEntity profilEntity) {
        ProfilEntity updateProfile = iprofilService.updateProfile(idProfilu, profilEntity);
        if (updateProfile !=null) {
            return new ResponseEntity<ProfilEntity>(updateProfile, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProfilEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idProfilu}", method = RequestMethod.GET)
    public ResponseEntity<ProfilEntity> getProfile (@PathVariable Integer idProfilu) {
        ProfilEntity getProfile = iprofilService.getProfile(idProfilu);
        if (getProfile !=null) {
            return new ResponseEntity<ProfilEntity>(getProfile, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProfilEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idProfilu}", method = RequestMethod.DELETE)
    public ResponseEntity<ProfilEntity> deleteProfile (@PathVariable Integer idProfilu) {
        ProfilEntity deleteProfile = iprofilService.deleteProfile(idProfilu);
        if (deleteProfile !=null) {
            return new ResponseEntity<ProfilEntity>(deleteProfile, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProfilEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idProfilu}/praktykodawca", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykodawcyEntity>> findEmployeerOfProfile (@PathVariable int idProfilu) {
        List list = iprofilService.findEmployeerOfProfile(idProfilu);
        if (list !=null) {
            return new ResponseEntity<List<PraktykodawcyEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<PraktykodawcyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idProfilu}/praktykodawca/{idPraktykodawcy}", method = RequestMethod.POST)
    public ResponseEntity<ProfilePraktykodawcowEntity> addEmployeer (@PathVariable Integer idProfilu, @PathVariable Integer idPraktykodawcy) {
        ProfilePraktykodawcowEntity addEmployeer = iprofilService.addRelation(idProfilu, idPraktykodawcy);
        if (addEmployeer !=null) {
            return new ResponseEntity<ProfilePraktykodawcowEntity>(addEmployeer, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<ProfilePraktykodawcowEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{idProfilu}/praktykodawca/{idPraktykodawcy}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykodawcyEntity>> getEmployeerOfProfile(@PathVariable int idProfilu, @PathVariable int idPraktykodawcy) {
        List list = iprofilService.getEmployeerOfProfile(idPraktykodawcy, idProfilu);
        if (list !=null) {
            return new ResponseEntity<List<PraktykodawcyEntity>>(list, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<List<PraktykodawcyEntity>>(HttpStatus.NOT_FOUND);
        }
    }

}
