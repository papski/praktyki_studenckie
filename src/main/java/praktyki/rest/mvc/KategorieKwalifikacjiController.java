package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.KategorieKwalifikacjiEntity;
import praktyki.core.service.Interface.iKategorieKwalifikacjiService;

import java.util.List;

/**
 * Created by dawid on 27.03.15.
 */

/**
 * QualificationCategoriesController
 * Unecessary
 */
@Controller
@RequestMapping(value = "/kategoriekwalifikacji")
public class KategorieKwalifikacjiController {

    private iKategorieKwalifikacjiService iKategorieKwalifikacjiService;

    @Autowired
    public KategorieKwalifikacjiController(iKategorieKwalifikacjiService iKategorieKwalifikacjiService) {
        this.iKategorieKwalifikacjiService = iKategorieKwalifikacjiService;
    }


    @RequestMapping(value = "/kategoria", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> GetAllCategories() {
        List list = iKategorieKwalifikacjiService.findCategory();
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/kategoria/{idKategoriiKwalifikacji}", method = RequestMethod.GET)
    public ResponseEntity<KategorieKwalifikacjiEntity> getCategorOrSubCategoryyById(@PathVariable int idKategoriiKwalifikacji) {
        KategorieKwalifikacjiEntity category = iKategorieKwalifikacjiService.getCategory(idKategoriiKwalifikacji);
        if (category !=null) {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(category, HttpStatus.OK);
        } else {
            return new ResponseEntity<KategorieKwalifikacjiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/kategoria/nazwa/{nazwaKategorii}", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getCategoryByName(@PathVariable String nazwaKategorii) {
        List list = iKategorieKwalifikacjiService.findCategoryByName(nazwaKategorii);
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/kategoria/{idKategoriiKwalifikacji}/subkategoria", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getSubCategoriesOfCategory(@PathVariable int idKategoriiKwalifikacji) {
        List list = iKategorieKwalifikacjiService.findSubCategoryOfCategory(idKategoriiKwalifikacji);
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/subkategoria/nazwa/{nazwaKategorii}", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getSubCategoriesByName(@PathVariable String nazwaKategorii) {
        List list = iKategorieKwalifikacjiService.findSubCategoryByName(nazwaKategorii);
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/subkategoria", method = RequestMethod.GET)
    public ResponseEntity<List<KategorieKwalifikacjiEntity>> getAllSubCategories() {
        List list = iKategorieKwalifikacjiService.findSubCategory();
        if (list !=null) {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<KategorieKwalifikacjiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

}
