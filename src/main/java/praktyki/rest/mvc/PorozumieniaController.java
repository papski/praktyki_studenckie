package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.PorozumieniaEntity;
import praktyki.core.service.Interface.iPorozumieniaService;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */

/**
 * AgreementsController<br>
 * Offers URL request for working with Agreements in DB.<br>
 * Agreement needs to be persisted along with Practice's Template
 * before Practice is being added into DB.
 * @see praktyki.rest.mvc.PraktykiController
 * @see praktyki.rest.mvc.SzablonyPraktykController
 */
@Controller
@RequestMapping("/porozumienia")
public class PorozumieniaController {

    private iPorozumieniaService iporozumieniaService;

    @Autowired
    public PorozumieniaController(iPorozumieniaService iporozumieniaService) {
        this.iporozumieniaService = iporozumieniaService;
    }

    /**
     * Adds Agreement into DB.
     * @see praktyki.core.entities.PorozumieniaEntity
     * @param porozumieniaEntity Agreement to be persisted
     * @return persisted Agreement
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PorozumieniaEntity> addAgreement(@RequestBody PorozumieniaEntity porozumieniaEntity) {
        PorozumieniaEntity addAgreement = iporozumieniaService.addAgreement(porozumieniaEntity);
        if (addAgreement !=null) {
            return new ResponseEntity<PorozumieniaEntity>(addAgreement, HttpStatus.OK);
        } else {
            return new ResponseEntity<PorozumieniaEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Agreement in DB.
     * @return list of every Agreement
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PorozumieniaEntity>> findAll() {
        List list = iporozumieniaService.getAll();
        if (list !=null) {
            return new ResponseEntity<List<PorozumieniaEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PorozumieniaEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets Agreement with given ID from DB.
     * @param idPorozumienia ID of Agreement
     * @return single Agreement
     */
    @RequestMapping(value = "/{idPorozumienia}", method = RequestMethod.GET)
    public ResponseEntity<PorozumieniaEntity> getAgreement(@PathVariable int idPorozumienia) {
        PorozumieniaEntity getAgreement = iporozumieniaService.getAgreement(idPorozumienia);
        if (getAgreement !=null) {
            return new ResponseEntity<PorozumieniaEntity>(getAgreement, HttpStatus.OK);
        } else {
            return new ResponseEntity<PorozumieniaEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Agreement with given ID.
     * @param idPorozumienia ID of Agreement
     * @return deleted Agreement
     */
    @RequestMapping(value = "/{idPorozumienia}", method = RequestMethod.DELETE)
    public ResponseEntity<PorozumieniaEntity> deleteAgreement(@PathVariable int idPorozumienia) {
        PorozumieniaEntity deleteAgreement = iporozumieniaService.deleteAgreements(idPorozumienia);
        if (deleteAgreement !=null) {
            return new ResponseEntity<PorozumieniaEntity>(deleteAgreement, HttpStatus.OK);
        } else {
            return new ResponseEntity<PorozumieniaEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Agreements of Employer.
     * @param idPraktykodawcy ID of Employer
     * @return list of Employer's Agreements
     */
    @RequestMapping(value = "/praktykodawca/{idPraktykodawcy}", method = RequestMethod.GET)
    public ResponseEntity<List<PorozumieniaEntity>> getAgreementsOfEmployer(@PathVariable int idPraktykodawcy) {
        List list = iporozumieniaService.getAgreementsOfEmployer(idPraktykodawcy);
        if (list !=null) {
            return new ResponseEntity<List<PorozumieniaEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PorozumieniaEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Agreements of Student
     * @param nrAlbumu (ID) Student's Number
     * @return list of Student's Agreements
     */
    @RequestMapping(value = "/student/{nrAlbumu}", method = RequestMethod.GET)
    public ResponseEntity<List<PorozumieniaEntity>> getAgreementsOfStudent(@PathVariable Long nrAlbumu) {
        List list = iporozumieniaService.getAgreementsOfStudent(nrAlbumu);
        if (list !=null) {
            return new ResponseEntity<List<PorozumieniaEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PorozumieniaEntity>>(HttpStatus.NOT_FOUND);
        }
    }

}
