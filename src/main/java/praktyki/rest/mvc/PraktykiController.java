package praktyki.rest.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktyki.core.entities.PraktykiEntity;
import praktyki.core.service.Interface.iPraktykiService;
import praktyki.rest.mvc.helpers.PraktykaInfo;

import java.util.List;

/**
 * Created by dawid on 14.03.15.
 */

/**
 * PracticesController<br>
 * Offers URL requests for working with Practices in DB<br>
 * When we want to add new Practice, its Template and Agreements
 * needs to be added 2 and theri IDs assigned to new Practice.
 */
@Controller
@RequestMapping("/praktyki")
public class PraktykiController {

    private iPraktykiService ipraktykiService;

    @Autowired
    public PraktykiController (iPraktykiService ipraktykiService) {
        this.ipraktykiService = ipraktykiService;
    }

    /**
     * Adds new Practice into DB. Before persisting new Practice
     * we need to persist Practice's Template and Agreement and
     * add theirs IDs into our new Practice to be persisted
     * (Requests one after another).
     * @see praktyki.rest.mvc.SzablonyPraktykController#addTemplate
     * @see praktyki.rest.mvc.PorozumieniaController#addAgreement
     * @see praktyki.core.entities.PraktykiEntity
     * @param praktykiEntity Practice to be persisted
     * @return persisted Practice
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PraktykiEntity> addPractice(@RequestBody PraktykiEntity praktykiEntity) {
        PraktykiEntity addRelation = ipraktykiService.addRelation(praktykiEntity);
        if (addRelation !=null) {
            return new ResponseEntity<PraktykiEntity>(addRelation, HttpStatus.OK);
        } else {
            return new ResponseEntity<PraktykiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds every Practice in DB
     * @return list of every Practice
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiEntity>> findAll() {
        List list = ipraktykiService.findAll();
        if (list !=null) {
            return new ResponseEntity<List<PraktykiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Free Practices (without Student assigned into it).
     * IDs needs to be provided in request to work.
     * by Field of Study and Academic Year.
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic year
     * @return list of searched Practices
     */
    @RequestMapping(value = "/brakzapisu/kierunek/{idKierunku}/rok/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiEntity>> getAvaliableCoursePractices (@PathVariable int idKierunku, @PathVariable int idRokuAkademickiego) {
        List list = ipraktykiService.getAvaliableCoursePractices(idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<PraktykiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Practices (with and without student) by Field of Study and Academic Year.
     * Each ID can be ignored by setting it to 0.
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return list of searched Practices
     */
    @RequestMapping(value = "/kierunek/{idKierunku}/rok/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiEntity>> getPracticeByCourseAcademicYear (@PathVariable int idKierunku, @PathVariable int idRokuAkademickiego) {
        List list = ipraktykiService.getPracticeByCourseAcademicYear(idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<PraktykiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Practices of Employer by Field of Study and Academic Year.
     * Each ID can be ignored my setting it to 0.
     * @param idPraktykodawcy ID of Employer
     * @param idKierunku ID of Course
     * @param idRokuAkademickiego ID of Academic Year
     * @return list of searched Practices
     */
    @RequestMapping(value = "/praktykodawca/{idPraktykodawcy}/kierunek/{idKierunku}/rok/{idRokuAkademickiego}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiEntity>> getPracticeByEmployerCourseAcademicYear (@PathVariable int idPraktykodawcy,@PathVariable int idKierunku, @PathVariable int idRokuAkademickiego) {
        List list = ipraktykiService.getPracticesOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
        if (list !=null) {
            return new ResponseEntity<List<PraktykiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Practices of Student
     * @param nrAlbumu (ID) Student's Number
     * @return list of Student's Practices
     */
    @RequestMapping(value = "/student/{nrAlbumu}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiEntity>> getPracticesOfStudent(@PathVariable Long nrAlbumu) {
        List list = ipraktykiService.getPracticesOfStudent(nrAlbumu);
        if (list !=null) {
            return new ResponseEntity<List<PraktykiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Finds Practices of Tutor
     * @param idOpiekunaPraktyk ID of Tutor
     * @return list of Tutor's Practices
     */
    @RequestMapping(value = "/opiekun/{idOpiekunaPraktyk}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiEntity>> getPracticeOfTutor(@PathVariable int idOpiekunaPraktyk) {
        List list = ipraktykiService.getPracticeOfTutor(idOpiekunaPraktyk);
        if (list !=null) {
            return new ResponseEntity<List<PraktykiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * To be deleted / unecessary
     * @param idKierunku
     * @return
     */
    @RequestMapping(value = "/kierunek/{idKierunku}", method = RequestMethod.GET)
    public ResponseEntity<List<PraktykiEntity>> getPracticeOfCourse(@PathVariable int idKierunku) {
        List list = ipraktykiService.getPracticeOfCourse(idKierunku);
        if (list !=null) {
            return new ResponseEntity<List<PraktykiEntity>>(list, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<PraktykiEntity>>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets info from multiple entites about Practice
     * @see praktyki.rest.mvc.helpers.PraktykaInfo
     * @param idPraktykiStudenckiej ID of Practice
     * @return helper class with info about Practice
     */
    @RequestMapping(value = "/{idPraktykiStudenckiej}", method = RequestMethod.GET)
    public ResponseEntity<PraktykaInfo> getPractice(@PathVariable Long idPraktykiStudenckiej) {
        PraktykaInfo getRow = ipraktykiService.getPracticeInfo(idPraktykiStudenckiej);
        if (getRow !=null) {
            return new ResponseEntity<PraktykaInfo>(getRow, HttpStatus.OK);
        } else {
            return new ResponseEntity<PraktykaInfo>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates Practice with given ID. We can add student and Agreement into
     * practice plus change Grade, URL to Practice's Referral
     * and Practice's Grade Chart.
     * @see praktyki.core.entities.PraktykiEntity
     * @param idPraktykiStudenckiej ID of Practice
     * @param praktykiEntity uddated Practice
     * @return updated Practice
     */
    @RequestMapping(value = "/{idPraktykiStudenckiej}", method = RequestMethod.PATCH)
    public ResponseEntity<PraktykiEntity> editPractice(@PathVariable Long idPraktykiStudenckiej, @RequestBody PraktykiEntity praktykiEntity) {
        PraktykiEntity editRow = ipraktykiService.editRow(idPraktykiStudenckiej, praktykiEntity);
        if (editRow !=null) {
            return new ResponseEntity<PraktykiEntity>(editRow, HttpStatus.OK);
        } else {
            return new ResponseEntity<PraktykiEntity>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes Practice with given ID
     * @param idPraktykiStudenckiej ID of Practice
     * @return number of delted rows
     */
    @RequestMapping(value = "/{idPraktykiStudenckiej}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> deletePractice(@PathVariable Long idPraktykiStudenckiej) {
        Long deleteRow = ipraktykiService.deleteRow(idPraktykiStudenckiej);
        if (deleteRow !=null) {
            return new ResponseEntity<Long>(deleteRow, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
        }
    }
}
