package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.ZgloszeniaPraktykodawcowEntity;
import praktyki.core.service.Interface.iZgloszeniaPraktykodawcowService;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid on 11.03.15.
 */

@Service
@Transactional
public class ZgloszeniaPraktykodawcowService implements iZgloszeniaPraktykodawcowService {

    @Autowired
    private iZgloszeniaPraktykodawcowDAO izgloszeniaPraktykodawcowDAO;

    @Autowired
    private iPraktykodawcyDAO iPraktykodawcyDAO;

    @Autowired
    private iStudenciDAO iStudenciDAO;

    @Autowired
    private iTypyZgloszenDAO iTypyZgloszenDAO;

    @Autowired
    private iKierunkiStudiowDAO iKierunkiStudiowDAO;

    @Autowired
    private iLataAkademickieDAO iLataAkademickieDAO;

    @Override
    public ZgloszeniaPraktykodawcowEntity addEmployerApplication(ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity) {
        List<ZgloszeniaPraktykodawcowEntity> list = new ArrayList<ZgloszeniaPraktykodawcowEntity>();
        list = izgloszeniaPraktykodawcowDAO.checkForStudent(zgloszeniaPraktykodawcowEntity);
        if(!list.isEmpty()) {
            throw new IllegalArgumentException("dany student na danym kierunku i roku jest juz przypisany do zgloszenia");
        } else {
            return izgloszeniaPraktykodawcowDAO.addEmployerApplication(zgloszeniaPraktykodawcowEntity);
        }
    }

    @Override
    public ZgloszeniaPraktykodawcowEntity editApplication(Long idZgloszeniaPraktykodawcy, ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity) {
        return izgloszeniaPraktykodawcowDAO.editApplication(idZgloszeniaPraktykodawcy, zgloszeniaPraktykodawcowEntity);
    }

    @Override
    public List<ZgloszeniaPraktykodawcowEntity> findAll() {
        return izgloszeniaPraktykodawcowDAO.findAll();
    }

    @Override
    public ZgloszenieInfo getApplication(Long idZgloszeniaPraktykodawcy) {
        ZgloszeniaPraktykodawcowEntity application = izgloszeniaPraktykodawcowDAO.getApplication(idZgloszeniaPraktykodawcy);
        if (application !=null) {
            ZgloszenieInfo info = new ZgloszenieInfo();
            info.setIdZgloszeniaPraktykodawcy(idZgloszeniaPraktykodawcy);
            info.setDataZgloszenia(application.getDataZgloszenia());
            info.setOswiadczeniePraktykodawcy(application.getOswiadczeniePraktykodawcy());
            info.setDataRozpoczecia(application.getDataRozpoczecia());
            info.setDataZakonczenia(application.getDataZakonczenia());
            info.setDecyzja(application.getDecyzja());
            info.setPraktykodawca(iPraktykodawcyDAO.findEmployeer(application.getIdPraktykodawcy()));
            info.setStudent(iStudenciDAO.getStudentInfo(application.getNrAlbumu()));
            info.setTypZgloszenia(iTypyZgloszenDAO.getRequest(application.getIdTypuZgloszenia()));
            info.setKierunek(iKierunkiStudiowDAO.getCourseInfo(application.getIdKierunku()));
            info.setRok(iLataAkademickieDAO.getYearOfCourse(application.getIdRokuAkademickiego()));
            return info;
        } else {
            return null;
        }
    }

    /*@Override
    public List<ZgloszeniaPraktykodawcowEntity> getApplication(Long idZgloszeniaPraktykodawcy) {
        return izgloszeniaPraktykodawcowDAO.getApplication(idZgloszeniaPraktykodawcy);
    }*/

    @Override
    public Long deleteApplication(Long idZgloszeniaPraktykodawcy) {
        return izgloszeniaPraktykodawcowDAO.deleteApplication(idZgloszeniaPraktykodawcy);
    }

    @Override
    public List getAllApplicationsOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        return izgloszeniaPraktykodawcowDAO.getApplicationsOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
    }
}
