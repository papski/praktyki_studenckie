package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iAdresyDAO;
import praktyki.core.DAO.Interface.iPraktykodawcyDAO;
import praktyki.core.DAO.Interface.iStudenciDAO;
import praktyki.core.entities.AdresyEntity;
import praktyki.core.entities.StudenciEntity;
import praktyki.core.service.Interface.iAdresyService;

import java.util.List;

/**
 * Created by dawid on 29.12.14.
 */

@Service
@Transactional
public class AdresyService implements iAdresyService {

    @Autowired
    private iAdresyDAO iadresyDAO;

    @Autowired
    private iStudenciDAO istudenciDAO;

    @Autowired
    private iPraktykodawcyDAO ipraktykodawcyDAO;

    @Override
    public AdresyEntity addAddress(AdresyEntity adresyEntity) {
        return iadresyDAO.addAddress(adresyEntity);
    }

    @Override
    public AdresyEntity updateAddress(AdresyEntity adresyEntity, int idAdresu) {
        return iadresyDAO.updateAddress(adresyEntity, idAdresu);
    }

    @Override
    public AdresyEntity deleteAddress(int idAdresu) {
        return iadresyDAO.deleteAddress(idAdresu);
    }

    @Override
    public List<AdresyEntity> findEveryAddress() {
        return iadresyDAO.findEveryAddress();
    }

    @Override
    public AdresyEntity findByIdAdresu(int idAdresu) {
        return iadresyDAO.findByIdAdresu(idAdresu);
    }

    @Override
    public StudenciEntity addStudent(int idAdresu, StudenciEntity studenciEntity) {
        AdresyEntity adresyEntity = iadresyDAO.findByIdAdresu(idAdresu);
        adresyEntity.setStudenciByIdAdresu(studenciEntity);
        if(adresyEntity == null) {
            return null;
        }
        studenciEntity = istudenciDAO.addStudent(studenciEntity);
        studenciEntity.setIdAdresu(idAdresu);
        studenciEntity.setAdresyByIdAdresu(adresyEntity);
        return studenciEntity;
    }

    @Override
    public List<StudenciEntity> findStudentByIdAdresu(Integer idAdresu) {
        AdresyEntity adresyEntity = iadresyDAO.findByIdAdresu(idAdresu);
        if(adresyEntity !=null) {
            List<StudenciEntity> list = istudenciDAO.findStudentByIdAdresu(idAdresu);
            return list;
        }
        else {
            return null;
        }
    }

    @Override
    public List<AdresyEntity> getAddressByStreetCityZipCode(String searchString, String sortBy, String sortType) {
        return iadresyDAO.getAddressByStreetCityZipCode(searchString, sortBy, sortType);
    }



    /*@Override
    public List<PraktykodawcyEntity> findEmployeerByIdAdresu(int idAdresu) {
        AdresyEntity adresyEntity = iadresyDAO.findByIdAdresu(idAdresu);
        if(adresyEntity !=null) {
            List<PraktykodawcyEntity> list = ipraktykodawcyDAO.findEmployeerByIdAdresu(idAdresu);
            return list;
        }
        else {
            return null;
        }
    }*/

}
