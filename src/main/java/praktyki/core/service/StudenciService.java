package praktyki.core.service;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.*;
import praktyki.core.exceptions.AlbumNumberUsedException;
import praktyki.core.exceptions.EmailUsedException;
import praktyki.core.exceptions.ExistenceException;
import praktyki.core.exceptions.LoginUsedException;
import praktyki.core.service.Interface.iStudenciService;
import praktyki.core.service.utilities.LoginService;
import praktyki.core.service.utilities.PasswordEncryptionService;
import praktyki.rest.mvc.helpers.LoginInfo;
import praktyki.rest.mvc.helpers.PraktykaInfo;
import praktyki.rest.mvc.helpers.StudentInfo;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid on 12.12.14.
 */

@Service
@Transactional
public class StudenciService implements iStudenciService {

    @Autowired
    private LoginService iloginService;

    @Autowired
    private PasswordEncryptionService passwordEncryptionService;

    @Autowired
    private iStudenciDAO istudenciDAO;

    @Autowired
    private iOsobyDAO iosobyDAO;

    @Autowired
    private iUzytkownicyDAO iuzytkownicyDAO;

    @Autowired
    private iAdresyDAO iadresyDAO;

    @Autowired
    private iKierunkiStudiowDAO ikierunkiStudiowDAO;

    @Autowired
    private iStudenciKierunkowDAO istudenciKierunkowDAO;

    @Autowired
    private iRocznikiStudiowDAO irocznikiStudiowDAO;

    @Autowired
    private iPraktykiDAO ipraktykiDAO;

    @Autowired
    private iSzablonyPraktykDAO iszablonyPraktykDAO;

    @Autowired
    private iPraktykodawcyDAO ipraktykodawcyDAO;

    @Autowired
    private iStatusyDAO istatusyDAO;

    @Autowired
    private iTypyPraktykDAO itypyPraktykDAO;

    @Autowired
    private iLataAkademickieDAO ilataAkademickieDAO;

    @Autowired
    private iOpiekunowiePraktykDAO iopiekunowiePraktykDAO;

    @Autowired
    private iKoordynatorzyPraktykDAO ikoordynatorzyPraktykDAO;

    @Autowired
    private iZgloszeniaPraktykodawcowDAO izgloszeniaPraktykodawcowDAO;

    @Autowired
    private iPorozumieniaDAO iporozumieniaDAO;

    @Autowired
    private iTypyZgloszenDAO itypyZgloszenDAO;

    @Override
    public Long checkNrAlbumu(Long nrAlbumu) {
        return istudenciDAO.checkNrAlbumu(nrAlbumu);
    }

    @Override
    public Long checkLogin(String login) {
        return iuzytkownicyDAO.checkLogin(login);
    }

    @Override
    public Long checkEmail(String email) {
        return iosobyDAO.checkEmail(email);
    }

    @Override
    public Long checkTelefonKomorkowy(String telefonKomorkowy) {
        return iosobyDAO.checkTelefonKomorkowy(telefonKomorkowy);
    }

    @Override
    public Long checkTelefonStacjonarny(String telefonStacjonarny) {
        return iosobyDAO.checkTelefonStacjonarny(telefonStacjonarny);
    }

    @Override
    public UzytkownicyEntity loginTEST(LoginInfo login) {
        return iloginService.authenticateUser(login);
    }

    @Override
    public UzytkownicyEntity passwordChangeTEST(LoginInfo login) {
        return iloginService.changeUserPassword(login);
    }

    @Override
    public StudenciEntity addStudent(StudenciEntity studenciEntity) {
        if(istudenciDAO.isAlbumNumberUsed(studenciEntity.getNrAlbumu()) == true) {
            throw new AlbumNumberUsedException("Album Number is already used, please contact with school staff");
        } else {
            return istudenciDAO.addStudent(studenciEntity);
        }
    }

    @Override
    public OsobyEntity addPerson(Long nrAlbumu, OsobyEntity osobyEntity) {
        if(iosobyDAO.isEmailUsed(osobyEntity.getEmail()) == true) {
            throw new EmailUsedException("Email is already used by someone");
        } else {
            StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
            if (student != null && student.getIdOsoby() == null) {
                OsobyEntity newPerson = iosobyDAO.addPerson(osobyEntity);
                newPerson.setStudent(student);
                Integer idOsoby = newPerson.getIdOsoby();
                student.setIdOsoby(idOsoby);
                student.setStudentByIdOsoby(newPerson);
                return newPerson;
            } else {
                throw new ExistenceException("There is no such student according to Album Number OR student already has his/her info persisted");
            }
        }
    }

    @Override
    public AdresyEntity addAddress(Long nrAlbumu, AdresyEntity adresyEntity) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null && student.getIdAdresu() == null) {
            AdresyEntity address = iadresyDAO.addAddress(adresyEntity);
            address.setStudenciByIdAdresu(student);
            Integer idAdresu = address.getIdAdresu();
            student.setIdAdresu(idAdresu);
            student.setAdresyByIdAdresu(address);
            return address;
        }
        else {
            throw new ExistenceException("There is no such student according to Album Number OR student already has his address persisted");
        }
    }

    @Override
    public AdresyEntity editAddressOfStudent(Long nrAlbumu, AdresyEntity adresyEntity) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null) {
            Integer idAdresu = student.getIdAdresu();
            return iadresyDAO.updateAddress(adresyEntity, idAdresu);
        }
        else {
            throw new ExistenceException("There is no such student according to Album Number");
        }
    }

    @Override
    public List<OsobyEntity> getInfoOfStudent(Long nrAlbumu) {
        return iosobyDAO.getInfoOfStudent(nrAlbumu);
    }

    @Override
    public StudentInfo getAllDataOfStudent(Long nrAlbumu) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null) {
            StudentInfo studentInfo = new StudentInfo();
            studentInfo.setAdresStudenta(iadresyDAO.getAddressOfStudent(nrAlbumu));
            studentInfo.setDaneStudenta(iosobyDAO.getInfoOfStudent(nrAlbumu));
            studentInfo.setLoginStudenta(iuzytkownicyDAO.getInfoOfStudent(student.getIdOsoby()));
            studentInfo.setKierunkiStudenta(ikierunkiStudiowDAO.findCourseOfStudent(nrAlbumu));
            studentInfo.setPorozumieniaStudenta(iporozumieniaDAO.getAgreementsOfStudent(nrAlbumu));
            return studentInfo;
        } else {
            throw new ExistenceException("There is no such student according to Album Number");
        }
    }

    @Override
    public List<PraktykaInfo> getPracticeOfStudent(Long nrAlbumu) {
        List<PraktykiEntity> list = ipraktykiDAO.getPracticeOfStudent(nrAlbumu);
        List<PraktykaInfo> info = new ArrayList<PraktykaInfo>();
        PraktykaInfo row = new PraktykaInfo();
        for(int i = 0; i<list.size(); i++) {
            PraktykiEntity practice = ipraktykiDAO.getRow(list.get(i).getIdPraktykiStudenckiej());
            if(practice !=null) {
                row.setIdPraktykiStudenckiej(practice.getIdPraktykiStudenckiej());
                row.setSzablonPraktyki(iszablonyPraktykDAO.getTemplate(practice.getIdSzablonu()));
                row.setPraktykodawca(ipraktykodawcyDAO.findEmployeer(practice.getIdPraktykodawcy()));
                row.setStatusPraktyki(istatusyDAO.getStatus(practice.getIdStatusu()));
                row.setKierunek(ikierunkiStudiowDAO.getCourseInfo(practice.getIdKierunku()));
                row.setTypPraktyki(itypyPraktykDAO.getTypeOfTraineeship(practice.getIdTypuPraktyki()));
                row.setRokAkademicki(ilataAkademickieDAO.getYearOfCourse(practice.getIdRokuAkademickiego()));
                if(practice.getNrAlbumu() !=null) {
                    row.setStudent(istudenciDAO.getStudentInfo(practice.getNrAlbumu()));
                }
                row.setOpiekunPraktyki(iopiekunowiePraktykDAO.getTutorInfo(practice.getIdOpiekunaPraktyk()));
                row.setKoordynatorPraktyki(ikoordynatorzyPraktykDAO.getCoordinatorInfo(practice.getIdKoordynatoraPraktyk()));
                row.setAdresPracodawcy(iadresyDAO.findByIdAdresu(practice.getIdAdresu()));
                if(practice.getIdPorozumienia() !=null) {
                    row.setPorozumienie(iporozumieniaDAO.getAgreement(practice.getIdPorozumienia()));
                }
                info.add(i, row);
            } else {
                throw new ExistenceException("Student doesn't have any current or past Practices");
            }
        }
        return info;
    }

    @Override
    public List<ZgloszenieInfo> getApplicationOfStudent(Long nrAlbumu) {
        List<ZgloszeniaPraktykodawcowEntity> list = izgloszeniaPraktykodawcowDAO.getApplicationOfStudent(nrAlbumu);
        List<ZgloszenieInfo> info = new ArrayList<ZgloszenieInfo>();
        ZgloszenieInfo row = new ZgloszenieInfo();
        for(int i = 0; i<list.size(); i++) {
            ZgloszeniaPraktykodawcowEntity application = izgloszeniaPraktykodawcowDAO.getApplication(list.get(i).getIdZgloszeniaPraktykodawcy());
            if (application !=null) {
                row.setIdZgloszeniaPraktykodawcy(application.getIdZgloszeniaPraktykodawcy());
                row.setDataZgloszenia(application.getDataZgloszenia());
                row.setOswiadczeniePraktykodawcy(application.getOswiadczeniePraktykodawcy());
                row.setDataRozpoczecia(application.getDataRozpoczecia());
                row.setDataZakonczenia(application.getDataZakonczenia());
                row.setDecyzja(application.getDecyzja());
                row.setPraktykodawca(ipraktykodawcyDAO.findEmployeer(application.getIdPraktykodawcy()));
                row.setStudent(istudenciDAO.getStudentInfo(application.getNrAlbumu()));
                row.setTypZgloszenia(itypyZgloszenDAO.getRequest(application.getIdTypuZgloszenia()));
                row.setKierunek(ikierunkiStudiowDAO.getCourseInfo(application.getIdKierunku()));
                row.setRok(ilataAkademickieDAO.getYearOfCourse(application.getIdRokuAkademickiego()));
                info.add(i, row);
            } else {
                throw new ExistenceException("Student is not included in any current or past Applications of Employer");
            }
        }
        return info;
    }

    @Override
    public UzytkownicyEntity addUser(UzytkownicyEntity uzytkownicyEntity, Long nrAlbumu) {
        if(iuzytkownicyDAO.isLoginUsed(uzytkownicyEntity.getLogin()) == true) {
            throw new LoginUsedException("Login is already used by someone");
        } else {
            StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
            if (student != null && iuzytkownicyDAO.checkForExistence(student.getIdOsoby()) == true) {
                try {
                    byte[] tempSalt = passwordEncryptionService.generateSalt();
                    String salt = new String(Base64.encode(tempSalt));
                    byte[] tempEncrypted = passwordEncryptionService.getEncryptedPassword(uzytkownicyEntity.getHaslo(), tempSalt);
                    String encryptedPassword = new String(Base64.encode(tempEncrypted));
                    uzytkownicyEntity.setHaslo(encryptedPassword);
                    uzytkownicyEntity.setSalt(salt);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                }
                return iuzytkownicyDAO.addUser(uzytkownicyEntity, student.getIdOsoby(), 2);
            } else {
                //throw new PersonExistenceException("Osoba juz przypisala login/haslo do siebie LUB dany student nie nie istnieje");
                throw new ExistenceException("Person already persisted his/her User info or person doesn't exist");
            }
        }
    }

    @Override
    public List<UzytkownicyEntity> getUserOfStudent(Long nrAlbumu) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null && student.getIdOsoby()!=null) {
            return iuzytkownicyDAO.getInfoOfStudent(student.getIdOsoby());
        } else {
            throw new ExistenceException("There is no User info of Student with that Album Number");
        }
    }

    @Override
    public OsobyEntity editInfoOfStudent(Long nrAlbumu, OsobyEntity osobyEntity) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null) {
            Integer idOsoby = student.getIdOsoby();
            return iosobyDAO.updatePerson(idOsoby, osobyEntity);
        } else {
            return null;
        }
    }


    @Override
    public StudenciEntity deleteStudent(Long NrAlbumu) {
        return istudenciDAO.deleteStudent(NrAlbumu);
    }

    @Override
    public StudenciKierunkowEntity addCourseToStudent(StudenciKierunkowEntity studenciKierunkowEntity) {
        if(istudenciKierunkowDAO.getRow(studenciKierunkowEntity.getNrAlbumu(), studenciKierunkowEntity.getIdKierunku(), studenciKierunkowEntity.getIdRokuAkademickiego()) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie bazie");
        } else {
            Long nrAlbumu = studenciKierunkowEntity.getNrAlbumu();
            RocznikiStudiowEntity course = irocznikiStudiowDAO.getCourse(studenciKierunkowEntity.getIdKierunku(), studenciKierunkowEntity.getIdRokuAkademickiego());
            return istudenciKierunkowDAO.addRelation(nrAlbumu, course);
        }
    }


    @Override
    public Long deleteStudentFromCourse(Long nrAlbumu, int idKierunku) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null) {
            KierunkiStudiowEntity kierunek = ikierunkiStudiowDAO.getCourse(idKierunku);
            if (kierunek !=null) {
                return istudenciKierunkowDAO.deleteStudentFromCourse(nrAlbumu, idKierunku);
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }


    @Override
    public List<StudenciEntity> findEveryStudent() {
        return istudenciDAO.findEveryStudent();
    }

    @Override
    public StudenciEntity findByNrAlbumu(Long nrAlbumu) {
        return istudenciDAO.findByNrAlbumu(nrAlbumu);
    }

    @Override
    public List<AdresyEntity> getAddressOfStudent(Long nrAlbumu) {
        return iadresyDAO.getAddressOfStudent(nrAlbumu);
    }

    @Override
    public List<KierunkiStudiowEntity> findCourseOfStudent(Long nrAlbumu) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null) {
            List<KierunkiStudiowEntity> list = ikierunkiStudiowDAO.findCourseOfStudent(nrAlbumu);
            return list;
        }
        else {
            return null;
        }
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseOfStudent(Long nrAlbumu, int idKierunku) {
        StudenciEntity student = istudenciDAO.findByNrAlbumu(nrAlbumu);
        if (student !=null) {
            KierunkiStudiowEntity kierunek = ikierunkiStudiowDAO.getCourse(idKierunku);
            if (kierunek !=null) {
                return ikierunkiStudiowDAO.getCourseOfStudent(nrAlbumu, idKierunku);
            }
        }
        return null;
    }

}