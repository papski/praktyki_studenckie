package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iOsobyDAO;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.service.Interface.iOsobyService;

import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */
@Service
@Transactional
public class OsobyService implements iOsobyService {

    @Autowired
    private iOsobyDAO iosobyDAO;


    @Override
    public OsobyEntity addPerson(OsobyEntity osobyEntity) {
        return iosobyDAO.addPerson(osobyEntity);
    }

    @Override
    public OsobyEntity updatePerson(Integer idOsoby, OsobyEntity osobyEntity) {
        return iosobyDAO.updatePerson(idOsoby, osobyEntity);
    }

    @Override
    public List<OsobyEntity> findAll() {
        return iosobyDAO.findAll();
    }

    @Override
    public OsobyEntity getPersonByIdOsoby(Integer idOsoby) {
        return iosobyDAO.getPersonByIdOsoby(idOsoby);
    }

    @Override
    public OsobyEntity deletePerson(Integer idOsoby) {
        return iosobyDAO.deletePerson(idOsoby);
    }

    @Override
    public Long checkEmail(String email) {
        return iosobyDAO.checkEmail(email);
    }

    @Override
    public Long checkTelefonKomorkowy(String telefonKomorkowy) {
        return iosobyDAO.checkTelefonKomorkowy(telefonKomorkowy);
    }

    @Override
    public Long checkTelefonStacjonarny(String telefonStacjonarny) {
        return iosobyDAO.checkTelefonStacjonarny(telefonStacjonarny);
    }

    @Override
    public List<OsobyEntity> getUserByNameSurname(String searchString, String sortBy, String sortType) {
        return iosobyDAO.getUserByNameSurname(searchString, sortBy, sortType);
    }
}
