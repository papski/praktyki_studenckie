package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import praktyki.core.DAO.Interface.iRocznikiStudiowDAO;
import praktyki.core.entities.RocznikiStudiowEntity;
import praktyki.core.service.Interface.iRocznikiStudiowService;

/**
 * Created by dawid on 17.02.15.
 */
public class RocznikiStudiowService implements iRocznikiStudiowService {

    @Autowired
    private iRocznikiStudiowDAO irocznikiStudiowDAO;

    @Override
    public RocznikiStudiowEntity addRelation(Integer idKierunku, Integer idRokuAkademickiego) {
        RocznikiStudiowEntity relation = new RocznikiStudiowEntity();
        relation.setIdKierunku(idKierunku);
        relation.setIdRokuAkademickiego(idRokuAkademickiego);
        return irocznikiStudiowDAO.addRelation(relation);
    }

    @Override
    public Long deleteYearsFromCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        return irocznikiStudiowDAO.deleteYearsFromCourse(idKierunku, idRokuAkademickiego);
    }

    @Override
    public Long deleteEveryYearFromCourse(Integer idKierunku) {
        return irocznikiStudiowDAO.deleteEveryYearFromCourse(idKierunku);
    }
}
