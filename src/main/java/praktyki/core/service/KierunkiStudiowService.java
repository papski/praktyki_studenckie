package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.*;
import praktyki.core.service.Interface.iKierunkiStudiowService;
import praktyki.rest.mvc.helpers.KierunekDodatkoweInfo;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
@Service
@Transactional
public class KierunkiStudiowService implements iKierunkiStudiowService {

    @Autowired
    private iKierunkiStudiowDAO ikierunkiStudiowDAO;

    @Autowired
    private iTrybyStudiowDAO itrybyStudiowDAO;

    @Autowired
    private iStopnieStudiowDAO istopnieStudiowDAO;

    @Autowired
    private iTytulyZawodoweDAO itytulyZawodoweDAO;

    @Autowired
    private iRocznikiStudiowDAO irocznikiStudiowDAO;

    @Autowired
    private iLataAkademickieDAO ilataAkademickieDAO;

    @Autowired
    private iStudenciDAO istudenciDAO;

    @Autowired
    private iStudenciKierunkowDAO istudenciKierunkowDAO;

    @Autowired
    private iKoordynatorzyPraktykDAO ikoordynatorzyPraktykDAO;

    @Autowired
    private iKoordynatorzyKierunkowiDAO ikoordynatorzyKierunkowiDAO;

    @Override
    public KierunkiStudiowEntity addCourse(KierunkiStudiowEntity kierunkiStudiowEntity, int idTytuluZawodowego, int idStopniaStudiow, int idTrybuStudiow) {
        return ikierunkiStudiowDAO.addCourse(kierunkiStudiowEntity, idTytuluZawodowego, idStopniaStudiow, idTrybuStudiow);
    }

    @Override
    public KierunkiStudiowEntity updateCourse(KierunkiStudiowEntity kierunkiStudiowEntity, int idKierunku) {
        return ikierunkiStudiowDAO.updateCourse(kierunkiStudiowEntity, idKierunku);
    }

    @Override
    public KierunkiStudiowEntity deleteCourse(int idKierunku) {
        return ikierunkiStudiowDAO.deleteCourse(idKierunku);
    }

    @Override
    public Long deleteStudentFromCourse(Long nrAlbumu, int idKierunku) {
        return istudenciKierunkowDAO.deleteStudentFromCourse(nrAlbumu, idKierunku);
    }

    @Override
    public Long deleteEveryStudentFromCourse(int idKierunku) {
        return istudenciKierunkowDAO.deleteEveryStudentFromCourse(idKierunku);
    }

    @Override
    public KierunkiStudiowEntity getCourse(int idKierunku) {
        return ikierunkiStudiowDAO.getCourse(idKierunku);
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseInfo(int idKierunku) {
        return ikierunkiStudiowDAO.getCourseInfo(idKierunku);
    }

    @Override
    public KierunekDodatkoweInfo getTypeDegreeTitle(int idKierunku) {
        KierunkiStudiowEntity course = ikierunkiStudiowDAO.getCourse(idKierunku);
        if (course !=null) {
            KierunekDodatkoweInfo info = new KierunekDodatkoweInfo();
            info.setTrybStudiow(itrybyStudiowDAO.getType(course.getIdTrybuStudiow()).getTrybStudiow());
            //System.out.println(itrybyStudiowDAO.getType(idKierunku).getTrybStudiow());
            info.setStopienStudiow(istopnieStudiowDAO.getDegree(course.getIdStopniaStudiow()).getStopienStudiow());
            //System.out.println(istopnieStudiowDAO.getDegree(idKierunku).getStopienStudiow());
            info.setTytulZawodowy(itytulyZawodoweDAO.getTitle(course.getIdTytuluZawodowego()).getTytulZawodowy());
            //System.out.println(itytulyZawodoweDAO.getTitle(idKierunku).getTytulZawodowy());
            return info;
        } else {
            return null;
        }
    }


    @Override
    public RocznikiStudiowEntity addAcademicYearToCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        RocznikiStudiowEntity relation = new RocznikiStudiowEntity();
        relation.setIdKierunku(idKierunku);
        relation.setIdRokuAkademickiego(idRokuAkademickiego);
        if(irocznikiStudiowDAO.getRow(idKierunku, idRokuAkademickiego) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie w bazie");
        } else {
            return irocznikiStudiowDAO.addRelation(relation);
        }
    }

    @Override
    public List<LataAkademickieEntity> findEveryYearOfCourse(Integer idKierunku) {
        return ilataAkademickieDAO.findEveryYearOfCourse(idKierunku);
    }

    @Override
    public List<LataAkademickieEntity> getYearOfCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        return ilataAkademickieDAO.getYearOfCourse(idKierunku, idRokuAkademickiego);
    }

    @Override
    public Long deleteYearFromCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        return irocznikiStudiowDAO.deleteYearsFromCourse(idKierunku, idRokuAkademickiego);
    }

    @Override
    public Long deleteYearsFromCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        return irocznikiStudiowDAO.deleteYearsFromCourse(idKierunku, idRokuAkademickiego);
    }

    @Override
    public Long deleteEveryYearFromCourse(Integer idKierunku) {
        return irocznikiStudiowDAO.deleteEveryYearFromCourse(idKierunku);
    }

    @Override
    public List<KierunkiStudiowEntity> getAllCourses() {
        return ikierunkiStudiowDAO.getAllCourses();
    }

    /*@Override
    public StudenciKierunkowEntity addStudentToCouse(int idKierunku, Long nrAlbumu) {
        StudenciKierunkowEntity studenciKierunkowEntity = new StudenciKierunkowEntity();
        studenciKierunkowEntity.setIdKierunku(idKierunku);
        studenciKierunkowEntity.setNrAlbumu(nrAlbumu);
        return istudenciKierunkowDAO.addRelation(studenciKierunkowEntity);
    }*/

    @Override
    public List<StudenciEntity> findStudentsOfCourse(int idKierunku) {
        KierunkiStudiowEntity kierunek = ikierunkiStudiowDAO.getCourse(idKierunku);
        if(kierunek !=null) {
            List<StudenciEntity> list = istudenciDAO.findStudentsOfCourse(idKierunku);
            return list;
        }
        else {
            return null;
        }
    }

    @Override
    public List<StudenciEntity> getStudentOfCourse(Long nrAlbumu, int idKierunku) {
        return istudenciDAO.getStudentOfCourse(nrAlbumu, idKierunku);
    }

    @Override
    public List<KoordynatorzyPraktykEntity> findCoordinatorOfCourse(int idKierunku) {
        KierunkiStudiowEntity course = ikierunkiStudiowDAO.getCourse(idKierunku);
        if (course !=null) {
            return ikoordynatorzyPraktykDAO.findCoordinatorOfCourse(idKierunku);
        }
        else {
            return null;
        }
    }

    @Override
    public List<KoordynatorzyPraktykEntity> getCoordinatorOfCourse(int idKierunku, int idKoordynatoraPraktyk) {
        KierunkiStudiowEntity course = ikierunkiStudiowDAO.getCourse(idKierunku);
        if (course !=null) {
            KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
            if (coordinator !=null) {
                return ikoordynatorzyPraktykDAO.getCoordinatorOfCourse(idKierunku, idKoordynatoraPraktyk);
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    @Override
    public KoordynatorzyKierunkowiEntity addCoordinatorToCourse(int idKierunku, int idKoordynatoraPraktyk) {
        if(ikoordynatorzyKierunkowiDAO.getRow(idKoordynatoraPraktyk, idKierunku) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie w bazie");
        } else {
            KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity = new KoordynatorzyKierunkowiEntity();
            koordynatorzyKierunkowiEntity.setIdKierunku(idKierunku);
            koordynatorzyKierunkowiEntity.setIdKoordynatoraPraktyk(idKoordynatoraPraktyk);
            return ikoordynatorzyKierunkowiDAO.addRelation(koordynatorzyKierunkowiEntity);
        }
    }

    @Override
    public Long deleteCoordinatorFromCourse(int idKierunku, int idKoordynatoraPraktky) {
        return ikoordynatorzyKierunkowiDAO.deleteCoordinatorFromCourse(idKoordynatoraPraktky, idKierunku);
    }

    @Override
    public Long deleteEveryCoordinatorFromCourse(int idKierunku) {
        return ikoordynatorzyKierunkowiDAO.deleteEveryCoordinatorFromCourse(idKierunku);
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseByNameSpeciality(String searchString, String sortBy, String sortType) {
        return ikierunkiStudiowDAO.getCourseByNameSpeciality(searchString, sortBy, sortType);
    }

}
