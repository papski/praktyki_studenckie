package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iTrybyStudiowDAO;
import praktyki.core.entities.TrybyStudiowEntity;
import praktyki.core.service.Interface.iTrybyStudiowService;

import java.util.List;

/**
 * Created by dawid on 14.02.15.
 */

@Service
@Transactional
public class TrybyStudiowService implements iTrybyStudiowService {

    @Autowired
    private iTrybyStudiowDAO itrybyStudiowDAO;

    @Override
    public List<TrybyStudiowEntity> findAll() {
        return itrybyStudiowDAO.findAll();
    }
}
