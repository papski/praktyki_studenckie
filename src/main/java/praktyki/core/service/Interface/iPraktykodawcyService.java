package praktyki.core.service.Interface;

import praktyki.core.entities.*;
import praktyki.rest.mvc.helpers.PraktykaInfo;
import praktyki.rest.mvc.helpers.PraktykodawcaInfo;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.List;

/**
 * Created by dawid on 06.01.15.
 */
public interface iPraktykodawcyService {

    public Boolean checkNazwa(String nazwa);

    public Long checkBranch(int idPraktykodawcy, int idAdresu);

    public PraktykodawcyEntity addEmployeer(PraktykodawcyEntity praktykodawcyEntity);

    public PraktykodawcyEntity updateEmployeer(PraktykodawcyEntity praktykodawcyEntity, int idPraktykodawcy);

    public PraktykodawcyEntity editTrustedStatus(int idPraktykodawcy, Boolean zaufany);

    public PraktykodawcyEntity deleteEmployeer(int idPraktykodawcy);

    public PraktykodawcyEntity findEmployeer(int idPraktykodawcy);

    public PraktykodawcaInfo getAllDataOfEmployeer(int idPraktykodawcy);

    public List<PraktykaInfo> getAllPracticesOfEmployer(int idPraktykodawcy,int idKierunku, int idRokuAkademickiego);

    public List<PraktykaInfo> getAllPracticesOfEmployerByTutor(int idPraktykodawcy, int idOpiekunaPraktyk);

    public List<ZgloszenieInfo> getAllApplicationsOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego);

    public List<PraktykodawcyEntity> findEveryEmployeer();

    public AdresyEntity addAddress(int idPraktykodawcy, Boolean oddzial, AdresyEntity adresyEntity);

    public List<AdresyEntity> findAddresses(int idPraktykodawcy);

    public List<AdresyEntity> getAddress(int idPraktykodawcy, int idAdresu);

    public Long deleteEveryAddressOfEmployer(int idPraktykodawcy);

    public Long deleteAddressOfEmployer(int idPraktykodawcy, int idAdresu);

    public ProfilEntity addProfile(int idPraktykodawcy, ProfilEntity profilEntity);

    public ProfilEntity updateProfile(int idPraktykodawcy, Integer idProfilu, ProfilEntity profilEntity);

    public List<ProfilEntity> findProfilesOfEmployeer(int idPraktykodawcy);

    public List<ProfilEntity> getProfileOfEmployeer(Integer idProfilu, int idPraktykodawcy);

    public Long deleteEveryProfileOfEmployeer(int idPraktykodawcy);

    public Long deleteProfileOfEmployeer(int idPraktykodawcy, int idProfilu);

    public List<KoordynatorzyPraktykEntity> findEveryCoordinatorOfEmployer(int idPraktykodawcy);

    public List<OsobyEntity> getCoordinatorOfEmployer(int idPraktykodawcy, int idKoordynatoraPraktyk);

    public OpiekunowieKierunkowiEntity addTutorCourse(OpiekunowieKierunkowiEntity opiekunowieKierunkowiEntity);

    public List<OpiekunowiePraktykEntity> getAllTutorsOfEmployee(int idPraktykodawcy);

    public List<OpiekunowiePraktykEntity> getTutorOfEmployer(int idPraktykodawcy, int idOpiekunaPraktyk);

    public Long deleteTutorOfEmployer(int idPraktykodawcy, int idOpiekunaPraktyk);

    public List<PraktykodawcyEntity> getEmployerByName(String searchString, String sortType);

    public List<StudenciEntity> getStudentsOfEmployerByCourseAcademicYear(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego);
}
