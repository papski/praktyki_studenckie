package praktyki.core.service.Interface;

import praktyki.core.entities.PraktykiKierunkoweEntity;
import praktyki.rest.mvc.helpers.PraktykiKierunkoweInfo;

import java.util.List;

/**
 * Created by dawid on 08.03.15.
 */
public interface iPraktykiKierunkoweService {

    public PraktykiKierunkoweEntity addRelation(PraktykiKierunkoweEntity praktykiKierunkoweEntity);

    public List<PraktykiKierunkoweEntity> findAll();

    public List<PraktykiKierunkoweInfo> getRelation(int idKierunku, int idRokuAkademickiego, int idTypuPraktyki);

    public PraktykiKierunkoweEntity editRelation(int idKierunku, int idTypuPraktyki, int idRokuAkademickiego, PraktykiKierunkoweEntity praktykiKierunkoweEntity);

    public Long deleteRelation(int idKierunku, int idRokuAkademickiego, int idTypyPraktyki);

}
