package praktyki.core.service.Interface;

import praktyki.core.entities.*;
import praktyki.rest.mvc.helpers.KierunekDodatkoweInfo;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
public interface iKierunkiStudiowService {

    public KierunkiStudiowEntity addCourse (KierunkiStudiowEntity kierunkiStudiowEntity, int idTytuluZawodowego, int idStopniaStudiow, int idTrybuStudiow);

    public KierunkiStudiowEntity updateCourse (KierunkiStudiowEntity kierunkiStudiowEntity, int idKierunku);

    public KierunkiStudiowEntity deleteCourse (int idKierunku);

    public KierunkiStudiowEntity getCourse (int idKierunku);

    public List<KierunkiStudiowEntity> getCourseInfo (int idKierunku);

    public KierunekDodatkoweInfo getTypeDegreeTitle (int idKierunku);

    public RocznikiStudiowEntity addAcademicYearToCourse(Integer idKierunku, Integer idRokuAkademickiego);

    public List<LataAkademickieEntity> findEveryYearOfCourse(Integer idKierunku);

    public List<LataAkademickieEntity> getYearOfCourse(Integer idKierunku, Integer idRokuAkademickiego);

    public Long deleteYearFromCourse(Integer idKierunku, Integer idRokuAkademickiego);

    public Long deleteYearsFromCourse(Integer idKierunku, Integer idRokuAkademickiego);

    public Long deleteEveryYearFromCourse(Integer idKierunku);

    public List<KierunkiStudiowEntity> getAllCourses ();

    //public StudenciKierunkowEntity addStudentToCouse(int idKierunku, Long nrAlbumu);

    public List<StudenciEntity> findStudentsOfCourse(int idKierunku);

    public List<StudenciEntity> getStudentOfCourse(Long nrAlbumu, int idKierunku);

    //public StudenciKierunkowEntity addStudentToCourse(StudenciKierunkowEntity studenciKierunkowEntity);

    public Long deleteStudentFromCourse(Long nrAlbumu, int idKierunku);

    public Long deleteEveryStudentFromCourse(int idKierunku);

    public List<KoordynatorzyPraktykEntity> findCoordinatorOfCourse(int idKierunku);

    public List<KoordynatorzyPraktykEntity> getCoordinatorOfCourse(int idKierunku, int idKoordynatoraPraktyk);

    public KoordynatorzyKierunkowiEntity addCoordinatorToCourse(int idKierunku, int idKoordynatoraPraktyk);

    public Long deleteCoordinatorFromCourse(int idKierunku, int idKoordynatoraPraktky);

    public Long deleteEveryCoordinatorFromCourse(int idKierunku);

    public List<KierunkiStudiowEntity> getCourseByNameSpeciality(String searchString, String sortBy, String sortType);
}
