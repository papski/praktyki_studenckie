package praktyki.core.service.Interface;

import praktyki.core.entities.OpiekunowiePraktykEntity;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.entities.UzytkownicyEntity;

import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */
public interface iOpiekunowiePraktykService {

    public OpiekunowiePraktykEntity addTutor (OpiekunowiePraktykEntity opiekunowiePraktykEntity);

    public OpiekunowiePraktykEntity updateTutor (Integer idOpiekunaPraktyk, OpiekunowiePraktykEntity opiekunowiePraktykEntity);

    public OpiekunowiePraktykEntity deleteTutor (Integer idOpiekunaPraktyk);

    public List<OpiekunowiePraktykEntity> findAll();

    public OpiekunowiePraktykEntity getTutor(Integer idOpiekunaPraktyk);

    public OsobyEntity addPerson (Integer idOpiekunaPraktyk, OsobyEntity osobyEntity);

    public OsobyEntity updatePerson (Integer idOpiekunaPraktyk, OsobyEntity osobyEntity);

    public List<OsobyEntity> getInfoOfTutor(Integer idOpiekunaPraktyk);

    public UzytkownicyEntity addUser(Integer idOpiekunaPraktyk, UzytkownicyEntity uzytkownicyEntity);

    public List<UzytkownicyEntity> getUser(Integer idOpiekunaPraktyk);

    public Long checkEmail(String email);

    public Long checkLogin(String login);
}
