package praktyki.core.service.Interface;

import praktyki.core.entities.PraktykiEntity;
import praktyki.rest.mvc.helpers.PraktykaInfo;

import java.util.List;

/**
 * Created by dawid on 14.03.15.
 */
public interface iPraktykiService {

    public PraktykiEntity addRelation(PraktykiEntity praktykiEntity);

    public List<PraktykiEntity> findAll();

    public List<PraktykiEntity> getAvaliableCoursePractices(int idKierunku, int idRokuAkademickiego);

    public List<PraktykiEntity> getPracticeByCourseAcademicYear(int idKierunku, int idRokuAkademickiego);

    public PraktykiEntity getRow(Long idPraktykiStudenckiej);

    public PraktykaInfo getPracticeInfo(Long idPraktykiStudenckiej);

    public PraktykiEntity editRow(Long idPraktykiStudenckiej, PraktykiEntity praktykiEntity);

    public Long deleteRow(Long idPraktykiStudenckiej);

    public List getPracticesOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego);

    public List getPracticesOfStudent(Long nrAlbumu);

    public List getPracticeOfTutor(int idOpiekunaPraktyk);

    public List getPracticeOfCourse(int idKierunku);
}
