package praktyki.core.service.Interface;

import praktyki.core.entities.UzytkownicyEntity;

import java.util.List;

/**
 * Created by dawid on 25.03.15.
 */

public interface iUzytkownicyService {

    public UzytkownicyEntity editBlockade(int idUzytkownika, Boolean blokada);

    public UzytkownicyEntity getUser(int idUzytkownika);

    public List<UzytkownicyEntity> findAll();

    public List<UzytkownicyEntity> getUserByLogin(String searchString, String sortType);
}
