package praktyki.core.service.Interface;

import praktyki.core.entities.KoordynatorzyKierunkowiEntity;

import java.util.List;

/**
 * Created by dawid on 22.01.15.
 */
public interface iKoordynatorzyKierunkowiService {

    public KoordynatorzyKierunkowiEntity addRelation(KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity);

    public List<KoordynatorzyKierunkowiEntity> findAll();

}
