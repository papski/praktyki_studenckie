package praktyki.core.service.Interface;

import praktyki.core.entities.*;
import praktyki.rest.mvc.helpers.LoginInfo;
import praktyki.rest.mvc.helpers.PraktykaInfo;
import praktyki.rest.mvc.helpers.StudentInfo;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.List;

/**
 * Created by dawid on 12.12.14.
 */
public interface iStudenciService {

    public Long checkNrAlbumu(Long nrAlbumu);

    //public Long isLoginUsed(String login);

    public Long checkLogin(String login);

    public Long checkEmail(String email);

    public Long checkTelefonKomorkowy(String telefonKomorkowy);

    public Long checkTelefonStacjonarny(String telefonStacjonarny);

    public UzytkownicyEntity loginTEST(LoginInfo login);

    public UzytkownicyEntity passwordChangeTEST(LoginInfo login);

    public StudenciEntity addStudent(StudenciEntity studenciEntity);

    public OsobyEntity addPerson(Long nrAlbumu, OsobyEntity osobyEntity);

    public OsobyEntity editInfoOfStudent(Long nrAlbumu, OsobyEntity osobyEntity);

    public List<OsobyEntity> getInfoOfStudent(Long nrAlbumu);

    public StudentInfo getAllDataOfStudent(Long nrAlbumu);

    public List<PraktykaInfo> getPracticeOfStudent(Long nrAlbumu);

    public List<ZgloszenieInfo> getApplicationOfStudent(Long nrAlbumu);

    public UzytkownicyEntity addUser(UzytkownicyEntity uzytkownicyEntity, Long nrAlbumu);

    public List<UzytkownicyEntity> getUserOfStudent(Long nrAlbumu);

    public AdresyEntity addAddress(Long nrAlbumu, AdresyEntity adresyEntity);

    public AdresyEntity editAddressOfStudent(Long nrAlbumu, AdresyEntity adresyEntity);

    public List<AdresyEntity> getAddressOfStudent(Long nrAlbumu);

    public StudenciEntity deleteStudent(Long NrAlbumu);

    public StudenciKierunkowEntity addCourseToStudent(StudenciKierunkowEntity studenciKierunkowEntity);

    public Long deleteStudentFromCourse(Long nrAlbumu, int idKierunku);

    public List<StudenciEntity> findEveryStudent();

    public StudenciEntity findByNrAlbumu(Long nrAlbumu);

    public List<KierunkiStudiowEntity> findCourseOfStudent(Long nrAlbumu);

    public List<KierunkiStudiowEntity> getCourseOfStudent(Long nrAlbumu, int idKierunku);

}