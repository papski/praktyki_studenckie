package praktyki.core.service.Interface;

import praktyki.core.entities.TrybyStudiowEntity;

import java.util.List;

/**
 * Created by dawid on 14.02.15.
 */
public interface iTrybyStudiowService {

    public List<TrybyStudiowEntity> findAll();

}
