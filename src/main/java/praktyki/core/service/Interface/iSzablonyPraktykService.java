package praktyki.core.service.Interface;

import praktyki.core.entities.*;
import praktyki.rest.mvc.helpers.CategoryInfo;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */
public interface iSzablonyPraktykService {

    public SzablonyPraktykEntity addTemplate(SzablonyPraktykEntity szablonyPraktykEntity);

    public SzablonyPraktykEntity getTemplate(int idSzablonu);

    public SzablonyPraktykEntity deleteTemplate(int idSzablonu);

    public List<SzablonyPraktykEntity> findAll();

    public WymaganeKwalifikacjeEntity addRequiredQualification(int idSzablonu, KwalifikacjeEntity kwalifikacjeEntity);

    public List<KwalifikacjeEntity> findEveryRequiredQualifcation(int idSzablonu);

    public List<KwalifikacjeEntity> getRequiredQualification(int idSzablonu, int idKwalifikacji);

    public OferowaneKwalifikacjeEntity addOfferedQualification(int idSzablonu, KwalifikacjeEntity kwalifikacjeEntity);

    public List<KwalifikacjeEntity> findEveryOfferedQualification(int idSzablonu);

    public List<KwalifikacjeEntity> getOfferedQualification(int idSzablonu, int idKwalifikacji);

    public KategorieKwalifikacjiEntity addCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public KategorieKwalifikacjiEntity addSubCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public List<KategorieKwalifikacjiEntity> findCategoryByName(String nazwaKategorii);

    public List<KategorieKwalifikacjiEntity> findCategory();

    public KategorieKwalifikacjiEntity getCategory(int idKategoriiKwalifikacji);

    public List<KategorieKwalifikacjiEntity> findSubCategoryByName (String nazwaKategorii);

    public List<KategorieKwalifikacjiEntity> findSubCategory();

    public List<KategorieKwalifikacjiEntity> findSubCategoryOfCategory(int idKategoriiKwalifikacji);

    public KategorieKwalifikacjiEntity getSubCategoryOfCategory(int idKategoriiKwalifikacji);

    public CategoryInfo getInfoOfCategory(int idKategoriiKwalifikacji);

    public List<KwalifikacjeEntity> getQualificationsOfTemplate(int idSzablonu);

    public KategorieKwalifikacjiEntity addCategoryOfRequiredQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public KategorieKwalifikacjiEntity addSubCategoryOfRequiredQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public KategorieKwalifikacjiEntity addCategoryOfOfferdQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public KategorieKwalifikacjiEntity addSubCategoryOfOfferedQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public Long deleteCategory(int idKategoriiKwalifikacji);

    public Long deleteSubCategory(int idKategoriiKwalifikacji);
}
