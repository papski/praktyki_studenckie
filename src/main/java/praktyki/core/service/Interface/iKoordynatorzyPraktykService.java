package praktyki.core.service.Interface;

import praktyki.core.entities.*;
import praktyki.rest.mvc.helpers.KoordynatorInfo;
import praktyki.rest.mvc.helpers.KoordynatorzyDate;
import praktyki.rest.mvc.helpers.PraktykaInfo;

import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */
public interface iKoordynatorzyPraktykService {

    public KoordynatorzyPraktykEntity addCoordinator(KoordynatorzyPraktykEntity koordynatorzyPraktykEntity);

    public KoordynatorzyPraktykEntity deleteCoordinator(int idKoordynatoraPraktyk);

    public List<KoordynatorzyPraktykEntity> findAll();

    public KoordynatorzyPraktykEntity getCoordinator(int idKoordynatoraPraktyk);

    public KoordynatorInfo getAllDataOfCoordinator(int idKoordynatoraPraktyk);

    public List<PraktykaInfo> getAllPracticesOfCoordinator(int idKoordynatoraPraktyk);

    public OsobyEntity addPerson(int idKoordynatoraPraktyk, OsobyEntity osobyEntity);

    public List<OsobyEntity> getInfoOfCoordinator(Integer idOsoby, int idKoordynatoraPraktyk);

    public OsobyEntity editInfoOfCoordinator(Integer idOsoby, int idKoordynatoraPraktyk, OsobyEntity osobyEntity);

    public UzytkownicyEntity addUser(int idKoordynatoraPraktyk, UzytkownicyEntity uzytkownicyEntity);

    public List<UzytkownicyEntity> getUser(int idKoordynatoraPraktyk);

    public KoordynatorzyKierunkowiEntity addCourseToCoordinator(int idKoordynatoraPraktyk, int idKieurnku);

    public List<KierunkiStudiowEntity> findCourseOfCoordinator(int idKoordynatoraPraktyk);

    public List<KierunkiStudiowEntity> getCourseOfCoordinator(int idKoordynatoraPraktyk, int idKierunku);

    public KoordynatorzyDate getDate(int idKoordynatoraPraktyk, int idKieurnku);

    public Long deleteCoordinatorFromCourse(int idKoordynatoraPraktyk, int idKierunku);

    public Long deleteCoordinatorFromEveryCourse(int idKoordynatoraPraktyk);

    public KoordynatorzyKierunkowiPraktykodawcowEntity addEmployerToCoordinator(int idKoordynatoraPraktky, KoordynatorzyKierunkowiPraktykodawcowEntity koordynatorzyKierunkowiPraktykodawcowEntity);

    public List findEmployersOfCoordinator(int idKoordynatoraPraktyk);

    public List getEmployerOfCoordinator(int idKoordynatoraPraktyk, int idPraktykodawcy);

    public Long deleteEmployerFromCoordinator(int idKoordynatoraPraktyk, int idPraktykodawcy);

    public Long deleteEveryEmployerFromCoordinator(int idKoordynatoraPraktyk);

    public Long checkEmail(String email);

    public Long checkLogin(String login);
}
