package praktyki.core.service.Interface;

import praktyki.core.entities.ZgloszeniaPraktykodawcowEntity;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.List;

/**
 * Created by dawid on 11.03.15.
 */
public interface iZgloszeniaPraktykodawcowService {

    public ZgloszeniaPraktykodawcowEntity addEmployerApplication(ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity);

    public ZgloszeniaPraktykodawcowEntity editApplication(Long idZgloszeniaPraktykodawcy, ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity);

    public List<ZgloszeniaPraktykodawcowEntity> findAll();

    //public List<ZgloszeniaPraktykodawcowEntity> getApplication(Long idZgloszeniaPraktykodawcy);

    public ZgloszenieInfo getApplication(Long idZgloszeniaPraktykodawcy);

    public Long deleteApplication(Long idZgloszeniaPraktykodawcy);

    public List getAllApplicationsOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego);
}
