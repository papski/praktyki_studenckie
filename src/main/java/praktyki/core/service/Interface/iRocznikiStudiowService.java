package praktyki.core.service.Interface;

import praktyki.core.entities.RocznikiStudiowEntity;

/**
 * Created by dawid on 17.02.15.
 */
public interface iRocznikiStudiowService {

    public RocznikiStudiowEntity addRelation(Integer idKierunku, Integer idRokuAkademickiego);

    public Long deleteYearsFromCourse(Integer idKierunku, Integer idRokuAkademickiego);

    public Long deleteEveryYearFromCourse(Integer idKierunku);
}
