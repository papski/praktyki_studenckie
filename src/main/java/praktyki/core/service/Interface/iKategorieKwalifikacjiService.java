package praktyki.core.service.Interface;

import praktyki.core.entities.KategorieKwalifikacjiEntity;

import java.util.List;

/**
 * Created by dawid on 27.03.15.
 */
public interface iKategorieKwalifikacjiService {

    public KategorieKwalifikacjiEntity addCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public KategorieKwalifikacjiEntity addSubCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    public List<KategorieKwalifikacjiEntity> findCategoryByName(String nazwaKategorii);

    public List<KategorieKwalifikacjiEntity> findCategory();

    public KategorieKwalifikacjiEntity getCategory(int idKategoriiKwalifikacji);

    public List<KategorieKwalifikacjiEntity> findSubCategoryByName (String nazwaKategorii);

    public List<KategorieKwalifikacjiEntity> findSubCategory();

    public List<KategorieKwalifikacjiEntity> findSubCategoryOfCategory(int idKategoriiKwalifikacji);

    public KategorieKwalifikacjiEntity getSubCategoryOfCategory(int idKategoriiKwalifikacji);

}
