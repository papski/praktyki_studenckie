package praktyki.core.service.Interface;

import praktyki.core.entities.PraktykodawcyEntity;
import praktyki.core.entities.ProfilEntity;
import praktyki.core.entities.ProfilePraktykodawcowEntity;

import java.util.List;

/**
 * Created by dawid on 25.01.15.
 */
public interface iProfilService {

    public ProfilEntity addProfile(ProfilEntity profilEntity);

    public ProfilEntity updateProfile(Integer idProfilu, ProfilEntity profilEntity);

    public ProfilEntity deleteProfile(Integer idProfilu);

    public List<ProfilEntity> findAll();

    public ProfilEntity getProfile(Integer idProfilu);

    public ProfilePraktykodawcowEntity addRelation(Integer idProfilu, Integer idPraktykodawcy);

    public List<PraktykodawcyEntity> findEmployeerOfProfile(int idProfilu);

    public List<PraktykodawcyEntity> getEmployeerOfProfile(int idPraktykodawcy, int idProfilu);

}
