package praktyki.core.service.Interface;

import praktyki.core.entities.AdresyEntity;
import praktyki.core.entities.StudenciEntity;

import java.util.List;

/**
 * Created by dawid on 29.12.14.
 */

public interface iAdresyService {

    public AdresyEntity addAddress(AdresyEntity adresyEntity);

    public AdresyEntity updateAddress(AdresyEntity adresyEntity, int idAdresu);

    public AdresyEntity deleteAddress(int idAdresu);

    public List<AdresyEntity> findEveryAddress();

    public AdresyEntity findByIdAdresu(int idAdresu);

    public StudenciEntity addStudent(int idAdresu, StudenciEntity studenciEntity);

    public List<StudenciEntity> findStudentByIdAdresu(Integer idAdresu);

    public List<AdresyEntity> getAddressByStreetCityZipCode(String searchString, String sortBy, String sortType);

    //public List<PraktykodawcyEntity> findEmployeerByIdAdresu(int idAdresu);


}
