package praktyki.core.service.Interface;

import praktyki.core.entities.StudenciKierunkowEntity;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
public interface iStudenciKierunkowService {

    //public StudenciKierunkowEntity addRelation(StudenciKierunkowEntity studenciKierunkowEntity);

    public List<StudenciKierunkowEntity> getByIdKierunku(int idKierunku);

    public List<StudenciKierunkowEntity> getByNrAlbumu(Long nrAlbumu);

    public List<StudenciKierunkowEntity> getAll();
}
