package praktyki.core.service.Interface;

import praktyki.core.entities.PorozumieniaEntity;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */
public interface iPorozumieniaService {

    public PorozumieniaEntity addAgreement (PorozumieniaEntity porozumieniaEntity);

    public PorozumieniaEntity getAgreement (Integer idPorozumienia);

    public PorozumieniaEntity deleteAgreements (Integer idPorozumienia);

    public List getAll();

    public List getAgreementsOfEmployer(int idPraktykodawcy);

    public List getAgreementsOfStudent(Long nrAlbumu);
}
