package praktyki.core.service.Interface;

import praktyki.core.entities.OsobyEntity;

import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */
public interface iOsobyService {

    public OsobyEntity addPerson(OsobyEntity osobyEntity);

    public OsobyEntity updatePerson(Integer idOsoby, OsobyEntity osobyEntity);

    public List<OsobyEntity> findAll();

    public OsobyEntity getPersonByIdOsoby(Integer idOsoby);

    public OsobyEntity deletePerson(Integer idOsoby);

    public Long checkEmail(String email);

    public Long checkTelefonKomorkowy(String telefonKomorkowy);

    public Long checkTelefonStacjonarny(String telefonStacjonarny);

    public List<OsobyEntity> getUserByNameSurname(String searchString, String sortBy, String sortType);

}
