package praktyki.core.service;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.*;
import praktyki.core.exceptions.EmailUsedException;
import praktyki.core.exceptions.ExistenceException;
import praktyki.core.exceptions.LoginUsedException;
import praktyki.core.service.Interface.iKoordynatorzyPraktykService;
import praktyki.core.service.utilities.PasswordEncryptionService;
import praktyki.rest.mvc.helpers.KoordynatorInfo;
import praktyki.rest.mvc.helpers.KoordynatorzyDate;
import praktyki.rest.mvc.helpers.PraktykaInfo;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */

@Service
@Transactional
public class KoordynatorzyPraktykService implements iKoordynatorzyPraktykService {

    @Autowired
    private iKoordynatorzyKierunkowiDAO ikoordynatorzyKierunkowiDAO;

    @Autowired
    private PasswordEncryptionService passwordEncryptionService;

    @Autowired
    private iKoordynatorzyKierunkowiPraktykodawcowDAO ikoordynatorzyKierunkowiPraktykodawcowDAO;

    @Autowired
    private iStudenciDAO istudenciDAO;

    @Autowired
    private iOsobyDAO iosobyDAO;

    @Autowired
    private iUzytkownicyDAO iuzytkownicyDAO;

    @Autowired
    private iAdresyDAO iadresyDAO;

    @Autowired
    private iKierunkiStudiowDAO ikierunkiStudiowDAO;

    @Autowired
    private iPraktykiDAO ipraktykiDAO;

    @Autowired
    private iSzablonyPraktykDAO iszablonyPraktykDAO;

    @Autowired
    private iPraktykodawcyDAO ipraktykodawcyDAO;

    @Autowired
    private iStatusyDAO istatusyDAO;

    @Autowired
    private iTypyPraktykDAO itypyPraktykDAO;

    @Autowired
    private iLataAkademickieDAO ilataAkademickieDAO;

    @Autowired
    private iOpiekunowiePraktykDAO iopiekunowiePraktykDAO;

    @Autowired
    private iPorozumieniaDAO iporozumieniaDAO;

    @Autowired
    private iKoordynatorzyPraktykDAO ikoordynatorzyPraktykDAO;

    @Override
    public KoordynatorzyPraktykEntity addCoordinator(KoordynatorzyPraktykEntity koordynatorzyPraktykEntity) {
        return ikoordynatorzyPraktykDAO.addCoordinator(koordynatorzyPraktykEntity);
    }

    @Override
    public OsobyEntity addPerson(int idKoordynatoraPraktyk, OsobyEntity osobyEntity) {
        if(iosobyDAO.isEmailUsed(osobyEntity.getEmail()) == true) {
            throw new EmailUsedException("Email is already used by someone");
        } else {
            KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
            if (coordinator != null && coordinator.getIdOsoby() == null) {
                OsobyEntity newPerson = iosobyDAO.addPerson(osobyEntity);
                newPerson.setKoordynator(coordinator);
                Integer idOsoby = newPerson.getIdOsoby();
                coordinator.setIdOsoby(idOsoby);
                coordinator.setKoordynatorByIdOsoby(newPerson);
                return newPerson;
            } else {
                throw new ExistenceException("There is no such Person according to ID of Coordinator OR Coordinator already has his/her info persisted");
            }
        }
    }

    @Override
    public KoordynatorzyPraktykEntity deleteCoordinator(int idKoordynatoraPraktyk) {
        ikoordynatorzyKierunkowiDAO.deleteCoordinatorFromEveryCourse(idKoordynatoraPraktyk);
        return ikoordynatorzyPraktykDAO.deleteCoordinator(idKoordynatoraPraktyk);
    }

    @Override
    public List<KoordynatorzyPraktykEntity> findAll() {
        return ikoordynatorzyPraktykDAO.findAll();
    }

    @Override
    public KoordynatorzyPraktykEntity getCoordinator(int idKoordynatoraPraktyk) {
        return ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
    }

    @Override
    public KoordynatorInfo getAllDataOfCoordinator(int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
        if (coordinator !=null) {
            KoordynatorInfo coordinatorInfo = new KoordynatorInfo();
            coordinatorInfo.setDaneKoordynatora(iosobyDAO.getInfoOfCoordinator(coordinator.getIdOsoby(),idKoordynatoraPraktyk));
            coordinatorInfo.setLoginKoordynatora(iuzytkownicyDAO.getInfoOfCoordinator(coordinator.getIdOsoby()));
            coordinatorInfo.setKierunkiKoordynatora(ikierunkiStudiowDAO.findCourseOfCoordinator(idKoordynatoraPraktyk));
            coordinatorInfo.setPraktykodawcy(ipraktykodawcyDAO.findEmployerOfCoordinator(idKoordynatoraPraktyk));
            return coordinatorInfo;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykaInfo> getAllPracticesOfCoordinator(int idKoordynatoraPraktyk) {
        List<PraktykiEntity> list = ipraktykiDAO.getPracticeOfCoordinator(idKoordynatoraPraktyk);
        List<PraktykaInfo> info = new ArrayList<PraktykaInfo>();
        PraktykaInfo row = new PraktykaInfo();
        //System.out.println(list.size());
        for(int i = 0; i<list.size(); i++) {
            PraktykiEntity practice = ipraktykiDAO.getRow(list.get(i).getIdPraktykiStudenckiej());
            //System.out.println(practice.getIdPraktykiStudenckiej());
            if(practice !=null) {
                row.setIdPraktykiStudenckiej(practice.getIdPraktykiStudenckiej());
                row.setSzablonPraktyki(iszablonyPraktykDAO.getTemplate(practice.getIdSzablonu()));
                row.setPraktykodawca(ipraktykodawcyDAO.findEmployeer(practice.getIdPraktykodawcy()));
                row.setStatusPraktyki(istatusyDAO.getStatus(practice.getIdStatusu()));
                row.setKierunek(ikierunkiStudiowDAO.getCourseInfo(practice.getIdKierunku()));
                row.setTypPraktyki(itypyPraktykDAO.getTypeOfTraineeship(practice.getIdTypuPraktyki()));
                row.setRokAkademicki(ilataAkademickieDAO.getYearOfCourse(practice.getIdRokuAkademickiego()));
                if(practice.getNrAlbumu() !=null) {
                    row.setStudent(istudenciDAO.getStudentInfo(practice.getNrAlbumu()));
                }
                row.setOpiekunPraktyki(iopiekunowiePraktykDAO.getTutorInfo(practice.getIdOpiekunaPraktyk()));
                row.setKoordynatorPraktyki(ikoordynatorzyPraktykDAO.getCoordinatorInfo(practice.getIdKoordynatoraPraktyk()));
                row.setAdresPracodawcy(iadresyDAO.findByIdAdresu(practice.getIdAdresu()));
                if(practice.getIdPorozumienia() !=null) {
                    row.setPorozumienie(iporozumieniaDAO.getAgreement(practice.getIdPorozumienia()));
                }
                info.add(i, row);
            } else {
                return null;
            }
        }
        return info;
    }

    @Override
    public List<OsobyEntity> getInfoOfCoordinator(Integer idOsoby, int idKoordynatoraPraktyk) {
        return iosobyDAO.getInfoOfCoordinator(idOsoby, idKoordynatoraPraktyk);
    }

    @Override
    public OsobyEntity editInfoOfCoordinator(Integer idOsoby, int idKoordynatoraPraktyk, OsobyEntity osobyEntity) {
        KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
        if (coordinator !=null) {
            return iosobyDAO.updatePerson(idOsoby, osobyEntity);
        }
        else {
            return null;
        }
    }

    @Override
    public UzytkownicyEntity addUser(int idKoordynatoraPraktyk, UzytkownicyEntity uzytkownicyEntity) {
        if(iuzytkownicyDAO.isLoginUsed(uzytkownicyEntity.getLogin()) == true) {
            throw new LoginUsedException("Login is already used by someone");
        } else {
            KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
            if (coordinator != null && iuzytkownicyDAO.checkForExistence(coordinator.getIdOsoby()) == true) {
                try {
                    byte[] tempSalt = passwordEncryptionService.generateSalt();
                    String salt = new String(Base64.encode(tempSalt));
                    byte[] tempEncrypted = passwordEncryptionService.getEncryptedPassword(uzytkownicyEntity.getHaslo(), tempSalt);
                    String encryptedPassword = new String(Base64.encode(tempEncrypted));
                    uzytkownicyEntity.setHaslo(encryptedPassword);
                    uzytkownicyEntity.setSalt(salt);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                }
                return iuzytkownicyDAO.addUser(uzytkownicyEntity, coordinator.getIdOsoby(), 4);
            } else{
                //throw new IllegalArgumentException("Osoba juz przypisala login/haslo do siebie LUB dany koordynator nie istnieje");
                throw new ExistenceException("Person already persisted his/her User info or person doesn't exist");
            }
        }
    }

    @Override
    public List<UzytkownicyEntity> getUser(int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
        if (coordinator !=null) {
            return iuzytkownicyDAO.getInfoOfCoordinator(coordinator.getIdOsoby());
        } else {
            return null;
        }
    }

    @Override
    public KoordynatorzyKierunkowiEntity addCourseToCoordinator(int idKoordynatoraPraktyk, int idKieurnku) {
        if(ikoordynatorzyKierunkowiDAO.getRow(idKoordynatoraPraktyk, idKieurnku) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie w bazie");
        } else {
            KoordynatorzyPraktykEntity coordinator = ikoordynatorzyPraktykDAO.getCoordinator(idKoordynatoraPraktyk);
            if (coordinator != null) {
                KierunkiStudiowEntity course = ikierunkiStudiowDAO.getCourse(idKieurnku);
                if (course != null) {
                    KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity = new KoordynatorzyKierunkowiEntity();
                    koordynatorzyKierunkowiEntity.setIdKierunku(course.getIdKierunku());
                    koordynatorzyKierunkowiEntity.setIdKoordynatoraPraktyk(coordinator.getIdKoordynatoraPraktyk());
                    return ikoordynatorzyKierunkowiDAO.addRelation(koordynatorzyKierunkowiEntity);
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    @Override
    public List<KierunkiStudiowEntity> findCourseOfCoordinator(int idKoordynatoraPraktyk) {
        return ikierunkiStudiowDAO.findCourseOfCoordinator(idKoordynatoraPraktyk);
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseOfCoordinator(int idKoordynatoraPraktyk, int idKierunku) {
        return ikierunkiStudiowDAO.getCourseOfCoordinator(idKoordynatoraPraktyk, idKierunku);
    }

    @Override
    public KoordynatorzyDate getDate(int idKoordynatoraPraktyk, int idKieurnku) {
        return ikoordynatorzyKierunkowiDAO.getDate(idKoordynatoraPraktyk, idKieurnku);
    }

    @Override
    public Long deleteCoordinatorFromCourse(int idKoordynatoraPraktyk, int idKierunku) {
        return ikoordynatorzyKierunkowiDAO.deleteCoordinatorFromCourse(idKoordynatoraPraktyk, idKierunku);
    }

    @Override
    public Long deleteCoordinatorFromEveryCourse(int idKoordynatoraPraktyk) {
        return ikoordynatorzyKierunkowiDAO.deleteCoordinatorFromEveryCourse(idKoordynatoraPraktyk);
    }

    @Override
    public KoordynatorzyKierunkowiPraktykodawcowEntity addEmployerToCoordinator(int idKoordynatoraPraktky, KoordynatorzyKierunkowiPraktykodawcowEntity koordynatorzyKierunkowiPraktykodawcowEntity) {
        if(ikoordynatorzyKierunkowiPraktykodawcowDAO.getRow(idKoordynatoraPraktky, idKoordynatoraPraktky, idKoordynatoraPraktky) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie w bazie");
        } else {
            KoordynatorzyKierunkowiEntity row = ikoordynatorzyKierunkowiDAO.getRow(koordynatorzyKierunkowiPraktykodawcowEntity.getIdKoordynatoraPraktyk(), koordynatorzyKierunkowiPraktykodawcowEntity.getIdKierunku());
            return ikoordynatorzyKierunkowiPraktykodawcowDAO.addRelation(koordynatorzyKierunkowiPraktykodawcowEntity.getIdPraktykodawcy(), row);
        }
    }

    @Override
    public List findEmployersOfCoordinator(int idKoordynatoraPraktyk) {
        return ipraktykodawcyDAO.findEmployerOfCoordinator(idKoordynatoraPraktyk);
    }

    @Override
    public List getEmployerOfCoordinator(int idKoordynatoraPraktyk, int idPraktykodawcy) {
        return ipraktykodawcyDAO.getEmployerOfCoordinator(idKoordynatoraPraktyk, idPraktykodawcy);
    }

    @Override
    public Long deleteEmployerFromCoordinator(int idKoordynatoraPraktyk, int idPraktykodawcy) {
        return ikoordynatorzyKierunkowiPraktykodawcowDAO.deleteEmployerFromCoordinator(idKoordynatoraPraktyk, idPraktykodawcy);
    }

    @Override
    public Long deleteEveryEmployerFromCoordinator(int idKoordynatoraPraktyk) {
        return ikoordynatorzyKierunkowiPraktykodawcowDAO.deleteEveryEmployerFromCoordinator(idKoordynatoraPraktyk);
    }

    @Override
    public Long checkEmail(String email) {
        return iosobyDAO.checkEmail(email);
    }

    @Override
    public Long checkLogin(String login) {
        return iuzytkownicyDAO.checkLogin(login);
    }
}
