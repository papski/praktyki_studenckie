package praktyki.core.service;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iOpiekunowiePraktykDAO;
import praktyki.core.DAO.Interface.iOsobyDAO;
import praktyki.core.DAO.Interface.iUzytkownicyDAO;
import praktyki.core.entities.OpiekunowiePraktykEntity;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.entities.UzytkownicyEntity;
import praktyki.core.exceptions.EmailUsedException;
import praktyki.core.exceptions.ExistenceException;
import praktyki.core.exceptions.LoginUsedException;
import praktyki.core.service.Interface.iOpiekunowiePraktykService;
import praktyki.core.service.utilities.PasswordEncryptionService;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */

@Service
@Transactional
public class OpiekunowiePraktykService implements iOpiekunowiePraktykService {

    @Autowired
    private iOpiekunowiePraktykDAO iopiekunowiePraktykDAO;

    @Autowired
    private PasswordEncryptionService passwordEncryptionService;

    @Autowired
    private iOsobyDAO iosobyDAO;

    @Autowired
    private iUzytkownicyDAO iuzytkownicyDAO;

    @Override
    public OpiekunowiePraktykEntity addTutor(OpiekunowiePraktykEntity opiekunowiePraktykEntity) {
        return iopiekunowiePraktykDAO.addTutor(opiekunowiePraktykEntity);
    }

    @Override
    public OpiekunowiePraktykEntity updateTutor(Integer idOpiekunaPraktyk, OpiekunowiePraktykEntity opiekunowiePraktykEntity) {
        return iopiekunowiePraktykDAO.updateTutor(idOpiekunaPraktyk, opiekunowiePraktykEntity);
    }

    @Override
    public OpiekunowiePraktykEntity deleteTutor(Integer idOpiekunaPraktyk) {
        return iopiekunowiePraktykDAO.deleteTutor(idOpiekunaPraktyk);
    }

    @Override
    public List<OpiekunowiePraktykEntity> findAll() {
        return iopiekunowiePraktykDAO.findAll();
    }

    @Override
    public OpiekunowiePraktykEntity getTutor(Integer idOpiekunaPraktyk) {
        return iopiekunowiePraktykDAO.getTutor(idOpiekunaPraktyk);
    }

    @Override
    public OsobyEntity addPerson(Integer idOpiekunaPraktyk, OsobyEntity osobyEntity) {
        if(iosobyDAO.isEmailUsed(osobyEntity.getEmail()) == true) {
            throw new EmailUsedException("Email is already used by someone");
        } else {
            OpiekunowiePraktykEntity tutor = iopiekunowiePraktykDAO.getTutor(idOpiekunaPraktyk);
            if (tutor != null && tutor.getIdOsoby() == null) {
                OsobyEntity newPerson = iosobyDAO.addPerson(osobyEntity);
                newPerson.setOpiekun(tutor);
                tutor.setOpiekunByIdOsoby(newPerson);
                tutor.setIdOsoby(newPerson.getIdOsoby());
                return newPerson;
            } else {
                throw new ExistenceException("There is no such Person according to ID of Tutor OR tutor already has/her his info persisted ");
            }
        }
    }

    @Override
    public OsobyEntity updatePerson(Integer idOpiekunaPraktyk, OsobyEntity osobyEntity) {
        OpiekunowiePraktykEntity tutor = iopiekunowiePraktykDAO.getTutor(idOpiekunaPraktyk);
        if (tutor !=null) {
            Integer idOsoby = tutor.getIdOsoby();
            OsobyEntity person = iosobyDAO.updatePerson(idOsoby, osobyEntity);
            return person;
        } else {
            return null;
        }
    }

    @Override
    public List<OsobyEntity> getInfoOfTutor(Integer idOpiekunaPraktyk) {
        return iosobyDAO.getInfoOfTutor(idOpiekunaPraktyk);
    }

    @Override
    public UzytkownicyEntity addUser(Integer idOpiekunaPraktyk, UzytkownicyEntity uzytkownicyEntity) {
        if (iuzytkownicyDAO.isLoginUsed(uzytkownicyEntity.getLogin()) == true) {
            throw new LoginUsedException("Login is already used by someone");
        } else {
            OpiekunowiePraktykEntity tutor = iopiekunowiePraktykDAO.getTutor(idOpiekunaPraktyk);
            if (tutor != null && iuzytkownicyDAO.checkForExistence(tutor.getIdOsoby()) == true) {
                try {
                    byte[] tempSalt = passwordEncryptionService.generateSalt();
                    String salt = new String(Base64.encode(tempSalt));
                    byte[] tempEncrypted = passwordEncryptionService.getEncryptedPassword(uzytkownicyEntity.getHaslo(), tempSalt);
                    String encryptedPassword = new String(Base64.encode(tempEncrypted));
                    uzytkownicyEntity.setHaslo(encryptedPassword);
                    uzytkownicyEntity.setSalt(salt);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                }
                return iuzytkownicyDAO.addUser(uzytkownicyEntity, tutor.getIdOsoby(), 3);
            } else {
                //throw new IllegalArgumentException("Osoba juz przypisala login/haslo do siebie LUB dany opiekun nie istnieje");
                throw new ExistenceException("Person already persisted his/her User info or person doesn't exist");
            }
        }
    }


    @Override
    public List<UzytkownicyEntity> getUser(Integer idOpiekunaPraktyk) {
        OpiekunowiePraktykEntity tutor = iopiekunowiePraktykDAO.getTutor(idOpiekunaPraktyk);
        if (tutor !=null) {
            return iuzytkownicyDAO.getInfoOfTutor(tutor.getIdOsoby());
        } else {
            return null;
        }

    }

    @Override
    public Long checkEmail(String email) {
        return iosobyDAO.checkEmail(email);
    }

    @Override
    public Long checkLogin(String login) {
        return iuzytkownicyDAO.checkLogin(login);
    }
}
