package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iKoordynatorzyKierunkowiDAO;
import praktyki.core.entities.KoordynatorzyKierunkowiEntity;
import praktyki.core.service.Interface.iKoordynatorzyKierunkowiService;

import java.util.List;

/**
 * Created by dawid on 22.01.15.
 */

@Service
@Transactional
public class KoordynatorzyKierunkowiService implements iKoordynatorzyKierunkowiService {

    @Autowired
    private iKoordynatorzyKierunkowiDAO ikoordynatorzyKierunkowiDAO;

    @Override
    public KoordynatorzyKierunkowiEntity addRelation(KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity) {
        if(ikoordynatorzyKierunkowiDAO.getRow(koordynatorzyKierunkowiEntity.getIdKoordynatoraPraktyk(), koordynatorzyKierunkowiEntity.getIdKierunku()) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie w bazie");
        } else {
            return ikoordynatorzyKierunkowiDAO.addRelation(koordynatorzyKierunkowiEntity);
        }
    }

    @Override
    public List<KoordynatorzyKierunkowiEntity> findAll() {
        return ikoordynatorzyKierunkowiDAO.findAll();
    }
}
