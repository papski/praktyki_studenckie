package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iPorozumieniaDAO;
import praktyki.core.entities.PorozumieniaEntity;
import praktyki.core.service.Interface.iPorozumieniaService;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */

@Service
@Transactional
public class PorozumieniaService implements iPorozumieniaService {

    @Autowired
    private iPorozumieniaDAO iporozumieniaDAO;

    @Override
    public PorozumieniaEntity addAgreement(PorozumieniaEntity porozumieniaEntity) {
        return iporozumieniaDAO.addAgreement(porozumieniaEntity);
    }

    @Override
    public PorozumieniaEntity getAgreement(Integer idPorozumienia) {
        return iporozumieniaDAO.getAgreement(idPorozumienia);
    }

    @Override
    public PorozumieniaEntity deleteAgreements(Integer idPorozumienia) {
        return iporozumieniaDAO.deleteAgreements(idPorozumienia);
    }

    @Override
    public List getAll() {
        return iporozumieniaDAO.getAll();
    }

    @Override
    public List getAgreementsOfEmployer(int idPraktykodawcy) {
        return iporozumieniaDAO.getAgreementsOfEmployer(idPraktykodawcy);
    }

    @Override
    public List getAgreementsOfStudent(Long nrAlbumu) {
        return iporozumieniaDAO.getAgreementsOfStudent(nrAlbumu);
    }
}
