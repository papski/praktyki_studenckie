package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.*;
import praktyki.core.exceptions.ExistenceException;
import praktyki.core.service.Interface.iSzablonyPraktykService;
import praktyki.rest.mvc.helpers.CategoryInfo;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */

@Service
@Transactional
public class SzablonyPraktykService implements iSzablonyPraktykService {

    @Autowired
    private iSzablonyPraktykDAO iszablonyPraktykDAO;

    @Autowired
    private iKwalifikacjeDAO ikwalifikacjeDAO;

    @Autowired
    private iWymaganeKwalifikacjeDAO iwymaganeKwalifikacjeDAO;

    @Autowired
    private iOferowaneKwalifikacjeDAO ioferowaneKwalifikacjeDAO;

    @Autowired
    private iKategorieKwalifikacjiDAO iKategorieKwalifikacjiDAO;

    @Override
    public SzablonyPraktykEntity addTemplate(SzablonyPraktykEntity szablonyPraktykEntity) {
        return iszablonyPraktykDAO.addTemplate(szablonyPraktykEntity);
    }

    @Override
    public SzablonyPraktykEntity getTemplate(int idSzablonu) {
        return iszablonyPraktykDAO.getTemplate(idSzablonu);
    }

    @Override
    public SzablonyPraktykEntity deleteTemplate(int idSzablonu) {
        return iszablonyPraktykDAO.deleteTemplate(idSzablonu);
    }

    @Override
    public List<SzablonyPraktykEntity> findAll() {
        return iszablonyPraktykDAO.findAll();
    }

    @Override
    public WymaganeKwalifikacjeEntity addRequiredQualification(int idSzablonu, KwalifikacjeEntity kwalifikacjeEntity) {
        KwalifikacjeEntity newReqQuali = ikwalifikacjeDAO.addQualification(kwalifikacjeEntity);
        return iwymaganeKwalifikacjeDAO.addRelation(idSzablonu, newReqQuali.getIdKwalifikacji());
    }

    @Override
    public List<KwalifikacjeEntity> findEveryRequiredQualifcation(int idSzablonu) {
        return ikwalifikacjeDAO.findRequiredQulificationsOfTemplate(idSzablonu);
    }

    @Override
    public List<KwalifikacjeEntity> getRequiredQualification(int idSzablonu, int idKwalifikacji) {
        return ikwalifikacjeDAO.getRequiredQualificationOfTemplate(idSzablonu, idKwalifikacji);
    }

    @Override
    public OferowaneKwalifikacjeEntity addOfferedQualification(int idSzablonu, KwalifikacjeEntity kwalifikacjeEntity) {
        KwalifikacjeEntity newOffQuali = ikwalifikacjeDAO.addQualification(kwalifikacjeEntity);
        return ioferowaneKwalifikacjeDAO.addRelation(idSzablonu, newOffQuali.getIdKwalifikacji());
    }

    @Override
    public List<KwalifikacjeEntity> findEveryOfferedQualification(int idSzablonu) {
        return ikwalifikacjeDAO.findOfferedQulificationsOfTemplate(idSzablonu);
    }

    @Override
    public List<KwalifikacjeEntity> getOfferedQualification(int idSzablonu, int idKwalifikacji) {
        return ikwalifikacjeDAO.getOfferedQualificationOfTemplate(idSzablonu, idKwalifikacji);
    }

    @Override
    public KategorieKwalifikacjiEntity addCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        return iKategorieKwalifikacjiDAO.addCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
    }

    @Override
    public KategorieKwalifikacjiEntity addSubCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        return iKategorieKwalifikacjiDAO.addSubCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findCategoryByName(String nazwaKategorii) {
        return iKategorieKwalifikacjiDAO.findCategoryByName(nazwaKategorii);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findCategory() {
        return iKategorieKwalifikacjiDAO.findCategory();
    }

    @Override
    public KategorieKwalifikacjiEntity getCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.getCategory(idKategoriiKwalifikacji);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategoryByName(String nazwaKategorii) {
        return iKategorieKwalifikacjiDAO.findSubCategoryByName(nazwaKategorii);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategory() {
        return iKategorieKwalifikacjiDAO.findSubCategory();
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategoryOfCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.findSubCategoryOfCategory(idKategoriiKwalifikacji);
    }

    @Override
    public KategorieKwalifikacjiEntity getSubCategoryOfCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.getSubCategoryOfCategory(idKategoriiKwalifikacji);
    }

    @Override
    public CategoryInfo getInfoOfCategory(int idKategoriiKwalifikacji) {
        CategoryInfo info = new CategoryInfo();
        info.setNazwaKategorii(iKategorieKwalifikacjiDAO.getInfoOfCategory(idKategoriiKwalifikacji));
        info.setSubkategorie(iKategorieKwalifikacjiDAO.findSubCategoryOfCategory(idKategoriiKwalifikacji));
        return info;
    }

    @Override
    public List<KwalifikacjeEntity> getQualificationsOfTemplate(int idSzablonu) {
        return ikwalifikacjeDAO.getQualificationsOfTemplate(idSzablonu);
    }

    @Override
    public KategorieKwalifikacjiEntity addCategoryOfRequiredQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        if (iwymaganeKwalifikacjeDAO.checkExistence(idKwalifikacji) == true) {
            return iKategorieKwalifikacjiDAO.addCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        } else {
            throw new ExistenceException("There is no required Qualification assigned to this ID of qualification");
        }
    }

    @Override
    public KategorieKwalifikacjiEntity addSubCategoryOfRequiredQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        if(iwymaganeKwalifikacjeDAO.checkExistence(idKwalifikacji) == true) {
            return iKategorieKwalifikacjiDAO.addSubCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        } else {
            throw new ExistenceException("There is no required Qualification assigned to this ID of qualification");
        }
     }

    public KategorieKwalifikacjiEntity addCategoryOfOfferdQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        if(ioferowaneKwalifikacjeDAO.checkExistence(idKwalifikacji) == true) {
            return iKategorieKwalifikacjiDAO.addCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        } else {
            throw new ExistenceException("There is no offered Qualification assigned to this ID of qualification");
        }
    }

    @Override
    public KategorieKwalifikacjiEntity addSubCategoryOfOfferedQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        if(ioferowaneKwalifikacjeDAO.checkExistence(idKwalifikacji) == true) {
            return iKategorieKwalifikacjiDAO.addSubCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
        } else {
            throw new ExistenceException("There is no offered Qualification assigned to this ID of qualification");
        }
    }

    @Override
    public Long deleteCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.deleteCategory(idKategoriiKwalifikacji);
    }

    @Override
    public Long deleteSubCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.deleteSubCategory(idKategoriiKwalifikacji);
    }
}
