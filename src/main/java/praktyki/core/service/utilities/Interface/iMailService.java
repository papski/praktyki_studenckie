package praktyki.core.service.utilities.Interface;

import praktyki.rest.mvc.helpers.MailHelper;

import java.util.List;

/**
 * Created by dawid on 19.04.15.
 */
public interface iMailService{

    public List sendMailsToStudents(MailHelper mailHelper);
}
