package praktyki.core.service.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import praktyki.core.DAO.Interface.iOsobyDAO;
import praktyki.core.service.utilities.Interface.iMailService;
import praktyki.rest.mvc.helpers.MailHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dawid on 18.04.15.
 */

@Service
public class MailService implements iMailService {

    @Autowired
    private MailSender mailSender;

    @Autowired
    private iOsobyDAO iOsobyDAO;

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

/*    public void sendMail(String from, String to, String subject, String msg) {
        //creating message
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(msg);
        //sending message
        try {
            this.mailSender.send(message);
        } catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }
    }*/

    public void sendManyMails(String from, String[] to, String subject, String msg) {
        //creating message
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);//passing array of recipients
        message.setSubject(subject);
        message.setText(msg);
        //sending message
        try {
            this.mailSender.send(message);
        } catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public List sendMailsToStudents(MailHelper mailHelper) {
        List<Integer> listOfPeronsId = new ArrayList<Integer>();
        for (int i = 0; i < mailHelper.getStudentsToBeMailed().size(); i++) {
            listOfPeronsId.add(mailHelper.getStudentsToBeMailed().get(i).getIdOsoby()); // if doesn't work check .add(i, ....)
        }
        String[] to = iOsobyDAO.getEmails(listOfPeronsId);
        sendManyMails(mailHelper.getFrom(), to,mailHelper.getSubject(), mailHelper.getMsg());
        List list = new ArrayList();
        Collections.addAll(list, to);
        return list;
    }
}
