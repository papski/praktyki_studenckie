package praktyki.core.service.utilities;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import praktyki.core.DAO.Interface.iUzytkownicyDAO;
import praktyki.core.entities.UzytkownicyEntity;
import praktyki.rest.mvc.helpers.LoginInfo;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by dawid on 27.03.15.
 */

@Service
public class LoginService {

    @Autowired
    private PasswordEncryptionService passwordEncryptionService;

    @Autowired
    private iUzytkownicyDAO iuzytkownicyDAO;

    public UzytkownicyEntity authenticateUser (LoginInfo login) {
        UzytkownicyEntity user = iuzytkownicyDAO.getRowByLogin(login.getLogin());
        if (user !=null) {
            if(user.getBlokada() == false) {
                try {
                    byte[] encryptedPassword = Base64.decode(user.getHaslo());
                    byte[] salt = Base64.decode(user.getSalt());
                    Boolean succesfulLogin = passwordEncryptionService.authenticate(login.getHaslo(), encryptedPassword, salt);
                    System.out.println(succesfulLogin);
                    if (succesfulLogin == false) {
                        throw new IllegalArgumentException("Incorrect Password");
                    }
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                } catch (Base64DecodingException e) {
                    e.printStackTrace();
                }
            } else {
                throw new IllegalArgumentException("User is blocked by Administrator");
            }
        } else {
            throw new IllegalArgumentException("User with such login doesn't exist in DB");
        }
        return user;
    }

    public UzytkownicyEntity changeUserPassword(LoginInfo login) {
        UzytkownicyEntity user = iuzytkownicyDAO.getRowByLogin(login.getLogin());
        if (user !=null) {
            if(user.getBlokada() == false) {
                try {
                    user.setIdUzytkownika(user.getIdUzytkownika());
                    byte[] salt = Base64.decode(user.getSalt());
                    byte[] tempEncrypted = passwordEncryptionService.getEncryptedPassword(login.getHaslo(), salt);
                    String encryptedPassword = new String(Base64.encode(tempEncrypted));
                    user.setHaslo(encryptedPassword);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                } catch (Base64DecodingException e) {
                    e.printStackTrace();
                }
            } else {
                throw new IllegalArgumentException("User is blocked by Administrator");
            }
        } else {
            throw new IllegalArgumentException("User with such login doesn't exist in DB");
        }
        return user;
    }
}
