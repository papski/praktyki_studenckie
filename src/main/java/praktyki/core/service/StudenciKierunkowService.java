package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iKierunkiStudiowDAO;
import praktyki.core.DAO.Interface.iStudenciDAO;
import praktyki.core.DAO.Interface.iStudenciKierunkowDAO;
import praktyki.core.entities.StudenciKierunkowEntity;
import praktyki.core.service.Interface.iStudenciKierunkowService;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
@Service
@Transactional
public class StudenciKierunkowService implements iStudenciKierunkowService {

    @Autowired
    iStudenciKierunkowDAO istudenciKierunkowDAO;

    @Autowired
    iStudenciDAO istudenciDAO;

    @Autowired
    iKierunkiStudiowDAO ikierunkistudiowDAO;

    /*@Override
    public StudenciKierunkowEntity addRelation(StudenciKierunkowEntity studenciKierunkowEntity) {
        return istudenciKierunkowDAO.addRelation(studenciKierunkowEntity);
    }*/

    @Override
    public List<StudenciKierunkowEntity> getByIdKierunku(int idKierunku) {
        return istudenciKierunkowDAO.getByIdKierunku(idKierunku);
    }

    @Override
    public List<StudenciKierunkowEntity> getByNrAlbumu(Long nrAlbumu) {
        return istudenciKierunkowDAO.getByNrAlbumu(nrAlbumu);
    }

    @Override
    public List<StudenciKierunkowEntity> getAll() {
        return istudenciKierunkowDAO.getAll();
    }
}
