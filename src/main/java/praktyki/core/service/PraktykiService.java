package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.PraktykiEntity;
import praktyki.core.service.Interface.iPraktykiService;
import praktyki.rest.mvc.helpers.PraktykaInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid on 14.03.15.
 */

@Service
@Transactional
public class PraktykiService implements iPraktykiService {

    @Autowired
    private iPraktykiDAO ipraktykiDAO;

    @Autowired
    private iStudenciDAO istudenciDAO;

    @Autowired
    private iSzablonyPraktykDAO iszablonyPraktykDAO;

    @Autowired
    private iPraktykodawcyDAO ipraktykodawcyDAO;

    @Autowired
    private iStatusyDAO istatusyDAO;

    @Autowired
    private iKierunkiStudiowDAO ikierunkiStudiowDAO;

    @Autowired
    private iTypyPraktykDAO itypyPraktykDAO;

    @Autowired
    private iLataAkademickieDAO ilataAkademickieDAO;

    @Autowired
    private iOpiekunowiePraktykDAO iopiekunowiePraktykDAO;

    @Autowired
    private iKoordynatorzyPraktykDAO ikoordynatorzyPraktykDAO;

    @Autowired
    private iAdresyDAO iadresyDAO;

    @Autowired
    private iPorozumieniaDAO iporozumieniaDAO;

    @Override
    public PraktykiEntity addRelation(PraktykiEntity praktykiEntity) {
        List<PraktykiEntity> list = new ArrayList<PraktykiEntity>();
        List<PraktykiEntity> list2 = new ArrayList<PraktykiEntity>();
        list = ipraktykiDAO.checkForStudent(praktykiEntity);
        list2 = ipraktykiDAO.checkForAgreement(praktykiEntity);
        if(!list.isEmpty()) {
            throw new IllegalArgumentException("dany student z danego kierunku i roku jest juz zapisany na praktyke");
        } else if(!list2.isEmpty()) {
            throw new IllegalArgumentException("dane porozumienie jest juz przypisane do praktyki");
        } else if (!list.isEmpty() && !list2.isEmpty()) {
            throw new IllegalArgumentException("zarowno student jak i porozumienie sa juz przypisane do praktyki");
        } else {
            return ipraktykiDAO.addRelation(praktykiEntity);
        }
    }

    @Override
    public List<PraktykiEntity> findAll() {
        return ipraktykiDAO.findAll();
    }

    @Override
    public List<PraktykiEntity> getAvaliableCoursePractices(int idKierunku, int idRokuAkademickiego) {
        return ipraktykiDAO.getAvaliableCoursePractices(idKierunku, idRokuAkademickiego);
    }

    @Override
    public List<PraktykiEntity> getPracticeByCourseAcademicYear(int idKierunku, int idRokuAkademickiego) {
        return ipraktykiDAO.getPracticeByCourseAcademicYear(idKierunku, idRokuAkademickiego);
    }

    @Override
    public PraktykaInfo getPracticeInfo(Long idPraktykiStudenckiej) {
        PraktykiEntity practice = ipraktykiDAO.getRow(idPraktykiStudenckiej);
        if (practice !=null) {
            PraktykaInfo info = new PraktykaInfo();
            info.setIdPraktykiStudenckiej(idPraktykiStudenckiej);
            info.setSzablonPraktyki(iszablonyPraktykDAO.getTemplate(practice.getIdSzablonu()));
            info.setPraktykodawca(ipraktykodawcyDAO.findEmployeer(practice.getIdPraktykodawcy()));
            info.setStatusPraktyki(istatusyDAO.getStatus(practice.getIdStatusu()));
            info.setKierunek(ikierunkiStudiowDAO.getCourseInfo(practice.getIdKierunku()));
            info.setTypPraktyki(itypyPraktykDAO.getTypeOfTraineeship(practice.getIdTypuPraktyki()));
            info.setRokAkademicki(ilataAkademickieDAO.getYearOfCourse(practice.getIdRokuAkademickiego()));
            if(practice.getNrAlbumu() !=null) {
                info.setStudent(istudenciDAO.getStudentInfo(practice.getNrAlbumu()));
            }
            info.setOpiekunPraktyki(iopiekunowiePraktykDAO.getTutorInfo(practice.getIdOpiekunaPraktyk()));
            info.setKoordynatorPraktyki(ikoordynatorzyPraktykDAO.getCoordinatorInfo(practice.getIdKoordynatoraPraktyk()));
            info.setAdresPracodawcy(iadresyDAO.findByIdAdresu(practice.getIdAdresu()));
            if(practice.getIdPorozumienia() !=null) {
                info.setPorozumienie(iporozumieniaDAO.getAgreement(practice.getIdPorozumienia()));
            }
            return info;
        } else {
            return null;
        }
    }

    @Override
    public PraktykiEntity getRow(Long idPraktykiStudenckiej) {
        return ipraktykiDAO.getRow(idPraktykiStudenckiej);
    }

    @Override
    public PraktykiEntity editRow(Long idPraktykiStudenckiej, PraktykiEntity praktykiEntity) {
        return ipraktykiDAO.editRow(idPraktykiStudenckiej, praktykiEntity);
    }

    @Override
    public Long deleteRow(Long idPraktykiStudenckiej) {
        return ipraktykiDAO.deleteRow(idPraktykiStudenckiej);
    }

    @Override
    public List getPracticesOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        return ipraktykiDAO.getPracticesOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
    }

    @Override
    public List getPracticesOfStudent(Long nrAlbumu) {
        return ipraktykiDAO.getPracticeOfStudent(nrAlbumu);
    }

    @Override
    public List getPracticeOfTutor(int idOpiekunaPraktyk) {
        return ipraktykiDAO.getPracticeOfTutor(idOpiekunaPraktyk);
    }

    @Override
    public List getPracticeOfCourse(int idKierunku) {
        return ipraktykiDAO.getPracticeOfCourse(idKierunku);
    }

}
