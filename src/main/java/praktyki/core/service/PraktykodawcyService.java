package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.*;
import praktyki.core.service.Interface.iPraktykodawcyService;
import praktyki.rest.mvc.helpers.PraktykaInfo;
import praktyki.rest.mvc.helpers.PraktykodawcaInfo;
import praktyki.rest.mvc.helpers.ZgloszenieInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid on 06.01.15.
 */

@Service
@Transactional
public class PraktykodawcyService implements iPraktykodawcyService {

    @Autowired
    private iPraktykodawcyDAO ipraktykodawcyDAO;

    @Autowired
    private iAdresyDAO iadresyDAO;

    @Autowired
    private iProfilDAO iprofilDAO;

    @Autowired
    private iOsobyDAO iosobyDAO;

    @Autowired
    private iProfilePraktykodawcowDAO iprofilePraktykodawcowDAO;

    @Autowired
    private iAdresyPraktykodawcyDAO iadresyPraktykodawcyDAO;

    @Autowired
    private iKoordynatorzyPraktykDAO ikoordynatorzyPraktykDAO;

    @Autowired
    private iOpiekunowieKierunkowiDAO iopiekunowieKierunkowiDAO;

    @Autowired
    private iOpiekunowiePraktykDAO iopiekunowiePraktykDAO;

    @Autowired
    private iZgloszeniaPraktykodawcowDAO izgloszeniaPraktykodawcowDAO;

    @Autowired
    private iStudenciDAO istudenciDAO;

    @Autowired
    private iKierunkiStudiowDAO ikierunkiStudiowDAO;

    @Autowired
    private iPraktykiDAO ipraktykiDAO;

    @Autowired
    private iSzablonyPraktykDAO iszablonyPraktykDAO;

    @Autowired
    private iStatusyDAO istatusyDAO;

    @Autowired
    private iTypyPraktykDAO itypyPraktykDAO;

    @Autowired
    private iLataAkademickieDAO ilataAkademickieDAO;

    @Autowired
    private iPorozumieniaDAO iporozumieniaDAO;

    @Autowired
    private iTypyZgloszenDAO itypyZgloszenDAO;


    @Override
    public Boolean checkNazwa(String nazwa) {
        return ipraktykodawcyDAO.checkNazwa(nazwa);
    }

    @Override
    public Long checkBranch(int idPraktykodawcy, int idAdresu) {
        return iadresyPraktykodawcyDAO.checkBranch(idPraktykodawcy, idAdresu);
    }

    @Override
    public PraktykodawcyEntity addEmployeer(PraktykodawcyEntity praktykodawcyEntity) {
        return ipraktykodawcyDAO.addEmployeer(praktykodawcyEntity);
    }

    @Override
    public PraktykodawcyEntity updateEmployeer(PraktykodawcyEntity praktykodawcyEntity, int idPraktykodawcy) {
        return ipraktykodawcyDAO.updateEmployeer(praktykodawcyEntity, idPraktykodawcy);
    }

    @Override
    public PraktykodawcyEntity editTrustedStatus(int idPraktykodawcy, Boolean zaufany) {
        return ipraktykodawcyDAO.editTrustedStatus(idPraktykodawcy, zaufany);
    }

    @Override
    public PraktykodawcyEntity deleteEmployeer(int idPraktykodawcy) {
        return ipraktykodawcyDAO.deleteEmployeer(idPraktykodawcy);
    }

    @Override
    public PraktykodawcyEntity findEmployeer(int idPraktykodawcy) {
        return ipraktykodawcyDAO.findEmployeer(idPraktykodawcy);
    }

    @Override
    public PraktykodawcaInfo getAllDataOfEmployeer(int idPraktykodawcy) {
        PraktykodawcyEntity employer = ipraktykodawcyDAO.findEmployeer(idPraktykodawcy);
        if (employer !=null) {
            PraktykodawcaInfo employerInfo = new PraktykodawcaInfo();
            employerInfo.setPraktykodawca(employer);
            employerInfo.setAdresy(iadresyDAO.findAddressesOfEmployer(idPraktykodawcy));
            employerInfo.setProfil(iprofilDAO.findProfilesOfEmployeer(idPraktykodawcy));
            employerInfo.setOpiekunowie(iopiekunowiePraktykDAO.getAllTutorsOfEmployer(idPraktykodawcy));
            employerInfo.setPorozumieniaPraktykodawcy(iporozumieniaDAO.getAgreementsOfEmployer(idPraktykodawcy));
            return employerInfo;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykaInfo> getAllPracticesOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        List<PraktykiEntity> list = ipraktykiDAO.getPracticesOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
        List<PraktykaInfo> info = new ArrayList<PraktykaInfo>();
        PraktykaInfo row = new PraktykaInfo();
        for(int i = 0; i<list.size(); i++) {
            PraktykiEntity practice = ipraktykiDAO.getRow(list.get(i).getIdPraktykiStudenckiej());
            if(practice !=null) {
                row.setIdPraktykiStudenckiej(practice.getIdPraktykiStudenckiej());
                row.setSzablonPraktyki(iszablonyPraktykDAO.getTemplate(practice.getIdSzablonu()));
                row.setPraktykodawca(ipraktykodawcyDAO.findEmployeer(practice.getIdPraktykodawcy()));
                row.setStatusPraktyki(istatusyDAO.getStatus(practice.getIdStatusu()));
                row.setKierunek(ikierunkiStudiowDAO.getCourseInfo(practice.getIdKierunku()));
                row.setTypPraktyki(itypyPraktykDAO.getTypeOfTraineeship(practice.getIdTypuPraktyki()));
                row.setRokAkademicki(ilataAkademickieDAO.getYearOfCourse(practice.getIdRokuAkademickiego()));
                if(practice.getNrAlbumu() !=null) {
                    row.setStudent(istudenciDAO.getStudentInfo(practice.getNrAlbumu()));
                }
                row.setOpiekunPraktyki(iopiekunowiePraktykDAO.getTutorInfo(practice.getIdOpiekunaPraktyk()));
                row.setKoordynatorPraktyki(ikoordynatorzyPraktykDAO.getCoordinatorInfo(practice.getIdKoordynatoraPraktyk()));
                row.setAdresPracodawcy(iadresyDAO.findByIdAdresu(practice.getIdAdresu()));
                if(practice.getIdPorozumienia() !=null) {
                    row.setPorozumienie(iporozumieniaDAO.getAgreement(practice.getIdPorozumienia()));
                }
                info.add(i, row);
            } else {
                return null;
            }
        }
        return info;
    }

    @Override
    public List<PraktykaInfo> getAllPracticesOfEmployerByTutor(int idPraktykodawcy, int idOpiekunaPraktyk) {
        List<PraktykiEntity> list = ipraktykiDAO.getPracticeOfEmployerByTutor(idPraktykodawcy, idOpiekunaPraktyk);
        List<PraktykaInfo> info = new ArrayList<PraktykaInfo>();
        PraktykaInfo row = new PraktykaInfo();
        for(int i = 0; i<list.size(); i++) {
            PraktykiEntity practice = ipraktykiDAO.getRow(list.get(i).getIdPraktykiStudenckiej());
            if(practice !=null) {
                row.setIdPraktykiStudenckiej(practice.getIdPraktykiStudenckiej());
                row.setSzablonPraktyki(iszablonyPraktykDAO.getTemplate(practice.getIdSzablonu()));
                row.setPraktykodawca(ipraktykodawcyDAO.findEmployeer(practice.getIdPraktykodawcy()));
                row.setStatusPraktyki(istatusyDAO.getStatus(practice.getIdStatusu()));
                row.setKierunek(ikierunkiStudiowDAO.getCourseInfo(practice.getIdKierunku()));
                row.setTypPraktyki(itypyPraktykDAO.getTypeOfTraineeship(practice.getIdTypuPraktyki()));
                row.setRokAkademicki(ilataAkademickieDAO.getYearOfCourse(practice.getIdRokuAkademickiego()));
                if(practice.getNrAlbumu() !=null) {
                    row.setStudent(istudenciDAO.getStudentInfo(practice.getNrAlbumu()));
                }
                row.setOpiekunPraktyki(iopiekunowiePraktykDAO.getTutorInfo(practice.getIdOpiekunaPraktyk()));
                row.setKoordynatorPraktyki(ikoordynatorzyPraktykDAO.getCoordinatorInfo(practice.getIdKoordynatoraPraktyk()));
                row.setAdresPracodawcy(iadresyDAO.findByIdAdresu(practice.getIdAdresu()));
                if(practice.getIdPorozumienia() !=null) {
                    row.setPorozumienie(iporozumieniaDAO.getAgreement(practice.getIdPorozumienia()));
                }
                info.add(i, row);
            } else {
                return null;
            }
        }
        return info;
    }

    @Override
    public List<ZgloszenieInfo> getAllApplicationsOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        List<ZgloszeniaPraktykodawcowEntity> list = izgloszeniaPraktykodawcowDAO.getApplicationsOfEmployer(idPraktykodawcy, idKierunku, idRokuAkademickiego);
        List<ZgloszenieInfo> info = new ArrayList<ZgloszenieInfo>();
        ZgloszenieInfo row = new ZgloszenieInfo();
        for(int i = 0; i<list.size(); i++) {
            ZgloszeniaPraktykodawcowEntity application = izgloszeniaPraktykodawcowDAO.getApplication(list.get(i).getIdZgloszeniaPraktykodawcy());
            if (application !=null) {
                row.setIdZgloszeniaPraktykodawcy(application.getIdZgloszeniaPraktykodawcy());
                row.setDataZgloszenia(application.getDataZgloszenia());
                row.setOswiadczeniePraktykodawcy(application.getOswiadczeniePraktykodawcy());
                row.setDataRozpoczecia(application.getDataRozpoczecia());
                row.setDataZakonczenia(application.getDataZakonczenia());
                row.setDecyzja(application.getDecyzja());
                row.setPraktykodawca(ipraktykodawcyDAO.findEmployeer(application.getIdPraktykodawcy()));
                row.setStudent(istudenciDAO.getStudentInfo(application.getNrAlbumu()));
                row.setTypZgloszenia(itypyZgloszenDAO.getRequest(application.getIdTypuZgloszenia()));
                row.setKierunek(ikierunkiStudiowDAO.getCourseInfo(application.getIdKierunku()));
                row.setRok(ilataAkademickieDAO.getYearOfCourse(application.getIdRokuAkademickiego()));
                info.add(i, row);
            } else {
                return null;
            }
        }
        return info;
    }


    @Override
    public List<PraktykodawcyEntity> findEveryEmployeer() {
        return ipraktykodawcyDAO.findEveryEmployeer();
    }

    @Override
    public AdresyEntity addAddress(int idPraktykodawcy, Boolean oddzial, AdresyEntity adresyEntity) {
        if(iadresyPraktykodawcyDAO.getRow(adresyEntity.getIdAdresu(), adresyEntity.getIdAdresu()) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie w bazie");
        } else {
            PraktykodawcyEntity employer = ipraktykodawcyDAO.findEmployeer(idPraktykodawcy);
            if (employer != null) {
                if (oddzial == true || oddzial == false) {
                    AdresyEntity newaddress = iadresyDAO.addAddress(adresyEntity);
                    AdresyPraktykodawcyEntity relation = new AdresyPraktykodawcyEntity();
                    relation.setIdAdresu(newaddress.getIdAdresu());
                    relation.setIdPraktykodawcy(idPraktykodawcy);
                    relation.setOddzial(oddzial);
                    iadresyPraktykodawcyDAO.addRelation(relation);
                    return adresyEntity;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    @Override
    public List<AdresyEntity> findAddresses(int idPraktykodawcy) {
        return iadresyDAO.findAddressesOfEmployer(idPraktykodawcy);
    }

    @Override
    public List<AdresyEntity> getAddress(int idPraktykodawcy, int idAdresu) {
        return iadresyDAO.getAddressOfEmployer(idPraktykodawcy, idAdresu);
    }

    @Override
    public Long deleteEveryAddressOfEmployer(int idPraktykodawcy) {
        return iadresyPraktykodawcyDAO.deleteEveryAddressFromEmployer(idPraktykodawcy);
    }

    @Override
    public Long deleteAddressOfEmployer(int idPraktykodawcy, int idAdresu) {
        return iadresyPraktykodawcyDAO.deleteAddressFromEmployer(idPraktykodawcy, idAdresu);
    }

    @Override
    public ProfilEntity addProfile(int idPraktykodawcy, ProfilEntity profilEntity) {
        ProfilEntity newProfile = iprofilDAO.addProfile(profilEntity);
        Integer idProfilu = newProfile.getIdProfilu();
        ProfilePraktykodawcowEntity relation = new ProfilePraktykodawcowEntity();
        relation.setIdPraktykodawcy(idPraktykodawcy);
        relation.setIdProfilu(idProfilu);
        iprofilePraktykodawcowDAO.addRelation(relation);
        return newProfile;
    }

    @Override
    public ProfilEntity updateProfile(int idPraktykodawcy, Integer idProfilu, ProfilEntity profilEntity) {
        PraktykodawcyEntity employeer = ipraktykodawcyDAO.findEmployeer(idPraktykodawcy);
        if (employeer !=null) {
            return iprofilDAO.updateProfile(idProfilu, profilEntity);
        }
        else {
            return null;
        }
    }

    @Override
    public List<ProfilEntity> findProfilesOfEmployeer(int idPraktykodawcy) {
        return iprofilDAO.findProfilesOfEmployeer(idPraktykodawcy);
    }

    @Override
    public List<ProfilEntity> getProfileOfEmployeer(Integer idProfilu, int idPraktykodawcy) {
        return iprofilDAO.getProfileOfEmployeer(idProfilu, idPraktykodawcy);
    }

    @Override
    public Long deleteEveryProfileOfEmployeer(int idPraktykodawcy) {
        return iprofilePraktykodawcowDAO.deleteEveryProfileFromEmployeer(idPraktykodawcy);
    }

    @Override
    public Long deleteProfileOfEmployeer(int idPraktykodawcy, int idProfilu) {
        return iprofilePraktykodawcowDAO.deleteProfileFromEmployeer(idPraktykodawcy, idProfilu);
    }

    @Override
    public List<KoordynatorzyPraktykEntity> findEveryCoordinatorOfEmployer(int idPraktykodawcy) {
        return ikoordynatorzyPraktykDAO.findEveryCoordinatorOfEmployer(idPraktykodawcy);
    }

    @Override
    public List<OsobyEntity> getCoordinatorOfEmployer(int idPraktykodawcy, int idKoordynatoraPraktyk) {
        return iosobyDAO.getCoordinatorOfEmployer(idPraktykodawcy, idKoordynatoraPraktyk);
    }

    @Override
    public OpiekunowieKierunkowiEntity addTutorCourse(OpiekunowieKierunkowiEntity opiekunowieKierunkowiEntity) {
        if (iopiekunowieKierunkowiDAO.getRow(opiekunowieKierunkowiEntity.getIdKierunku(), opiekunowieKierunkowiEntity.getIdOpiekunaPraktyk(), opiekunowieKierunkowiEntity.getIdPraktykodawcy()) !=null) {
            throw new IllegalArgumentException("dana relacja juz znajduje sie w bazie");
        } else {
            return iopiekunowieKierunkowiDAO.addRelation(opiekunowieKierunkowiEntity);
        }
    }

    @Override
    public List<OpiekunowiePraktykEntity> getAllTutorsOfEmployee(int idPraktykodawcy) {
        return iopiekunowiePraktykDAO.getAllTutorsOfEmployer(idPraktykodawcy);
    }

    @Override
    public List<OpiekunowiePraktykEntity> getTutorOfEmployer(int idPraktykodawcy, int idOpiekunaPraktyk) {
        return iopiekunowiePraktykDAO.getTutorOfEmployer(idPraktykodawcy, idOpiekunaPraktyk);
    }

    @Override
    public Long deleteTutorOfEmployer(int idPraktykodawcy, int idOpiekunaPraktyk) {
        return iopiekunowieKierunkowiDAO.deleteTutorFromEmployer(idPraktykodawcy, idOpiekunaPraktyk);
    }

    @Override
    public List<PraktykodawcyEntity> getEmployerByName(String searchString, String sortType) {
        return ipraktykodawcyDAO.getEmployerByName(searchString, sortType);
    }

    @Override
    public List<StudenciEntity> getStudentsOfEmployerByCourseAcademicYear(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        return ipraktykiDAO.getStudentsOfEmployerByCourseAcademicYear(idPraktykodawcy, idKierunku, idRokuAkademickiego);
    }
}
