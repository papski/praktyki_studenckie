package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iTypyZgloszenDAO;
import praktyki.core.entities.TypyZgloszenEntity;
import praktyki.core.service.Interface.iTypyZgloszenService;

import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */

@Service
@Transactional
public class TypyZgloszenService implements iTypyZgloszenService {

    @Autowired
    private iTypyZgloszenDAO itypZgloszeniaDAO;

    @Override
    public TypyZgloszenEntity addRequest(TypyZgloszenEntity typyZgloszenEntity) {
        return itypZgloszeniaDAO.addRequest(typyZgloszenEntity);
    }

    @Override
    public TypyZgloszenEntity updateRequest(Integer idTypuZgloszenia, TypyZgloszenEntity typyZgloszenEntity) {
        return itypZgloszeniaDAO.updateRequest(idTypuZgloszenia, typyZgloszenEntity);
    }

    @Override
    public TypyZgloszenEntity deleteRequest(Integer idTypuZgloszenia) {
        return itypZgloszeniaDAO.deleteRequest(idTypuZgloszenia);
    }

    @Override
    public List<TypyZgloszenEntity> findAll() {
        return itypZgloszeniaDAO.findAll();
    }

    @Override
    public TypyZgloszenEntity getRequest(Integer idTypuZgloszenia) {
        return itypZgloszeniaDAO.getRequest(idTypuZgloszenia);
    }
}
