package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iTypyPraktykDAO;
import praktyki.core.entities.TypyPraktykEntity;
import praktyki.core.service.Interface.iTypyPraktykService;

import java.util.List;

/**
 * Created by dawid on 05.02.15.
 */

@Service
@Transactional
public class TypyPraktykService implements iTypyPraktykService {

    @Autowired
    private iTypyPraktykDAO itypyPraktykDAO;

    @Override
    public TypyPraktykEntity addTypeOfTraineeship(TypyPraktykEntity typyPraktykEntity) {
        return itypyPraktykDAO.addTypeOfTraineeship(typyPraktykEntity);
    }

    @Override
    public TypyPraktykEntity updateTypeOfTraineeship(Integer idTypuPraktyki, TypyPraktykEntity typyPraktykEntity) {
        return itypyPraktykDAO.updateTypeOfTraineeship(idTypuPraktyki, typyPraktykEntity);
    }

    @Override
    public TypyPraktykEntity deleteTypeOfTraineeship(Integer idTypuPraktyki) {
        return itypyPraktykDAO.deleteTypeOfTraineeship(idTypuPraktyki);
    }

    @Override
    public List<TypyPraktykEntity> findAll() {
        return itypyPraktykDAO.findAll();
    }

    @Override
    public TypyPraktykEntity getTypeOfTraineeship(Integer idTypuPraktyki) {
        return itypyPraktykDAO.getTypeOfTraineeship(idTypuPraktyki);
    }
}
