package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iUzytkownicyDAO;
import praktyki.core.entities.UzytkownicyEntity;
import praktyki.core.service.Interface.iUzytkownicyService;

import java.util.List;

/**
 * Created by dawid on 25.03.15.
 */

@Service
@Transactional
public class UzytkownicyService implements iUzytkownicyService {

    @Autowired
    private iUzytkownicyDAO iUzytkownicyDAO;

    @Override
    public UzytkownicyEntity editBlockade(int idUzytkownika, Boolean blokada) {
        return iUzytkownicyDAO.editBlockade(idUzytkownika, blokada);
    }

    @Override
    public UzytkownicyEntity getUser(int idUzytkownika) {
        return iUzytkownicyDAO.getUser(idUzytkownika);
    }

    @Override
    public List<UzytkownicyEntity> findAll() {
        return iUzytkownicyDAO.findAll();
    }

    @Override
    public List<UzytkownicyEntity> getUserByLogin(String searchString, String sortType) {
        return iUzytkownicyDAO.getUserByLogin(searchString, sortType);
    }
}
