package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iPraktykodawcyDAO;
import praktyki.core.DAO.Interface.iProfilDAO;
import praktyki.core.DAO.Interface.iProfilePraktykodawcowDAO;
import praktyki.core.entities.PraktykodawcyEntity;
import praktyki.core.entities.ProfilEntity;
import praktyki.core.entities.ProfilePraktykodawcowEntity;
import praktyki.core.service.Interface.iProfilService;

import java.util.List;

/**
 * Created by dawid on 25.01.15.
 */

@Service
@Transactional
public class ProfilService implements iProfilService {

    @Autowired
    private iProfilDAO iprofilDAO;

    @Autowired
    private iProfilePraktykodawcowDAO iprofilePraktykodawcowDAO;

    @Autowired
    private iPraktykodawcyDAO ipraktykodawcyDAO;

    @Override
    public ProfilEntity addProfile(ProfilEntity profilEntity) {
        return iprofilDAO.addProfile(profilEntity);
    }

    @Override
    public ProfilEntity updateProfile(Integer idProfilu, ProfilEntity profilEntity) {
        return iprofilDAO.updateProfile(idProfilu, profilEntity);
    }

    @Override
    public ProfilEntity deleteProfile(Integer idProfilu) {
        return iprofilDAO.deleteProfile(idProfilu);
    }

    @Override
    public List<ProfilEntity> findAll() {
        return iprofilDAO.findAll();
    }

    @Override
    public ProfilEntity getProfile(Integer idProfilu) {
        return iprofilDAO.getProfile(idProfilu);
    }

    @Override
    public ProfilePraktykodawcowEntity addRelation(Integer idProfilu, Integer idPraktykodawcy) {
        ProfilePraktykodawcowEntity relation = new ProfilePraktykodawcowEntity();
        relation.setIdProfilu(idProfilu);
        relation.setIdPraktykodawcy(idPraktykodawcy);
        return iprofilePraktykodawcowDAO.addRelation(relation);
    }

    @Override
    public List<PraktykodawcyEntity> findEmployeerOfProfile(int idProfilu) {
        return ipraktykodawcyDAO.findEmployeerOfProfile(idProfilu);
    }

    @Override
    public List<PraktykodawcyEntity> getEmployeerOfProfile(int idPraktykodawcy, int idProfilu) {
        return ipraktykodawcyDAO.getEmployeerOfProfile(idPraktykodawcy, idProfilu);
    }

}
