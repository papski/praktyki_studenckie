package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.iKategorieKwalifikacjiDAO;
import praktyki.core.entities.KategorieKwalifikacjiEntity;
import praktyki.core.service.Interface.iKategorieKwalifikacjiService;

import java.util.List;

/**
 * Created by dawid on 27.03.15.
 */

@Service
@Transactional
public class KategorieKwalifikacjiService implements iKategorieKwalifikacjiService {

    @Autowired
    private  iKategorieKwalifikacjiDAO iKategorieKwalifikacjiDAO;

    @Override
    public KategorieKwalifikacjiEntity addCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        return iKategorieKwalifikacjiDAO.addCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
    }

    @Override
    public KategorieKwalifikacjiEntity addSubCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        return iKategorieKwalifikacjiDAO.addSubCategoryOfQualification(kategorieKwalifikacjiEntity, idKwalifikacji);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findCategoryByName(String nazwaKategorii) {
        return iKategorieKwalifikacjiDAO.findCategoryByName(nazwaKategorii);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findCategory() {
        return iKategorieKwalifikacjiDAO.findCategory();
    }

    @Override
    public KategorieKwalifikacjiEntity getCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.getCategory(idKategoriiKwalifikacji);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategoryByName(String nazwaKategorii) {
        return iKategorieKwalifikacjiDAO.findSubCategoryByName(nazwaKategorii);
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategory() {
        return iKategorieKwalifikacjiDAO.findSubCategory();
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategoryOfCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.findSubCategoryOfCategory(idKategoriiKwalifikacji);
    }

    @Override
    public KategorieKwalifikacjiEntity getSubCategoryOfCategory(int idKategoriiKwalifikacji) {
        return iKategorieKwalifikacjiDAO.getSubCategoryOfCategory(idKategoriiKwalifikacji);
    }
}
