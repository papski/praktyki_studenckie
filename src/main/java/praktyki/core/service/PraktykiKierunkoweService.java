package praktyki.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.PraktykiKierunkoweEntity;
import praktyki.core.service.Interface.iPraktykiKierunkoweService;
import praktyki.rest.mvc.helpers.PraktykiKierunkoweInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid on 08.03.15.
 */

@Service
@Transactional
public class PraktykiKierunkoweService implements iPraktykiKierunkoweService {

    @Autowired
    private iPraktykiKierunkoweDAO ipraktykiKierunkoweDAO;

    @Autowired
    private iKierunkiStudiowDAO ikierunkiStudiowDAO;

    @Autowired
    private iTypyPraktykDAO itypyPraktykDAO;

    @Autowired
    private iLataAkademickieDAO ilataAkademickieDAO;

    @Autowired
    private iJednostkiKalendarzoweDAO ijednostkiKalendarzoweDAO;

    @Override
    public PraktykiKierunkoweEntity addRelation(PraktykiKierunkoweEntity praktykiKierunkoweEntity) {
        if(ipraktykiKierunkoweDAO.getRow(praktykiKierunkoweEntity.getIdKierunku(), praktykiKierunkoweEntity.getIdTypuPraktyki(), praktykiKierunkoweEntity.getIdRokuAkademickiego()) !=null) {
            throw new IllegalArgumentException("Praktyka dla danego kierunku i o danym typie praktyki juz sie znajduje w bazie");
        } else {
            return ipraktykiKierunkoweDAO.addRelation(praktykiKierunkoweEntity);
        }
    }

    @Override
    public List<PraktykiKierunkoweEntity> findAll() {
        return ipraktykiKierunkoweDAO.findAll();
    }

    @Override
    public List<PraktykiKierunkoweInfo> getRelation(int idKierunku, int idRokuAkademickiego, int idTypuPraktyki) {
        List<PraktykiKierunkoweEntity> list = ipraktykiKierunkoweDAO.getRelation(idKierunku, idRokuAkademickiego, idTypuPraktyki);
        List<PraktykiKierunkoweInfo> info = new ArrayList<PraktykiKierunkoweInfo>();
        PraktykiKierunkoweInfo row = new PraktykiKierunkoweInfo();
        for (int i = 0 ; i < list.size() ; i++) {
            row.setKierunekStudiow(ikierunkiStudiowDAO.getCourseInfo(list.get(i).getIdKierunku()));
            row.setTypPraktyki(itypyPraktykDAO.getTypeOfTraineeship(list.get(i).getIdTypuPraktyki()));
            row.setRokAkademicki(ilataAkademickieDAO.getYearOfCourse(list.get(i).getIdRokuAkademickiego()));
            row.setMinDataRozpoczecia(list.get(i).getMinDataRozpoczecia());
            row.setMaxDataZakonczenia(list.get(i).getMaxDataZakonczenia());
            row.setLiczbaGodzin(list.get(i).getLiczbaGodzin());
            row.setCzasTrwania(list.get(i).getCzasTrwania());
            row.setJednostkaKalendarzowa(ijednostkiKalendarzoweDAO.getRow(list.get(i).getIdJednostki()));
            info.add(i, row);
        }
        return info;
    }

    @Override
    public PraktykiKierunkoweEntity editRelation(int idKierunku, int idTypuPraktyki, int idRokuAkademickiego, PraktykiKierunkoweEntity praktykiKierunkoweEntity) {
        return ipraktykiKierunkoweDAO.editRow(idKierunku, idTypuPraktyki, idRokuAkademickiego, praktykiKierunkoweEntity);
    }

    @Override
    public Long deleteRelation(int idKierunku, int idRokuAkademickiego, int idTypyPraktyki) {
        return ipraktykiKierunkoweDAO.deleteRelation(idKierunku, idRokuAkademickiego, idTypyPraktyki);
    }
}
