package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iStopnieStudiowDAO;
import praktyki.core.entities.StopnieStudiowEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 21.02.15.
 */
@Repository
public class StopnieStudiowDAO implements iStopnieStudiowDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public StopnieStudiowEntity getDegree(int idStopniaStudiow) {
        Query query = em.createQuery("SELECT s FROM StopnieStudiowEntity s JOIN s.kierunkiByIdStopnia ks WHERE ks.idStopniaStudiow=?1");
        query.setParameter(1, idStopniaStudiow);
        List list = query.getResultList();
        StopnieStudiowEntity row;
        if (!list.isEmpty()) {
            return row = (StopnieStudiowEntity) list.get(0);
        } else {
            return null;
        }
    }
}
