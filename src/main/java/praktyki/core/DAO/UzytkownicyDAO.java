package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iUzytkownicyDAO;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.entities.RoleEntity;
import praktyki.core.entities.UzytkownicyEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 10.02.15.
 */

@Repository
public class UzytkownicyDAO implements iUzytkownicyDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public UzytkownicyEntity addUser(UzytkownicyEntity uzytkownicyEntity, Integer idOsoby, Integer idRoli) {
        OsobyEntity person = em.find(OsobyEntity.class, idOsoby);
        if (person !=null) {
            RoleEntity role = em.find(RoleEntity.class, idRoli);
            if (role !=null) {
                em.persist(uzytkownicyEntity);
                uzytkownicyEntity.setIdOsoby(idOsoby);
                uzytkownicyEntity.setUzytkownikByIdOsoby(person);
                person.setUzytkownik(uzytkownicyEntity);
                uzytkownicyEntity.setIdRoli(idRoli);
                uzytkownicyEntity.setUzytkownikByIdRoli(role);
                Collection<UzytkownicyEntity> user = new HashSet<UzytkownicyEntity>();
                user.add(uzytkownicyEntity);
                role.setUzytkownik(user);
                return uzytkownicyEntity;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public UzytkownicyEntity getUser(int idUzytkownika) {
        return em.find(UzytkownicyEntity.class, idUzytkownika);
    }

    @Override
    public List<UzytkownicyEntity> getUserByLogin(String searchString, String sortType) {
        Query query = em.createQuery("SELECT u, r FROM UzytkownicyEntity u JOIN u.uzytkownikByIdRoli r WHERE u.login LIKE :searchString ORDER BY u.login "+sortType);
        query.setParameter("searchString", "%" +searchString +"%");
        return query.getResultList();
    }

    @Override
    public UzytkownicyEntity editBlockade(int idUzytkownika, Boolean blokada) {
        if(blokada == true || blokada == false){
            UzytkownicyEntity user = em.find(UzytkownicyEntity.class, idUzytkownika);
            if (user !=null) {
                user.setIdUzytkownika(idUzytkownika);
                user.setBlokada(blokada);
                return em.merge(user);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public UzytkownicyEntity getRowByLogin(String login) {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u WHERE u.login=?1");
        query.setParameter(1, login);
        UzytkownicyEntity user = new UzytkownicyEntity();
        if(!query.getResultList().isEmpty()) {
            return user = (UzytkownicyEntity) query.getResultList().get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<UzytkownicyEntity> findAll() {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u ORDER BY u.idUzytkownika ASC");
        return query.getResultList();
    }

    @Override
    public List<UzytkownicyEntity> getInfoOfStudent(Integer idOsoby) {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u JOIN u.uzytkownikByIdOsoby o JOIN o.student s WHERE s.idOsoby=?1");
        query.setParameter(1, idOsoby);
        return query.getResultList();
    }

    @Override
    public List<UzytkownicyEntity> getInfoOfCoordinator(Integer idOsoby) {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u JOIN u.uzytkownikByIdOsoby o JOIN o.koordynator k WHERE k.idOsoby=?1");
        query.setParameter(1, idOsoby);
        return query.getResultList();
    }

    @Override
    public List<UzytkownicyEntity> getInfoOfTutor(Integer idOsoby) {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u JOIN u.uzytkownikByIdOsoby o Join o.opiekun oo WHERE oo.idOsoby=?1");
        query.setParameter(1, idOsoby);
        return query.getResultList();
    }

    @Override
    public Boolean checkForExistence(Integer idOsoby) {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u WHERE u.idOsoby=?1");
        query.setParameter(1, idOsoby);
        if (query.getResultList().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Long checkLogin(String login) {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u WHERE u.login=?1");
        query.setParameter(1, login);
        if (query.getResultList().isEmpty()) {
            return new Long(1);
        } else {
            return new Long(-1);
        }
    }

    @Override
    public Boolean isLoginUsed(String login) {
        Query query = em.createQuery("SELECT u FROM UzytkownicyEntity u WHERE u.login=?1");
        query.setParameter(1, login);
        if (query.getResultList().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

}
