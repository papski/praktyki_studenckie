package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iProfilDAO;
import praktyki.core.entities.ProfilEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 25.01.15.
 */

@Repository
public class ProfilDAO implements iProfilDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public ProfilEntity addProfile(ProfilEntity profilEntity) {
        em.persist(profilEntity);
        return profilEntity;
    }

    @Override
    public ProfilEntity updateProfile(Integer idProfilu, ProfilEntity profilEntity) {
        ProfilEntity profil = em.find(ProfilEntity.class, idProfilu);
        if (profil !=null) {
            profil.setIdProfilu(idProfilu);
            profil.setBranza(profilEntity.getBranza());
            return em.merge(profil);
        }
        else {
            return null;
        }
    }

    @Override
    public ProfilEntity deleteProfile(Integer idProfilu) {
        ProfilEntity profil = em.find(ProfilEntity.class, idProfilu);
        if (profil !=null) {
            Query query = em.createQuery("DELETE ProfilePraktykodawcowEntity pp WHERE pp.idProfilu=?1");
            query.setParameter(1, idProfilu);
            query.executeUpdate();
            em.remove(profil);
            return profil;
        }
        else {
            return null;
        }
    }

    @Override
    public List<ProfilEntity> findAll() {
        Query query = em.createQuery("SELECT p FROM ProfilEntity p ORDER BY idProfilu ASC");
        return query.getResultList();
    }

    @Override
    public ProfilEntity getProfile(Integer idProfilu) {
        return em.find(ProfilEntity.class, idProfilu);
    }

    @Override
    public List<ProfilEntity> findProfilesOfEmployeer(int idPraktykodawcy) {
        Query query = em.createQuery("SELECT p FROM ProfilEntity p JOIN p.praktykodawcyByIdProfilu pp WHERE pp.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        return query.getResultList();
    }

    @Override
    public List<ProfilEntity> getProfileOfEmployeer(Integer idProfilu, int idPraktykodawcy) {
        Query query = em.createQuery("SELECT p FROM ProfilEntity p JOIN p.praktykodawcyByIdProfilu pp WHERE pp.idProfilu=?1 AND pp.idPraktykodawcy=?2");
        query.setParameter(1, idProfilu);
        query.setParameter(2, idPraktykodawcy);
        return query.getResultList();
    }
}
