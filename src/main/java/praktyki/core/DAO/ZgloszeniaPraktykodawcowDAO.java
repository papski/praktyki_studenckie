package praktyki.core.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iStudenciKierunkowDAO;
import praktyki.core.DAO.Interface.iZgloszeniaPraktykodawcowDAO;
import praktyki.core.entities.PraktykodawcyEntity;
import praktyki.core.entities.StudenciKierunkowEntity;
import praktyki.core.entities.TypyZgloszenEntity;
import praktyki.core.entities.ZgloszeniaPraktykodawcowEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 11.03.15.
 */

@Repository
public class ZgloszeniaPraktykodawcowDAO implements iZgloszeniaPraktykodawcowDAO {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private iStudenciKierunkowDAO istudenciKierunkowDAO;

    @Override
    public ZgloszeniaPraktykodawcowEntity addEmployerApplication(ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity) {
        StudenciKierunkowEntity student = istudenciKierunkowDAO.getRow(zgloszeniaPraktykodawcowEntity.getNrAlbumu(), zgloszeniaPraktykodawcowEntity.getIdKierunku(), zgloszeniaPraktykodawcowEntity.getIdRokuAkademickiego());
        if (student !=null) {
            PraktykodawcyEntity employer = em.find(PraktykodawcyEntity.class, zgloszeniaPraktykodawcowEntity.getIdPraktykodawcy());
            if (employer !=null) {
                TypyZgloszenEntity type = em.find(TypyZgloszenEntity.class, zgloszeniaPraktykodawcowEntity.getIdTypuZgloszenia());
                if (type !=null) {
                    Calendar calendar = Calendar.getInstance();
                    java.sql.Timestamp dataZgloszenia = new java.sql.Timestamp(calendar.getTime().getTime());
                    zgloszeniaPraktykodawcowEntity.setDataZgloszenia(dataZgloszenia);
                    em.persist(zgloszeniaPraktykodawcowEntity);
                    zgloszeniaPraktykodawcowEntity.setPraktykodawca(employer);
                    zgloszeniaPraktykodawcowEntity.setStudentKierunku(student);
                    zgloszeniaPraktykodawcowEntity.setTypZgloszenia(type);
                    Collection<ZgloszeniaPraktykodawcowEntity> relation = new HashSet<ZgloszeniaPraktykodawcowEntity>();
                    relation.add(zgloszeniaPraktykodawcowEntity);
                    student.setZgloszenie(zgloszeniaPraktykodawcowEntity);
                    employer.setZgloszenie(relation);
                    type.setZgloszenie(relation);
                    return zgloszeniaPraktykodawcowEntity;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<ZgloszeniaPraktykodawcowEntity> findAll() {
        Query query = em.createQuery("SELECT zp FROM ZgloszeniaPraktykodawcowEntity zp ORDER BY zp.idZgloszeniaPraktykodawcy ASC");
        return query.getResultList();
    }

    @Override
    public ZgloszeniaPraktykodawcowEntity getApplication(Long idZgloszeniaPraktykodawcy) {
        return em.find(ZgloszeniaPraktykodawcowEntity.class, idZgloszeniaPraktykodawcy);
    }

    @Override
    public ZgloszeniaPraktykodawcowEntity editApplication(Long idZgloszeniaPraktykodawcy, ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity) {
        ZgloszeniaPraktykodawcowEntity application = em.find(ZgloszeniaPraktykodawcowEntity.class, idZgloszeniaPraktykodawcy);
        if (application !=null) {
            application.setIdZgloszeniaPraktykodawcy(idZgloszeniaPraktykodawcy);
            if (zgloszeniaPraktykodawcowEntity.getDecyzja() !=null) {
                application.setDecyzja(zgloszeniaPraktykodawcowEntity.getDecyzja());
            }
            return em.merge(application);
        } else {
            return null;
        }
    }

    @Override
    public Long deleteApplication(Long idZgloszeniaPraktykodawcy) {
        Query query = em.createQuery("DELETE ZgloszeniaPraktykodawcowEntity zp WHERE zp.idZgloszeniaPraktykodawcy=?1");
        query.setParameter(1, idZgloszeniaPraktykodawcy);
        int temp = query.executeUpdate();
        Long updated = Long.valueOf(temp);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }

    @Override
    public List<ZgloszeniaPraktykodawcowEntity> getApplicationOfStudent(Long nrAlbumu) {
        Query query = em.createQuery("SELECT zp FROM ZgloszeniaPraktykodawcowEntity zp WHERE zp.nrAlbumu=?1");
        query.setParameter(1, nrAlbumu);
        return query.getResultList();
    }

    @Override
    public List<ZgloszeniaPraktykodawcowEntity> checkForStudent(ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity) {
        Query query = em.createQuery("SELECT zp FROM ZgloszeniaPraktykodawcowEntity zp WHERE zp.nrAlbumu=?1 AND zp.idKierunku=?2 AND zp.idRokuAkademickiego=?3");
        query.setParameter(1, zgloszeniaPraktykodawcowEntity.getNrAlbumu());
        query.setParameter(2, zgloszeniaPraktykodawcowEntity.getIdKierunku());
        query.setParameter(3, zgloszeniaPraktykodawcowEntity.getIdRokuAkademickiego());
        return query.getResultList();
    }

    @Override
    public List<ZgloszeniaPraktykodawcowEntity> getApplicationsOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        if(idPraktykodawcy > 0 && idKierunku > 0 && idRokuAkademickiego > 0 ) {
            Query query = em.createQuery("SELECT zp FROM ZgloszeniaPraktykodawcowEntity zp WHERE zp.idPraktykodawcy=?1 AND zp.idKierunku=?2 AND zp.idRokuAkademickiego=?3");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idKierunku);
            query.setParameter(3, idRokuAkademickiego);
            return query.getResultList();
        } else if (idPraktykodawcy > 0 && idKierunku > 0 && idRokuAkademickiego == 0) {
            Query query = em.createQuery("SELECT zp FROM ZgloszeniaPraktykodawcowEntity zp WHERE zp.idPraktykodawcy=?1 AND zp.idKierunku=?2");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idKierunku);
            return query.getResultList();
        } else if(idPraktykodawcy > 0 && idKierunku == 0 && idRokuAkademickiego > 0) {
            Query query = em.createQuery("SELECT zp FROM ZgloszeniaPraktykodawcowEntity zp WHERE zp.idPraktykodawcy=?1 AND zp.idRokuAkademickiego=?2");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idRokuAkademickiego);
            return query.getResultList();
        } else if (idPraktykodawcy > 0 && idKierunku == 0 && idRokuAkademickiego == 0) {
            Query query = em.createQuery("SELECT zp FROM ZgloszeniaPraktykodawcowEntity zp WHERE zp.idPraktykodawcy=?1");
            query.setParameter(1, idPraktykodawcy);
            return query.getResultList();
        } else {
            return null;
        }
    }
}
