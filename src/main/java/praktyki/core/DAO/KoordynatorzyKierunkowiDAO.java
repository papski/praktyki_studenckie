package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iKoordynatorzyKierunkowiDAO;
import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.core.entities.KoordynatorzyKierunkowiEntity;
import praktyki.core.entities.KoordynatorzyPraktykEntity;
import praktyki.rest.mvc.helpers.KoordynatorzyDate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 22.01.15.
 */

@Repository
public class KoordynatorzyKierunkowiDAO implements iKoordynatorzyKierunkowiDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public KoordynatorzyKierunkowiEntity addRelation(KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity) {
        int idKierunku = koordynatorzyKierunkowiEntity.getIdKierunku();
        int idKoordynatoraPraktyk = koordynatorzyKierunkowiEntity.getIdKoordynatoraPraktyk();
        KoordynatorzyPraktykEntity coordinator = em.find(KoordynatorzyPraktykEntity.class, idKoordynatoraPraktyk);
        if (coordinator !=null) {
            KierunkiStudiowEntity course = em.find(KierunkiStudiowEntity.class, idKierunku);
            if (course !=null) {
                em.persist(koordynatorzyKierunkowiEntity);
                java.util.Date date = new java.util.Date();
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                koordynatorzyKierunkowiEntity.setDataPowolania(sqlDate);
                koordynatorzyKierunkowiEntity.setKierunekByIdKoordynatora(course);
                koordynatorzyKierunkowiEntity.setKoordynatorzyByIdKierunku(coordinator);
                Collection<KoordynatorzyKierunkowiEntity> coordinator2 = new HashSet<KoordynatorzyKierunkowiEntity>();
                coordinator2.add(koordynatorzyKierunkowiEntity);
                coordinator.setKoordynatorzyByIdKierunku(coordinator2);
                Collection<KoordynatorzyKierunkowiEntity> course2 = new HashSet<KoordynatorzyKierunkowiEntity>();
                course2.add(koordynatorzyKierunkowiEntity);
                course.setKoordynatorByIdKierunku(course2);
                return koordynatorzyKierunkowiEntity;
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    @Override
    public List<KoordynatorzyKierunkowiEntity> findAll() {
        Query query = em.createQuery("SELECT kk FROM KoordynatorzyKierunkowiEntity kk ORDER BY kk.idKierunku ASC ");
        return query.getResultList();
    }

    @Override
    public KoordynatorzyKierunkowiEntity getRow(int idKoordynatoraPraktyk, int idKierunku) {
        Query query = em.createQuery("SELECT kk FROM KoordynatorzyKierunkowiEntity kk WHERE kk.idKoordynatoraPraktyk=?1 AND kk.idKierunku=?2");
        query.setParameter(1, idKoordynatoraPraktyk);
        query.setParameter(2, idKierunku);
        List list = query.getResultList();
        KoordynatorzyKierunkowiEntity row = new KoordynatorzyKierunkowiEntity();
        if(!list.isEmpty()) {
            return row = (KoordynatorzyKierunkowiEntity)list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public KoordynatorzyDate getDate(int idKoordynatoraPraktyk, int idKierunku) {
        Query query = em.createQuery("SELECT kk FROM KoordynatorzyKierunkowiEntity kk WHERE kk.idKoordynatoraPraktyk=?1 AND kk.idKierunku=?2");
        query.setParameter(1, idKoordynatoraPraktyk);
        query.setParameter(2, idKierunku);
        List list = query.getResultList();
        if (!list.isEmpty()) {
            KoordynatorzyKierunkowiEntity temp = (KoordynatorzyKierunkowiEntity) list.get(0);
            KoordynatorzyDate date = new KoordynatorzyDate();
            date.setDataPowolania(temp.getDataPowolania());
            return date;
        } else {
            return null;
        }
    }

    @Override
    public Long deleteCoordinatorFromCourse(int idKoordynatoraPraktyk, int idKierunku) {
        Query query = em.createQuery("DELETE KoordynatorzyKierunkowiEntity kk WHERE kk.idKoordynatoraPraktyk=?1 AND kk.idKierunku=?2");
        query.setParameter(1, idKoordynatoraPraktyk);
        query.setParameter(2, idKierunku);
        int number = query.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        }
        else {
            return new Long(-1);
        }
    }

    @Override
    public Long deleteCoordinatorFromEveryCourse(int idKoordynatoraPraktyk) {
        Query query = em.createQuery("DELETE KoordynatorzyKierunkowiEntity kk WHERE kk.idKoordynatoraPraktyk=?1");
        query.setParameter(1, idKoordynatoraPraktyk);
        int number = query.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        }
        else {
            return new Long(-1);
        }
    }

    @Override
    public Long deleteEveryCoordinatorFromCourse(int idKierunku) {
        Query query = em.createQuery("DELETE KoordynatorzyKierunkowiEntity kk WHERE kk.idKierunku=?1");
        query.setParameter(1, idKierunku);
        int number = query.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        }
        else {
            return new Long(-1);
        }
    }
}
