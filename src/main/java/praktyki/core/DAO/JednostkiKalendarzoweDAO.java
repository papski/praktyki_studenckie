package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iJednostkiKalendarzoweDAO;
import praktyki.core.entities.JednostkiKalendarzoweEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by dawid on 22.03.15.
 */
@Repository
public class JednostkiKalendarzoweDAO implements iJednostkiKalendarzoweDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public JednostkiKalendarzoweEntity getRow(int idJednostkiKalendarzowej) {
        return em.find(JednostkiKalendarzoweEntity.class, idJednostkiKalendarzowej);
    }
}
