package praktyki.core.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.*;
import praktyki.core.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 14.03.15.
 */

@Repository
public class PraktykiDAO implements iPraktykiDAO {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private iPraktykiKierunkoweDAO ipraktykiKierunkoweDAO;

    @Autowired
    private iStudenciKierunkowDAO istudenciKierunkowDAO;

    @Autowired
    private iKoordynatorzyKierunkowiPraktykodawcowDAO ikoordynatorzyKierunkowiPraktykodawcowDAO;

    @Autowired
    private iAdresyPraktykodawcyDAO iadresyPraktykodawcyDAO;

    @Autowired
    private iOpiekunowieKierunkowiDAO iopiekunowieKierunkowiDAO;


    @Override
    public PraktykiEntity addRelation(PraktykiEntity praktykiEntity) {
        PraktykiKierunkoweEntity coursePractice = ipraktykiKierunkoweDAO.getRow(praktykiEntity.getIdKierunku(), praktykiEntity.getIdTypuPraktyki(), praktykiEntity.getIdRokuAkademickiego());
        if (coursePractice !=null) {
            StudenciKierunkowEntity student = istudenciKierunkowDAO.getRow(praktykiEntity.getNrAlbumu(), praktykiEntity.getIdKierunku(), praktykiEntity.getIdRokuAkademickiego());
            KoordynatorzyKierunkowiPraktykodawcowEntity coordinator = ikoordynatorzyKierunkowiPraktykodawcowDAO.getRow(praktykiEntity.getIdPraktykodawcy(), praktykiEntity.getIdKierunku(), praktykiEntity.getIdKoordynatoraPraktyk());
            if (coordinator !=null) {
                AdresyPraktykodawcyEntity employerAddress = iadresyPraktykodawcyDAO.getRow(praktykiEntity.getIdAdresu(), praktykiEntity.getIdPraktykodawcy());
                if (employerAddress !=null) {
                    OpiekunowieKierunkowiEntity tutor = iopiekunowieKierunkowiDAO.getRow(praktykiEntity.getIdKierunku(), praktykiEntity.getIdOpiekunaPraktyk(), praktykiEntity.getIdPraktykodawcy());
                    if (tutor !=null) {
                        PraktykodawcyEntity employer = em.find(PraktykodawcyEntity.class, praktykiEntity.getIdPraktykodawcy());
                        if (employer !=null) {
                            StatusyEntity status = em.find(StatusyEntity.class, praktykiEntity.getIdStatusu());
                            if (status !=null) {
                                SzablonyPraktykEntity template = em.find(SzablonyPraktykEntity.class, praktykiEntity.getIdSzablonu());
                                if (template !=null) {
                                    em.persist(praktykiEntity);
                                    praktykiEntity.setPraktyka(coursePractice);
                                    praktykiEntity.setStudent(student);
                                    praktykiEntity.setKoordynator(coordinator);
                                    praktykiEntity.setAdresPraktykodawcy(employerAddress);
                                    praktykiEntity.setPraktykodawca(employer);
                                    praktykiEntity.setOpiekun(tutor);
                                    praktykiEntity.setStatus(status);
                                    if(praktykiEntity.getIdPorozumienia() !=null) {
                                        PorozumieniaEntity agreement = em.find(PorozumieniaEntity.class, praktykiEntity.getIdPorozumienia());
                                        praktykiEntity.setPorozumienie(agreement);
                                        agreement.setPraktyka(praktykiEntity);
                                    }
                                    praktykiEntity.setSzablon(template);
                                    Collection<PraktykiEntity> relation = new HashSet<PraktykiEntity>();
                                    relation.add(praktykiEntity);
                                    coursePractice.setPraktyka(relation);
                                    if(student != null) {
                                        student.setPraktyka(praktykiEntity);
                                    }
                                    coordinator.setPraktyka(relation);
                                    employerAddress.setPraktyka(relation);
                                    tutor.setPraktyka(relation);
                                    employer.setPraktyki(relation);
                                    status.setPraktyka(relation);
                                    template.setPraktyka(relation);
                                    return praktykiEntity;
                                } else {
                                    return null;
                                }
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> findAll() {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p ORDER BY p.idPraktykiStudenckiej ASC");
        return query.getResultList();
    }

    @Override
    public List<PraktykiEntity> getAvaliableCoursePractices(int idKierunku, int idRokuAkademickiego) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idKierunku=?1 AND p.idRokuAkademickiego=?2 AND p.nrAlbumu IS NULL ORDER BY p.idPraktykiStudenckiej ASC");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idRokuAkademickiego);
        return query.getResultList();
    }

    @Override
    public List<PraktykiEntity> getPracticeByCourseAcademicYear(int idKierunku, int idRokuAkademickiego) {
        if (idKierunku > 0 && idRokuAkademickiego > 0 ) {
            Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idKierunku=?1 AND p.idRokuAkademickiego=?2 ORDER BY p.idPraktykiStudenckiej ASC");
            query.setParameter(1, idKierunku);
            query.setParameter(2, idRokuAkademickiego);
            return query.getResultList();
        } else if (idKierunku > 0 && idRokuAkademickiego == 0) {
            Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idKierunku=?1 ORDER BY p.idPraktykiStudenckiej ASC");
            query.setParameter(1, idKierunku);
            return query.getResultList();
        } else if(idKierunku == 0 && idRokuAkademickiego > 0) {
            Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idRokuAkademickiego=?1 ORDER BY p.idPraktykiStudenckiej ASC");
            query.setParameter(1, idRokuAkademickiego);
            return query.getResultList();
        } else {
            return null;
        }
    }

    @Override
    public PraktykiEntity getRow(Long idPraktykiStudenckiej) {
        PraktykiEntity row = em.find(PraktykiEntity.class, idPraktykiStudenckiej);
        return row;
    }

    @Override
    public PraktykiEntity editRow(Long idPraktykiStudenckiej, PraktykiEntity praktykiEntity) {
        PraktykiEntity row = em.find(PraktykiEntity.class, idPraktykiStudenckiej);
        if (row !=null) {
            row.setIdPraktykiStudenckiej(idPraktykiStudenckiej);
            if(praktykiEntity.getNrAlbumu() != null) {
                if (row.getNrAlbumu() == null) {
                    StudenciKierunkowEntity student = istudenciKierunkowDAO.getRow(praktykiEntity.getNrAlbumu(), row.getIdKierunku(), row.getIdRokuAkademickiego());
                    if (student != null) {
                        row.setNrAlbumu(praktykiEntity.getNrAlbumu());
                        row.setStudent(student);
                        student.setPraktyka(row);
                    }
                } else {
                    throw new IllegalArgumentException("ktos juz sie zapisal na dana praktyke");
                }
            }
            if(praktykiEntity.getIdPorozumienia() !=null) {
                if(row.getIdPorozumienia() == null) {
                    PorozumieniaEntity agreement = em.find(PorozumieniaEntity.class, praktykiEntity.getIdPorozumienia());
                    if (agreement !=null) {
                        row.setIdPorozumienia(praktykiEntity.getIdPorozumienia());
                        row.setPorozumienie(agreement);
                        agreement.setPraktyka(row);
                    }
                } else {
                    throw new IllegalArgumentException("to porozumienie jest juz przylaczone do danej praktyki");
                }
            }
            if (praktykiEntity.getDataZapisu() != null) {
                row.setDataZapisu(praktykiEntity.getDataZapisu());
            }
            if(praktykiEntity.getOcena() > 0) {
                row.setOcena(praktykiEntity.getOcena());
            }
            if(praktykiEntity.getSkierowanie() != null) {
                row.setSkierowanie(praktykiEntity.getSkierowanie());
            }
            if(praktykiEntity.getKartaOceny() != null) {
                row.setKartaOceny(praktykiEntity.getKartaOceny());
            }
            return em.merge(row);
        } else {
            return null;
        }
    }

    @Override
    public Long deleteRow(Long idPraktykiStudenckiej) {
        Query query = em.createQuery("DELETE PraktykiEntity p WHERE p.idPraktykiStudenckiej=?1");
        query.setParameter(1, idPraktykiStudenckiej);
        int temp = query.executeUpdate();
        Long update = Long.valueOf(temp);
        if (temp > 0) {
            return update;
        } else {
            return null;
        }
    }

    @Override
    public List<StudenciEntity> getStudentsOfEmployerByCourseAcademicYear(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        if (idPraktykodawcy > 0 && idKierunku > 0 && idRokuAkademickiego > 0) {
            Query query = em.createQuery("SELECT s FROM PraktykiEntity p JOIN p.student sk JOIN sk.studenciByNrAlbumu s WHERE p.idPraktykodawcy=?1 AND p.idKierunku=?2 AND p.idRokuAkademickiego=?3 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idKierunku);
            query.setParameter(3, idRokuAkademickiego);
            List<StudenciEntity> list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else if (idPraktykodawcy > 0 && idKierunku > 0 && idRokuAkademickiego == 0) {
            Query query = em.createQuery("SELECT s FROM PraktykiEntity p JOIN p.student sk JOIN sk.studenciByNrAlbumu s WHERE p.idPraktykodawcy=?1 AND p.idKierunku=?2 ORDER BY p.idRokuAkademickiego DESC");
            //Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 AND p.idKierunku=?2 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idKierunku);
            List<StudenciEntity> list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else if (idPraktykodawcy > 0 && idKierunku == 0 && idRokuAkademickiego > 0) {
            Query query = em.createQuery("SELECT s FROM PraktykiEntity p JOIN p.student sk JOIN sk.studenciByNrAlbumu s WHERE p.idPraktykodawcy=?1 AND p.idRokuAkademickiego=?2 ORDER BY p.idRokuAkademickiego DESC");
            //Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 AND p.idRokuAkademickiego=?2 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idRokuAkademickiego);
            List<StudenciEntity> list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else if (idPraktykodawcy > 0 && idKierunku == 0 && idRokuAkademickiego == 0 ) {
            Query query = em.createQuery("SELECT s FROM PraktykiEntity p JOIN p.student sk JOIN sk.studenciByNrAlbumu s WHERE p.idPraktykodawcy=?1 ORDER BY p.idRokuAkademickiego DESC");
            //Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            List<StudenciEntity> list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> getPracticeOfStudent(Long nrAlbumu) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.nrAlbumu=?1");
        query.setParameter(1, nrAlbumu);
        List list = query.getResultList();
        if (!list.isEmpty()) {
            return list;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> checkForStudent(PraktykiEntity praktykiEntity) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.nrAlbumu=?1 AND p.idKierunku=?2 AND p.idRokuAkademickiego=?3");
        query.setParameter(1, praktykiEntity.getNrAlbumu());
        query.setParameter(2, praktykiEntity.getIdKierunku());
        query.setParameter(3, praktykiEntity.getIdRokuAkademickiego());
        return query.getResultList();
    }

    @Override
    public List<PraktykiEntity> checkForAgreement(PraktykiEntity praktykiEntity) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPorozumienia=?1");
        query.setParameter(1, praktykiEntity.getIdPorozumienia());
        return query.getResultList();
    }

    @Override
    public List<PraktykiEntity> getPracticeOfEmployerByTutor(int idPraktykodawcy, int idOpiekunaPraktyk) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 AND p.idOpiekunaPraktyk=?2 ORDER BY p.idRokuAkademickiego DESC");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idOpiekunaPraktyk);
        List list = query.getResultList();
        if (!list.isEmpty()) {
            return list;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> getPracticesOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego) {
        if (idPraktykodawcy > 0 && idKierunku > 0 && idRokuAkademickiego > 0) {
            Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 AND p.idKierunku=?2 AND p.idRokuAkademickiego=?3 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idKierunku);
            query.setParameter(3, idRokuAkademickiego);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else if (idPraktykodawcy > 0 && idKierunku > 0 && idRokuAkademickiego == 0) {
            Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 AND p.idKierunku=?2 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idKierunku);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else if (idPraktykodawcy > 0 && idKierunku == 0 && idRokuAkademickiego > 0) {
            Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 AND p.idRokuAkademickiego=?2 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            query.setParameter(2, idRokuAkademickiego);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else if (idPraktykodawcy > 0 && idKierunku == 0 && idRokuAkademickiego == 0 ) {
            Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idPraktykodawcy=?1 ORDER BY p.idRokuAkademickiego DESC");
            query.setParameter(1, idPraktykodawcy);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return list;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> getPracticeOfTutor(int idOpiekunaPraktyk) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idOpiekunaPraktyk=?1 ORDER BY p.idRokuAkademickiego DESC");
        query.setParameter(1, idOpiekunaPraktyk);
        List list = query.getResultList();
        if (!list.isEmpty()) {
            return list;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> getPracticeOfCoordinator(int idKoordynatoraPraktyk) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idKoordynatoraPraktyk=?1 ORDER BY p.idRokuAkademickiego DESC");
        query.setParameter(1, idKoordynatoraPraktyk);
        List list = query.getResultList();
        if (!list.isEmpty()) {
            return list;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> getPracticeOfCourse(int idKierunku) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idKierunku=?1 ORDER BY p.idRokuAkademickiego DESC ");
        query.setParameter(1, idKierunku);
        List list = query.getResultList();
        if (!list.isEmpty()) {
            return list;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiEntity> getPracticeOfCourseByAcademicYear(int idKierunku, int idRokuAkademickiego) {
        Query query = em.createQuery("SELECT p FROM PraktykiEntity p WHERE p.idKierunku=?1 AND p.idRokuAkademickiego=?2");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idRokuAkademickiego);
        List list = query.getResultList();
        if (!list.isEmpty()) {
            return list;
        } else {
            return null;
        }
    }
}
