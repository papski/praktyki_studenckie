package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iStatusyDAO;
import praktyki.core.entities.StatusyEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by dawid on 20.03.15.
 */

@Repository
public class StatusyDAO implements iStatusyDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public StatusyEntity getStatus(int idStatusu) {
        return em.find(StatusyEntity.class, idStatusu);
    }
}
