package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iOsobyDAO;
import praktyki.core.entities.KoordynatorzyPraktykEntity;
import praktyki.core.entities.OpiekunowiePraktykEntity;
import praktyki.core.entities.OsobyEntity;
import praktyki.core.entities.StudenciEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */
@Repository
public class OsobyDAO implements iOsobyDAO {

    @PersistenceContext
    private EntityManager em;

    /*
    SPRAWDZANIE EMAILA, TELEFONUKOMORKOWEGO I
    TELEFONUSTACJONARNEGO PRZED DODANIEM OSOBY
     */

    @Override
    public Long checkEmail(String email) {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o WHERE o.email LIKE :email");
        query.setParameter("email", email);
        if (query.getResultList().isEmpty()) {
            return new Long(1);
        }
        else {
            return new Long(-1);
        }
    }

    @Override
    public boolean isEmailUsed(String email) {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o WHERE o.email LIKE :email");
        query.setParameter("email", email);
        if (query.getResultList().isEmpty()) {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public Long checkTelefonKomorkowy(String telefonKomorkowy) {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o WHERE o.telefonKomorkowy LIKE :telefonKomorkowy");
        query.setParameter("telefonKomorkowy", telefonKomorkowy);
        if (!query.getResultList().isEmpty()) {
            return new Long(1);
        }
        else {
            return new Long(-1);
        }
    }

    @Override
    public boolean isCellPhoneNumberUsed(String telefonKomorkowy) {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o WHERE o.telefonKomorkowy LIKE :telefonKomorkowy");
        query.setParameter("telefonKomorkowy", telefonKomorkowy);
        if (query.getResultList().isEmpty()) {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public Long checkTelefonStacjonarny(String telefonStacjonarny) {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o WHERE o.telefonStacjonarny LIKE :telefonStacjonarny");
        query.setParameter("telefonStacjonarny", telefonStacjonarny);
        if (!query.getResultList().isEmpty()) {
            return new Long(1);
        }
        else {
            return new Long(-1);
        }
    }

    @Override
    public boolean isHomePhoneNumberUsed(String telefonStacjonarny) {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o WHERE o.telefonStacjonarny LIKE :telefonStacjonarny");
        query.setParameter("telefonStacjonarny", telefonStacjonarny);
        if (query.getResultList().isEmpty()) {
            return false;
        }
        else {
            return true;
        }
    }

    /*
    KONIEC FUNKCJI SPRAWDZAJACYCH
     */

    @Override
    public OsobyEntity addPerson(OsobyEntity osobyEntity) {
        em.persist(osobyEntity);
        return osobyEntity;
    }

    @Override
    public OsobyEntity updatePerson(Integer idOsoby, OsobyEntity osobyEntity) {
        OsobyEntity osoba = em.find(OsobyEntity.class, idOsoby);
        if (osoba !=null) {
            osoba.setIdOsoby(idOsoby);
            if(osobyEntity.getTytulZawodowy() !=null) {
                osoba.setTytulZawodowy(osobyEntity.getTytulZawodowy());
            }
            if (osobyEntity.getImie() !=null) {
                osoba.setImie(osobyEntity.getImie());
            }
            if (osoba.getNazwisko() !=null) {
                osoba.setNazwisko(osobyEntity.getNazwisko());
            }
            /*if(osoba.getEmail() !=null) {
                osoba.setEmail(osobyEntity.getEmail());
            }*/
            if (osoba.getTelefonKomorkowy() !=null) {
                osoba.setTelefonKomorkowy(osobyEntity.getTelefonKomorkowy());
            }
            if(osoba.getTelefonStacjonarny() !=null) {
                osoba.setTelefonStacjonarny(osobyEntity.getTelefonStacjonarny());
            }
            return em.merge(osoba);
        }
        else {
            return null;
        }
    }

    @Override
    public List<OsobyEntity> getUserByNameSurname(String searchString, String sortBy, String sortType) {
        /*Query query = em.createQuery("SELECT o FROM OsobyEntity o WHERE o.imie LIKE :searchString OR o.nazwisko LIKE :searchString OR o.email LIKE :searchString ORDER BY o." +sortBy +" " +sortType);
        query.setParameter("searchString","%" +searchString + "%" );
        return query.getResultList();*/
        Query query = em.createQuery("SELECT o, r FROM OsobyEntity o JOIN o.uzytkownik u JOIN u.uzytkownikByIdRoli r WHERE o.imie LIKE :searchString OR o.nazwisko LIKE :searchString OR o.email LIKE :searchString ORDER BY o." +sortBy +" " +sortType);
        query.setParameter("searchString","%" +searchString + "%" );
        return query.getResultList();
    }

    @Override
    public String[] getEmails(List<Integer> listOfPersonsId) {
        List emailList = new ArrayList();
        for (int i = 0 ; i < listOfPersonsId.size(); i++) {
            Query query = em.createQuery("SELECT o.email FROM OsobyEntity o WHERE o.idOsoby=?1");
            query.setParameter(1, listOfPersonsId.get(i));
            if (!query.getResultList().isEmpty()) {
                emailList.add(query.getResultList().get(0));
            }
        }
        String[] to = new String[emailList.size()];
        emailList.toArray(to);
        return to;
    }


    @Override
    public List<OsobyEntity> findAll() {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o ORDER BY o.imie ASC ");
        return query.getResultList();
    }

    @Override
    public OsobyEntity getPersonByIdOsoby(Integer idOsoby) {
        return em.find(OsobyEntity.class, idOsoby);
    }

    @Override
    public List<OsobyEntity> getInfoOfCoordinator(Integer idOsoby, int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity coordinator = em.find(KoordynatorzyPraktykEntity.class, idKoordynatoraPraktyk);
        if (coordinator !=null) {
            Query query = em.createQuery("SELECT o FROM OsobyEntity o JOIN o.koordynator ok WHERE ok.idOsoby=?1");
            query.setParameter(1, idOsoby);
            return query.getResultList();
        }
        else {
            return null;
        }
    }

    @Override
    public List<OsobyEntity> getInfoOfStudent(Long nrAlbumu) {
        StudenciEntity student = em.find(StudenciEntity.class, nrAlbumu);
        if (student !=null) {
            Integer idOsoby = student.getIdOsoby();
            Query query = em.createQuery("SELECT o FROM OsobyEntity o JOIN o.student os WHERE os.idOsoby=?1");
            query.setParameter(1, idOsoby);
            return query.getResultList();
        }
        else {
            return null;
        }
    }

    @Override
    public List<OsobyEntity> getInfoOfTutor(Integer idOpiekunaPraktyk) {
        OpiekunowiePraktykEntity tutor = em.find(OpiekunowiePraktykEntity.class, idOpiekunaPraktyk);
        if (tutor !=null) {
            Integer idOsoby = tutor.getIdOsoby();
            Query query = em.createQuery("SELECT o FROM OsobyEntity o JOIN o.opiekun oo WHERE oo.idOsoby=?1");
            query.setParameter(1, idOsoby);
            return query.getResultList();
        } else {
            return null;
        }
    }

    @Override
    public OsobyEntity deletePerson(Integer idOsoby) {
        OsobyEntity osoba = em.find(OsobyEntity.class, idOsoby);
        if (osoba !=null) {
            em.remove(osoba);
            return osoba;
        }
        else {
            return null;
        }
    }

    @Override
    public List<OsobyEntity> getCoordinatorOfEmployer(int idPraktykodawcy, int idKoordynatoraPraktyk) {
        Query query = em.createQuery("SELECT o FROM OsobyEntity o JOIN o.koordynator kp JOIN kp.koordynatorzyByIdKierunku kk JOIN kk.praktykodawcaByKoordynator kkp WHERE kkp.idPraktykodawcy=?1 AND kkp.idKoordynatoraPraktyk=?2");
        query.setParameter(1, idKoordynatoraPraktyk);
        query.setParameter(2, idPraktykodawcy);
        return query.getResultList();
    }
}
