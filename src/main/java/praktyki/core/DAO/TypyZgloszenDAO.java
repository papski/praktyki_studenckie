package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iTypyZgloszenDAO;
import praktyki.core.entities.TypyZgloszenEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */

@Repository
public class TypyZgloszenDAO implements iTypyZgloszenDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public TypyZgloszenEntity addRequest(TypyZgloszenEntity typyZgloszenEntity) {
        em.persist(typyZgloszenEntity);
        return typyZgloszenEntity;
    }

    @Override
    public TypyZgloszenEntity updateRequest(Integer idTypuZgloszenia, TypyZgloszenEntity typyZgloszenEntity) {
        TypyZgloszenEntity request = em.find(TypyZgloszenEntity.class, idTypuZgloszenia);
        if (request !=null) {
            request.setIdTypuZgloszenia(idTypuZgloszenia);
            return request;
        } else {
            return null;
        }
    }

    @Override
    public TypyZgloszenEntity deleteRequest(Integer idTypuZgloszenia) {
        TypyZgloszenEntity request = em.find(TypyZgloszenEntity.class, idTypuZgloszenia);
        if (request !=null) {
            em.remove(request);
            return request;
        } else {
            return null;
        }
    }

    @Override
    public List<TypyZgloszenEntity> findAll() {
        Query query = em.createQuery("SELECT tz FROM TypZgloszeniaEntity tz ORDER BY tz.idTypuZgloszenia ASC ");
        return query.getResultList();
    }

    @Override
    public TypyZgloszenEntity getRequest(Integer idTypuZgloszenia) {
        return em.find(TypyZgloszenEntity.class, idTypuZgloszenia);
    }
}
