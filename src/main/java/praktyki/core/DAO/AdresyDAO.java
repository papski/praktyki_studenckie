package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iAdresyDAO;
import praktyki.core.entities.AdresyEntity;
import praktyki.core.entities.StudenciEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 29.12.14.
 */

@Repository
public class AdresyDAO implements iAdresyDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public AdresyEntity addAddress(AdresyEntity adresyEntity) {
        em.persist(adresyEntity);
        return adresyEntity;
    }

    @Override
    public AdresyEntity updateAddress(AdresyEntity adresyEntity, int idAdresu) {
        AdresyEntity adres = em.find(AdresyEntity.class, idAdresu);
        if (adres !=null) {
            adres.setIdAdresu(idAdresu);
            adres.setUlica(adresyEntity.getUlica());
            adres.setNrBudynku(adresyEntity.getNrBudynku());
            adres.setMiejscowosc(adresyEntity.getMiejscowosc());
            adres.setKodPocztowy(adresyEntity.getKodPocztowy());
            adres.setKraj(adresyEntity.getKraj());
            adres.setFax(adresyEntity.getFax());
            return em.merge(adres);
        }
        else {
            return null;
        }
    }

    @Override
    public AdresyEntity deleteAddress(int idAdresu) {
        AdresyEntity adresyEntity = em.find(AdresyEntity.class, idAdresu);
        if(adresyEntity != null) {
            Query query = em.createQuery("DELETE AdresyPraktykodawcyEntity a WHERE a.idAdresu=?1");
            query.setParameter(1, idAdresu);
            query.executeUpdate();
            em.remove(adresyEntity);
            return adresyEntity;
        }
        else {
            return null;
        }
    }

    @Override
    public List<AdresyEntity> findEveryAddress() {
        Query query = em.createQuery("SELECT a from AdresyEntity a ORDER BY a.idAdresu ASC");
        return query.getResultList();
    }

    @Override
    public AdresyEntity findByIdAdresu(int idAdresu) {
        return em.find(AdresyEntity.class, idAdresu);
    }

    @Override
    public List<AdresyEntity> getAddressByStreetCityZipCode(String searchString, String sortBy, String sortType) {
        Query query = em.createQuery("SELECT a FROM AdresyEntity a WHERE a.ulica LIKE :searchString OR a.miejscowosc LIKE :searchString OR a.kodPocztowy LIKE :searchString ORDER BY a." +sortBy +" " +sortType);
        query.setParameter("searchString", "%" +searchString +"%");
        return query.getResultList();
    }

    @Override
    public List<AdresyEntity> getAddressOfStudent(Long nrAlbumu) {
        StudenciEntity student = em.find(StudenciEntity.class, nrAlbumu);
        if (student !=null) {
            Integer idAdresu = student.getIdAdresu();
            Query query = em.createQuery("SELECT a FROM AdresyEntity a JOIN a.studenciByIdAdresu aa WHERE aa.idAdresu=?1");
            query.setParameter(1, idAdresu);
            return query.getResultList();
        }
        else {
            return null;
        }
    }

    @Override
    public List<AdresyEntity> findAddressesOfEmployer(int idPraktykodawcy) {
        //Query query = em.createQuery("SELECT a FROM AdresyEntity a WHERE a.praktykodawcyByIdAdresu.idAdresu=?1");
        Query query = em.createQuery("SELECT a, s.oddzial FROM AdresyEntity a JOIN a.praktykodawcyByIdAdresu s WHERE s.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        return query.getResultList();
    }

    @Override
    public List<AdresyEntity> getAddressOfEmployer(int idPraktykodawcy, int idAdresu) {
        Query query = em.createQuery("SELECT a, s.oddzial FROM AdresyEntity a JOIN a.praktykodawcyByIdAdresu s WHERE s.idPraktykodawcy=?1 AND s.idAdresu=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idAdresu);
        return query.getResultList();
    }


}
