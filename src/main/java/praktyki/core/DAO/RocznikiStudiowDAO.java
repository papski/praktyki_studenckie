package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iRocznikiStudiowDAO;
import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.core.entities.LataAkademickieEntity;
import praktyki.core.entities.RocznikiStudiowEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 17.02.15.
 */
@Repository
public class RocznikiStudiowDAO implements iRocznikiStudiowDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public RocznikiStudiowEntity addRelation(RocznikiStudiowEntity rocznikiStudiowEntity) {
        KierunkiStudiowEntity course = em.find(KierunkiStudiowEntity.class, rocznikiStudiowEntity.getIdKierunku());
        if (course !=null) {
            LataAkademickieEntity year = em.find(LataAkademickieEntity.class, rocznikiStudiowEntity.getIdRokuAkademickiego());
            if (year !=null) {
                em.persist(rocznikiStudiowEntity);
                rocznikiStudiowEntity.setKierunek(course);
                rocznikiStudiowEntity.setRok(year);
                Collection<RocznikiStudiowEntity> relation = new HashSet<RocznikiStudiowEntity>();
                relation.add(rocznikiStudiowEntity);
                course.setRocznikiByIdKierunku(relation);
                year.setKierunkiByIdRoku(relation);
                return rocznikiStudiowEntity;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public RocznikiStudiowEntity getCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        Query query = em.createQuery("SELECT rs FROM RocznikiStudiowEntity rs WHERE rs.idKierunku=?1 AND rs.idRokuAkademickiego=?2");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idRokuAkademickiego);
        List list = query.getResultList();
        RocznikiStudiowEntity row = new RocznikiStudiowEntity();
        if (!list.isEmpty()) {
            return row = (RocznikiStudiowEntity) list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Long deleteYearsFromCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        Query query = em.createQuery("DELETE RocznikiStudiowEntity rs WHERE rs.idKierunku=?1 AND rs.idRokuAkademickiego=?2");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idRokuAkademickiego);
        int temp = query.executeUpdate();
        Long updated = Long.valueOf(temp);
        if(updated > 0) {
            return updated;
        } else {
            return null;
        }
    }

    @Override
    public Long deleteEveryYearFromCourse(Integer idKierunku) {
        Query query = em.createQuery("DELETE RocznikiStudiowEntity rs WHERE rs.idKierunku=?1");
        query.setParameter(1, idKierunku);
        int temp = query.executeUpdate();
        Long updated = Long.valueOf(temp);
        if(updated > 0) {
            return updated;
        } else {
            return null;
        }
    }

    @Override
    public RocznikiStudiowEntity getRow(int idKierunku, int idRokuAkademickiego) {
        Query query = em.createQuery("SELECT rs FROM RocznikiStudiowEntity rs WHERE rs.idKierunku=?1 AND rs.idRokuAkademickiego=?2");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idRokuAkademickiego);
        RocznikiStudiowEntity row = null;
        List list = query.getResultList();
        if(!list.isEmpty()) {
            return row = (RocznikiStudiowEntity) list.get(0);
        } else {
            return null;
        }
    }
}
