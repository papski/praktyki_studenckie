package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iStudenciKierunkowDAO;
import praktyki.core.entities.RocznikiStudiowEntity;
import praktyki.core.entities.StudenciEntity;
import praktyki.core.entities.StudenciKierunkowEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
@Repository
public class StudenciKierunkowDAO implements iStudenciKierunkowDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public StudenciKierunkowEntity addRelation(Long nrAlbumu, RocznikiStudiowEntity rocznikiStudiowEntity) {
        StudenciEntity student = em.find(StudenciEntity.class, nrAlbumu);
        if (student !=null) {
            StudenciKierunkowEntity relation = new StudenciKierunkowEntity();
            relation.setNrAlbumu(nrAlbumu);
            relation.setIdKierunku(rocznikiStudiowEntity.getIdKierunku());
            relation.setIdRokuAkademickiego(rocznikiStudiowEntity.getIdRokuAkademickiego());
            em.persist(relation);
            relation.setStudenciByNrAlbumu(student);
            relation.setKierunek(rocznikiStudiowEntity);
            Collection<StudenciKierunkowEntity> relation2 = new HashSet<StudenciKierunkowEntity>();
            relation2.add(relation);
            student.setstudenciByIdKierunku(relation2);
            rocznikiStudiowEntity.setStudenci(relation2);
            return relation;
        } else {
            return null;
        }
    }

    @Override
    public StudenciKierunkowEntity getStudent(Long nrAlbumu, int idKierunku) {
        Query query = em.createQuery("SELECT sk FROM StudenciKierunkowEntity sk WHERE sk.nrAlbumu=?1 AND sk.idKierunku=?2");
        query.setParameter(1, nrAlbumu);
        query.setParameter(2, idKierunku);
        List wynik = query.getResultList();
        StudenciKierunkowEntity wynik2 = new StudenciKierunkowEntity();
        if (!wynik.isEmpty()) {
            return wynik2=(StudenciKierunkowEntity)wynik.get(0);
        }
        else {
            return null;
        }
    }

    @Override
    public StudenciKierunkowEntity getRow(Long nrAlbumu, int idKierunku, int idRokuAkademickiego) {
        Query query = em.createQuery("SELECT sk FROM StudenciKierunkowEntity sk WHERE sk.nrAlbumu=?1 AND sk.idKierunku=?2 AND sk.idRokuAkademickiego=?3");
        query.setParameter(1, nrAlbumu);
        query.setParameter(2, idKierunku);
        query.setParameter(3, idRokuAkademickiego);
        List wynik = query.getResultList();
        StudenciKierunkowEntity wynik2 = new StudenciKierunkowEntity();
        if (!wynik.isEmpty()) {
            return wynik2=(StudenciKierunkowEntity)wynik.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<StudenciKierunkowEntity> getByIdKierunku(int idKierunku) {
        Query query = em.createQuery("SELECT sk FROM StudenciKierunkowENtity sk WHERE sk.idKierunku=?1");
        query.setParameter(1, idKierunku);
        return query.getResultList();
    }

    @Override
    public List<StudenciKierunkowEntity> getByNrAlbumu(Long nrAlbumu) {
        Query query = em.createQuery("SELECT sk FROM StudenciKierunkowEntity sk WHERE sk.nrAlbumu=?1 ORDER BY nrAlbumu ASC");
        query.setParameter(1, nrAlbumu);
        return query.getResultList();
    }

    @Override
    public List<StudenciKierunkowEntity> getAll() {
        Query query = em.createQuery("SELECT sk FROM StudenciKierunkowEntity sk ORDER BY sk.idKierunku ASC");
        return query.getResultList();
    }

    @Override
    public Long deleteStudentFromCourse(Long nrAlbumu, int idKierunku) {
        Query query = em.createQuery("DELETE StudenciKierunkowEntity WHERE nrAlbumu=?1 AND idKierunku=?2");
        query.setParameter(1, nrAlbumu);
        query.setParameter(2, idKierunku);
        int updated = query.executeUpdate();
        if (updated > 0) {
            return nrAlbumu;
        }
        else {
            return new Long(-1);
        }
    }

    @Override
    public Long deleteEveryStudentFromCourse(int idKierunku) {
        Query query = em.createQuery("DELETE StudenciKierunkowEntity WHERE idKierunku=?1");
        query.setParameter(1, idKierunku);
        int number = query.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        }
        else {
            return null;
        }
    }
}
