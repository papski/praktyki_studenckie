package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iWymaganeKwalifikacjeDAO;
import praktyki.core.entities.KwalifikacjeEntity;
import praktyki.core.entities.SzablonyPraktykEntity;
import praktyki.core.entities.WymaganeKwalifikacjeEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by dawid on 28.02.15.
 */

@Repository
public class WymaganeKwalifikacjeDAO implements iWymaganeKwalifikacjeDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public WymaganeKwalifikacjeEntity addRelation(int idSzablonu, int idKwalifikacji) {
        SzablonyPraktykEntity template = em.find(SzablonyPraktykEntity.class, idSzablonu);
        if (template !=null) {
            KwalifikacjeEntity qualification = em.find(KwalifikacjeEntity.class, idKwalifikacji);
            if (qualification !=null) {
                WymaganeKwalifikacjeEntity newRow = new WymaganeKwalifikacjeEntity();
                newRow.setIdSzablonu(idSzablonu);
                newRow.setIdKwalifikacji(idKwalifikacji);
                em.persist(newRow);
                newRow.setSzablon(template);
                newRow.setKwalifikacja(qualification);
                Collection<WymaganeKwalifikacjeEntity> relation = new HashSet<WymaganeKwalifikacjeEntity>();
                relation.add(newRow);
                template.setKwalifikacjeWymaganeByIdSzablonu(relation);
                qualification.setSzablonyByIdKwalifikacjiWymaganej(relation);
                return newRow;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Boolean checkExistence(int idKwalifikacji) {
        Query query = em.createQuery("SELECT wk FROM WymaganeKwalifikacjeEntity wk WHERE wk.idKwalifikacji=?1");
        query.setParameter(1, idKwalifikacji);
        if (query.getResultList().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
