package praktyki.core.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iPraktykiKierunkoweDAO;
import praktyki.core.DAO.Interface.iRocznikiStudiowDAO;
import praktyki.core.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 08.03.15.
 */

@Repository
public class PraktykiKierunkoweDAO implements iPraktykiKierunkoweDAO {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private iRocznikiStudiowDAO irocznikiStudiowDAO;

    @Override
    public PraktykiKierunkoweEntity addRelation(PraktykiKierunkoweEntity praktykiKierunkoweEntity) {
        RocznikiStudiowEntity course = irocznikiStudiowDAO.getRow(praktykiKierunkoweEntity.getIdKierunku(), praktykiKierunkoweEntity.getIdRokuAkademickiego());
        if (course != null) {
            LataAkademickieEntity academicYear = em.find(LataAkademickieEntity.class, praktykiKierunkoweEntity.getIdRokuAkademickiego());
            if (academicYear != null) {
                TypyPraktykEntity type = em.find(TypyPraktykEntity.class, praktykiKierunkoweEntity.getIdTypuPraktyki());
                if (type != null) {
                    JednostkiKalendarzoweEntity calendarUnit = em.find(JednostkiKalendarzoweEntity.class, praktykiKierunkoweEntity.getIdJednostki());
                    if (calendarUnit != null) {
                        em.persist(praktykiKierunkoweEntity);
                        praktykiKierunkoweEntity.setKierunek(course);
                        praktykiKierunkoweEntity.setLataAkademickie(academicYear);
                        praktykiKierunkoweEntity.setJednostkiKalendarzowe(calendarUnit);
                        praktykiKierunkoweEntity.setTypPraktyki(type);
                        Collection<PraktykiKierunkoweEntity> relation = new HashSet<PraktykiKierunkoweEntity>();
                        relation.add(praktykiKierunkoweEntity);
                        course.setPraktykiKierunkoweByIdKierunku(relation);
                        academicYear.setPraktykiKierunkoweByIdRoku(relation);
                        type.setPraktykiKierunkoweByIdTypu(relation);
                        calendarUnit.setPraktykiKierunkoweByIdJednostki(relation);
                        return praktykiKierunkoweEntity;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else
                return null;
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiKierunkoweEntity> findAll() {
        Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk ORDER BY pk.idKierunku ASC");
        return query.getResultList();
    }

    @Override
    public PraktykiKierunkoweEntity getRow(int idKierunku, int idTypuPraktyki, int idRokuAkademickiego) {
        Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1 AND pk.idTypuPraktyki=?2 AND pk.idRokuAkademickiego=?3");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idTypuPraktyki);
        query.setParameter(3, idRokuAkademickiego);
        List list = query.getResultList();
        PraktykiKierunkoweEntity row = null;
        if (!list.isEmpty()) {
            return row = (PraktykiKierunkoweEntity) list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public PraktykiKierunkoweEntity editRow(int idKierunku, int idTypuPraktyki, int idRokuAkademickiego, PraktykiKierunkoweEntity praktykiKierunkoweEntity) {
        Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1 AND pk.idTypuPraktyki=?2 AND pk.idRokuAkademickiego=?3");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idTypuPraktyki);
        query.setParameter(3, idRokuAkademickiego);
        List list = query.getResultList();
        PraktykiKierunkoweEntity row = null;
        if (!list.isEmpty()) {
            row = (PraktykiKierunkoweEntity) list.get(0);
            if(praktykiKierunkoweEntity.getMinDataRozpoczecia() !=null) {
                row.setMinDataRozpoczecia(praktykiKierunkoweEntity.getMinDataRozpoczecia());
            }
            if (praktykiKierunkoweEntity.getMaxDataZakonczenia() !=null) {
                row.setMaxDataZakonczenia(praktykiKierunkoweEntity.getMaxDataZakonczenia());
            }
            if (praktykiKierunkoweEntity.getLiczbaGodzin() > 0) {
                row.setLiczbaGodzin(praktykiKierunkoweEntity.getLiczbaGodzin());
            }
            if (praktykiKierunkoweEntity.getCzasTrwania() > 0) {
                row.setCzasTrwania(praktykiKierunkoweEntity.getCzasTrwania());
            }
            return em.merge(row);
        } else {
            return null;
        }
    }

    @Override
    public List<PraktykiKierunkoweEntity> getRelation(int idKierunku, int idRokuAkademickiego, int idTypyPraktyki) {
        if (idKierunku > 0 && idRokuAkademickiego > 0 && idTypyPraktyki > 0) {
            Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1 AND pk.idRokuAkademickiego=?2 AND pk.idTypuPraktyki=?3");
            query.setParameter(1, idKierunku);
            query.setParameter(2, idRokuAkademickiego);
            query.setParameter(3, idTypyPraktyki);
            return query.getResultList();
        } else if (idKierunku > 0 && idRokuAkademickiego > 0 && idTypyPraktyki == 0) {
            Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1 AND pk.idRokuAkademickiego=?2");
            query.setParameter(1, idKierunku);
            query.setParameter(2, idRokuAkademickiego);
            return query.getResultList();
        } else if (idKierunku > 0 && idRokuAkademickiego == 0 && idTypyPraktyki > 0) {
            Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1 AND pk.idTypuPraktyki=?2");
            query.setParameter(1, idKierunku);
            query.setParameter(2, idTypyPraktyki);
            return query.getResultList();
        } else  if (idKierunku > 0 && idRokuAkademickiego == 0 && idTypyPraktyki == 0) {
            Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1");
            query.setParameter(1, idKierunku);
            return query.getResultList();
        } else if (idKierunku == 0 && idRokuAkademickiego > 0 && idTypyPraktyki > 0) {
            Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idRokuAkademickiego=?1 AND pk.idTypuPraktyki=?2");
            query.setParameter(1, idRokuAkademickiego);
            query.setParameter(2, idTypyPraktyki);
            return query.getResultList();
        } else if (idKierunku == 0 && idRokuAkademickiego > 0 && idTypyPraktyki == 0) {
            Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idRokuAkademickiego=?1");
            query.setParameter(1, idRokuAkademickiego);
            return query.getResultList();
        } else if (idKierunku == 0 && idRokuAkademickiego == 0 && idTypyPraktyki > 0) {
            Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idTypuPraktyki=?1");
            query.setParameter(1, idTypyPraktyki);
            return query.getResultList();
        } else {
            return null;
        }
    }

    /*@Override
    public List<PraktykiKierunkoweEntity> getRelation(PraktykiKierunkoweEntity praktykiKierunkoweEntity) {
        Query query = em.createQuery("SELECT pk FROM PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1 AND pk.idTypuPraktyki=?2 AND pk.idRokuAkademickiego=?3");
        query.setParameter(1, praktykiKierunkoweEntity.getIdKierunku());
        query.setParameter(2, praktykiKierunkoweEntity.getIdTypuPraktyki());
        query.setParameter(3, praktykiKierunkoweEntity.getIdRokuAkademickiego());
        return query.getResultList();
    }*/

    @Override
    public Long deleteRelation(int idKierunku, int idRokuAkademickiego, int idTypyPraktyki) {
        Query query = em.createQuery("DELETE PraktykiKierunkoweEntity pk WHERE pk.idKierunku=?1 AND pk.idRokuAkademickiego=?2 AND pk.idTypuPraktyki=?3");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idRokuAkademickiego);
        query.setParameter(3, idTypyPraktyki);
        int temp = query.executeUpdate();
        System.out.println(temp);
        Long updated = Long.valueOf(temp);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }
}
