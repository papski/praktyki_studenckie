package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iStudenciDAO;
import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.core.entities.StudenciEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 12.12.14.
 */

@Repository
public class StudenciDAO implements iStudenciDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public StudenciEntity addStudent(StudenciEntity studenciEntity) {
        em.persist(studenciEntity);
        return studenciEntity;
    }


    @Override
    public StudenciEntity deleteStudent(Long nrAlbumu) {
        StudenciEntity studenciEntity = em.find(StudenciEntity.class, nrAlbumu);
        if (studenciEntity != null) {
            Query query = em.createQuery("DELETE StudenciKierunkowEntity WHERE nrAlbumu=?1");
            query.setParameter(1, nrAlbumu);
            query.executeUpdate();
            Query query2 = em.createQuery("DELETE UzytkownicyEntity WHERE idOsoby=?1");
            query2.setParameter(1,studenciEntity.getIdOsoby());
            query2.executeUpdate();
            Query query3 = em.createQuery("DELETE ZgloszeniaPraktykodawcowEntity WHERE nrAlbumu=?1");
            query3.setParameter(1, nrAlbumu);
            query3.executeUpdate();
            Query query4 = em.createQuery("DELETE PraktykiEntity WHERE nrAlbumu=?1");
            query4.setParameter(1, nrAlbumu);
            query4.executeUpdate();
            em.remove(studenciEntity);
            return studenciEntity;
        } else {
            return null;
        }
    }

    @Override
    public List<StudenciEntity> findEveryStudent() {
        Query query = em.createQuery("SELECT s from StudenciEntity s ORDER BY s.nrAlbumu ASC");
        return query.getResultList();
    }

    @Override
    public StudenciEntity findByNrAlbumu(Long nrAlbumu) {
        return em.find(StudenciEntity.class, nrAlbumu);
    }

    @Override
    public List<StudenciEntity> getStudentInfo(Long nrAlbumu) {
        StudenciEntity student = em.find(StudenciEntity.class, nrAlbumu);
        if (student !=null) {
            Query query = em.createQuery("SELECT s, o FROM StudenciEntity s JOIN s.studentByIdOsoby o WHERE o.idOsoby=?1 AND s.nrAlbumu=?2");
            query.setParameter(1, student.getIdOsoby());
            query.setParameter(2, nrAlbumu);
            return query.getResultList();
        } else {
            return null;
        }
    }

    @Override
    public List<StudenciEntity> findByIdAdresu(Integer idAdresu) {
        Query query = em.createQuery("SELECT s FROM StudenciEntity s WHERE s.idAdresu=?1");
        query.setParameter(1, idAdresu);
        return query.getResultList();
    }

    @Override
    public List<StudenciEntity> findStudentByIdAdresu(Integer idAdresu) {
        Query query = em.createQuery("SELECT s FROM StudenciEntity s WHERE s.adresyByIdAdresu.idAdresu=?1");
        query.setParameter(1, idAdresu);
        return query.getResultList();
    }

    @Override
    public List<StudenciEntity> findStudentsOfCourse(int idKierunku) {
        Query query = em.createQuery("SELECT s FROM StudenciEntity s JOIN s.studenciByIdKierunku sk WHERE sk.idKierunku=?1");
        query.setParameter(1, idKierunku);
        return query.getResultList();
    }

    @Override
    public List<StudenciEntity> getStudentOfCourse(Long nrAlbumu, int idKierunku) {
        StudenciEntity student = em.find(StudenciEntity.class, nrAlbumu);
        if (student !=null) {
            KierunkiStudiowEntity kierunek = em.find(KierunkiStudiowEntity.class, idKierunku);
            if (kierunek !=null) {
                Query query = em.createQuery("SELECT s FROM StudenciEntity s JOIN s.studenciByIdKierunku sk WHERE sk.nrAlbumu=?1 AND sk.idKierunku=?2");
                query.setParameter(1, nrAlbumu);
                query.setParameter(2, idKierunku);
                return query.getResultList();
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    @Override
    public Long checkNrAlbumu(Long nrAlbumu) {
        StudenciEntity check = em.find(StudenciEntity.class, nrAlbumu);
        if (check != null) {
            return new Long(1);
        }
        else {
            return new Long(-1);
        }
    }

    @Override
    public boolean isAlbumNumberUsed(Long nrAlbumu) {
        StudenciEntity check = em.find(StudenciEntity.class, nrAlbumu);
        if (check == null) {
            return false;
        }
        else {
            return true;
        }
    }


}
