package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iKwalifikacjeDAO;
import praktyki.core.entities.KwalifikacjeEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */

@Repository
public class KwalifikacjeDAO implements iKwalifikacjeDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public KwalifikacjeEntity addQualification(KwalifikacjeEntity kwalifikacjeEntity) {
        em.persist(kwalifikacjeEntity);
        return kwalifikacjeEntity;
    }

    @Override
    public List<KwalifikacjeEntity> findRequiredQulificationsOfTemplate(int idSzablonu) {
        Query query = em.createQuery("SELECT k FROM KwalifikacjeEntity k JOIN k.szablonyByIdKwalifikacjiWymaganej wk WHERE wk.idSzablonu=?1");
        query.setParameter(1, idSzablonu);
        return query.getResultList();
    }

    @Override
    public List<KwalifikacjeEntity> getRequiredQualificationOfTemplate(int idSzablonu, int idKwalifikacji) {
        Query query = em.createQuery("SELECT k FROM KwalifikacjeEntity k JOIN k.szablonyByIdKwalifikacjiWymaganej wk WHERE wk.idSzablonu=?1 AND wk.idKwalifikacji=?2");
        query.setParameter(1, idSzablonu);
        query.setParameter(2, idKwalifikacji);
        return query.getResultList();
    }

    @Override
    public List<KwalifikacjeEntity> findOfferedQulificationsOfTemplate(int idSzablonu) {
        Query query = em.createQuery("SELECT k FROM KwalifikacjeEntity k JOIN k.szablonyByIdKwalifikacjiOferowanej ok WHERE ok.idSzablonu=?1");
        query.setParameter(1, idSzablonu);
        return query.getResultList();
    }

    @Override
    public List<KwalifikacjeEntity> getOfferedQualificationOfTemplate(int idSzablonu, int idKwalifikacji) {
        Query query = em.createQuery("SELECT k FROM KwalifikacjeEntity k JOIN k.szablonyByIdKwalifikacjiOferowanej ok WHERE ok.idSzablonu=?1 AND ok.idKwalifikacji=?2");
        query.setParameter(1, idSzablonu);
        query.setParameter(2, idKwalifikacji);
        return query.getResultList();
    }

    @Override
    public List<KwalifikacjeEntity> getQualificationsOfTemplate(int idSzablonu) {
        Query query = em.createQuery("SELECT k FROM KwalifikacjeEntity k JOIN k.szablonyByIdKwalifikacjiOferowanej ok JOIN k.szablonyByIdKwalifikacjiWymaganej wk WHERE ok.idSzablonu=?1 OR wk.idSzablonu=?1");
        query.setParameter(1, idSzablonu);
        return query.getResultList();
    }

}
