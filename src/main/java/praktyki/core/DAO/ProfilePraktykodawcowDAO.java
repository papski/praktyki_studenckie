package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iProfilePraktykodawcowDAO;
import praktyki.core.entities.PraktykodawcyEntity;
import praktyki.core.entities.ProfilEntity;
import praktyki.core.entities.ProfilePraktykodawcowEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 25.01.15.
 */

@Repository
public class ProfilePraktykodawcowDAO implements iProfilePraktykodawcowDAO {

    @PersistenceContext
    EntityManager em;

    @Override
    public ProfilePraktykodawcowEntity addRelation(ProfilePraktykodawcowEntity profilePraktykodawcowEntity) {
        Integer idProfilu = profilePraktykodawcowEntity.getIdProfilu();
        int idPraktykodawcy = profilePraktykodawcowEntity.getIdPraktykodawcy();
        ProfilEntity profile = em.find(ProfilEntity.class, idProfilu);
        if (profile !=null) {
            PraktykodawcyEntity employeer = em.find(PraktykodawcyEntity.class, idPraktykodawcy);
            if (employeer !=null) {
                em.persist(profilePraktykodawcowEntity);
                profilePraktykodawcowEntity.setProfil(profile);
                profilePraktykodawcowEntity.setPraktykodawca(employeer);
                Collection<ProfilePraktykodawcowEntity> relation = new HashSet<ProfilePraktykodawcowEntity>();
                relation.add(profilePraktykodawcowEntity);
                profile.setPraktykodawcyByIdProfilu(relation);
                employeer.setProfileByIdPraktykodawcy(relation);
                return profilePraktykodawcowEntity;
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    @Override
    public List<ProfilePraktykodawcowEntity> findAll() {
        Query query = em.createQuery("SELECT pp FROM ProfilePraktykodawcowEntity pp ORDER BY pp.Praktykodawcy ASC");
        return query.getResultList();
    }

    /*@Override
    public Long deleteProfileFromEmployeer(int idPraktykodawcy, Integer idProfilu) {
        Query query = em.createQuery("DELETE ProfilePraktykodawcowEntity pp WHERE pp.idPraktykodawcy=?1 AND pp.idProfilu=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idProfilu);
        int number = query.executeUpdate();
        Query query2 = em.createQuery("DELETE ProfilEntity WHERE idProfilu NOT IN (SELECT idProfilu FROM ProfilePraktykodawcowEntity)");
        query2.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        }
        else {
            return null;
        }
    }

    @Override
    public Long deleteEveryProfileFromEmployeer(int idPraktykodawcy) {
        Query query = em.createQuery("DELETE ProfilePraktykodawcowEntity pp WHERE pp.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        int number = query.executeUpdate();
        Query query2 = em.createQuery("DELETE ProfilEntity WHERE idProfilu NOT IN (SELECT idProfilu FROM ProfilePraktykodawcowEntity)");
        query2.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        }
        else {
            return null;
        }
    }*/

    @Override
    public Long deleteProfileFromEmployeer(int idPraktykodawcy, Integer idProfilu) {
        Query query = em.createQuery("DELETE ProfilePraktykodawcowEntity pp WHERE pp.idPraktykodawcy=?1 AND pp.idProfilu=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idProfilu);
        int number = query.executeUpdate();
        Query query2 = em.createQuery("DELETE ProfilEntity p WHERE p.idProfilu =?1");
        query2.setParameter(1, idProfilu);
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }

    @Override
    public Long deleteEveryProfileFromEmployeer(int idPraktykodawcy) {
        Query query = em.createQuery("SELECT pp FROM ProfilePraktykodawcowEntity pp WHERE pp.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        List<ProfilePraktykodawcowEntity> temp = query.getResultList();
        Query query2 = em.createQuery("DELETE ProfilePraktykodawcowEntity pp WHERE pp.idPraktykodawcy=?1");
        query2.setParameter(1, idPraktykodawcy);
        int number = query2.executeUpdate();
        for(int i = 0; i < temp.size(); i++) {
            Query query3 = em.createQuery("DELETE ProfilEntity p WHERE p.idProfilu=?1");
            query3.setParameter(1, temp.get(i).getIdProfilu());
            query3.executeUpdate();
        }
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }

    //DO WAYWALENIA
    @Override
    public Long deleteEveryEmployeerFromProfil(Integer idProfilu) {
        Query query = em.createQuery("DELETE ProfilePraktykodawcowEntity pp WHERE pp.idProfilu=?1");
        query.setParameter(1, idProfilu);
        int number = query.executeUpdate();
        Query query2 = em.createQuery("DELETE ProfilEntity WHERE idProfilu NOT IN (SELECT idProfilu FROM ProfilePraktykodawcowEntity)");
        query2.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        }
        else {
            return null;
        }
    }
}
