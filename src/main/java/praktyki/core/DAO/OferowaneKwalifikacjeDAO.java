package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iOferowaneKwalifikacjeDAO;
import praktyki.core.entities.KwalifikacjeEntity;
import praktyki.core.entities.OferowaneKwalifikacjeEntity;
import praktyki.core.entities.SzablonyPraktykEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by dawid on 28.02.15.
 */

@Repository
public class OferowaneKwalifikacjeDAO implements iOferowaneKwalifikacjeDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public OferowaneKwalifikacjeEntity addRelation(int idSzablonu, int idKwalifikacji) {
        SzablonyPraktykEntity template = em.find(SzablonyPraktykEntity.class, idSzablonu);
        if (template !=null) {
            KwalifikacjeEntity qualification = em.find(KwalifikacjeEntity.class, idKwalifikacji);
            if (qualification !=null) {
                OferowaneKwalifikacjeEntity newRow = new OferowaneKwalifikacjeEntity();
                newRow.setIdSzablonu(idSzablonu);
                newRow.setIdKwalifikacji(idKwalifikacji);
                em.persist(newRow);
                newRow.setSzablon(template);
                newRow.setKwalifikacja(qualification);
                Collection<OferowaneKwalifikacjeEntity> relation = new HashSet<OferowaneKwalifikacjeEntity>();
                relation.add(newRow);
                template.setKwalifikacjeOferowaneByIdSzablonu(relation);
                qualification.setSzablonyByIdKwalifikacjiOferowanej(relation);
                return newRow;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Boolean checkExistence(int idKwalifikacji) {
        Query query = em.createQuery("SELECT ok FROM OferowaneKwalifikacjeEntity ok WHERE ok.idKwalifikacji=?1");
        query.setParameter(1, idKwalifikacji);
        if (query.getResultList().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
