package praktyki.core.DAO.Interface;

import praktyki.core.entities.OpiekunowieKierunkowiEntity;

/**
 * Created by dawid on 26.02.15.
 */
public interface iOpiekunowieKierunkowiDAO {

    public OpiekunowieKierunkowiEntity addRelation(OpiekunowieKierunkowiEntity opiekunowieKierunkowiEntity);

    public OpiekunowieKierunkowiEntity getRow(int idKierunku, int idOpiekunaPraktyk, int idPraktykodawcy);

    public Long deleteTutorFromEmployer(int idPraktykodawcy, int idOpiekunaPraktyk);

}
