package praktyki.core.DAO.Interface;

import praktyki.core.entities.TytulyZawodoweEntity;

/**
 * Created by dawid on 21.02.15.
 */
public interface iTytulyZawodoweDAO {

    public TytulyZawodoweEntity getTitle(int idTytuluZawodowego);
}
