package praktyki.core.DAO.Interface;

import praktyki.core.entities.OpiekunowiePraktykEntity;

import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */
public interface iOpiekunowiePraktykDAO {

    public OpiekunowiePraktykEntity addTutor (OpiekunowiePraktykEntity opiekunowiePraktykEntity);

    public OpiekunowiePraktykEntity updateTutor (Integer idOpiekunaPraktyk, OpiekunowiePraktykEntity opiekunowiePraktykEntity);

    public OpiekunowiePraktykEntity deleteTutor (Integer idOpiekunaPraktyk);

    public List<OpiekunowiePraktykEntity> findAll();

    public OpiekunowiePraktykEntity getTutor(Integer idOpiekunaPraktyk);

    public List<OpiekunowiePraktykEntity> getTutorInfo(int idOpiekunaPraktyk);


    public List<OpiekunowiePraktykEntity> getAllTutorsOfEmployer(int idPraktykodawcy);

    public List<OpiekunowiePraktykEntity> getTutorOfEmployer(int idPraktykodawcy, int idOpiekunaPraktyk);

}
