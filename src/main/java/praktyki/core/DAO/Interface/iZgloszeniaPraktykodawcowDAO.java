package praktyki.core.DAO.Interface;

import praktyki.core.entities.ZgloszeniaPraktykodawcowEntity;

import java.util.List;

/**
 * Created by dawid on 11.03.15.
 */
public interface iZgloszeniaPraktykodawcowDAO {

    public ZgloszeniaPraktykodawcowEntity addEmployerApplication(ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity);

    public List<ZgloszeniaPraktykodawcowEntity> findAll();

    public ZgloszeniaPraktykodawcowEntity getApplication(Long idZgloszeniaPraktykodawcy);

    public ZgloszeniaPraktykodawcowEntity editApplication(Long idZgloszeniaPraktykodawcy, ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity);

    public Long deleteApplication(Long idZgloszeniaPraktykodawcy);

    public List<ZgloszeniaPraktykodawcowEntity> getApplicationOfStudent(Long nrAlbumu);

    public List<ZgloszeniaPraktykodawcowEntity> checkForStudent(ZgloszeniaPraktykodawcowEntity zgloszeniaPraktykodawcowEntity);

    public List<ZgloszeniaPraktykodawcowEntity> getApplicationsOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego);

}
