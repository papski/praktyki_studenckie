package praktyki.core.DAO.Interface;

import praktyki.core.entities.PraktykiKierunkoweEntity;

import java.util.List;

/**
 * Created by dawid on 08.03.15.
 */
public interface iPraktykiKierunkoweDAO {

    public PraktykiKierunkoweEntity addRelation(PraktykiKierunkoweEntity praktykiKierunkoweEntity);

    public List<PraktykiKierunkoweEntity> findAll();

    public PraktykiKierunkoweEntity getRow(int idKierunku, int idTypuPraktyki, int idRokuAkademickiego);

    public PraktykiKierunkoweEntity editRow(int idKierunku, int idTypuPraktyki, int idRokuAkademickiego, PraktykiKierunkoweEntity praktykiKierunkoweEntity);

    public List<PraktykiKierunkoweEntity> getRelation(int idKierunku, int idRokuAkademickiego, int idTypyPraktyki);

    //public List<PraktykiKierunkoweEntity> getRelation(PraktykiKierunkoweEntity praktykiKierunkoweEntity);

    public Long deleteRelation(int idKierunku, int idRokuAkademickiego, int idTypyPraktyki);

}
