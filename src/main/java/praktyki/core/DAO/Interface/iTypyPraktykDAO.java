package praktyki.core.DAO.Interface;

import praktyki.core.entities.TypyPraktykEntity;

import java.util.List;

/**
 * Created by dawid on 05.02.15.
 */
public interface iTypyPraktykDAO {

    public TypyPraktykEntity addTypeOfTraineeship(TypyPraktykEntity typyPraktykEntity);

    public TypyPraktykEntity updateTypeOfTraineeship(Integer idTypuPraktyki, TypyPraktykEntity typyPraktykEntity);

    public TypyPraktykEntity deleteTypeOfTraineeship(Integer idTypuPraktyki);

    public List<TypyPraktykEntity> findAll();

    public TypyPraktykEntity getTypeOfTraineeship(Integer idTypuPraktyki);

}
