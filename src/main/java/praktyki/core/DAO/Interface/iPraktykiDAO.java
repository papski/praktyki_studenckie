package praktyki.core.DAO.Interface;

import praktyki.core.entities.PraktykiEntity;
import praktyki.core.entities.StudenciEntity;

import java.util.List;

/**
 * Created by dawid on 14.03.15.
 */
public interface iPraktykiDAO {

    public PraktykiEntity addRelation(PraktykiEntity praktykiEntity);

    public List<PraktykiEntity> findAll();

    public List<PraktykiEntity> getAvaliableCoursePractices(int idKierunku, int idRokuAkademickiego);

    public List<PraktykiEntity> getPracticeByCourseAcademicYear(int idKierunku, int idRokuAkademickiego);

    public PraktykiEntity getRow(Long idPraktykiStudenckiej);

    public PraktykiEntity editRow(Long idPraktykiStudenckiej, PraktykiEntity praktykiEntity);

    public Long deleteRow(Long idPraktykiStudenckiej);

    public List<StudenciEntity> getStudentsOfEmployerByCourseAcademicYear(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego);

    public List<PraktykiEntity> getPracticeOfStudent(Long nrAlbumu);

    public List<PraktykiEntity> checkForStudent(PraktykiEntity praktykiEntity);

    public List<PraktykiEntity> checkForAgreement(PraktykiEntity praktykiEntity);

    public List<PraktykiEntity> getPracticeOfEmployerByTutor(int idPraktykodawcy, int idOpiekunaPraktyk);

    public List<PraktykiEntity> getPracticesOfEmployer(int idPraktykodawcy, int idKierunku, int idRokuAkademickiego);

    public List<PraktykiEntity> getPracticeOfTutor(int idOpiekunaPraktyk);

    public List<PraktykiEntity> getPracticeOfCoordinator(int idKoordynatoraPraktyk);

    public List<PraktykiEntity> getPracticeOfCourse(int idKierunku);

    public List<PraktykiEntity> getPracticeOfCourseByAcademicYear(int idKierunku, int idRokuAkademickiego);

}
