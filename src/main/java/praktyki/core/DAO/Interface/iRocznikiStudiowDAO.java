package praktyki.core.DAO.Interface;

import praktyki.core.entities.RocznikiStudiowEntity;

/**
 * Created by dawid on 17.02.15.
 */
public interface iRocznikiStudiowDAO {

    public RocznikiStudiowEntity addRelation(RocznikiStudiowEntity rocznikiStudiowEntity);

    public RocznikiStudiowEntity getCourse(Integer idKierunku, Integer idRokuAkademickiego);

    public Long deleteYearsFromCourse(Integer idKierunku, Integer idRokuAkademickiego);

    public Long deleteEveryYearFromCourse(Integer idKierunku);

    public RocznikiStudiowEntity getRow(int idKierunku, int idRokuAkademickiego);

}
