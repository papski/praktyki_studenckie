package praktyki.core.DAO.Interface;

import praktyki.core.entities.TrybyStudiowEntity;

import java.util.List;

/**
 * Created by dawid on 14.02.15.
 */
public interface iTrybyStudiowDAO {

    public List<TrybyStudiowEntity> findAll();

    public TrybyStudiowEntity getType(int idTrybuStudiow);

}
