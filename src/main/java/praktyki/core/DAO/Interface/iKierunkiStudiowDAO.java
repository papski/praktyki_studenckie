package praktyki.core.DAO.Interface;

import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.rest.mvc.helpers.KierunekInfo;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
/**
 * Interface of FieldOfStudiesDAO, used for operations on FieldOfStudiesEntity
 */
public interface iKierunkiStudiowDAO {

    /**
     * Adds Field Of Study into DB.
     * @see praktyki.core.entities.KierunkiStudiowEntity
     * @param kierunkiStudiowEntity entity to be persisted
     * @param idTytuluZawodowego ID of Course's Degree
     * @param idStopniaStudiow ID of Course's Grade
     * @param idTrybuStudiow ID of Course's Mode
     * @return persisted entity
     */
    public KierunkiStudiowEntity addCourse (KierunkiStudiowEntity kierunkiStudiowEntity, int idTytuluZawodowego, int idStopniaStudiow, int idTrybuStudiow);

    /**
     * Updates Speciality or/and number of semesters of Field Of Study with given ID.
     * @param kierunkiStudiowEntity entity containing updated info
     * @param idKierunku ID of Course
     * @return updated entity
     */
    public KierunkiStudiowEntity updateCourse (KierunkiStudiowEntity kierunkiStudiowEntity, int idKierunku);

    /**
     * Deletes Field Of Study. Needs to be used at least 2x times to ensure
     * that everthing has been deleted.
     * @param idKierunku ID of Course
     * @return deleted row
     */
    public KierunkiStudiowEntity deleteCourse (int idKierunku);

    /**
     * Returns Field of Study with given ID
     * @param idKierunku ID of Course
     * @return row
     */
    public KierunkiStudiowEntity getCourse (int idKierunku);

    /**
     * Search for Field of Study  by it's name or/and Speciality. When there is no
     * need for searchString it should be set to "0".
     * @see praktyki.core.entities.KierunkiStudiowEntity
     * @param searchString Name or Speciality of Course
     * @param sortBy sort by which attribute of Field of Study Entity. Preferable ones in searchString
     * @param sortType ASC or DESC
     * @return search results
     */
    public List<KierunkiStudiowEntity> getCourseByNameSpeciality(String searchString, String sortBy, String sortType);

    /**
     * TO DELETE
     * @param idKierunku
     * @return
     */
    public List<KierunkiStudiowEntity> getCourseInfo (int idKierunku);

    /**
     * TO DELETE
     * @param idKierunku
     * @return
     */
    public KierunekInfo getSmallDataOfCourse(int idKierunku);

    /**
     * Finds Every Course in DB
     * @return every course
     */
    public List<KierunkiStudiowEntity> getAllCourses ();

    /**
     * Finds Course of Student with given ID
     * @param nrAlbumu Student Number
     * @return course
     */
    public List<KierunkiStudiowEntity> findCourseOfStudent(Long nrAlbumu);

    /**
     * Gets Course of Student with given ID
     * @param nrAlbumu Student Number
     * @param idKieurnku ID of Course
     * @return course
     */
    public List<KierunkiStudiowEntity> getCourseOfStudent(Long nrAlbumu, int idKieurnku);

    /**
     * Finds Courses of Coordinator with given ID
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @return Coordinator's courses
     */
    public List<KierunkiStudiowEntity> findCourseOfCoordinator(int idKoordynatoraPraktyk);

    /**
     * Gets Course of Coordinator with given ID
     * @param idKoordynatoraPraktyk ID of Coordinator
     * @param idKierunku ID of COurse
     * @return Coordinator's course
     */
    public List<KierunkiStudiowEntity> getCourseOfCoordinator(int idKoordynatoraPraktyk, int idKierunku);

}
