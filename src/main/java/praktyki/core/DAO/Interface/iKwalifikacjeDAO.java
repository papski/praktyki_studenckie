package praktyki.core.DAO.Interface;

import praktyki.core.entities.KwalifikacjeEntity;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */
public interface iKwalifikacjeDAO {

    public KwalifikacjeEntity addQualification(KwalifikacjeEntity kwalifikacjeEntity);

    public List<KwalifikacjeEntity> findRequiredQulificationsOfTemplate(int idSzablonu);

    public List<KwalifikacjeEntity> getRequiredQualificationOfTemplate(int idSzablonu, int idKwalifikacji);

    public List<KwalifikacjeEntity> findOfferedQulificationsOfTemplate(int idSzablonu);

    public List<KwalifikacjeEntity> getOfferedQualificationOfTemplate(int idSzablonu, int idKwalifikacji);

    public List<KwalifikacjeEntity> getQualificationsOfTemplate(int idSzablonu);
}
