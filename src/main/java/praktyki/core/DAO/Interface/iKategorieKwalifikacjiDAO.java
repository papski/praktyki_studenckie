package praktyki.core.DAO.Interface;

import praktyki.core.entities.KategorieKwalifikacjiEntity;

import java.util.List;

/**
 * Created by dawid on 22.03.15.
 */

/**
 * Interface of QualificationCategoriesDAO, used for operations on QualificationsCategoriesEntity
 */
public interface iKategorieKwalifikacjiDAO {

    /**
     * Adds Category of Qualification to Qualification with given ID
     * @see praktyki.core.entities.KategorieKwalifikacjiEntity
     * @param kategorieKwalifikacjiEntity entity to be persisted
     * @param idKwalifikacji ID of Qualification
     * @return persisted row
     */
    public KategorieKwalifikacjiEntity addCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    /**
     * Adds SubCategory of Qualification to Qualification with given ID
     * @see praktyki.core.entities.KategorieKwalifikacjiEntity
     * @param kategorieKwalifikacjiEntity entity to be persisted
     * @param idKwalifikacji ID of Qualification
     * @return persisted row
     */
    public KategorieKwalifikacjiEntity addSubCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji);

    /**
     * Returns list of Categories which does have name of searched string
     * @param nazwaKategorii name of Category
     * @return searched results
     */
    public List<KategorieKwalifikacjiEntity> findCategoryByName(String nazwaKategorii);

    /**
     * Returns every category
     * @return every category
     */
    public List<KategorieKwalifikacjiEntity> findCategory();

    /**
     * Returns Category with given ID
     * @param idKategoriiKwalifikacji ID of Qulifications Category
     * @return one category
     */
    public KategorieKwalifikacjiEntity getCategory(int idKategoriiKwalifikacji);

    /**
     * Returns category with given ID
     * @param idKategoriiKwalifikacji ID of Qulifications Category
     * @return category
     */
    public List<KategorieKwalifikacjiEntity> getInfoOfCategory(int idKategoriiKwalifikacji);

    /**
     * Returns list of subcategories which does have name of searched string
     * @param nazwaKategorii name of Subcategory
     * @return searched results
     */
    public List<KategorieKwalifikacjiEntity> findSubCategoryByName (String nazwaKategorii);

    /**
     * Returns every subcategory
     * @return every subcategory
     */
    public List<KategorieKwalifikacjiEntity> findSubCategory();

    /**
     * Finds list of Category's subcategories for given ID
     * @param idKategoriiKwalifikacji ID of Qualification Category
     * @return subcategories of category
     */
    public List<KategorieKwalifikacjiEntity> findSubCategoryOfCategory(int idKategoriiKwalifikacji);

    /**
     * Returns SubCategory with given ID
     * @param idKategoriiKwalifikacji ID of Qualification Category
     * @return one subcategory
     */
    public KategorieKwalifikacjiEntity getSubCategoryOfCategory(int idKategoriiKwalifikacji);

    /**
     * Deletes category and all his subcategories with given ID
     * @param idKategoriiKwalifikacji ID of Qualification Category
     * @return amount of deleted rows
     */
    public Long deleteCategory(int idKategoriiKwalifikacji);

    /**
     * Deletes subcategory with given ID
     * @param idKategoriiKwalifikacji ID of Qualification Category
     * @return amount of deleted rows
     */
    public Long deleteSubCategory(int idKategoriiKwalifikacji);
}
