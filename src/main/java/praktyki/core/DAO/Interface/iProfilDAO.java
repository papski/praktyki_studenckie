package praktyki.core.DAO.Interface;

import praktyki.core.entities.ProfilEntity;
import praktyki.core.entities.ProfilePraktykodawcowEntity;

import java.util.List;

/**
 * Created by dawid on 25.01.15.
 */
public interface iProfilDAO {

    public ProfilEntity addProfile(ProfilEntity profilEntity);

    public ProfilEntity updateProfile(Integer idProfilu, ProfilEntity profilEntity);

    public ProfilEntity deleteProfile(Integer idProfilu);

    public List<ProfilEntity> findAll();

    public ProfilEntity getProfile(Integer idProfilu);

    public List<ProfilEntity> findProfilesOfEmployeer(int idPraktykodawcy);

    public List<ProfilEntity> getProfileOfEmployeer(Integer idProfilu, int idPraktykodawcy);

}
