package praktyki.core.DAO.Interface;

import praktyki.core.entities.LataAkademickieEntity;

import java.util.List;

/**
 * Created by dawid on 17.02.15.
 */
public interface iLataAkademickieDAO {

    public LataAkademickieEntity getYearOfCourse(int idRokuAkademickiego);

    public List<LataAkademickieEntity> findEveryYearOfCourse(Integer idKierunku);

    public List<LataAkademickieEntity> getYearOfCourse(Integer idKierunku, Integer idRokuAkademickiego);

}
