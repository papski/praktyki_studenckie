package praktyki.core.DAO.Interface;

import praktyki.core.entities.StopnieStudiowEntity;

/**
 * Created by dawid on 21.02.15.
 */
public interface iStopnieStudiowDAO {

    public StopnieStudiowEntity getDegree(int idStopniaStudiow);

}
