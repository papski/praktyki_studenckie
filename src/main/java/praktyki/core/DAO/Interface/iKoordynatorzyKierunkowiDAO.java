package praktyki.core.DAO.Interface;

import praktyki.core.entities.KoordynatorzyKierunkowiEntity;
import praktyki.rest.mvc.helpers.KoordynatorzyDate;

import java.util.List;

/**
 * Created by dawid on 22.01.15.
 */
public interface iKoordynatorzyKierunkowiDAO {

    public KoordynatorzyKierunkowiEntity addRelation(KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity);

    public List<KoordynatorzyKierunkowiEntity> findAll();

    public KoordynatorzyKierunkowiEntity getRow(int idKoordynatoraPraktyk, int idKierunku);

    public KoordynatorzyDate getDate(int idKoordynatoraPraktyk, int idKierunku);

    public Long deleteCoordinatorFromCourse(int idKoordynatoraPraktyk, int idKierunku);

    public Long deleteCoordinatorFromEveryCourse(int idKoordynatoraPraktyk);

    public Long deleteEveryCoordinatorFromCourse(int idKierunku);

}
