package praktyki.core.DAO.Interface;

import praktyki.core.entities.KoordynatorzyKierunkowiEntity;
import praktyki.core.entities.KoordynatorzyKierunkowiPraktykodawcowEntity;

/**
 * Created by dawid on 22.02.15.
 */
public interface iKoordynatorzyKierunkowiPraktykodawcowDAO {

    public KoordynatorzyKierunkowiPraktykodawcowEntity addRelation(int idPraktykodawcy, KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity);

    public KoordynatorzyKierunkowiPraktykodawcowEntity getRow(int idPraktykodawcy, int idKierunku, int idKoordynatoraPraktyk);

    public Long deleteEmployerFromCoordinator (int idKoordynatoraPraktyk, int idPraktykodawcy);

    public Long deleteEveryEmployerFromCoordinator (int idKoordynatoraPraktyk);
}
