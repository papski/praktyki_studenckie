package praktyki.core.DAO.Interface;

import praktyki.core.entities.ProfilePraktykodawcowEntity;

import java.util.List;

/**
 * Created by dawid on 25.01.15.
 */
public interface iProfilePraktykodawcowDAO {

    public ProfilePraktykodawcowEntity addRelation(ProfilePraktykodawcowEntity profilePraktykodawcowEntity);

    public List<ProfilePraktykodawcowEntity> findAll();

    public Long deleteProfileFromEmployeer(int idPraktykodawcy, Integer idProfilu);

    public Long deleteEveryProfileFromEmployeer(int idPraktykodawcy);

    public Long deleteEveryEmployeerFromProfil(Integer idProfilu);

}
