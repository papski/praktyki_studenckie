package praktyki.core.DAO.Interface;

import praktyki.core.entities.StatusyEntity;

/**
 * Created by dawid on 20.03.15.
 */
public interface iStatusyDAO {

    public StatusyEntity getStatus(int idStatusu);

}
