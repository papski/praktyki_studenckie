package praktyki.core.DAO.Interface;

import praktyki.core.entities.PraktykodawcyEntity;

import java.util.List;

/**
 * Created by dawid on 06.01.15.
 */
public interface iPraktykodawcyDAO {

    public PraktykodawcyEntity addEmployeer(PraktykodawcyEntity praktykodawcyEntity);

    public PraktykodawcyEntity updateEmployeer(PraktykodawcyEntity praktykodawcyEntity, int idPraktykodawcy);

    public PraktykodawcyEntity editTrustedStatus(int idPraktykodawcy, Boolean zaufany);

    public PraktykodawcyEntity deleteEmployeer(int idPraktykodawcy);

    public PraktykodawcyEntity findEmployeer(int idPraktykodawcy);

    public List<PraktykodawcyEntity> getEmployerByName(String searchString, String sortType);

    public List<PraktykodawcyEntity> findEveryEmployeer();

    public Boolean checkNazwa(String nazwa);

    public List<PraktykodawcyEntity> findEmployeerOfProfile(int idProfilu);

    public List<PraktykodawcyEntity> getEmployeerOfProfile(int idPraktykodawcy, int idProfilu);

    public List<PraktykodawcyEntity> findEmployerOfCoordinator(int idKoordynatoraPraktyk);

    public List<PraktykodawcyEntity> getEmployerOfCoordinator(int idKoordynatoraPraktyk, int idPraktykodawcy);

}
