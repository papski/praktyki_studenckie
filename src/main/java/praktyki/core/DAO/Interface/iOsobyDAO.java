package praktyki.core.DAO.Interface;

import praktyki.core.entities.OsobyEntity;

import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */
public interface iOsobyDAO {

    public Long checkEmail(String email);

    public boolean isEmailUsed(String email);

    public Long checkTelefonKomorkowy(String telefonKomorkowy);

    public boolean isCellPhoneNumberUsed(String telefonKomorkowy);

    public Long checkTelefonStacjonarny(String telefonStacjonarny);

    public boolean isHomePhoneNumberUsed(String telefonStacjonarny);

    public OsobyEntity addPerson(OsobyEntity osobyEntity);

    public OsobyEntity updatePerson(Integer idOsoby, OsobyEntity osobyEntity);

    public List<OsobyEntity> getUserByNameSurname(String searchString, String sortBy, String sortType);

    public String[] getEmails(List<Integer> listOfPersonsId);

    public List<OsobyEntity> findAll();

    public OsobyEntity getPersonByIdOsoby(Integer idOsoby);

    public List<OsobyEntity> getInfoOfCoordinator(Integer idOsoby, int idKoordynatoraPraktyk);

    public List<OsobyEntity> getInfoOfStudent(Long nrAlbumu);

    public List<OsobyEntity> getInfoOfTutor(Integer idOpiekunaPraktyk);

    public OsobyEntity deletePerson(Integer idOsoby);

    public List<OsobyEntity> getCoordinatorOfEmployer(int idPraktykodawcy, int idKoordynatoraPraktyk);
}
