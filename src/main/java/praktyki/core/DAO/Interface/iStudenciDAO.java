package praktyki.core.DAO.Interface;

import praktyki.core.entities.StudenciEntity;

import java.util.List;

/**
 * Created by dawid on 12.12.14.
 */
public interface iStudenciDAO {

    public StudenciEntity addStudent(StudenciEntity studenciEntity);

    public StudenciEntity deleteStudent(Long nrAlbumu);

    public List<StudenciEntity> findEveryStudent();

    public StudenciEntity findByNrAlbumu(Long nrAlbumu);

    public List<StudenciEntity> getStudentInfo(Long nrAlbumu);

    public List<StudenciEntity> findByIdAdresu(Integer idAdresu);

    public List<StudenciEntity> findStudentByIdAdresu(Integer idAdresu);

    public List<StudenciEntity> findStudentsOfCourse(int idKierunku);

    public List<StudenciEntity> getStudentOfCourse(Long nrAlbumu, int idKierunku);

    public Long checkNrAlbumu(Long nrAlbumu);

    boolean isAlbumNumberUsed(Long nrAlbumu);
}
