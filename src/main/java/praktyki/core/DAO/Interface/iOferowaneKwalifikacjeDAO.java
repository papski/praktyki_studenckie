package praktyki.core.DAO.Interface;

import praktyki.core.entities.OferowaneKwalifikacjeEntity;

/**
 * Created by dawid on 28.02.15.
 */
public interface iOferowaneKwalifikacjeDAO {

    public OferowaneKwalifikacjeEntity addRelation(int idSzablonu, int idKwalifikacji);

    public Boolean checkExistence (int idKwalifikacji);
}
