package praktyki.core.DAO.Interface;

import praktyki.core.entities.KoordynatorzyPraktykEntity;

import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */
public interface iKoordynatorzyPraktykDAO {

    public KoordynatorzyPraktykEntity addCoordinator(KoordynatorzyPraktykEntity koordynatorzyPraktykEntity);

    public List<KoordynatorzyPraktykEntity> findAll();

    public KoordynatorzyPraktykEntity getCoordinator(int idKoordynatoraPraktyk);

    public List<KoordynatorzyPraktykEntity> getCoordinatorInfo(int idKoordynatoraPraktyk);

    public KoordynatorzyPraktykEntity deleteCoordinator(int idKoordynatoraPraktyk);

    public List<KoordynatorzyPraktykEntity> findCoordinatorOfCourse(int idKierunku);

    public List<KoordynatorzyPraktykEntity> getCoordinatorOfCourse(int idKierunku, int idKoordynatoraPraktyk);

    public List<KoordynatorzyPraktykEntity> findEveryCoordinatorOfEmployer(int idPraktykodawcy);

    //public List<KoordynatorzyPraktykEntity> getCoordinatorOfEmployer(int idPraktykodawcy, int idKoordynatoraPraktyk);
}
