package praktyki.core.DAO.Interface;

import praktyki.core.entities.AdresyEntity;

import java.util.List;

/**
 * Created by dawid on 29.12.14.
 */

/**
 * Interface of AddressesDAO, used for operations on AddressesEntity
 */
public interface iAdresyDAO {

    /**
     * Persisting new address into DB
     * @see praktyki.core.entities.AdresyEntity
     * @param adresyEntity AddressEntity to be persisted
     * @return persisted address
     */
    public AdresyEntity addAddress(AdresyEntity adresyEntity);

    /**
     * Updates address with given ID of address. When showed in browser
     * it should grab current data (ID, name, etc.) and set them in box.
     * Without this it will merge them with null value.
     * @see praktyki.core.entities.AdresyEntity
     * @param adresyEntity AddressEntity to be persisted
     * @param idAdresu ID of address
     * @return updated address
     */
    public AdresyEntity updateAddress(AdresyEntity adresyEntity, int idAdresu);

    /**
     * Deletes address with given ID. It MUST be used at least 2x times
     * to ensure proper deletion of address.
     * @param idAdresu ID of address
     * @return deleted address
     */
    public AdresyEntity deleteAddress(int idAdresu);

    /**
     * Retures every address from AddressesEntity
     * @return list of every address
     */
    public List<AdresyEntity> findEveryAddress();

    /**
     * Returns address with given ID
     * @param idAdresu ID of address
     * @return address with given ID
     */
    public AdresyEntity findByIdAdresu(int idAdresu);

    /**
     * Returns addresses searched by either Street Name, City or/and Zip Code.
     * When You don't want to use any search parameter You should put "0" in request.
     * @param searchString Street Name, City or/and Zip Code of address we want to search for
     * @param sortBy attributes by which we will sort our results, preferable the one from searchString
     * @param sortType ASC or DESC
     * @return list of searched addresses
     */
    public List<AdresyEntity> getAddressByStreetCityZipCode(String searchString, String sortBy, String sortType);

    /**
     * Returns list of student's address with given Student Number.
     * @param nrAlbumu (ID) Student Number
     * @return address of student
     */
    public List<AdresyEntity> getAddressOfStudent(Long nrAlbumu);

    /**
     * Returns list of employer's every address with given ID of employer
     * @param idPraktykodawcy ID of employer
     * @return employer's every address
     */
    public List<AdresyEntity> findAddressesOfEmployer(int idPraktykodawcy);

    /**
     * Returns employer's address with given ID of employer and address
     * @param idPraktykodawcy ID of employer
     * @param idAdresu ID of address
     * @return employer's address
     */
    public List<AdresyEntity> getAddressOfEmployer(int idPraktykodawcy, int idAdresu);
}
