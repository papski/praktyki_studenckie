package praktyki.core.DAO.Interface;
import praktyki.core.entities.UzytkownicyEntity;

import java.util.List;

/**
 * Created by dawid on 10.02.15.
 */
public interface iUzytkownicyDAO {

    public UzytkownicyEntity addUser(UzytkownicyEntity uzytkownicyEntity, Integer idOsoby, Integer idRoli);

    public UzytkownicyEntity getUser(int idUzytkownika);

    public List<UzytkownicyEntity> getUserByLogin(String searchString, String sortType);

    public UzytkownicyEntity editBlockade(int idUzytkownika, Boolean blokada);

    public UzytkownicyEntity getRowByLogin(String login);

    public List<UzytkownicyEntity> findAll();

    public List<UzytkownicyEntity> getInfoOfStudent(Integer idOsoby);

    public List<UzytkownicyEntity> getInfoOfCoordinator(Integer idOsoby);

    public List<UzytkownicyEntity> getInfoOfTutor(Integer idOsoby);

    public Boolean checkForExistence(Integer idOsoby);

    public Long checkLogin(String login);

    public Boolean isLoginUsed(String login);
}
