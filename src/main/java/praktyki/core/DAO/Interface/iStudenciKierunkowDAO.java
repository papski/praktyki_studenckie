package praktyki.core.DAO.Interface;

import praktyki.core.entities.RocznikiStudiowEntity;
import praktyki.core.entities.StudenciKierunkowEntity;

import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
public interface iStudenciKierunkowDAO {

    public StudenciKierunkowEntity addRelation(Long nrAlbumu, RocznikiStudiowEntity rocznikiStudiowEntity);

    public StudenciKierunkowEntity getStudent(Long nrAlbumu, int idKierunku);

    public StudenciKierunkowEntity getRow(Long nrAlbumu, int idKierunku, int idRokuAkademickiego);

    public List<StudenciKierunkowEntity> getByIdKierunku(int idKierunku);

    public List<StudenciKierunkowEntity> getByNrAlbumu(Long nrAlbumu);

    public List<StudenciKierunkowEntity> getAll();

    public Long deleteStudentFromCourse(Long nrAlbumu, int idKierunku);

    public Long deleteEveryStudentFromCourse(int idKierunku);
}
