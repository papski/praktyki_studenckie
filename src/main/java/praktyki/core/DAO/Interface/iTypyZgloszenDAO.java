package praktyki.core.DAO.Interface;

import praktyki.core.entities.TypyZgloszenEntity;

import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */
public interface iTypyZgloszenDAO {

    public TypyZgloszenEntity addRequest(TypyZgloszenEntity typyZgloszenEntity);

    public TypyZgloszenEntity updateRequest(Integer idTypuZgloszenia, TypyZgloszenEntity typyZgloszenEntity);

    public TypyZgloszenEntity deleteRequest(Integer idTypuZgloszenia);

    public List<TypyZgloszenEntity> findAll();

    public TypyZgloszenEntity getRequest(Integer idTypuZgloszenia);

}
