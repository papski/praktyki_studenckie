package praktyki.core.DAO.Interface;

import praktyki.core.entities.WymaganeKwalifikacjeEntity;

/**
 * Created by dawid on 28.02.15.
 */
public interface iWymaganeKwalifikacjeDAO {

    public WymaganeKwalifikacjeEntity addRelation(int idSzablonu, int idKwalifikacji);

    public Boolean checkExistence(int idKwalifikacji);
}
