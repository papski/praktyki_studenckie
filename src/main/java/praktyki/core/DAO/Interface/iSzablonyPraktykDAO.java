package praktyki.core.DAO.Interface;

import praktyki.core.entities.SzablonyPraktykEntity;

import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */
public interface iSzablonyPraktykDAO {

    public SzablonyPraktykEntity addTemplate(SzablonyPraktykEntity szablonyPraktykEntity);

    public SzablonyPraktykEntity getTemplate(int idSzablonu);

    public SzablonyPraktykEntity deleteTemplate(int idSzablonu);

    public List<SzablonyPraktykEntity> findAll();
}
