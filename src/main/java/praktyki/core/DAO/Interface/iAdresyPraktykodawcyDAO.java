package praktyki.core.DAO.Interface;

import praktyki.core.entities.AdresyPraktykodawcyEntity;

/**
 * Created by dawid on 11.02.15.
 */

/**
 * Interface of EmployerAddressesDAO, used for operations on EmployerAddressesEntity
 */
public interface iAdresyPraktykodawcyDAO {

    /**
     * Checks if employer's address is branch
     * @param idPraktykodawcy ID of employer
     * @param idAdresu ID of address
     * @return 1 for true , -1 for false
     */
    public Long checkBranch(int idPraktykodawcy, int idAdresu);

    /**
     * Adds relation between Employer and Address
     * @see praktyki.core.entities.AdresyPraktykodawcyEntity
     * @param adresyPraktykodawcyEntity relation to be persisted
     * @return persisted relation
     */
    public AdresyPraktykodawcyEntity addRelation(AdresyPraktykodawcyEntity adresyPraktykodawcyEntity);

    /**
     * Returns row with both matching ID params
     * @param idAdresu ID of address
     * @param idPraktykodawcy ID of employer
     * @return row
     */
    public AdresyPraktykodawcyEntity getRow(int idAdresu, int idPraktykodawcy);

    /**
     * Deletes address from employer
     * @param idPraktykodawcy ID of employer
     * @param idAdresu ID of address
     * @return amount of deleted rows
     */
    public Long deleteAddressFromEmployer(Integer idPraktykodawcy, Integer idAdresu);

    /**
     * Deletes every address of employer
     * @param idPraktykodawcy ID of employer
     * @return amount of deleted rows
     */
    public Long deleteEveryAddressFromEmployer(Integer idPraktykodawcy);
}
