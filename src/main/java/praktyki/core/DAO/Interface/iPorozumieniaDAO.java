package praktyki.core.DAO.Interface;

import praktyki.core.entities.PorozumieniaEntity;

import java.util.List;

/**
 * Created by dawid on 17.02.15.
 */
public interface iPorozumieniaDAO {

    public PorozumieniaEntity addAgreement (PorozumieniaEntity porozumieniaEntity);

    public PorozumieniaEntity getAgreement (Integer idPorozumienia);

    public PorozumieniaEntity deleteAgreements (Integer idPorozumienia);

    public List<PorozumieniaEntity> getAgreementsOfStudent(Long nrAlbumu);

    public List<PorozumieniaEntity> getAgreementsOfEmployer(int idPraktykodawcy);

    public List getAll();
}
