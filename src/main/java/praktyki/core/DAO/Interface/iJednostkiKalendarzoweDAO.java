package praktyki.core.DAO.Interface;

import praktyki.core.entities.JednostkiKalendarzoweEntity;

/**
 * Created by dawid on 22.03.15.
 */
/**
 * Interface of CalendarUnitsDAO, used for operations on CalendarUnitsEntity
 */
public interface iJednostkiKalendarzoweDAO {

    /**
     * Returns row from CalendarUnitsEntity
     * @param idJednostkiKalendarzowej ID of Calendar Unit
     * @return row
     */
    public JednostkiKalendarzoweEntity getRow(int idJednostkiKalendarzowej);

}
