package praktyki.core.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iKierunkiStudiowDAO;
import praktyki.core.DAO.Interface.iStopnieStudiowDAO;
import praktyki.core.DAO.Interface.iTrybyStudiowDAO;
import praktyki.core.DAO.Interface.iTytulyZawodoweDAO;
import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.core.entities.StopnieStudiowEntity;
import praktyki.core.entities.TrybyStudiowEntity;
import praktyki.core.entities.TytulyZawodoweEntity;
import praktyki.rest.mvc.helpers.KierunekDodatkoweInfo;
import praktyki.rest.mvc.helpers.KierunekInfo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 08.01.15.
 */
@Repository
public class KierunkiStudiowDAO implements iKierunkiStudiowDAO {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private iTrybyStudiowDAO itrybyStudiowDAO;

    @Autowired
    private iStopnieStudiowDAO istopnieStudiowDAO;

    @Autowired
    private iTytulyZawodoweDAO itytulyZawodoweDAO;


    @Override
    public KierunkiStudiowEntity addCourse(KierunkiStudiowEntity kierunkiStudiowEntity, int idTytuluZawodowego, int idStopniaStudiow, int idTrybuStudiow) {
        TytulyZawodoweEntity title = em.find(TytulyZawodoweEntity.class, idTytuluZawodowego);
        if (title !=null) {
            StopnieStudiowEntity degree = em.find(StopnieStudiowEntity.class, idStopniaStudiow);
            if (degree !=null) {
                TrybyStudiowEntity type = em.find(TrybyStudiowEntity.class, idTrybuStudiow);
                if (type !=null) {
                    em.persist(kierunkiStudiowEntity);
                    kierunkiStudiowEntity.setTytul(title);
                    kierunkiStudiowEntity.setIdTytuluZawodowego(idTytuluZawodowego);
                    kierunkiStudiowEntity.setStopien(degree);
                    kierunkiStudiowEntity.setIdStopniaStudiow(idStopniaStudiow);
                    kierunkiStudiowEntity.setTryb(type);
                    kierunkiStudiowEntity.setIdTrybuStudiow(idTrybuStudiow);
                    Collection<KierunkiStudiowEntity> course = new HashSet<KierunkiStudiowEntity>();
                    course.add(kierunkiStudiowEntity);
                    title.setKierunkiByIdTytulu(course);
                    degree.setKierunkiByIdStopnia(course);
                    type.setKierunkiByIdTrybu(course);
                    return kierunkiStudiowEntity;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public KierunkiStudiowEntity updateCourse(KierunkiStudiowEntity kierunkiStudiowEntity, int idKierunku) {
        KierunkiStudiowEntity course = em.find(KierunkiStudiowEntity.class, idKierunku);
        if (course !=null) {
            course.setIdKierunku(idKierunku);
            if (kierunkiStudiowEntity.getNazwaKierunku() !=null) {
                course.setNazwaKierunku(kierunkiStudiowEntity.getNazwaKierunku());
            }
            if(kierunkiStudiowEntity.getNazwaSpecjalnosci() !=null) {
                course.setNazwaSpecjalnosci(kierunkiStudiowEntity.getNazwaSpecjalnosci());
            }
            if(kierunkiStudiowEntity.getLiczbaSemestrow() > 0) {
                course.setLiczbaSemestrow(kierunkiStudiowEntity.getLiczbaSemestrow());
            }
            return course;
        }
        else {
            return null;
        }
    }

    @Override
    public KierunkiStudiowEntity deleteCourse(int idKierunku) {
        KierunkiStudiowEntity kierunkiStudiowEntity = em.find(KierunkiStudiowEntity.class, idKierunku);
        if(kierunkiStudiowEntity !=null) {
            Query query = em.createQuery("DELETE StudenciKierunkowEntity WHERE idKierunku=?1");
            query.setParameter(1, idKierunku);
            query.executeUpdate();
            Query query2 = em.createQuery("DELETE KoordynatorzyKierunkowiEntity WHERE idKierunku=?1");
            query2.setParameter(1, idKierunku);
            query2.executeUpdate();
            Query query3 = em.createQuery("DELETE RocznikiStudiowEntity WHERE idKierunku=?1");
            query3.setParameter(1, idKierunku);
            query3.executeUpdate();
            Query query4 = em.createQuery("DELETE OpiekunowieKierunkowiEntity ok WHERE ok.idKierunku=?1");
            query4.setParameter(1, idKierunku);
            query4.executeUpdate();
            Query query5 = em.createQuery("DELETE PraktykiKierunkoweEntity WHERE idKierunku=?1");
            query5.setParameter(1, idKierunku);
            query5.executeUpdate();
            Query query10 = em.createQuery("DELETE KierunkiStudiowEntity WHERE idKierunku=?1");
            query10.setParameter(1, idKierunku);
            query10.executeUpdate();
            //em.remove(kierunkiStudiowEntity);
            return kierunkiStudiowEntity;
        }
        else {
            return null;
        }
    }

    @Override
    public KierunkiStudiowEntity getCourse(int idKierunku) {
        KierunkiStudiowEntity kierunkiStudiowEntity = em.find(KierunkiStudiowEntity.class, idKierunku);
        if(kierunkiStudiowEntity !=null) {
            return kierunkiStudiowEntity;
        }
        else {
            return null;
        }
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseByNameSpeciality(String searchString, String sortBy, String sortType) {
        Query query = em.createQuery("SELECT ks, t, s, tt FROM KierunkiStudiowEntity ks JOIN ks.tytul t JOIN ks.stopien s JOIN ks.tryb tt WHERE ks.nazwaKierunku LIKE :searchString OR ks.nazwaSpecjalnosci LIKE :searchString ORDER BY ks." +sortBy +" " +sortType);
        query.setParameter("searchString", "%" +searchString +"%");
        return query.getResultList();
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseInfo(int idKierunku) {
        Query query = em.createQuery("SELECT ks, t, s, tt FROM KierunkiStudiowEntity ks JOIN ks.tytul t JOIN ks.stopien s JOIN ks.tryb tt WHERE ks.idKierunku=?1");
        query.setParameter(1, idKierunku);
        return query.getResultList();
    }

    @Override
    public KierunekInfo getSmallDataOfCourse(int idKierunku) {
        KierunkiStudiowEntity course = em.find(KierunkiStudiowEntity.class, idKierunku);
        if (course !=null) {
            KierunekInfo kierunekInfo = new KierunekInfo();
            kierunekInfo.setKierunek(course);
            KierunekDodatkoweInfo info = new KierunekDodatkoweInfo();
            info.setTrybStudiow(itrybyStudiowDAO.getType(course.getIdTrybuStudiow()).getTrybStudiow());
            //System.out.println(itrybyStudiowDAO.getType(idKierunku).getTrybStudiow());
            info.setStopienStudiow(istopnieStudiowDAO.getDegree(course.getIdStopniaStudiow()).getStopienStudiow());
            //System.out.println(istopnieStudiowDAO.getDegree(idKierunku).getStopienStudiow());
            info.setTytulZawodowy(itytulyZawodoweDAO.getTitle(course.getIdTytuluZawodowego()).getTytulZawodowy());
            //System.out.println(itytulyZawodoweDAO.getTitle(idKierunku).getTytulZawodowy());
            kierunekInfo.setDodatkoweInfo(info);
            return kierunekInfo;
        } else {
            return null;
        }
    }

    @Override
    public List<KierunkiStudiowEntity> getAllCourses() {
        Query query = em.createQuery("SELECT ks, t, s, tt FROM KierunkiStudiowEntity ks JOIN ks.tytul t JOIN ks.stopien s JOIN ks.tryb tt ORDER BY ks.idKierunku ASC");
        return query.getResultList();
    }

    @Override
    public List<KierunkiStudiowEntity> findCourseOfStudent(Long nrAlbumu) {
        Query query = em.createQuery("SELECT ks, t, s, tt FROM KierunkiStudiowEntity ks JOIN ks.rocznikiByIdKierunku rs JOIN rs.studenci ss JOIN ks.tytul t JOIN ks.stopien s JOIN ks.tryb tt WHERE ss.nrAlbumu=?1");
        //Query query = em.createQuery("SELECT ks FROM KierunkiStudiowEntity ks WHERE ks.kierunekByNrAlbumu.nrAlbumu=?1");
        query.setParameter(1, nrAlbumu);
        return query.getResultList();
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseOfStudent(Long nrAlbumu, int idKieurnku) {
        Query query = em.createQuery("SELECT ks, t, s ,tt FROM KierunkiStudiowEntity ks JOIN ks.rocznikiByIdKierunku rs JOIN rs.studenci ss JOIN ks.tytul t JOIN ks.stopien s JOIN ks.tryb tt WHERE ss.nrAlbumu=?1 AND ss.idKierunku=?2");
        query.setParameter(1, nrAlbumu);
        query.setParameter(2, idKieurnku);
        return query.getResultList();
    }

    @Override
    public List<KierunkiStudiowEntity> findCourseOfCoordinator(int idKoordynatoraPraktyk) {
        //Query query = em.createQuery("SELECT ks FROM KierunkiStudiowEntity ks JOIN ks.koordynatorByIdKierunku kk WHERE kk.idKoordynatoraPraktyk=?1");
        Query query = em.createQuery("SELECT ks, t, s, tt, kk.dataPowolania AS dataPowolania FROM KierunkiStudiowEntity ks JOIN ks.koordynatorByIdKierunku kk JOIN ks.tytul t JOIN ks.stopien s JOIN ks.tryb tt WHERE kk.idKoordynatoraPraktyk=?1");
        query.setParameter(1,idKoordynatoraPraktyk);
        return query.getResultList();
    }

    @Override
    public List<KierunkiStudiowEntity> getCourseOfCoordinator(int idKoordynatoraPraktyk, int idKierunku) {
        Query query = em.createQuery("SELECT ks, t, s, tt, kk.dataPowolania AS dataPowolania FROM KierunkiStudiowEntity ks JOIN ks.koordynatorByIdKierunku kk JOIN ks.tytul t JOIN ks.stopien s JOIN ks.tryb tt WHERE kk.idKoordynatoraPraktyk=?1 AND kk.idKierunku=?2");
        query.setParameter(1,idKoordynatoraPraktyk);
        query.setParameter(2, idKierunku);
        return query.getResultList();
    }
}
