package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iKoordynatorzyPraktykDAO;
import praktyki.core.entities.KoordynatorzyPraktykEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 21.01.15.
 */

@Repository
public class KoordynatorzyPraktykDAO implements iKoordynatorzyPraktykDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public KoordynatorzyPraktykEntity addCoordinator(KoordynatorzyPraktykEntity koordynatorzyPraktykEntity) {
        koordynatorzyPraktykEntity.setIdOsoby(null);
        em.persist(koordynatorzyPraktykEntity);
        return koordynatorzyPraktykEntity;
    }

    @Override
    public List<KoordynatorzyPraktykEntity> findAll() {
        Query query = em.createQuery("SELECT kp FROM KoordynatorzyPraktykEntity kp ORDER BY kp.idKoordynatoraPraktyk ASC");
        return query.getResultList();
    }

    @Override
    public KoordynatorzyPraktykEntity getCoordinator(int idKoordynatoraPraktyk) {
        return em.find(KoordynatorzyPraktykEntity.class, idKoordynatoraPraktyk);
    }

    @Override
    public List<KoordynatorzyPraktykEntity> getCoordinatorInfo(int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity coordinator = em.find(KoordynatorzyPraktykEntity.class, idKoordynatoraPraktyk);
        if (coordinator !=null) {
            Query query = em.createQuery("SELECT k, o FROM KoordynatorzyPraktykEntity k JOIN k.koordynatorByIdOsoby o WHERE o.idOsoby=?1 AND k.idKoordynatoraPraktyk=?2");
            query.setParameter(1, coordinator.getIdOsoby());
            query.setParameter(2, idKoordynatoraPraktyk);
            return query.getResultList();
        } else {
            return null;
        }
    }

    @Override
    public KoordynatorzyPraktykEntity deleteCoordinator(int idKoordynatoraPraktyk) {
        KoordynatorzyPraktykEntity coordinator = em.find(KoordynatorzyPraktykEntity.class, idKoordynatoraPraktyk);
        if (coordinator !=null) {
            Query query = em.createQuery("DELETE KoordynatorzyKierunkowiEntity k WHERE k.idKoordynatoraPraktyk=?1");
            query.setParameter(1, idKoordynatoraPraktyk);
            query.executeUpdate();
            Query query2 = em.createQuery("DELETE KoordynatorzyKierunkowiPraktykodawcowEntity kkp WHERE kkp.idKoordynatoraPraktyk=?1");
            query2.setParameter(1, idKoordynatoraPraktyk);
            query2.executeUpdate();
            Query query3 = em.createQuery("DELETE UzytkownicyEntity u WHERE u.idOsoby=?1");
            query3.setParameter(1, coordinator.getIdOsoby());
            query3.executeUpdate();
            Query query4 = em.createQuery("DELETE PraktykiEntity p WHERE p.idKoordynatoraPraktyk=?1");
            query4.setParameter(1, idKoordynatoraPraktyk);
            query4.executeUpdate();
            em.remove(coordinator);
            return coordinator;
        }
        else {
            return null;
        }
    }

    @Override
    public List<KoordynatorzyPraktykEntity> findCoordinatorOfCourse(int idKierunku) {
        Query query = em.createQuery("SELECT kp FROM KoordynatorzyPraktykEntity kp JOIN kp.koordynatorzyByIdKierunku kk WHERE kk.idKierunku=?1");
        query.setParameter(1, idKierunku);
        return query.getResultList();
    }

    @Override
    public List<KoordynatorzyPraktykEntity> getCoordinatorOfCourse(int idKierunku, int idKoordynatoraPraktyk) {
        Query query = em.createQuery("SELECT kp FROM KoordynatorzyPraktykEntity kp JOIN kp.koordynatorzyByIdKierunku kk WHERE kk.idKierunku=?1 AND kk.idKoordynatoraPraktyk=?2");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idKoordynatoraPraktyk);
        return query.getResultList();
    }

    @Override
    public List<KoordynatorzyPraktykEntity> findEveryCoordinatorOfEmployer(int idPraktykodawcy) {
        Query query = em.createQuery("SELECT kp FROM KoordynatorzyPraktykEntity kp JOIN kp.koordynatorzyByIdKierunku kk JOIN kk.praktykodawcaByKoordynator kkp WHERE kkp.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        return query.getResultList();
    }

}
