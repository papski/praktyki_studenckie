package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iPraktykodawcyDAO;
import praktyki.core.entities.PraktykodawcyEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 06.01.15.
 */

@Repository
public class PraktykodawcyDAO implements iPraktykodawcyDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Boolean checkNazwa(String nazwa) {
        Query query = em.createQuery("SELECT p FROM PraktykodawcyEntity p WHERE p.nazwa LIKE :nazwa");
        query.setParameter("nazwa", nazwa);
        if (query.getResultList().isEmpty()) {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public PraktykodawcyEntity addEmployeer(PraktykodawcyEntity praktykodawcyEntity) {
        em.persist(praktykodawcyEntity);
        return praktykodawcyEntity;
    }

    @Override
    public PraktykodawcyEntity updateEmployeer(PraktykodawcyEntity praktykodawcyEntity, int idPraktykodawcy) {
        PraktykodawcyEntity praktykodawca = em.find(PraktykodawcyEntity.class, idPraktykodawcy);
        if (praktykodawca !=null) {
            if(praktykodawca.getNazwa() !=null) {
                praktykodawca.setNazwa(praktykodawcyEntity.getNazwa());
            }
            if(praktykodawca.getPelnaNazwa() !=null) {
                praktykodawca.setPelnaNazwa(praktykodawcyEntity.getPelnaNazwa());
            }
        }
        return praktykodawca;
    }

    @Override
    public PraktykodawcyEntity editTrustedStatus(int idPraktykodawcy, Boolean zaufany) {
        if (zaufany == true || zaufany == false) {
            PraktykodawcyEntity employer = em.find(PraktykodawcyEntity.class, idPraktykodawcy);
            if (employer != null) {
                employer.setIdPraktykodawcy(idPraktykodawcy);
                employer.setZaufany(zaufany);
                return em.merge(employer);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public PraktykodawcyEntity deleteEmployeer(int idPraktykodawcy) {
        PraktykodawcyEntity praktykodawcyEntity = em.find(PraktykodawcyEntity.class, idPraktykodawcy);
        if(praktykodawcyEntity !=null) {
            Query query = em.createQuery("DELETE ProfilePraktykodawcowEntity p WHERE p.idPraktykodawcy=?1");
            query.setParameter(1, idPraktykodawcy);
            query.executeUpdate();
            Query query2 = em.createQuery("DELETE ProfilEntity WHERE idProfilu NOT IN (SELECT idProfilu FROM ProfilePraktykodawcowEntity)");
            query2.executeUpdate();
            Query query3 = em.createQuery("DELETE AdresyPraktykodawcyEntity a WHERE a.idPraktykodawcy=?1");
            query3.setParameter(1, idPraktykodawcy);
            query3.executeUpdate();
            Query query4 = em.createQuery("DELETE AdresyEntity  WHERE idAdresu NOT IN (SELECT idAdresu FROM AdresyPraktykodawcyEntity)");
            query4.executeUpdate();
            Query query5 = em.createQuery("DELETE KoordynatorzyKierunkowiPraktykodawcowEntity kkp WHERE kkp.idPraktykodawcy=?1");
            query5.setParameter(1, idPraktykodawcy);
            query5.executeUpdate();
            Query query6 = em.createQuery("DELETE OpiekunowieKierunkowiEntity ok WHERE ok.idPraktykodawcy=?1");
            query6.setParameter(1, idPraktykodawcy);
            query6.executeUpdate();
            Query query7 = em.createQuery("DELETE ZgloszeniaPraktykodawcowEntity zp WHERE zp.idPraktykodawcy=?1");
            query7.setParameter(1, idPraktykodawcy);
            query7.executeUpdate();
            Query query8 = em.createQuery("DELETE PraktykiEntity p WHERE p.idPraktykodawcy=?1");
            query8.setParameter(1, idPraktykodawcy);
            query8.executeUpdate();
            em.remove(praktykodawcyEntity);
            return praktykodawcyEntity;
        }
        else {
            return null;
        }
    }

    @Override
    public PraktykodawcyEntity findEmployeer(int idPraktykodawcy) {
        System.out.println(idPraktykodawcy);
        return em.find(PraktykodawcyEntity.class, idPraktykodawcy);
    }

    @Override
    public List<PraktykodawcyEntity> getEmployerByName(String searchString, String sortType) {
        Query query = em.createQuery("SELECT p FROM PraktykodawcyEntity p WHERE p.nazwa LIKE :searchString ORDER BY p.nazwa" +" " +sortType);
        query.setParameter("searchString", "%" +searchString +"%");
        return query.getResultList();
    }

    @Override
    public List<PraktykodawcyEntity> findEveryEmployeer() {
        Query query = em.createQuery("SELECT p FROM PraktykodawcyEntity p ORDER BY  p.idPraktykodawcy ASC");
        return query.getResultList();
    }

    @Override
    public List<PraktykodawcyEntity> findEmployeerOfProfile(int idProfilu) {
        Query query = em.createQuery("SELECT e FROM PraktykodawcyEntity e JOIN e.profileByIdPraktykodawcy pp WHERE pp.idProfilu=?1");
        query.setParameter(1, idProfilu);
        return query.getResultList();
    }

    @Override
    public List<PraktykodawcyEntity> getEmployeerOfProfile(int idPraktykodawcy, int idProfilu) {
        Query query = em.createQuery("SELECT e FROM PraktykodawcyEntity e JOIN e.profileByIdPraktykodawcy pp WHERE pp.idProfilu=?1 AND pp.idPraktykodawcy=?2");
        query.setParameter(1, idProfilu);
        query.setParameter(2, idPraktykodawcy);
        return query.getResultList();
    }

    @Override
    public List<PraktykodawcyEntity> findEmployerOfCoordinator(int idKoordynatoraPraktyk) {
        Query query = em.createQuery("SELECT p FROM PraktykodawcyEntity p JOIN p.koordynatorzyByIdPraktykodawcy kkp WHERE kkp.idKoordynatoraPraktyk=?1");
        query.setParameter(1, idKoordynatoraPraktyk);
        return query.getResultList();
    }

    @Override
    public List<PraktykodawcyEntity> getEmployerOfCoordinator(int idKoordynatoraPraktyk, int idPraktykodawcy) {
        Query query = em.createQuery("SELECT p FROM PraktykodawcyEntity p JOIN p.koordynatorzyByIdPraktykodawcy kkp  WHERE kkp.idKoordynatoraPraktyk=?1 AND kkp.idPraktykodawcy=?2");
        query.setParameter(1, idKoordynatoraPraktyk);
        query.setParameter(2, idPraktykodawcy);
        return query.getResultList();
    }
}
