package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iLataAkademickieDAO;
import praktyki.core.entities.LataAkademickieEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 17.02.15.
 */
@Repository
public class LataAkademickieDAO implements iLataAkademickieDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public LataAkademickieEntity getYearOfCourse(int idRokuAkademickiego) {
        return em.find(LataAkademickieEntity.class, idRokuAkademickiego);
    }

    @Override
    public List<LataAkademickieEntity> findEveryYearOfCourse(Integer idKierunku) {
        Query query = em.createQuery("SELECT la FROM LataAkademickieEntity la JOIN la.kierunkiByIdRoku rs WHERE rs.idKierunku=?1");
        query.setParameter(1, idKierunku);
        return query.getResultList();
    }

    @Override
    public List<LataAkademickieEntity> getYearOfCourse(Integer idKierunku, Integer idRokuAkademickiego) {
        Query query = em.createQuery("SELECT la FROM LataAkademickieEntity la JOIN la.kierunkiByIdRoku rs WHERE rs.idKierunku=?1 AND rs.idRokuAkademickiego=?2");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idRokuAkademickiego);
        return query.getResultList();
    }
}
