package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iTytulyZawodoweDAO;
import praktyki.core.entities.TytulyZawodoweEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 21.02.15.
 */

@Repository
public class TytulyZawodoweDAO implements iTytulyZawodoweDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public TytulyZawodoweEntity getTitle(int idTytuluZawodowego) {
        Query query = em.createQuery("SELECT t FROM TytulyZawodoweEntity t JOIN t.kierunkiByIdTytulu ks WHERE ks.idTytuluZawodowego=?1");
        query.setParameter(1, idTytuluZawodowego);
        List list = query.getResultList();
        System.out.println(list.get(0));
        TytulyZawodoweEntity row;
        if (!list.isEmpty()) {
            return row = (TytulyZawodoweEntity) list.get(0);
        } else {
            return null;
        }
    }
}
