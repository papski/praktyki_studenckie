package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iKategorieKwalifikacjiDAO;
import praktyki.core.entities.KategorieKwalifikacjiEntity;
import praktyki.core.entities.KwalifikacjeEntity;
import praktyki.core.exceptions.ExistenceException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 22.03.15.
 */

@Repository
public class KategorieKwalifikacjiDAO implements iKategorieKwalifikacjiDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public KategorieKwalifikacjiEntity addCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        KwalifikacjeEntity mainQuali = em.find(KwalifikacjeEntity.class, idKwalifikacji);
        if (mainQuali !=null) {
            em.persist(kategorieKwalifikacjiEntity);
            kategorieKwalifikacjiEntity.setIdKwalifikacji(idKwalifikacji);
            kategorieKwalifikacjiEntity.setIdKategoriiNadrzednej(null);
            //kategorieKwalifikacjiEntity.setParent(kategorieKwalifikacjiEntity);
            kategorieKwalifikacjiEntity.setParent(null);
            kategorieKwalifikacjiEntity.setChild(null);
            kategorieKwalifikacjiEntity.setKwalifikacja(mainQuali);
            Collection<KategorieKwalifikacjiEntity> relation = new HashSet<KategorieKwalifikacjiEntity>();
            relation.add(kategorieKwalifikacjiEntity);
            mainQuali.setKwalifikacje(relation);
            return kategorieKwalifikacjiEntity;
        } else {
            return null;
        }
    }

    @Override
    public KategorieKwalifikacjiEntity addSubCategoryOfQualification(KategorieKwalifikacjiEntity kategorieKwalifikacjiEntity, int idKwalifikacji) {
        KwalifikacjeEntity mainQuali = em.find(KwalifikacjeEntity.class, idKwalifikacji);
        if (mainQuali !=null) {
            KategorieKwalifikacjiEntity category = em.find(KategorieKwalifikacjiEntity.class, kategorieKwalifikacjiEntity.getIdKategoriiNadrzednej());
            if (category != null) {
                em.persist(kategorieKwalifikacjiEntity);
                kategorieKwalifikacjiEntity.setIdKwalifikacji(idKwalifikacji);
                kategorieKwalifikacjiEntity.setParent(category);
                kategorieKwalifikacjiEntity.setChild(null);
                kategorieKwalifikacjiEntity.setKwalifikacja(mainQuali);
                Collection<KategorieKwalifikacjiEntity> relation = new HashSet<KategorieKwalifikacjiEntity>();
                relation.add(kategorieKwalifikacjiEntity);
                category.setChild(relation);
                mainQuali.setKwalifikacje(relation);
                return kategorieKwalifikacjiEntity;
            } else {
                throw new ExistenceException("there is no category with such ID");
            }
        } else {
            throw  new ExistenceException("there is no main qualification with such ID");
        }
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findCategoryByName(String nazwaKategorii) {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.nazwaKategorii=?1 AND kk.idKategoriiNadrzednej IS NULL");
        return query.getResultList();
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findCategory() {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiNadrzednej IS NULL");
        return query.getResultList();
    }

    @Override
    public KategorieKwalifikacjiEntity getCategory(int idKategoriiKwalifikacji) {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiKwalifikacji=?1 AND kk.idKategoriiNadrzednej IS NULL");
        //Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiKwalifikacji=?1 AND kk.idKategoriiNadrzednej=?1");
        query.setParameter(1, idKategoriiKwalifikacji);
        KategorieKwalifikacjiEntity row = new KategorieKwalifikacjiEntity();
        if (!query.getResultList().isEmpty()) {
            return row = (KategorieKwalifikacjiEntity) query.getResultList().get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<KategorieKwalifikacjiEntity> getInfoOfCategory(int idKategoriiKwalifikacji) {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiKwalifikacji=?1 AND kk.idKategoriiNadrzednej IS NULL");
        query.setParameter(1, idKategoriiKwalifikacji);
        return query.getResultList();
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategoryByName(String nazwaKategorii) {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.nazwaKategorii=?1 AND kk.idKategoriiNadrzednej IS NOT NULL");
        return query.getResultList();
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategory() {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiNadrzednej IS NOT NULL");
        return query.getResultList();
    }

    @Override
    public List<KategorieKwalifikacjiEntity> findSubCategoryOfCategory(int idKategoriiNadrzednej) {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiNadrzednej=?1");
        query.setParameter(1, idKategoriiNadrzednej);
        return query.getResultList();
    }

    @Override
    public KategorieKwalifikacjiEntity getSubCategoryOfCategory(int idKategoriiNadrzednej) {
        Query query = em.createQuery("SELECT kk FROM KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiNadrzednej=?1");
        query.setParameter(1, idKategoriiNadrzednej);
        KategorieKwalifikacjiEntity row = new KategorieKwalifikacjiEntity();
        if (!query.getResultList().isEmpty()) {
            return row = (KategorieKwalifikacjiEntity) query.getResultList().get(0);
        } else {
            return null;
        }
    }

    @Override
    public Long deleteCategory(int idKategoriiKwalifikacji) {
        Query query = em.createQuery("DELETE KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiKwalifikacji=?1 OR kk.idKategoriiNadrzednej=?1");
        query.setParameter(1, idKategoriiKwalifikacji);
        int temp = query.executeUpdate();
        Long update = Long.valueOf(temp);
        if (temp > 0) {
            return update;
        } else {
            return null;
        }
    }

    @Override
    public Long deleteSubCategory(int idKategoriiKwalifikacji) {
        Query query = em.createQuery("DELETE KategorieKwalifikacjiEntity kk WHERE kk.idKategoriiKwalifikacji=?1 AND kk.idKategoriiNadrzednej IS NOT NULL");
        query.setParameter(1, idKategoriiKwalifikacji);
        int temp = query.executeUpdate();
        Long update = Long.valueOf(temp);
        if (temp > 0) {
            return update;
        } else {
            return null;
        }
    }
}
