package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iKoordynatorzyKierunkowiPraktykodawcowDAO;
import praktyki.core.entities.KoordynatorzyKierunkowiEntity;
import praktyki.core.entities.KoordynatorzyKierunkowiPraktykodawcowEntity;
import praktyki.core.entities.PraktykodawcyEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 22.02.15.
 */

@Repository
public class KoordynatorzyKierunkowiPraktykodawcowDAO implements iKoordynatorzyKierunkowiPraktykodawcowDAO {

    @PersistenceContext
    private EntityManager em;


    @Override
    public KoordynatorzyKierunkowiPraktykodawcowEntity addRelation(int idPraktykodawcy, KoordynatorzyKierunkowiEntity koordynatorzyKierunkowiEntity) {
        PraktykodawcyEntity employer = em.find(PraktykodawcyEntity.class, idPraktykodawcy);
        if (employer !=null) {
            KoordynatorzyKierunkowiPraktykodawcowEntity row = new KoordynatorzyKierunkowiPraktykodawcowEntity();
            row.setIdPraktykodawcy(idPraktykodawcy);
            row.setIdKierunku(koordynatorzyKierunkowiEntity.getIdKierunku());
            row.setIdKoordynatoraPraktyk(koordynatorzyKierunkowiEntity.getIdKoordynatoraPraktyk());
            em.persist(row);
            row.setPraktykodawca(employer);
            row.setKoordynatorKierunku(koordynatorzyKierunkowiEntity);
            Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> relation = new HashSet<KoordynatorzyKierunkowiPraktykodawcowEntity>();
            relation.add(row);
            employer.setKoordynatorzyByIdPraktykodawcy(relation);
            koordynatorzyKierunkowiEntity.setPraktykodawcaByKoordynator(relation);
            return row;
        } else {
            return null;
        }
    }

    @Override
    public KoordynatorzyKierunkowiPraktykodawcowEntity getRow(int idPraktykodawcy, int idKierunku, int idKoordynatoraPraktyk) {
        Query query = em.createQuery("SELECT kkp FROM KoordynatorzyKierunkowiPraktykodawcowEntity kkp WHERE kkp.idPraktykodawcy=?1 AND kkp.idKierunku=?2 AND kkp.idKoordynatoraPraktyk=?3");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idKierunku);
        query.setParameter(3, idKoordynatoraPraktyk);
        List list = query.getResultList();
        KoordynatorzyKierunkowiPraktykodawcowEntity row = new KoordynatorzyKierunkowiPraktykodawcowEntity();
        if (!list.isEmpty()) {
            return row = (KoordynatorzyKierunkowiPraktykodawcowEntity) list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Long deleteEmployerFromCoordinator(int idKoordynatoraPraktyk, int idPraktykodawcy) {
        Query query = em.createQuery("DELETE KoordynatorzyKierunkowiPraktykodawcowEntity kkp WHERE kkp.idKoordynatoraPraktyk=?1 AND kkp.idPraktykodawcy=?2");
        query.setParameter(1, idKoordynatoraPraktyk);
        query.setParameter(2, idPraktykodawcy);
        int temp = query.executeUpdate();
        Long updated = Long.valueOf(temp);
        if (temp > 0) {
            return updated;
        } else {
            return null;
        }
    }

    @Override
    public Long deleteEveryEmployerFromCoordinator(int idKoordynatoraPraktyk) {
        Query query = em.createQuery("DELETE KoordynatorzyKierunkowiPraktykodawcowEntity kkp WHERE kkp.idKoordynatoraPraktyk=?1");
        query.setParameter(1, idKoordynatoraPraktyk);
        int temp = query.executeUpdate();
        Long updated = Long.valueOf(temp);
        if (temp > 0) {
            return updated;
        } else {
            return null;
        }
    }
}
