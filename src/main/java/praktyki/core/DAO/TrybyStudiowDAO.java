package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iTrybyStudiowDAO;
import praktyki.core.entities.TrybyStudiowEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 14.02.15.
 */

@Repository
public class TrybyStudiowDAO implements iTrybyStudiowDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TrybyStudiowEntity> findAll() {
        Query query = em.createQuery("SELECT t FROM TrybyStudiowEntity t");
        return query.getResultList();
    }

    @Override
    public TrybyStudiowEntity getType(int idTrybuStudiow) {
        Query query = em.createQuery("SELECT t FROM TrybyStudiowEntity t JOIN t.kierunkiByIdTrybu ks WHERE ks.idTrybuStudiow=?1");
        query.setParameter(1, idTrybuStudiow);
        List list = query.getResultList();
        TrybyStudiowEntity row;
        if (!list.isEmpty()) {
            return row = (TrybyStudiowEntity) list.get(0);
        } else {
            return null;
        }
    }
}
