package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iAdresyPraktykodawcyDAO;
import praktyki.core.entities.AdresyEntity;
import praktyki.core.entities.AdresyPraktykodawcyEntity;
import praktyki.core.entities.PraktykodawcyEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 11.02.15.
 */

@Repository
public class AdresyPraktykodawcyDAO implements iAdresyPraktykodawcyDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Long checkBranch(int idPraktykodawcy, int idAdresu) {
        Query query = em.createQuery("SELECT a FROM AdresyPraktykodawcyEntity a WHERE a.idPraktykodawcy=?1 AND a.idAdresu=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idAdresu);
        List list = query.getResultList();
        AdresyPraktykodawcyEntity row = new AdresyPraktykodawcyEntity();
        if (!query.getResultList().isEmpty()) {
            row = (AdresyPraktykodawcyEntity) list.get(0);
            if (row.getOddzial() == true) {
                return new Long(1);
            } else {
                return new Long(-1);
            }
        } else {
            return null;
        }
    }

    @Override
    public AdresyPraktykodawcyEntity addRelation(AdresyPraktykodawcyEntity adresyPraktykodawcyEntity) {
        Integer idPraktykodawcy = adresyPraktykodawcyEntity.getIdPraktykodawcy();
        PraktykodawcyEntity employer = em.find(PraktykodawcyEntity.class, idPraktykodawcy);
        if (employer != null) {
            Integer idAdresu = adresyPraktykodawcyEntity.getIdAdresu();
            AdresyEntity address = em.find(AdresyEntity.class, idAdresu);
            if (address != null) {
                em.persist(adresyPraktykodawcyEntity);
                adresyPraktykodawcyEntity.setAdres(address);
                adresyPraktykodawcyEntity.setPraktykodawca(employer);
                Collection<AdresyPraktykodawcyEntity> relation = new HashSet<AdresyPraktykodawcyEntity>();
                relation.add(adresyPraktykodawcyEntity);
                employer.setAdresyByIdPraktykodawcy(relation);
                address.setPraktykodawcyByIdAdresu(relation);
                return adresyPraktykodawcyEntity;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public AdresyPraktykodawcyEntity getRow(int idAdresu, int idPraktykodawcy) {
        Query query = em.createQuery("SELECT ap FROM AdresyPraktykodawcyEntity ap WHERE ap.idAdresu=?1 AND ap.idPraktykodawcy=?2");
        query.setParameter(1, idAdresu);
        query.setParameter(2, idPraktykodawcy);
        List list = query.getResultList();
        AdresyPraktykodawcyEntity row = new AdresyPraktykodawcyEntity();
        if (!list.isEmpty()) {
            return row = (AdresyPraktykodawcyEntity) list.get(0);
        } else {
            return null;
        }
    }

    /*@Override
    public Long deleteAddressFromEmployer(Integer idPraktykodawcy, Integer idAdresu) {
        Query query = em.createQuery("DELETE AdresyPraktykodawcyEntity ap WHERE ap.idPraktykodawcy=?1 AND ap.idAdresu=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idAdresu);
        int number = query.executeUpdate();
        Query query2 = em.createQuery("DELETE AdresyEntity  WHERE idAdresu NOT IN (SELECT idAdresu FROM AdresyPraktykodawcyEntity)");
        query2.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }

    @Override
    public Long deleteEveryAddressFromEmployer(Integer idPraktykodawcy) {
        Query query = em.createQuery("DELETE AdresyPraktykodawcyEntity ap WHERE ap.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        int number = query.executeUpdate();
        Query query2 = em.createQuery("DELETE AdresyEntity  WHERE idAdresu NOT IN (SELECT idAdresu FROM AdresyPraktykodawcyEntity)");
        query2.executeUpdate();
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }*/

    @Override
    public Long deleteAddressFromEmployer(Integer idPraktykodawcy, Integer idAdresu) {
        Query query = em.createQuery("DELETE AdresyPraktykodawcyEntity ap WHERE ap.idPraktykodawcy=?1 AND ap.idAdresu=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idAdresu);
        int number = query.executeUpdate();
        Query query2 = em.createQuery("DELETE AdresyEntity a WHERE a.idAdresu =?1");
        query2.setParameter(1, idAdresu);
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }

    @Override
    public Long deleteEveryAddressFromEmployer(Integer idPraktykodawcy) {
        Query query = em.createQuery("SELECT ap FROM AdresyPraktykodawcyEntity ap WHERE ap.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        List<AdresyPraktykodawcyEntity> temp = query.getResultList();
        Query query2 = em.createQuery("DELETE AdresyPraktykodawcyEntity ap WHERE ap.idPraktykodawcy=?1");
        query2.setParameter(1, idPraktykodawcy);
        int number = query2.executeUpdate();
        for(int i = 0; i < temp.size(); i++) {
            Query query3 = em.createQuery("DELETE AdresyEntity a WHERE a.idAdresu=?1");
            query3.setParameter(1, temp.get(i).getIdAdresu());
            query3.executeUpdate();
        }
        Long updated = Long.valueOf(number);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }
}
