package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iOpiekunowiePraktykDAO;
import praktyki.core.entities.OpiekunowiePraktykEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 04.02.15.
 */

@Repository
public class OpiekunowiePraktykDAO implements iOpiekunowiePraktykDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public OpiekunowiePraktykEntity addTutor(OpiekunowiePraktykEntity opiekunowiePraktykEntity) {
        em.persist(opiekunowiePraktykEntity);
        return opiekunowiePraktykEntity;
    }

    @Override
    public OpiekunowiePraktykEntity updateTutor(Integer idOpiekunaPraktyk, OpiekunowiePraktykEntity opiekunowiePraktykEntity) {
        OpiekunowiePraktykEntity tutor = em.find(OpiekunowiePraktykEntity.class, idOpiekunaPraktyk);
        if (tutor !=null) {
            tutor.setIdOpiekunaPraktyk(idOpiekunaPraktyk);
            tutor.setStanowisko(opiekunowiePraktykEntity.getStanowisko());
            return tutor;
        } else {
            return null;
        }
    }

    @Override
    public OpiekunowiePraktykEntity deleteTutor(Integer idOpiekunaPraktyk) {
        OpiekunowiePraktykEntity tutor = em.find(OpiekunowiePraktykEntity.class, idOpiekunaPraktyk);
        if (tutor !=null) {
            Query query = em.createQuery("DELETE UzytkownicyEntity WHERE idOsoby=?1");
            query.setParameter(1, tutor.getIdOsoby());
            query.executeUpdate();
            Query query2 = em.createQuery("DELETE OpiekunowieKierunkowiEntity ok WHERE ok.idOpiekunaPraktyk=?1");
            query2.setParameter(1, idOpiekunaPraktyk);
            query2.executeUpdate();
            Query query3 = em.createQuery("DELETE PraktykiEntity p WHERE p.idOpiekunaPraktyk=?1");
            query3.setParameter(1, idOpiekunaPraktyk);
            query3.executeUpdate();
            em.remove(tutor);
            return tutor;
        } else {
            return null;
        }
    }

    @Override
    public List<OpiekunowiePraktykEntity> findAll() {
        Query query = em.createQuery("SELECT op FROM OpiekunowiePraktykEntity op ORDER BY op.idOpiekunaPraktyk ASC");
        return query.getResultList();
    }

    @Override
    public OpiekunowiePraktykEntity getTutor(Integer idOpiekunaPraktyk) {
        return em.find(OpiekunowiePraktykEntity.class, idOpiekunaPraktyk);
    }

    @Override
    public List<OpiekunowiePraktykEntity> getTutorInfo(int idOpiekunaPraktyk) {
        OpiekunowiePraktykEntity tutor = em.find(OpiekunowiePraktykEntity.class, idOpiekunaPraktyk);
        if (tutor !=null) {
            Query query = em.createQuery("SELECT op, o FROM OpiekunowiePraktykEntity op JOIN op.opiekunByIdOsoby o WHERE o.idOsoby=?1 AND op.idOpiekunaPraktyk=?2");
            query.setParameter(1, tutor.getIdOsoby());
            query.setParameter(2, idOpiekunaPraktyk);
            return query.getResultList();
        } else {
            return null;
        }
    }

    @Override
    public List<OpiekunowiePraktykEntity> getAllTutorsOfEmployer(int idPraktykodawcy) {
        Query query = em.createQuery("SELECT op, o  FROM OpiekunowiePraktykEntity op JOIN op.opiekunByIdOsoby o JOIN op.kierunekPraktykodawcy ok WHERE ok.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        return query.getResultList();
    }

    @Override
    public List<OpiekunowiePraktykEntity> getTutorOfEmployer(int idPraktykodawcy, int idOpiekunaPraktyk) {
        Query query = em.createQuery("SELECT op, o  FROM OpiekunowiePraktykEntity op JOIN op.opiekunByIdOsoby o JOIN op.kierunekPraktykodawcy ok WHERE ok.idPraktykodawcy=?1 AND ok.idOpiekunaPraktyk=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idOpiekunaPraktyk);
        return query.getResultList();
    }

}
