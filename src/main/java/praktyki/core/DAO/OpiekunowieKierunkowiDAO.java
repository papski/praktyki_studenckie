package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iOpiekunowieKierunkowiDAO;
import praktyki.core.entities.KierunkiStudiowEntity;
import praktyki.core.entities.OpiekunowieKierunkowiEntity;
import praktyki.core.entities.OpiekunowiePraktykEntity;
import praktyki.core.entities.PraktykodawcyEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by dawid on 26.02.15.
 */

@Repository
public class OpiekunowieKierunkowiDAO implements iOpiekunowieKierunkowiDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public OpiekunowieKierunkowiEntity addRelation(OpiekunowieKierunkowiEntity opiekunowieKierunkowiEntity) {
        KierunkiStudiowEntity course = em.find(KierunkiStudiowEntity.class, opiekunowieKierunkowiEntity.getIdKierunku());
        if (course !=null) {
            OpiekunowiePraktykEntity tutor = em.find(OpiekunowiePraktykEntity.class, opiekunowieKierunkowiEntity.getIdOpiekunaPraktyk());
            if (tutor !=null) {
                PraktykodawcyEntity employer = em.find(PraktykodawcyEntity.class, opiekunowieKierunkowiEntity.getIdPraktykodawcy());
                if (employer !=null) {
                    em.persist(opiekunowieKierunkowiEntity);
                    opiekunowieKierunkowiEntity.setKierunek(course);
                    opiekunowieKierunkowiEntity.setOpiekun(tutor);
                    opiekunowieKierunkowiEntity.setPraktykodawca(employer);
                    Collection<OpiekunowieKierunkowiEntity> relation = new HashSet<OpiekunowieKierunkowiEntity>();
                    relation.add(opiekunowieKierunkowiEntity);
                    course.setOpiekunPraktykodawcy(relation);
                    tutor.setKierunekPraktykodawcy(relation);
                    employer.setOpiekunKierunku(relation);
                    return opiekunowieKierunkowiEntity;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public OpiekunowieKierunkowiEntity getRow(int idKierunku, int idOpiekunaPraktyk, int idPraktykodawcy) {
        Query query = em.createQuery("SELECT ok FROM OpiekunowieKierunkowiEntity ok WHERE ok.idKierunku=?1 AND ok.idOpiekunaPraktyk=?2 AND ok.idPraktykodawcy=?3");
        query.setParameter(1, idKierunku);
        query.setParameter(2, idOpiekunaPraktyk);
        query.setParameter(3, idPraktykodawcy);
        List list = query.getResultList();
        OpiekunowieKierunkowiEntity row = new OpiekunowieKierunkowiEntity();
        if (!list.isEmpty()) {
            return row = (OpiekunowieKierunkowiEntity) list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Long deleteTutorFromEmployer(int idPraktykodawcy, int idOpiekunaPraktyk) {
        Query query = em.createQuery("DELETE OpiekunowieKierunkowiEntity ok WHERE ok.idPraktykodawcy=?1 AND ok.idOpiekunaPraktyk=?2");
        query.setParameter(1, idPraktykodawcy);
        query.setParameter(2, idOpiekunaPraktyk);
        int temp = query.executeUpdate();
        Long updated = Long.valueOf(temp);
        if (updated > 0) {
            return updated;
        } else {
            return null;
        }
    }
}
