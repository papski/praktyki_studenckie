package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iSzablonyPraktykDAO;
import praktyki.core.entities.SzablonyPraktykEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 28.02.15.
 */

@Repository
public class SzablonyPraktykDAO implements iSzablonyPraktykDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public SzablonyPraktykEntity addTemplate(SzablonyPraktykEntity szablonyPraktykEntity) {
        em.persist(szablonyPraktykEntity);
        return szablonyPraktykEntity;
    }

    @Override
    public SzablonyPraktykEntity getTemplate(int idSzablonu) {
        return em.find(SzablonyPraktykEntity.class, idSzablonu);
    }

    @Override
    public SzablonyPraktykEntity deleteTemplate(int idSzablonu) {
        SzablonyPraktykEntity template = em.find(SzablonyPraktykEntity.class, idSzablonu);
        if (template !=null) {
            em.remove(template);
            return template;
        } else {
            return null;
        }
    }

    @Override
    public List<SzablonyPraktykEntity> findAll() {
        Query query = em.createQuery("SELECT sp FROM SzablonyPraktykEntity sp ORDER BY sp.idSzablonu ASC");
        return query.getResultList();
    }
}
