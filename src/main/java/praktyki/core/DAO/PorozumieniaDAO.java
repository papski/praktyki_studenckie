package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iPorozumieniaDAO;
import praktyki.core.entities.PorozumieniaEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 17.02.15.
 */
@Repository
public class PorozumieniaDAO implements iPorozumieniaDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public PorozumieniaEntity addAgreement(PorozumieniaEntity porozumieniaEntity) {
        em.persist(porozumieniaEntity);
        return porozumieniaEntity;
    }

    @Override
    public PorozumieniaEntity getAgreement(Integer idPorozumienia) {
        return em.find(PorozumieniaEntity.class, idPorozumienia);
    }

    @Override
    public PorozumieniaEntity deleteAgreements(Integer idPorozumienia) {
        PorozumieniaEntity agreement = em.find(PorozumieniaEntity.class, idPorozumienia);
        if (agreement !=null) {
            em.remove(agreement);
            return agreement;
        } else {
            return null;
        }
    }

    @Override
    public List<PorozumieniaEntity> getAgreementsOfStudent(Long nrAlbumu) {
        Query query = em.createQuery("SELECT p FROM PorozumieniaEntity p JOIN p.praktyka pp WHERE pp.nrAlbumu=?1");
        query.setParameter(1, nrAlbumu);
        return query.getResultList();
    }

    @Override
    public List<PorozumieniaEntity> getAgreementsOfEmployer(int idPraktykodawcy) {
        Query query = em.createQuery("SELECT p FROM PorozumieniaEntity p JOIN p.praktyka pp WHERE pp.idPraktykodawcy=?1");
        query.setParameter(1, idPraktykodawcy);
        return query.getResultList();
    }

    @Override
    public List getAll() {
        Query query = em.createQuery("SELECT p FROM PorozumieniaEntity p ORDER BY p.idPorozumienia ASC");
        return query.getResultList();
    }
}
