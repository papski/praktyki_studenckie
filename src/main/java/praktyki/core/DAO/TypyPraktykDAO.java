package praktyki.core.DAO;

import org.springframework.stereotype.Repository;
import praktyki.core.DAO.Interface.iTypyPraktykDAO;
import praktyki.core.entities.TypyPraktykEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by dawid on 05.02.15.
 */

@Repository
public class TypyPraktykDAO implements iTypyPraktykDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public TypyPraktykEntity addTypeOfTraineeship(TypyPraktykEntity typyPraktykEntity) {
        em.persist(typyPraktykEntity);
        return typyPraktykEntity;
    }

    @Override
    public TypyPraktykEntity updateTypeOfTraineeship(Integer idTypuPraktyki, TypyPraktykEntity typyPraktykEntity) {
        TypyPraktykEntity typeOfTraineeship = em.find(TypyPraktykEntity.class, idTypuPraktyki);
        if (typeOfTraineeship !=null) {
            typeOfTraineeship.setIdTypuPraktyki(idTypuPraktyki);
            typeOfTraineeship.setTypPraktyki(typyPraktykEntity.getTypPraktyki());
            return typeOfTraineeship;
        } else {
            return null;
        }
    }

    @Override
    public TypyPraktykEntity deleteTypeOfTraineeship(Integer idTypuPraktyki) {
        TypyPraktykEntity typeOfTraineeship = em.find(TypyPraktykEntity.class, idTypuPraktyki);
        if (typeOfTraineeship !=null) {
            em.remove(typeOfTraineeship);
            return typeOfTraineeship;
        } else {
            return null;
        }
    }

    @Override
    public List<TypyPraktykEntity> findAll() {
        Query query = em.createQuery("SELECT tp FROM TypyPraktykEntity tp ORDER BY tp.idTypuPraktyki ASC");
        return query.getResultList();
    }

    @Override
    public TypyPraktykEntity getTypeOfTraineeship(Integer idTypuPraktyki) {
        return em.find(TypyPraktykEntity.class, idTypuPraktyki);
    }
}
