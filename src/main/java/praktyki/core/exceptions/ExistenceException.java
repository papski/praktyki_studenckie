package praktyki.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by dawid on 17.04.15.
 */

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ExistenceException extends RuntimeException {

    public ExistenceException(String message) {
        super(message);
    }
}
