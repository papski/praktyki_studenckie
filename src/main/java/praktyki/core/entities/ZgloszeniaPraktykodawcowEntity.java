package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by dawid on 11.03.15.
 */
/**
EmployerApplicationsEntity<br>
ENTITY CONTAINING EMPLOYER'S APPLICATIONS FOR STUDENT'S PRACTICES<br><br>
 ATTRIBUTES:<br>
 Long idZgloszeniaPraktykodawcy;     //ID of Application<br>
 Integer idPraktykodawcy;            //ID of Employer<br>
 Long nrAlbumu;                      //(ID) Student Number<br>
 Integer idTypuZgloszenia;           //ID of Application's Type<br>
 Timestamp dataZgloszenia;           //Timestamp of issued Application<br>
 String oswiadczeniePraktykodawcy;   //String containing URL to Employer's Statement<br>
 Date dataRozpoczecia;               //Beginning Date of Practice<br>
 Date dataZakonczenia;               //End Date of Practice<br>
 Boolean decyzja;                    //Decision<br>
 Integer idKierunku;                 //ID of Course<br>
 Integer idRokuAkademickiego;        //ID of Academic Year<br><br>
 RELATIONS:<br>
 PraktykodawcyEntity praktykodawca;  //M:1 relation with EmployersEntity<br>
 StudenciKierunkowEntity studentKierunku;    //1:1 relation with CourseStudentsEntity<br>
 TypyZgloszenEntity typZgloszenia;   //M:1 relation with ApplicationTypesEntity<br>
 */
@Entity
@Table(name = "zgloszenia_praktykodawcow", schema = "public", catalog = "praktykidb")
public class ZgloszeniaPraktykodawcowEntity {
    private Long idZgloszeniaPraktykodawcy;     //ID of Application
    private Integer idPraktykodawcy;            //ID of Employer
    private Long nrAlbumu;                      //(ID) Student Number
    private Integer idTypuZgloszenia;           //ID of Application's Type
    private Timestamp dataZgloszenia;           //Timestamp of issued Application
    private String oswiadczeniePraktykodawcy;   //String containing URL to Employer's Statement
    private Date dataRozpoczecia;               //Beginning Date of Practice
    private Date dataZakonczenia;               //End Date of Practice
    private Boolean decyzja;                    //Decision
    private Integer idKierunku;                 //ID of Course
    private Integer idRokuAkademickiego;        //ID of Academic Year

    private PraktykodawcyEntity praktykodawca;  //M:1 relation with EmployersEntity
    private StudenciKierunkowEntity studentKierunku;    //1:1 relation with CourseStudentsEntity
    private TypyZgloszenEntity typZgloszenia;   //M:1 relation with ApplicationTypesEntity

    /*
    ATRYBUTY
    */

    //ID of Application
    @Id
    @GeneratedValue
    @Column(name = "id_zgloszenia", nullable = false)
    public Long getIdZgloszeniaPraktykodawcy() {
        return idZgloszeniaPraktykodawcy;
    }

    public void setIdZgloszeniaPraktykodawcy(Long idZgloszeniaPraktykodawcy) {
        this.idZgloszeniaPraktykodawcy = idZgloszeniaPraktykodawcy;
    }

    //ID of Employer
    @Basic
    @Column(name = "id_praktykodawcy", nullable = false)
    public Integer getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(Integer idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //(ID) Student Number
    @Basic
    @Column(name = "nr_albumu", nullable = false)
    public Long getNrAlbumu() {
        return nrAlbumu;
    }

    public void setNrAlbumu(Long nrAlbumu) {
        this.nrAlbumu = nrAlbumu;
    }

    //ID of Application's Type
    @Basic
    @Column(name = "id_typu_zgloszenia", nullable = false)
    public Integer getIdTypuZgloszenia() {
        return idTypuZgloszenia;
    }

    public void setIdTypuZgloszenia(Integer idTypuZgloszenia) {
        this.idTypuZgloszenia = idTypuZgloszenia;
    }

    //Timestamp of issued Application
    @Basic
    @Column(name = "data_zgloszenia", nullable = false)
    public Timestamp getDataZgloszenia() {
        return dataZgloszenia;
    }

    public void setDataZgloszenia(Timestamp dataZgloszenia) {
        this.dataZgloszenia = dataZgloszenia;
    }

    //String containing URL to Employer's Statement
    @Basic
    @Column(name = "oswiadczenie_praktykodawcy", nullable = false)
    public String getOswiadczeniePraktykodawcy() {
        return oswiadczeniePraktykodawcy;
    }

    public void setOswiadczeniePraktykodawcy(String oswiadczeniePraktykodawcy) {
        this.oswiadczeniePraktykodawcy = oswiadczeniePraktykodawcy;
    }

    //Beginning Date of Practice
    @Basic
    @Column(name = "data_rozpoczecia", nullable = false)
    public Date getDataRozpoczecia() {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(Date dataRozpoczecia) {
        this.dataRozpoczecia = dataRozpoczecia;
    }

    //End Date of Practice
    @Basic
    @Column(name = "data_zakonczenia", nullable = false)
    public Date getDataZakonczenia() {
        return dataZakonczenia;
    }

    public void setDataZakonczenia(Date dataZakonczenia) {
        this.dataZakonczenia = dataZakonczenia;
    }

    //Decision
    @Basic
    @Column(name = "decyzja")
    public Boolean getDecyzja() {
        return decyzja;
    }

    public void setDecyzja(Boolean decyzja) {
        this.decyzja = decyzja;
    }

    //ID of Course
    @Basic
    @Column(name = "id_kierunku", nullable = false)
    public Integer getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(Integer idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Academic Year
    @Basic
    @Column(name = "id_roku_akademickiego", nullable = false)
    public Integer getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(Integer idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    /*
    RELACJE
     */

    //M:1 relation with EmployersEntity
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    //1:1 relation with CourseStudentsEntity
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumns({@JoinColumn(name = "nr_albumu", referencedColumnName = "nr_albumu", insertable = false, updatable = false), @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false), @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)})
    public StudenciKierunkowEntity getStudentKierunku() {
        return studentKierunku;
    }

    public void setStudentKierunku(StudenciKierunkowEntity studentKierunku) {
        this.studentKierunku = studentKierunku;
    }

    //M:1 relation with ApplicationTypesEntity
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_typu_zgloszenia", referencedColumnName = "id_typu_zgloszenia", insertable = false, updatable = false)
    public TypyZgloszenEntity getTypZgloszenia() {
        return typZgloszenia;
    }

    public void setTypZgloszenia(TypyZgloszenEntity typZgloszenia) {
        this.typZgloszenia = typZgloszenia;
    }

    /*
    EQUALS I HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ZgloszeniaPraktykodawcowEntity)) return false;

        ZgloszeniaPraktykodawcowEntity that = (ZgloszeniaPraktykodawcowEntity) o;

        if (!dataRozpoczecia.equals(that.dataRozpoczecia)) return false;
        if (!dataZakonczenia.equals(that.dataZakonczenia)) return false;
        if (!dataZgloszenia.equals(that.dataZgloszenia)) return false;
        if (decyzja != null ? !decyzja.equals(that.decyzja) : that.decyzja != null) return false;
        if (idKierunku != null ? !idKierunku.equals(that.idKierunku) : that.idKierunku != null) return false;
        if (!idPraktykodawcy.equals(that.idPraktykodawcy)) return false;
        if (idRokuAkademickiego != null ? !idRokuAkademickiego.equals(that.idRokuAkademickiego) : that.idRokuAkademickiego != null)
            return false;
        if (!idTypuZgloszenia.equals(that.idTypuZgloszenia)) return false;
        if (!idZgloszeniaPraktykodawcy.equals(that.idZgloszeniaPraktykodawcy)) return false;
        if (!nrAlbumu.equals(that.nrAlbumu)) return false;
        if (!oswiadczeniePraktykodawcy.equals(that.oswiadczeniePraktykodawcy)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idZgloszeniaPraktykodawcy.hashCode();
        result = 31 * result + idPraktykodawcy.hashCode();
        result = 31 * result + nrAlbumu.hashCode();
        result = 31 * result + idTypuZgloszenia.hashCode();
        result = 31 * result + dataZgloszenia.hashCode();
        result = 31 * result + oswiadczeniePraktykodawcy.hashCode();
        result = 31 * result + dataRozpoczecia.hashCode();
        result = 31 * result + dataZakonczenia.hashCode();
        result = 31 * result + (decyzja != null ? decyzja.hashCode() : 0);
        result = 31 * result + (idKierunku != null ? idKierunku.hashCode() : 0);
        result = 31 * result + (idRokuAkademickiego != null ? idRokuAkademickiego.hashCode() : 0);
        return result;
    }
}
