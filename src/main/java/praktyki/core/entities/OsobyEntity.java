package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by dawid on 21.01.15.
 */
/**
PersonsEntity<br>
ENTITY CONTAINING INFORMATION ABOUT STUDENT, TUTOR AND COORDINATOR<br><br>
ATTRIBUTES:<br>
 Integer idOsoby;            //ID of Person<br>
 String tytulZawodowy;       //Degree<br>
 String imie;                //Name<br>
 String nazwisko;            //Surname<br>
 Date dataUrodzenia;         //DOB<br>
 String email;               //Email<br>
 String telefonKomorkowy;    //Cellphone Number<br>
 String telefonStacjonarny;  //Landline Number<br><br>
RELATIONS:<br>
 KoordynatorzyPraktykEntity koordynator; //1:1 relation with CoordinatorsEntity<br>
 StudenciEntity student; //1:1 relation with StudentsEntity<br>
 OpiekunowiePraktykEntity opiekun; //1:1 relation with TutorsEntity<br>
 UzytkownicyEntity uzytkownik;   //1:1 relation with UsersEntity<br>
 */
@Entity
@Table(name = "osoby", schema = "public", catalog = "praktykidb")
public class OsobyEntity {
    private Integer idOsoby;            //ID of Person
    private String tytulZawodowy;       //Degree
    private String imie;                //Name
    private String nazwisko;            //Surname
    private Date dataUrodzenia;         //DOB
    private String email;               //Email
    private String telefonKomorkowy;    //Cellphone Number
    private String telefonStacjonarny;  //Landline Number

    private KoordynatorzyPraktykEntity koordynator; //1:1 relation with CoordinatorsEntity
    private StudenciEntity student; //1:1 relation with StudentsEntity
    private OpiekunowiePraktykEntity opiekun; //1:1 relation with TutorsEntity
    private UzytkownicyEntity uzytkownik;   //1:1 relation with UsersEntity

    /*
    ATTRIBUTES
    */

    //ID of Person
    @Id
    @GeneratedValue
    @Column(name = "id_osoby")
    public Integer getIdOsoby() {
        return idOsoby;
    }

    public void setIdOsoby(Integer idOsoby) {
        this.idOsoby = idOsoby;
    }

    //Degree
    @Basic
    @Column(name = "tyul_zawodoy")
    public String getTytulZawodowy() {
        return tytulZawodowy;
    }

    public void setTytulZawodowy(String tytulZawodowy) {
        this.tytulZawodowy = tytulZawodowy;
    }

    //Name
    @Basic
    @Column(name = "imie", nullable = false)
    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    //Surname
    @Basic
    @Column(name = "nazwisko", nullable = false)
    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    //DOB
    @Basic
    @Column(name = "data_urodzenia")
    public Date getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(Date dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    //Email
    @Basic
    @Column(name = "email", nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //Cellphone Number
    @Basic
    @Column(name = "telefon_komorkowy")
    public String getTelefonKomorkowy() {
        return telefonKomorkowy;
    }

    public void setTelefonKomorkowy(String telefonKomorkowy) {
        this.telefonKomorkowy = telefonKomorkowy;
    }

    //Landline Number
    @Basic
    @Column(name = "telefon_stacjonarny")
    public String getTelefonStacjonarny() {
        return telefonStacjonarny;
    }

    public void setTelefonStacjonarny(String telefonStacjonarny) {
        this.telefonStacjonarny = telefonStacjonarny;
    }

    /*
    RELATIONS
     */

    //1:1 relation with CoordinatorsEntity
    @JsonIgnore
    @OneToOne(mappedBy = "koordynatorByIdOsoby", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public KoordynatorzyPraktykEntity getKoordynator() {
        return koordynator;
    }

    public void setKoordynator(KoordynatorzyPraktykEntity koordynator) {
        this.koordynator = koordynator;
    }

    //1:1 relation with StudentsEntity
    @JsonIgnore
    @OneToOne(mappedBy = "studentByIdOsoby", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public StudenciEntity getStudent() {
        return student;
    }

    public void setStudent(StudenciEntity student) {
        this.student = student;
    }

    //1:1 relation with TutorsEntity
    @JsonIgnore
    @OneToOne(mappedBy = "opiekunByIdOsoby", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public OpiekunowiePraktykEntity getOpiekun() {
        return opiekun;
    }

    public void setOpiekun(OpiekunowiePraktykEntity opiekun) {
        this.opiekun = opiekun;
    }

    //1:1 relation with UsersEntity
    @JsonIgnore
    @OneToOne(mappedBy = "uzytkownikByIdOsoby", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public UzytkownicyEntity getUzytkownik() {
        return uzytkownik;
    }

    public void setUzytkownik(UzytkownicyEntity uzytkownik) {
        this.uzytkownik = uzytkownik;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OsobyEntity)) return false;

        OsobyEntity that = (OsobyEntity) o;

        if (dataUrodzenia != null ? !dataUrodzenia.equals(that.dataUrodzenia) : that.dataUrodzenia != null)
            return false;
        if (!email.equals(that.email)) return false;
        if (idOsoby != null ? !idOsoby.equals(that.idOsoby) : that.idOsoby != null) return false;
        if (!imie.equals(that.imie)) return false;
        if (!nazwisko.equals(that.nazwisko)) return false;
        if (telefonKomorkowy != null ? !telefonKomorkowy.equals(that.telefonKomorkowy) : that.telefonKomorkowy != null)
            return false;
        if (telefonStacjonarny != null ? !telefonStacjonarny.equals(that.telefonStacjonarny) : that.telefonStacjonarny != null)
            return false;
        if (tytulZawodowy != null ? !tytulZawodowy.equals(that.tytulZawodowy) : that.tytulZawodowy != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idOsoby != null ? idOsoby.hashCode() : 0;
        result = 31 * result + (tytulZawodowy != null ? tytulZawodowy.hashCode() : 0);
        result = 31 * result + imie.hashCode();
        result = 31 * result + nazwisko.hashCode();
        result = 31 * result + (dataUrodzenia != null ? dataUrodzenia.hashCode() : 0);
        result = 31 * result + email.hashCode();
        result = 31 * result + (telefonKomorkowy != null ? telefonKomorkowy.hashCode() : 0);
        result = 31 * result + (telefonStacjonarny != null ? telefonStacjonarny.hashCode() : 0);
        return result;
    }
}