package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

/**
 * Created by dawid on 08.03.15.
 */
/**
CoursePracticesEntity<br>
JOINT TABLE BETWEEN FIELD OF STUDY, ACADEMIC YEARS, CALENDAR UNITS, PRACTICE TYPES AND PRACTICES<br><br>
ATTRIBUTES:<br>
 int idKierunku;             //ID of Course (PFK)<br>
 int idTypuPraktyki;         //ID of Practice's Type (PFK)<br>
 int idRokuAkademickiego;    //ID of Academic Year (PFK)<br>
 Date minDataRozpoczecia;    //Minimal Starting Date for Course's Practices<br>
 Date maxDataZakonczenia;    //Maximum Ending Date for Course's Practices<br>
 int liczbaGodzin;           //Number of Hours<br>
 int czasTrwania;            //Duration of Course's Practices (preferable weeks)<br>
 int idJednostki;            //ID of Calendar Unit<br><br>
RELATIONS:<br>
 RocznikiStudiowEntity kierunek; //M:1 relation with CourseYearbookEntity<br>
 LataAkademickieEntity lataAkademickie;  //M:1 relation with AcademicYearsEntity<br>
 JednostkiKalendarzoweEntity jednostkiKalendarzowe;  //M:1 relation with CalendarUnitsEntity<br>
 TypyPraktykEntity typPraktyki;  //M:1 relation with PracticeTypesEntity<br>
 Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity<br>
 */
@Entity
@Table(name = "praktyki_kierunkowe", schema = "public", catalog = "praktykidb")
@IdClass(PraktykiKierunkoweEntityPK.class)
public class PraktykiKierunkoweEntity {
    private int idKierunku;             //ID of Course (PFK)
    private int idTypuPraktyki;         //ID of Practice's Type (PFK)
    private int idRokuAkademickiego;    //ID of Academic Year (PFK)
    private Date minDataRozpoczecia;    //Minimal Starting Date for Course's Practices
    private Date maxDataZakonczenia;    //Maximum Ending Date for Course's Practices
    private int liczbaGodzin;           //Number of Hours
    private int czasTrwania;            //Duration of Course's Practices (preferable weeks)
    private int idJednostki;            //ID of Calendar Unit

    private RocznikiStudiowEntity kierunek; //M:1 relation with CourseYearbookEntity
    private LataAkademickieEntity lataAkademickie;  //M:1 relation with AcademicYearsEntity
    private JednostkiKalendarzoweEntity jednostkiKalendarzowe;  //M:1 relation with CalendarUnitsEntity
    private TypyPraktykEntity typPraktyki;  //M:1 relation with PracticeTypesEntity
    private Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity

    /*
    ATTRIBUTES
    */

    //ID of Course (PFK)
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_kierunku", nullable = false)
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Practice's Type (PFK)
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_typu_praktyki", nullable = false)
    public int getIdTypuPraktyki() {
        return idTypuPraktyki;
    }

    public void setIdTypuPraktyki(int idTypuPraktyki) {
        this.idTypuPraktyki = idTypuPraktyki;
    }

    //ID of Academic Year (PFK)
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_roku_akademickiego", nullable = false)
    public int getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(int idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    //Minimal Starting Date for Course's Practices
    @Basic
    @Column(name = "min_data_rozpoczecia")
    public Date getMinDataRozpoczecia() {
        return minDataRozpoczecia;
    }

    public void setMinDataRozpoczecia(Date minDataRozpoczecia) {
        this.minDataRozpoczecia = minDataRozpoczecia;
    }

    //Maximum Ending Date for Course's Practices
    @Basic
    @Column(name = "max_data_zakonczenia")
    public Date getMaxDataZakonczenia() {
        return maxDataZakonczenia;
    }

    public void setMaxDataZakonczenia(Date maxDataZakonczenia) {
        this.maxDataZakonczenia = maxDataZakonczenia;
    }

    //Number of Hours
    @Basic
    @Column(name = "liczba_godzin")
    public int getLiczbaGodzin() {
        return liczbaGodzin;
    }

    public void setLiczbaGodzin(int liczbaGodzin) {
        this.liczbaGodzin = liczbaGodzin;
    }

    //Duration of Course's Practices (preferable weeks)
    @Basic
    @Column(name = "czas_trwania")
    public int getCzasTrwania() {
        return czasTrwania;
    }

    public void setCzasTrwania(int czasTrwania) {
        this.czasTrwania = czasTrwania;
    }

    //ID of Calendar Unit
    @Basic
    @Column(name = "id_jednostki", nullable = false)
    public int getIdJednostki() {
        return idJednostki;
    }

    public void setIdJednostki(int idJednostki) {
        this.idJednostki = idJednostki;
    }

    /*
    RELATIONS
    */

    //M:1 relation with CourseYearbookEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false),@JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)})
    public RocznikiStudiowEntity getKierunek() {
        return kierunek;
    }

    public void setKierunek(RocznikiStudiowEntity kierunek) {
        this.kierunek = kierunek;
    }

    //M:1 relation with AcademicYearsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)
    public LataAkademickieEntity getLataAkademickie() {
        return lataAkademickie;
    }

    public void setLataAkademickie(LataAkademickieEntity lataAkademickie) {
        this.lataAkademickie = lataAkademickie;
    }

    //M:1 relation with CalendarUnitsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_jednostki", referencedColumnName = "id_jednostki_kalendarzowej", insertable = false, updatable = false)
    public JednostkiKalendarzoweEntity getJednostkiKalendarzowe() {
        return jednostkiKalendarzowe;
    }

    public void setJednostkiKalendarzowe(JednostkiKalendarzoweEntity jednostkiKalendarzowe) {
        this.jednostkiKalendarzowe = jednostkiKalendarzowe;
    }

    //M:1 relation with PracticeTypesEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_typu_praktyki", referencedColumnName = "id_typu_praktyki", insertable = false, updatable = false)
    public TypyPraktykEntity getTypPraktyki() {
        return typPraktyki;
    }

    public void setTypPraktyki(TypyPraktykEntity typPraktyki) {
        this.typPraktyki = typPraktyki;
    }

    //1:M relation with PracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "praktyka")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiEntity> getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(Collection<PraktykiEntity> praktyka) {
        this.praktyka = praktyka;
    }


    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PraktykiKierunkoweEntity)) return false;

        PraktykiKierunkoweEntity that = (PraktykiKierunkoweEntity) o;

        if (czasTrwania != that.czasTrwania) return false;
        if (idJednostki != that.idJednostki) return false;
        if (idKierunku != that.idKierunku) return false;
        if (idRokuAkademickiego != that.idRokuAkademickiego) return false;
        if (idTypuPraktyki != that.idTypuPraktyki) return false;
        if (liczbaGodzin != that.liczbaGodzin) return false;
        if (maxDataZakonczenia != null ? !maxDataZakonczenia.equals(that.maxDataZakonczenia) : that.maxDataZakonczenia != null)
            return false;
        if (minDataRozpoczecia != null ? !minDataRozpoczecia.equals(that.minDataRozpoczecia) : that.minDataRozpoczecia != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku;
        result = 31 * result + idTypuPraktyki;
        result = 31 * result + idRokuAkademickiego;
        result = 31 * result + (minDataRozpoczecia != null ? minDataRozpoczecia.hashCode() : 0);
        result = 31 * result + (maxDataZakonczenia != null ? maxDataZakonczenia.hashCode() : 0);
        result = 31 * result + liczbaGodzin;
        result = 31 * result + czasTrwania;
        result = 31 * result + idJednostki;
        return result;
    }
}
