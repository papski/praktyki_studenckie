package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 07.02.15.
 */
/**
RolesEntity<br>
ENTITY CONTAINING ROLES OF PLATFORM (Admin, Coordinator, Employer/Tutor, Student)<br><br>
 ATTRIBUTES:<br>
 Integer idRoli;     //ID of Role<br>
 String nazwaRoli;   //Name<br><br>
 RELATIONS:<br>
 Collection<UzytkownicyEntity> uzytkownik;   //1:M relation with UsersEntity<br>
 */
@Entity
@Table(name = "role", schema = "public", catalog = "praktykidb")
public class RoleEntity {
    private Integer idRoli;     //ID of Role
    private String nazwaRoli;   //Name

    private Collection<UzytkownicyEntity> uzytkownik;   //1:M relation with UsersEntity

    /*
    ATTRIBUTES
    */

    //ID of Role
    @Id
    @GeneratedValue
    @Column(name = "id_roli")
    public Integer getIdRoli() {
        return idRoli;
    }

    public void setIdRoli(Integer idRoli) {
        this.idRoli = idRoli;
    }

    //Name
    @Basic
    @Column(name = "nazwa_roli")
    public String getNazwaRoli() {
        return nazwaRoli;
    }

    public void setNazwaRoli(String nazwaRoli) {
        this.nazwaRoli = nazwaRoli;
    }

    /*
    RELATIONS
    */

    //1:M relation with UsersEntity
    @JsonIgnore
    @OneToMany(mappedBy = "uzytkownikByIdRoli", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<UzytkownicyEntity> getUzytkownik() {
        return uzytkownik;
    }

    public void setUzytkownik(Collection<UzytkownicyEntity> uzytkownik) {
        this.uzytkownik = uzytkownik;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoleEntity)) return false;

        RoleEntity that = (RoleEntity) o;

        if (idRoli != null ? !idRoli.equals(that.idRoli) : that.idRoli != null) return false;
        if (nazwaRoli != null ? !nazwaRoli.equals(that.nazwaRoli) : that.nazwaRoli != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRoli != null ? idRoli.hashCode() : 0;
        result = 31 * result + (nazwaRoli != null ? nazwaRoli.hashCode() : 0);
        return result;
    }
}
