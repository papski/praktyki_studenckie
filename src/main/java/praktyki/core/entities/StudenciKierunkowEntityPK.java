package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 08.12.14.
 */
/**
PRIMARY KEYS FOR CourseStudents ("StudenciKierunkowEntity")
 */
public class StudenciKierunkowEntityPK implements Serializable {
    private Long nrAlbumu;              //(ID) Student Number
    private int idKierunku;             //ID of Course
    private int idRokuAkademickiego;    //ID of Academic Year

    //(ID) Student Number
    @Column(name = "nr_albumu")
    @Id
    public Long getNrAlbumu() {
        return nrAlbumu;
    }

    public void setNrAlbumu(Long nrAlbumu) {
        this.nrAlbumu = nrAlbumu;
    }

    //ID of Course
    @Column(name = "id_kierunku")
    @Id
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Academic Year
    @Column(name = "id_roku_akademickiego")
    @Id
    public int getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(int idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudenciKierunkowEntityPK)) return false;

        StudenciKierunkowEntityPK that = (StudenciKierunkowEntityPK) o;

        if (idKierunku != that.idKierunku) return false;
        if (idRokuAkademickiego != that.idRokuAkademickiego) return false;
        if (nrAlbumu != null ? !nrAlbumu.equals(that.nrAlbumu) : that.nrAlbumu != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nrAlbumu != null ? nrAlbumu.hashCode() : 0;
        result = 31 * result + idKierunku;
        result = 31 * result + idRokuAkademickiego;
        return result;
    }
}
