package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 21.02.15.
 */
/**
EmployersCourseCoordinatorsEntity<br>
JOIN TABLE BETWEEN EMPLOYER, FIELD OF STUDY (COURSE) AND COORDINATOR<br><br>
 ATTRIBUTES:<br>
 int idPraktykodawcy;        //ID of Employer<br>
 int idKierunku;             //ID of Course<br>
 int idKoordynatoraPraktyk;  //ID of Coordinator<br><br>
 RELATIONS:<br>
 KoordynatorzyKierunkowiEntity koordynatorKierunku;  //M:1 relation with CourseCoordinatorsEntity<br>
 PraktykodawcyEntity praktykodawca;  //M:1 relation with EmployersEntity<br>
 Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity<br>
 */
@Entity
@IdClass(KoordynatorzyKierunkowiPraktykodawcowEntityPK.class)
@Table(name = "koordynatorzy_kierunkowi_praktykodawcow", schema = "public", catalog = "praktykidb")
public class KoordynatorzyKierunkowiPraktykodawcowEntity {
    private int idPraktykodawcy;        //ID of Employer
    private int idKierunku;             //ID of Course
    private int idKoordynatoraPraktyk;  //ID of Coordinator

    private KoordynatorzyKierunkowiEntity koordynatorKierunku;  //M:1 relation with CourseCoordinatorsEntity
    private PraktykodawcyEntity praktykodawca;  //M:1 relation with EmployersEntity
    private Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity

    /*
    ATTRIBUTES
     */

    //ID of Employer
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_praktykodawcy", nullable = false)
    public int getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(int idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //ID of Course
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_kierunku", nullable = false)
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Coordinator
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_koordynatora_praktyk", nullable = false)
    public int getIdKoordynatoraPraktyk() {
        return idKoordynatoraPraktyk;
    }

    public void setIdKoordynatoraPraktyk(int idKoordynatoraPraktyk) {
        this.idKoordynatoraPraktyk = idKoordynatoraPraktyk;
    }

    /*
    RELATIONS
     */

    //M:1 relation with CourseCoordinatorsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false), @JoinColumn(name = "id_koordynatora_praktyk", referencedColumnName = "id_koordynatora_praktyk", insertable = false, updatable = false)})
    public KoordynatorzyKierunkowiEntity getKoordynatorKierunku() {
        return koordynatorKierunku;
    }

    public void setKoordynatorKierunku(KoordynatorzyKierunkowiEntity koordynatorKierunku) {
        this.koordynatorKierunku = koordynatorKierunku;
    }

    //M:1 relation with EmployersEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    //1:M relation with PracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "koordynator")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiEntity> getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(Collection<PraktykiEntity> praktyka) {
        this.praktyka = praktyka;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KoordynatorzyKierunkowiPraktykodawcowEntity)) return false;

        KoordynatorzyKierunkowiPraktykodawcowEntity that = (KoordynatorzyKierunkowiPraktykodawcowEntity) o;

        if (idKierunku != that.idKierunku) return false;
        if (idKoordynatoraPraktyk != that.idKoordynatoraPraktyk) return false;
        if (idPraktykodawcy != that.idPraktykodawcy) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPraktykodawcy;
        result = 31 * result + idKierunku;
        result = 31 * result + idKoordynatoraPraktyk;
        return result;
    }
}
