package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 04.02.15.
 */
/**
TutorsEntity<br>
ENTITY CONTAINING TUTORS<br><br>
ATTRIBUTES:<br>
 Integer idOpiekunaPraktyk;  //ID of Tutor<br>
 String stanowisko;          //position<br>
 Integer idOsoby;            //ID of Person<br><br>
RELATIONS:<br>
 OsobyEntity opiekunByIdOsoby;   //1:1 relation with PersonsEntity<br>
 Collection<OpiekunowieKierunkowiEntity> kierunekPraktykodawcy;  //1:M relation with CourseTutorsEntity<br>
 */
@Entity
@Table(name = "opiekunowie_praktyk", schema = "public", catalog = "praktykidb")
public class OpiekunowiePraktykEntity {
    private Integer idOpiekunaPraktyk;  //ID of Tutor
    private String stanowisko;          //position
    private Integer idOsoby;            //ID of Person

    private OsobyEntity opiekunByIdOsoby;   //1:1 relation with PersonsEntity
    private Collection<OpiekunowieKierunkowiEntity> kierunekPraktykodawcy;  //1:M relation with CourseTutorsEntity

    /*
    ATTRIBUTES
    */

    //ID of Tutor
    @Id
    @GeneratedValue
    @Column(name = "id_opiekuna_praktyk")
    public Integer getIdOpiekunaPraktyk() {
        return idOpiekunaPraktyk;
    }

    public void setIdOpiekunaPraktyk(Integer idOpiekunaPraktyk) {
        this.idOpiekunaPraktyk = idOpiekunaPraktyk;
    }

    //position
    @Basic
    @Column(name = "stanowisko", nullable = false)
    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    //ID of Person
    @JsonIgnore
    @Basic
    @Column(name = "id_osoby")
    public Integer getIdOsoby() {
        return idOsoby;
    }

    public void setIdOsoby(Integer idOsoby) {
        this.idOsoby = idOsoby;
    }

    /*
    RELATIONS
     */

    //1:1 relation with PersonsEntity
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_osoby", referencedColumnName = "id_osoby", insertable = false, updatable = false)
    public OsobyEntity getOpiekunByIdOsoby() {
        return opiekunByIdOsoby;
    }

    public void setOpiekunByIdOsoby(OsobyEntity opiekunByIdOsoby) {
        this.opiekunByIdOsoby = opiekunByIdOsoby;
    }

    //1:M relation with CourseTutorsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "opiekun", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<OpiekunowieKierunkowiEntity> getKierunekPraktykodawcy() {
        return kierunekPraktykodawcy;
    }

    public void setKierunekPraktykodawcy(Collection<OpiekunowieKierunkowiEntity> kierunekPraktykodawcy) {
        this.kierunekPraktykodawcy = kierunekPraktykodawcy;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OpiekunowiePraktykEntity)) return false;

        OpiekunowiePraktykEntity that = (OpiekunowiePraktykEntity) o;

        if (idOpiekunaPraktyk != null ? !idOpiekunaPraktyk.equals(that.idOpiekunaPraktyk) : that.idOpiekunaPraktyk != null)
            return false;
        if (idOsoby != null ? !idOsoby.equals(that.idOsoby) : that.idOsoby != null) return false;
        if (!stanowisko.equals(that.stanowisko)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idOpiekunaPraktyk != null ? idOpiekunaPraktyk.hashCode() : 0;
        result = 31 * result + stanowisko.hashCode();
        result = 31 * result + (idOsoby != null ? idOsoby.hashCode() : 0);
        return result;
    }
}
