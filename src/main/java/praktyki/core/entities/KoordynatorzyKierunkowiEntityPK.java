package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 18.01.15.
 */
/**
PRIMARY KEYS FOR CourseCoordinatorsEntity ("KoordnatorzyKierunkowiEntityPK")
 */
public class KoordynatorzyKierunkowiEntityPK implements Serializable {
    private int idKierunku; //ID of Course
    private int idKoordynatoraPraktyk;  //ID of Coordinator

    //ID of Course
    @Id
    @Column(name = "id_kierunku")
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Coordinator
    @Id
    @Column(name = "id_koordynatora_praktyk")
    public int getIdKoordynatoraPraktyk() {
        return idKoordynatoraPraktyk;
    }

    public void setIdKoordynatoraPraktyk(int idKoordynatoraPraktyk) {
        this.idKoordynatoraPraktyk = idKoordynatoraPraktyk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KoordynatorzyKierunkowiEntityPK)) return false;

        KoordynatorzyKierunkowiEntityPK that = (KoordynatorzyKierunkowiEntityPK) o;

        if (idKierunku != that.idKierunku) return false;
        if (idKoordynatoraPraktyk != that.idKoordynatoraPraktyk) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku;
        result = 31 * result + idKoordynatoraPraktyk;
        return result;
    }
}
