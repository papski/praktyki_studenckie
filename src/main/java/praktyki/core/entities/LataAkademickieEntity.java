package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

/**
 * Created by dawid on 15.02.15.
 */
/**
AcademicYearsEntity<br>
ENTITY CONTAINING ACADEMIC YEARS FOR FIELDS OF STUDY (COURSES)<br><br>
ATTRIBUTES:<br>
 private int idRokuAkademickiego;    //ID of Academic Year<br>
 private String rokAkademicki;       //name of Academic Year<br>
 private Date dataRozpoczecia;       //Date of Academic Year's Beginning<br>
 private Date dataZakonczenia;       //Date of Academic Year's End<br><br>
RELATIONS:<br>
 private Collection<RocznikiStudiowEntity> kierunkiByIdRoku; //1:M relation with CourseYearbookEntity<br>
 private Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdRoku;    //1:M relation with CoursePracticesEntity<br>
 */
@Entity
@Table(name = "lata_akademickie", schema = "public", catalog = "praktykidb")
public class LataAkademickieEntity {
    private int idRokuAkademickiego;    //ID of Academic Year
    private String rokAkademicki;       //name of Academic Year
    private Date dataRozpoczecia;       //Date of Academic Year's Beginning
    private Date dataZakonczenia;       //Date of Academic Year's End

    private Collection<RocznikiStudiowEntity> kierunkiByIdRoku; //1:M relation with CourseYearbookEntity
    private Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdRoku;    //1:M relation with CoursePracticesEntity

    /*
    ATTRIBUTES
    */

    //ID of Academic Year
    @Id
    @GeneratedValue
    @Column(name = "id_roku_akademickiego", nullable = false)
    public int getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(int idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    //name of Academic Year
    @Basic
    @Column(name = "rok_akademicki", nullable = false)
    public String getRokAkademicki() {
        return rokAkademicki;
    }

    public void setRokAkademicki(String rokAkademicki) {
        this.rokAkademicki = rokAkademicki;
    }

    //Date of Academic Year's Beginning
    @Basic
    @Column(name = "data_rozpoczecia", nullable = false)
    public Date getDataRozpoczecia() {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(Date dataRozpoczecia) {
        this.dataRozpoczecia = dataRozpoczecia;
    }

    //Date of Academic Year's End
    @Basic
    @Column(name = "data_zakonczenia", nullable = false)
    public Date getDataZakonczenia() {
        return dataZakonczenia;
    }

    public void setDataZakonczenia(Date dataZakonczenia) {
        this.dataZakonczenia = dataZakonczenia;
    }

    /*
    RELATIONS
     */

    //1:M relation with CourseYearbookEntity
    @JsonIgnore
    @OneToMany(mappedBy = "rok", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<RocznikiStudiowEntity> getKierunkiByIdRoku() {
        return kierunkiByIdRoku;
    }

    public void setKierunkiByIdRoku(Collection<RocznikiStudiowEntity> kierunkiByIdRoku) {
        this.kierunkiByIdRoku = kierunkiByIdRoku;
    }

    //1:M relation with CoursePracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "lataAkademickie", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiKierunkoweEntity> getPraktykiKierunkoweByIdRoku() {
        return praktykiKierunkoweByIdRoku;
    }

    public void setPraktykiKierunkoweByIdRoku(Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdRoku) {
        this.praktykiKierunkoweByIdRoku = praktykiKierunkoweByIdRoku;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LataAkademickieEntity)) return false;

        LataAkademickieEntity that = (LataAkademickieEntity) o;

        if (idRokuAkademickiego != that.idRokuAkademickiego) return false;
        if (!dataRozpoczecia.equals(that.dataRozpoczecia)) return false;
        if (!dataZakonczenia.equals(that.dataZakonczenia)) return false;
        if (!rokAkademicki.equals(that.rokAkademicki)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRokuAkademickiego;
        result = 31 * result + rokAkademicki.hashCode();
        result = 31 * result + dataRozpoczecia.hashCode();
        result = 31 * result + dataZakonczenia.hashCode();
        return result;
    }
}
