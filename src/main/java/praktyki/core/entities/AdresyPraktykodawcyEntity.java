package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 10.02.15.
 */

/**
EmployersAddressesEntity<br>
JOIN TABLE BETWEEN EMPLOYERS AND ADDRESSES<br><br>
ATTRIBUTES:<br>
Integer idAdresu;        //ID of address<br>
Integer idPraktykodawcy; //ID of employer<br>
Boolean oddzial;         //branch office<br><br>
RELATIONS:<br>
AdresyEntity adres; //ManyToOne relation with AddressesEntity<br>
PraktykodawcyEntity praktykodawca;  //ManyToOne relation with EmployersEntity<br>
Collection<PraktykiEntity> praktyka;    //OneToMany relaton with PracticesEntity<br>
 */
@Entity
@Table(name = "adresy_praktykodawcy", schema = "public", catalog = "praktykidb")
@IdClass(AdresyPraktykodawcyEntityPK.class)
public class AdresyPraktykodawcyEntity {
    private Integer idAdresu;        //ID of address
    private Integer idPraktykodawcy; //ID of employer
    private Boolean oddzial;         //branch office

    private AdresyEntity adres; //ManyToOne relation with AddressesEntity
    private PraktykodawcyEntity praktykodawca;  //ManyToOne relation with EmployersEntity
    private Collection<PraktykiEntity> praktyka;    //OneToMany relaton with PracticesEntity

    /*
    ATTRIBUTES
     */

    //ID of address
    @Id
    @Column(name = "id_adresu")
    public Integer getIdAdresu() {
        return idAdresu;
    }

    public void setIdAdresu(Integer idAdresu) {
        this.idAdresu = idAdresu;
    }

    //ID of employer
    @Id
    @Column(name = "id_praktykodawcy")
    public Integer getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(Integer idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //branch office
    @Basic
    @Column(name = "oddzial", nullable = false)
    public Boolean getOddzial() {
        return oddzial;
    }

    public void setOddzial(Boolean oddzial) {
        this.oddzial = oddzial;
    }

    /*
    RELATIONS
    */

    //ManyToOne relation with AddressesEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_adresu", referencedColumnName = "id_adresu", insertable = false, updatable = false)
    public AdresyEntity getAdres() {
        return adres;
    }

    public void setAdres(AdresyEntity adres) {
        this.adres = adres;
    }

    //ManyToOne relation with EmployersEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    //OneToMany relaton with PracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "adresPraktykodawcy")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiEntity> getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(Collection<PraktykiEntity> praktyka) {
        this.praktyka = praktyka;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdresyPraktykodawcyEntity that = (AdresyPraktykodawcyEntity) o;

        if (idAdresu != null ? !idAdresu.equals(that.idAdresu) : that.idAdresu != null) return false;
        if (idPraktykodawcy != null ? !idPraktykodawcy.equals(that.idPraktykodawcy) : that.idPraktykodawcy != null)
            return false;
        if (oddzial != null ? !oddzial.equals(that.oddzial) : that.oddzial != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAdresu != null ? idAdresu.hashCode() : 0;
        result = 31 * result + (idPraktykodawcy != null ? idPraktykodawcy.hashCode() : 0);
        result = 31 * result + (oddzial != null ? oddzial.hashCode() : 0);
        return result;
    }
}
