package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 22.02.15.
 */
/**
PRIMARY KEYS FOR CourseTutorsEntity ("OpikeunowieKierunkowiEntityPK")
 */
public class OpiekunowieKierunkowiEntityPK implements Serializable {
    private int idKierunku;         //ID of Course
    private int idOpiekunaPraktyk;  //ID of Tutor
    private int idPraktykodawcy;    //ID of Employer

    //ID of Course
    @Id
    @Column(name = "id_kierunku")
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Tutor
    @Id
    @Column(name = "id_opiekuna_praktyk")
    public int getIdOpiekunaPraktyk() {
        return idOpiekunaPraktyk;
    }

    public void setIdOpiekunaPraktyk(int idOpiekunaPraktyk) {
        this.idOpiekunaPraktyk = idOpiekunaPraktyk;
    }

    //ID of Employer
    @Id
    @Column(name = "id_praktykodawcy")
    public int getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(int idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OpiekunowieKierunkowiEntityPK)) return false;

        OpiekunowieKierunkowiEntityPK that = (OpiekunowieKierunkowiEntityPK) o;

        if (idKierunku != that.idKierunku) return false;
        if (idOpiekunaPraktyk != that.idOpiekunaPraktyk) return false;
        if (idPraktykodawcy != that.idPraktykodawcy) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku;
        result = 31 * result + idOpiekunaPraktyk;
        result = 31 * result + idPraktykodawcy;
        return result;
    }
}
