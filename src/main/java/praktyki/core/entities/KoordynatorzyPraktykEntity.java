package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 18.01.15.
 */
/**
CoordinatorsEntity<br>
ENTITY CONTAINING COORDINATORS<br><br>
 ATTRIBUTES:<br>
 int idKoordynatoraPraktyk;  //ID of Coordinator<br>
 Integer idOsoby;            //ID of Person<br><br>
 RELATIONS:<br>
 OsobyEntity koordynatorByIdOsoby;   //M:1 relation with PersonsEntity<br>
 Collection<KoordynatorzyKierunkowiEntity> koordynatorzyByIdKierunku;    //1:M relation with CourseCoordinatorsEntity<br>
 */
@Entity
@Table(name = "koordynatorzy_praktyk", schema = "public", catalog = "praktykidb")
public class KoordynatorzyPraktykEntity {
    private int idKoordynatoraPraktyk;  //ID of Coordinator
    private Integer idOsoby;            //ID of Person

    private OsobyEntity koordynatorByIdOsoby;   //M:1 relation with PersonsEntity
    private Collection<KoordynatorzyKierunkowiEntity> koordynatorzyByIdKierunku;    //1:M relation with CourseCoordinatorsEntity

    /*
    ATTRIBUTES
    */

    //ID of Coordinator
    @Id
    @GeneratedValue
    @Column(name = "id_koordynatora_praktyk")
    public int getIdKoordynatoraPraktyk() {
        return idKoordynatoraPraktyk;
    }

    public void setIdKoordynatoraPraktyk(int idKoordynatoraPraktyk) {
        this.idKoordynatoraPraktyk = idKoordynatoraPraktyk;
    }

    //ID of Person
    @JsonIgnore
    @Basic
    @Column(name = "id_osoby")
    public Integer getIdOsoby() {
        return idOsoby;
    }

    public void setIdOsoby(Integer idOsoby) {
        this.idOsoby = idOsoby;
    }

    /*
    RELATIONS
     */

    //M:1 relation with PersonsEntity
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_osoby", referencedColumnName = "id_osoby", insertable = false , updatable = false)
    public OsobyEntity getKoordynatorByIdOsoby() {
        return koordynatorByIdOsoby;
    }

    public void setKoordynatorByIdOsoby(OsobyEntity koordynatorByIdOsoby) {
        this.koordynatorByIdOsoby = koordynatorByIdOsoby;
    }

    //1:M relation with CourseCoordinatorsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "koordynatorzyByIdKierunku", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KoordynatorzyKierunkowiEntity> getKoordynatorzyByIdKierunku() {
        return koordynatorzyByIdKierunku;
    }

    public void setKoordynatorzyByIdKierunku(Collection<KoordynatorzyKierunkowiEntity> koordynatorzyByIdKierunku) {
        this.koordynatorzyByIdKierunku = koordynatorzyByIdKierunku;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KoordynatorzyPraktykEntity)) return false;

        KoordynatorzyPraktykEntity that = (KoordynatorzyPraktykEntity) o;

        if (idKoordynatoraPraktyk != that.idKoordynatoraPraktyk) return false;
        if (idOsoby != that.idOsoby) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKoordynatoraPraktyk;
        result = 31 * result + idOsoby;
        return result;
    }
}
