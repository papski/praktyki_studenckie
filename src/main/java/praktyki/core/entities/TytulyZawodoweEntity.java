package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 10.02.15.
 */
/**
CourseDegreesEntity<br>
ENTITY CONTAINING FIELD OF STUDIES' DEGREES (bachelor, masters etc.)<br><br>
 ATTRIBUTES:<br>
 int idTytuluZawodowego;     //ID of Degree<br>
 String tytulZawodowy;       //Name<br><br>
 RELATIONS:<br>
 Collection<KierunkiStudiowEntity> kierunkiByIdTytulu;   //1:M relation with FieldOfStudiesEntity<br>
 */
@Entity
@Table(name = "tytuly_zawodowe", schema = "public", catalog = "praktykidb")
public class TytulyZawodoweEntity {
    private int idTytuluZawodowego;     //ID of Degree
    private String tytulZawodowy;       //Name

    private Collection<KierunkiStudiowEntity> kierunkiByIdTytulu;   //1:M relation with FieldOfStudiesEntity

    /*
    ATTRIBUTES
    */

    //ID of Type
    @JsonIgnore
    @Id
    @GeneratedValue
    @Column(name = "id_tytulu_zawodowego", nullable = false)
    public int getIdTytuluZawodowego() {
        return idTytuluZawodowego;
    }

    public void setIdTytuluZawodowego(int idTytuluZawodowego) {
        this.idTytuluZawodowego = idTytuluZawodowego;
    }

    //Name
    @Basic
    @Column(name = "tytul_zawodowy", nullable = false)
    public String getTytulZawodowy() {
        return tytulZawodowy;
    }

    public void setTytulZawodowy(String tytulZawodowy) {
        this.tytulZawodowy = tytulZawodowy;
    }

    /*
    RELATIONS
    */

    //1:M relation with EmployerApplicationsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "tytul")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KierunkiStudiowEntity> getKierunkiByIdTytulu() {
        return kierunkiByIdTytulu;
    }

    public void setKierunkiByIdTytulu(Collection<KierunkiStudiowEntity> kierunkiByIdTytulu) {
        this.kierunkiByIdTytulu = kierunkiByIdTytulu;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TytulyZawodoweEntity)) return false;

        TytulyZawodoweEntity that = (TytulyZawodoweEntity) o;

        if (idTytuluZawodowego != that.idTytuluZawodowego) return false;
        if (!tytulZawodowy.equals(that.tytulZawodowy)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTytuluZawodowego;
        result = 31 * result + tytulZawodowy.hashCode();
        return result;
    }
}