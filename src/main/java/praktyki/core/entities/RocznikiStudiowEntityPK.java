package praktyki.core.entities;


import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 17.02.15.
 */
/**
PRIMARY KEYS FOR CourseYearbookEntity ("RocznikiStudiowEntity")
 */
public class RocznikiStudiowEntityPK implements Serializable {
    private Integer idKierunku;             //ID of Course
    private Integer idRokuAkademickiego;    //ID of Academic Year

    //ID of Course
    @Id
    @Column(name = "id_kierunku")
    public Integer getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(Integer idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Academic Year
    @Id
    @Column(name = "id_roku_akademickiego")
    public Integer getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(Integer idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RocznikiStudiowEntityPK)) return false;

        RocznikiStudiowEntityPK that = (RocznikiStudiowEntityPK) o;

        if (idKierunku != null ? !idKierunku.equals(that.idKierunku) : that.idKierunku != null) return false;
        if (idRokuAkademickiego != null ? !idRokuAkademickiego.equals(that.idRokuAkademickiego) : that.idRokuAkademickiego != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku != null ? idKierunku.hashCode() : 0;
        result = 31 * result + (idRokuAkademickiego != null ? idRokuAkademickiego.hashCode() : 0);
        return result;
    }
}
