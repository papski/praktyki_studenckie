package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 25.01.15.
 */
/**
PRIMARY KEYS FOR EmployersProfilesEntity ("ProfilePraktykodawcowEntity")
 */
public class ProfilePraktykodawcowEntityPK implements Serializable{
    private Integer idPraktykodawcy;    //ID of Employer
    private Integer idProfilu;          //ID of Profile

    //ID of Employer
    @Id
    @Column(name = "id_praktykodawcy")
    public Integer getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(Integer idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //ID of Profile
    @Id
    @Column(name = "id_profilu")
    public Integer getIdProfilu() {
        return idProfilu;
    }

    public void setIdProfilu(Integer idProfilu) {
        this.idProfilu = idProfilu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfilePraktykodawcowEntityPK)) return false;

        ProfilePraktykodawcowEntityPK that = (ProfilePraktykodawcowEntityPK) o;

        if (idPraktykodawcy != null ? !idPraktykodawcy.equals(that.idPraktykodawcy) : that.idPraktykodawcy != null)
            return false;
        if (idProfilu != null ? !idProfilu.equals(that.idProfilu) : that.idProfilu != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPraktykodawcy != null ? idPraktykodawcy.hashCode() : 0;
        result = 31 * result + (idProfilu != null ? idProfilu.hashCode() : 0);
        return result;
    }
}
