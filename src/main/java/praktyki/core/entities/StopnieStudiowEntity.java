package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 10.02.15.
 */
/**
CourseGradeEntity<br>
ENTITY CONTAINING GRADES OF FIELD OF STUDY (1st - undegraduate, 2nd - graduate etc.)<br><br>
 ATTRIBUTES:<br>
 int idStopniaStudiow;       //ID of Grade<br>
 String stopienStudiow;      //Name<br><br>
 RELATIONS:<br>
 Collection<KierunkiStudiowEntity> kierunkiByIdStopnia;  //1:M relation with FieldOfStudiesEntity<br>
 */
@Entity
@Table(name = "stopnie_studiow", schema = "public", catalog = "praktykidb")
public class StopnieStudiowEntity {
    private int idStopniaStudiow;       //ID of Grade
    private String stopienStudiow;      //Name

    private Collection<KierunkiStudiowEntity> kierunkiByIdStopnia;  //1:M relation with FieldOfStudiesEntity

    /*
    ATTRIBUTES
    */

    //ID of Grade
    @JsonIgnore
    @Id
    @GeneratedValue
    @Column(name = "id_stopnia_studiow", nullable = false)
    public int getIdStopniaStudiow() {
        return idStopniaStudiow;
    }

    public void setIdStopniaStudiow(int idStopniaStudiow) {
        this.idStopniaStudiow = idStopniaStudiow;
    }

    //Name
    @Basic
    @Column(name = "stopien_studiow", nullable = false)
    public String getStopienStudiow() {
        return stopienStudiow;
    }

    public void setStopienStudiow(String stopienStudiow) {
        this.stopienStudiow = stopienStudiow;
    }

    /*
    RELATIONS
    */

    //1:M relation with FieldOfStudiesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "stopien")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KierunkiStudiowEntity> getKierunkiByIdStopnia() {
        return kierunkiByIdStopnia;
    }

    public void setKierunkiByIdStopnia(Collection<KierunkiStudiowEntity> kierunkiByIdStopnia) {
        this.kierunkiByIdStopnia = kierunkiByIdStopnia;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StopnieStudiowEntity)) return false;

        StopnieStudiowEntity that = (StopnieStudiowEntity) o;

        if (idStopniaStudiow != that.idStopniaStudiow) return false;
        if (!stopienStudiow.equals(that.stopienStudiow)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStopniaStudiow;
        result = 31 * result + stopienStudiow.hashCode();
        return result;
    }
}
