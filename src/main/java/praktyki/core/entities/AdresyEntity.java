package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 08.12.14.
 */

/**
AddressesEntity<br>
ENTITY CONTAINING ADDRESSES OF STUDENT AND EMPLOYER<br><br>
ATTRIBUTES:<br>
int idAdresu;       //ID of address<br>
String ulica;       //street name<br>
String nrBudynku;   //building number<br>
String miejscowosc; //city<br>
String kodPocztowy; //postal code<br>
String kraj;        //country<br>
String fax;         //fax<br>
String telefon1;    //phone no.1<br>
String telefon2;    //phone no.2<br><br>
RELATIONS:<br>
StudenciEntity studenciByIdAdresu;  //OneToOne relation with StudentsEntity<br>
Collection<AdresyPraktykodawcyEntity> praktykodawcyByIdAdresu;  //OneToMany relation with EmployersAddressesEntity<br>
 */
@Entity
@Table(name = "adresy", schema = "public", catalog = "praktykidb")
public class AdresyEntity {
    private int idAdresu;       //ID of address
    private String ulica;       //street name
    private String nrBudynku;   //building number
    private String miejscowosc; //city
    private String kodPocztowy; //postal code
    private String kraj;        //country
    private String fax;         //fax
    private String telefon1;    //phone no.1
    private String telefon2;    //phone no.2

    private StudenciEntity studenciByIdAdresu;  //OneToOne relation with StudentsEntity
    private Collection<AdresyPraktykodawcyEntity> praktykodawcyByIdAdresu;  //OneToMany relation with EmployersAddressesEntity

    /*
    ATTRIBUTES
     */

    //ID of address
    @Id
    @Column(name = "id_adresu")
    @GeneratedValue
    public int getIdAdresu() {
        return idAdresu;
    }

    public void setIdAdresu(int idAdresu) {
        this.idAdresu = idAdresu;
    }

    //street name
    @Basic
    @Column(name = "ulica", nullable = false)
    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    //building number
    @Basic
    @Column(name = "nr_budynku", nullable = false)
    public String getNrBudynku() {
        return nrBudynku;
    }

    public void setNrBudynku(String nrBudynku) {
        this.nrBudynku = nrBudynku;
    }

    //city
    @Basic
    @Column(name = "miejscowosc", nullable = false)
    public String getMiejscowosc() {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) {
        this.miejscowosc = miejscowosc;
    }

    //postal code
    @Basic
    @Column(name = "kod_pocztowy", nullable = false)
    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    //country
    @Basic
    @Column(name = "kraj", nullable = false)
    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    //fax
    @Basic
    @Column(name = "fax")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    //phone no.1
    @Basic
    @Column(name = "telefon1")
    public String getTelefon1() {
        return telefon1;
    }

    public void setTelefon1(String telefon1) {
        this.telefon1 = telefon1;
    }

    //phone no.2
    @Basic
    @Column(name = "telefon2")
    public String getTelefon2() {
        return telefon2;
    }

    public void setTelefon2(String telefon2) {
        this.telefon2 = telefon2;
    }

    /*
    RELATIONS
     */

    //OneToOne relation with StudentsEntity
    @JsonIgnore
    @OneToOne(mappedBy = "adresyByIdAdresu", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public StudenciEntity getStudenciByIdAdresu() {
        return studenciByIdAdresu;
    }

    public void setStudenciByIdAdresu(StudenciEntity studenciByIdAdresu) {
        this.studenciByIdAdresu = studenciByIdAdresu;
    }

    //OneToMany relation with EmployersAddressesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "adres", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<AdresyPraktykodawcyEntity> getPraktykodawcyByIdAdresu() {
        return praktykodawcyByIdAdresu;
    }

    public void setPraktykodawcyByIdAdresu(Collection<AdresyPraktykodawcyEntity> praktykodawcyByIdAdresu) {
        this.praktykodawcyByIdAdresu = praktykodawcyByIdAdresu;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdresyEntity)) return false;

        AdresyEntity that = (AdresyEntity) o;

        if (idAdresu != that.idAdresu) return false;
        if (fax != null ? !fax.equals(that.fax) : that.fax != null) return false;
        if (!kodPocztowy.equals(that.kodPocztowy)) return false;
        if (!kraj.equals(that.kraj)) return false;
        if (!miejscowosc.equals(that.miejscowosc)) return false;
        if (!nrBudynku.equals(that.nrBudynku)) return false;
        if (telefon1 != null ? !telefon1.equals(that.telefon1) : that.telefon1 != null) return false;
        if (telefon2 != null ? !telefon2.equals(that.telefon2) : that.telefon2 != null) return false;
        if (!ulica.equals(that.ulica)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAdresu;
        result = 31 * result + ulica.hashCode();
        result = 31 * result + nrBudynku.hashCode();
        result = 31 * result + miejscowosc.hashCode();
        result = 31 * result + kodPocztowy.hashCode();
        result = 31 * result + kraj.hashCode();
        result = 31 * result + (fax != null ? fax.hashCode() : 0);
        result = 31 * result + (telefon1 != null ? telefon1.hashCode() : 0);
        result = 31 * result + (telefon2 != null ? telefon2.hashCode() : 0);
        return result;
    }
}
