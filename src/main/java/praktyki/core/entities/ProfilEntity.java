package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 25.01.15.
 */
/**
ProfilesEntity<br>
ENTITY CONTAINING PROFILES OF EMPLOYER<br><br>
 ATTRIBUTES:<br>
 Integer idProfilu;  //ID of Profile<br>
 String branza;      //Type of Business<br><br>
 RELATIONS:<br>
 Collection<ProfilePraktykodawcowEntity> praktykodawcyByIdProfilu;   //1:M relation with EmployersProfilesEntity<br>
 */
@Entity
@Table(name = "profil", schema = "public", catalog = "praktykidb")
public class ProfilEntity {
    private Integer idProfilu;  //ID of Profile
    private String branza;      //Type of Business

    private Collection<ProfilePraktykodawcowEntity> praktykodawcyByIdProfilu;   //1:M relation with EmployersProfilesEntity

    /*
    ATTRIBUTES
    */

    //ID of Profile
    @Id
    @GeneratedValue
    @Column(name = "id_profilu")
    public Integer getIdProfilu() {
        return idProfilu;
    }

    public void setIdProfilu(Integer idProfilu) {
        this.idProfilu = idProfilu;
    }

    //Type of Business
    @Basic
    @Column(name = "branza", nullable = false)
    public String getBranza() {
        return branza;
    }

    public void setBranza(String branza) {
        this.branza = branza;
    }

    /*
    RELATIONS
    */

    //1:M relation with EmployersProfilesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "profil", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<ProfilePraktykodawcowEntity> getPraktykodawcyByIdProfilu() {
        return praktykodawcyByIdProfilu;
    }

    public void setPraktykodawcyByIdProfilu(Collection<ProfilePraktykodawcowEntity> praktykodawcyByIdProfilu) {
        this.praktykodawcyByIdProfilu = praktykodawcyByIdProfilu;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfilEntity)) return false;

        ProfilEntity that = (ProfilEntity) o;

        if (!branza.equals(that.branza)) return false;
        if (idProfilu != null ? !idProfilu.equals(that.idProfilu) : that.idProfilu != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idProfilu != null ? idProfilu.hashCode() : 0;
        result = 31 * result + branza.hashCode();
        return result;
    }
}
