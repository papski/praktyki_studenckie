package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 22.02.15.
 */
/**
CourseTutorsEntity<br>
JOIN TABLE BETWEEN TUTORS, FIELDS OF STUDY (COURSES) AND EMPLOYERS<br><br>
ATTRIBUTES:<br>
 int idKierunku;         //ID of Course<br>
 int idOpiekunaPraktyk;  //ID of Tutor<br>
 int idPraktykodawcy;    //ID of Employer<br><br>
RELATIONS:<br>
 KierunkiStudiowEntity kierunek; //1:M relation with FieldsOfStudyEntity<br>
 OpiekunowiePraktykEntity opiekun;   //1:M relation with TutorsEntity<br>
 PraktykodawcyEntity praktykodawca;  //1:M relation with EmployersEntity<br>
 Collection<PraktykiEntity> praktyka;    //M:1 relation with PracticesEntity<br>
 */
@Entity
@IdClass(OpiekunowieKierunkowiEntityPK.class)
@Table(name = "opiekunowie_kierunkowi", schema = "public", catalog = "praktykidb")
public class OpiekunowieKierunkowiEntity {
    private int idKierunku;         //ID of Course
    private int idOpiekunaPraktyk;  //ID of Tutor
    private int idPraktykodawcy;    //ID of Employer

    private KierunkiStudiowEntity kierunek; //1:M relation with FieldsOfStudyEntity
    private OpiekunowiePraktykEntity opiekun;   //1:M relation with TutorsEntity
    private PraktykodawcyEntity praktykodawca;  //1:M relation with EmployersEntity
    private Collection<PraktykiEntity> praktyka;    //M:1 relation with PracticesEntity

    /*
    ATTRIBUTES
    */

    //ID of Course
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_kierunku")
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Tutor
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_opiekuna_praktyk")
    public int getIdOpiekunaPraktyk() {
        return idOpiekunaPraktyk;
    }

    public void setIdOpiekunaPraktyk(int idOpiekunaPraktyk) {
        this.idOpiekunaPraktyk = idOpiekunaPraktyk;
    }

    //ID of Employer
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_praktykodawcy")
    public int getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(int idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    /*
    RELATIONS
     */

    //1:M relation with FieldsOfStudyEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
    public KierunkiStudiowEntity getKierunek() {
        return kierunek;
    }

    public void setKierunek(KierunkiStudiowEntity kierunek) {
        this.kierunek = kierunek;
    }

    //1:M relation with TutorsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_opiekuna_praktyk", referencedColumnName = "id_opiekuna_praktyk", insertable = false, updatable = false)
    public OpiekunowiePraktykEntity getOpiekun() {
        return opiekun;
    }

    public void setOpiekun(OpiekunowiePraktykEntity opiekun) {
        this.opiekun = opiekun;
    }

    //1:M relation with EmployersEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    //M:1 relation with PracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "opiekun")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiEntity> getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(Collection<PraktykiEntity> praktyka) {
        this.praktyka = praktyka;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OpiekunowieKierunkowiEntity)) return false;

        OpiekunowieKierunkowiEntity that = (OpiekunowieKierunkowiEntity) o;

        if (idKierunku != that.idKierunku) return false;
        if (idOpiekunaPraktyk != that.idOpiekunaPraktyk) return false;
        if (idPraktykodawcy != that.idPraktykodawcy) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku;
        result = 31 * result + idOpiekunaPraktyk;
        result = 31 * result + idPraktykodawcy;
        return result;
    }
}
