package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by dawid on 07.02.15.
 */
/**
UsersEntity<br>
ENTITY CONTAINING USER'S INFO OF EACH PERSON<br><br>
 ATTRIBUTES:<br>
 Integer idUzytkownika;      //ID of User<br>
 Integer idOsoby;            //ID of Person<br>
 Integer idRoli;             //ID of Role<br>
 String login;               //Login<br>
 String haslo;               //Password (Hashed)<br>
 String salt;                //Salt (Unique for each User)<br>
 Boolean blokada;            //Blockade<br><br>
 RELATIONS:<br>
 OsobyEntity uzytkownikByIdOsoby;    //1:1 relation with PersonsEntity<br>
 RoleEntity uzytkownikByIdRoli;  //M:1 relation with RolesEntity<br>
 */
@Entity
@Table(name = "uzytkownicy", schema = "public", catalog = "praktykidb")
public class UzytkownicyEntity {
    private Integer idUzytkownika;      //ID of User
    private Integer idOsoby;            //ID of Person
    private Integer idRoli;             //ID of Role
    private String login;               //Login
    private String haslo;               //Password (Hashed)
    private String salt;                //Salt (Unique for each User)
    private Boolean blokada;            //Blockade

    private OsobyEntity uzytkownikByIdOsoby;    //1:1 relation with PersonsEntity
    private RoleEntity uzytkownikByIdRoli;  //M:1 relation with RolesEntity

    /*
    ATTRIBUTES
    */

    //Set Blockade for each persisted User to false if Blockade is set to null
    @PrePersist
    public void prePersist() {
        if(blokada == null)
            blokada = false;
    }

    //ID of User
    @Id
    @GeneratedValue
    @Column(name = "id_uzytkownika", nullable = false)
    public Integer getIdUzytkownika() {
        return idUzytkownika;
    }

    public void setIdUzytkownika(Integer idUzytkownika) {
        this.idUzytkownika = idUzytkownika;
    }

    //ID of Person
    @Basic
    @Column(name = "id_osoby")
    public Integer getIdOsoby() {
        return idOsoby;
    }

    public void setIdOsoby(Integer idOsoby) {
        this.idOsoby = idOsoby;
    }

    //ID of Role
    @Basic
    @Column(name = "id_roli")
    public Integer getIdRoli() {
        return idRoli;
    }

    public void setIdRoli(Integer idRoli) {
        this.idRoli = idRoli;
    }

    //Login
    @Basic
    @Column(name = "login", nullable = false)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    //Password (Hashed)
    @Basic
    @Column(name = "haslo", nullable = false)
    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    //Salt (Unique for each User)
    @JsonIgnore
    @Basic
    @Column(name = "salt")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    //Blockade
    @JsonIgnore
    @Basic
    @Column(name = "blokada")
    public Boolean getBlokada() {
        return blokada;
    }

    public void setBlokada(Boolean blokada) {
        this.blokada = blokada;
    }

    /*
    RELATIONS
    */

    //1:1 relation with PersonsEntity
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_osoby", referencedColumnName = "id_osoby", insertable = false, updatable = false)
    public OsobyEntity getUzytkownikByIdOsoby() {
        return uzytkownikByIdOsoby;
    }

    public void setUzytkownikByIdOsoby(OsobyEntity uzytkownikByIdOsoby) {
        this.uzytkownikByIdOsoby = uzytkownikByIdOsoby;
    }

    //M:1 relation with RolesEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_roli", referencedColumnName = "id_roli", insertable = false, updatable = false)
    public RoleEntity getUzytkownikByIdRoli() {
        return uzytkownikByIdRoli;
    }

    public void setUzytkownikByIdRoli(RoleEntity uzytkownikByIdRoli) {
        this.uzytkownikByIdRoli = uzytkownikByIdRoli;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UzytkownicyEntity)) return false;

        UzytkownicyEntity that = (UzytkownicyEntity) o;

        if (blokada != null ? !blokada.equals(that.blokada) : that.blokada != null) return false;
        if (!haslo.equals(that.haslo)) return false;
        if (!idOsoby.equals(that.idOsoby)) return false;
        if (!idRoli.equals(that.idRoli)) return false;
        if (!idUzytkownika.equals(that.idUzytkownika)) return false;
        if (!login.equals(that.login)) return false;
        if (salt != null ? !salt.equals(that.salt) : that.salt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idUzytkownika.hashCode();
        result = 31 * result + idOsoby.hashCode();
        result = 31 * result + idRoli.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + haslo.hashCode();
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (blokada != null ? blokada.hashCode() : 0);
        return result;
    }
}
