package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 21.02.15.
 */
/**
PRIMARY KEYS FOR EmployersCourseCoordinatorsEntity ("KoordynatorzyKierunkowiPraktykodawcowEntity")
 */
public class KoordynatorzyKierunkowiPraktykodawcowEntityPK implements Serializable {
    private Integer idPraktykodawcy;        //ID of Employer
    private Integer idKierunku;             //ID of Course
    private Integer idKoordynatoraPraktyk;  //ID of Coordinator

    //ID of Employer
    @Column(name = "id_praktykodawcy", nullable = false)
    @Id
    public Integer getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(Integer idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //ID of Course
    @Column(name = "id_kierunku", nullable = false)
    @Id
    public Integer getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(Integer idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Coordinator
    @Column(name = "id_koordynatora_praktyk", nullable = false)
    @Id
    public Integer getIdKoordynatoraPraktyk() {
        return idKoordynatoraPraktyk;
    }

    public void setIdKoordynatoraPraktyk(Integer idKoordynatoraPraktyk) {
        this.idKoordynatoraPraktyk = idKoordynatoraPraktyk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KoordynatorzyKierunkowiPraktykodawcowEntityPK)) return false;

        KoordynatorzyKierunkowiPraktykodawcowEntityPK that = (KoordynatorzyKierunkowiPraktykodawcowEntityPK) o;

        if (!idKierunku.equals(that.idKierunku)) return false;
        if (!idKoordynatoraPraktyk.equals(that.idKoordynatoraPraktyk)) return false;
        if (!idPraktykodawcy.equals(that.idPraktykodawcy)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPraktykodawcy.hashCode();
        result = 31 * result + idKierunku.hashCode();
        result = 31 * result + idKoordynatoraPraktyk.hashCode();
        return result;
    }
}
