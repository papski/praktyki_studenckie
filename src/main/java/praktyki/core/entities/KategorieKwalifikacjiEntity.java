package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 21.03.15.
 */
/**
QualificationsCategoriesEntity<br>
ENTITY CONTAINING CATEGORIES OF QUALIFICATIONS<br><br>
 ATTRIBUTES:<br>
 Integer idKategoriiKwalifikacji;    //ID of Qualifications Category<br>
 Integer idKwalifikacji;             //ID of Qualification<br>
 String nazwaKategorii;              //name of category<br>
 Integer idKategoriiNadrzednej;      //ID of parent category<br><br>
 RELATIONS:<br>
 KategorieKwalifikacjiEntity parent; //parent of self-relation<br>
 Collection<KategorieKwalifikacjiEntity> child; //child of self-relation<br>
 KwalifikacjeEntity kwalifikacja; //ManyToOne relation with QualificationsEntity<br>
 */
@Entity
@Table(name = "kategorie_kwalifikacji", schema = "public", catalog = "praktykidb")
public class KategorieKwalifikacjiEntity {
    private Integer idKategoriiKwalifikacji;    //ID of Qualifications Category
    private Integer idKwalifikacji;             //ID of Qualification
    private String nazwaKategorii;              //name of category
    private Integer idKategoriiNadrzednej;      //ID of parent category


    private KategorieKwalifikacjiEntity parent; //parent of self-relation
    private Collection<KategorieKwalifikacjiEntity> child; //child of self-relation
    private KwalifikacjeEntity kwalifikacja; //ManyToOne relation with QualificationsEntity

    /*
    ATTRIBUTES
     */

    //ID of Qualifications Category
    @Id
    @GeneratedValue
    @Column(name = "id_kategorii_kwalifikacji", nullable = false)
    public Integer getIdKategoriiKwalifikacji() {
        return idKategoriiKwalifikacji;
    }

    public void setIdKategoriiKwalifikacji(Integer idKategoriiKwalifikacji) {
        this.idKategoriiKwalifikacji = idKategoriiKwalifikacji;
    }

    //ID of Qualification
    @Basic
    @Column(name = "id_kwalifikacji")
    public Integer getIdKwalifikacji() {
        return idKwalifikacji;
    }

    public void setIdKwalifikacji(Integer idKwalifikacji) {
        this.idKwalifikacji = idKwalifikacji;
    }

    //name of category
    @Basic
    @Column(name = "nazwa_kategorii", nullable = false)
    public String getNazwaKategorii() {
        return nazwaKategorii;
    }

    public void setNazwaKategorii(String nazwaKategorii) {
        this.nazwaKategorii = nazwaKategorii;
    }

    //ID of parent category
    @Basic
    @Column(name = "id_kategorii_nadrzednej")
    public Integer getIdKategoriiNadrzednej() {
        return idKategoriiNadrzednej;
    }

    public void setIdKategoriiNadrzednej(Integer idKategoriiNadrzednej) {
        this.idKategoriiNadrzednej = idKategoriiNadrzednej;
    }

    /*
    RELATIONS
     */

    //parent of self-relation
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_kategorii_nadrzednej", referencedColumnName = "id_kategorii_kwalifikacji", updatable = false, insertable = false)
    public KategorieKwalifikacjiEntity getParent() {
        return parent;
    }

    public void setParent(KategorieKwalifikacjiEntity parent) {
        this.parent = parent;
    }

    //child of self-relation
    @JsonIgnore
    @OneToMany(mappedBy = "parent")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KategorieKwalifikacjiEntity> getChild() {
        return child;
    }

    public void setChild(Collection<KategorieKwalifikacjiEntity> child) {
        this.child = child;
    }


    //ManyToOne relation with QualificationsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_kwalifikacji", referencedColumnName = "id_kwalifikacji", updatable = false, insertable = false)
    public KwalifikacjeEntity getKwalifikacja() {
        return kwalifikacja;
    }

    public void setKwalifikacja(KwalifikacjeEntity kwalifikacja) {
        this.kwalifikacja = kwalifikacja;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KategorieKwalifikacjiEntity that = (KategorieKwalifikacjiEntity) o;

        if (idKategoriiKwalifikacji != null ? !idKategoriiKwalifikacji.equals(that.idKategoriiKwalifikacji) : that.idKategoriiKwalifikacji != null)
            return false;
        if (idKategoriiNadrzednej != null ? !idKategoriiNadrzednej.equals(that.idKategoriiNadrzednej) : that.idKategoriiNadrzednej != null)
            return false;
        if (nazwaKategorii != null ? !nazwaKategorii.equals(that.nazwaKategorii) : that.nazwaKategorii != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKategoriiKwalifikacji != null ? idKategoriiKwalifikacji.hashCode() : 0;
        result = 31 * result + (nazwaKategorii != null ? nazwaKategorii.hashCode() : 0);
        result = 31 * result + (idKategoriiNadrzednej != null ? idKategoriiNadrzednej.hashCode() : 0);
        return result;
    }
}
