package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 08.12.14.
 */
/**
FieldOfStudiesEntity (also named as COURSE in project)<br>
ENTITY CONTAINING FIELDS OF STUDY AT UNIVERSITY<br><br>
 ATTRIBUTES:<br>
 int idKierunku;             //ID of Course<br>
 String nazwaKierunku;       //name of Course<br>
 String nazwaSpecjalnosci;   //name of Speciality<br>
 int liczbaSemestrow;        //number of semesters<br>
 Integer idStopniaStudiow;   //ID of Course Grade ( 1st degree - undergraduate, 2nd degree - graduate, etc)<br>
 Integer idTrybuStudiow;     //ID of Course Mode (full time or external)<br>
 Integer idTytuluZawodowego; //ID of Course Degree (polish inz. , mgr. , etc)<br><br>
 RELATIONS:<br>
 TytulyZawodoweEntity tytul; //ManyToOne realation with CourseDegreeEntity<br>
 StopnieStudiowEntity stopien; //ManyToOne relation with CourseGradeEntity<br>
 TrybyStudiowEntity tryb;    //ManyToOne relation with CourseModeEntity<br>
 Collection<KoordynatorzyKierunkowiEntity> koordynatorByIdKierunku;  //OneToMany relation with CourseCoordinatorsEntity<br>
 Collection<RocznikiStudiowEntity> rocznikiByIdKierunku; //OneToMany relation with CourseYearbookEntity<br>
 Collection<OpiekunowieKierunkowiEntity> opiekunPraktykodawcy;   //OneToMany relation with CourseTutorEntity<br>
 */
@Entity
@Table(name = "kierunki_studiow", schema = "public", catalog = "praktykidb")
public class KierunkiStudiowEntity {
    private int idKierunku;             //ID of Course
    private String nazwaKierunku;       //name of Course
    private String nazwaSpecjalnosci;   //name of Speciality
    private int liczbaSemestrow;        //number of semesters
    private Integer idStopniaStudiow;   //ID of Course Grade ( 1st degree - undergraduate, 2nd degree - graduate, etc)
    private Integer idTrybuStudiow;     //ID of Course Mode (full time or external)
    private Integer idTytuluZawodowego; //ID of Course Degree (polish inz. , mgr. , etc)

    private TytulyZawodoweEntity tytul; //ManyToOne realation with CourseDegreeEntity
    private StopnieStudiowEntity stopien; //ManyToOne relation with CourseGradeEntity
    private TrybyStudiowEntity tryb;    //ManyToOne relation with CourseModeEntity
    private Collection<KoordynatorzyKierunkowiEntity> koordynatorByIdKierunku;  //OneToMany relation with CourseCoordinatorsEntity
    private Collection<RocznikiStudiowEntity> rocznikiByIdKierunku; //OneToMany relation with CourseYearbookEntity
    private Collection<OpiekunowieKierunkowiEntity> opiekunPraktykodawcy;   //OneToMany relation with CourseTutorEntity

    /*
    ATTRIBUTES
     */

    //ID of Course
    @Id
    @Column(name = "id_kierunku", updatable = true)
    @GeneratedValue
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //name of Course
    @Basic
    @Column(name = "nazwa_kierunku", nullable = false)
    public String getNazwaKierunku() {
        return nazwaKierunku;
    }

    public void setNazwaKierunku(String nazwaKierunku) {
        this.nazwaKierunku = nazwaKierunku;
    }

    //name of Speciality
    @Basic
    @Column(name = "nazwa_specjalnosci", nullable = false)
    public String getNazwaSpecjalnosci() {
        return nazwaSpecjalnosci;
    }

    public void setNazwaSpecjalnosci(String nazwaSpecjalnosci) {
        this.nazwaSpecjalnosci = nazwaSpecjalnosci;
    }

    //number of semesters
    @Basic
    @Column(name = "liczba_semestrow", nullable = false)
    public int getLiczbaSemestrow() {
        return liczbaSemestrow;
    }

    public void setLiczbaSemestrow(int liczbaSemestrow) {
        this.liczbaSemestrow = liczbaSemestrow;
    }

    //ID of Course Grade ( 1st degree - undergraduate, 2nd degree - graduate, etc)
    @JsonIgnore
    @Basic
    @Column(name = "id_stopnia_studiow")
    public Integer getIdStopniaStudiow() {
        return idStopniaStudiow;
    }

    public void setIdStopniaStudiow(Integer idStopniaStudiow) {
        this.idStopniaStudiow = idStopniaStudiow;
    }

    //ID of Course Mode (full time or external)
    @JsonIgnore
    @Basic
    @Column(name = "id_trybu_studiow")
    public Integer getIdTrybuStudiow() {
        return idTrybuStudiow;
    }

    public void setIdTrybuStudiow(Integer idTrybuStudiow) {
        this.idTrybuStudiow = idTrybuStudiow;
    }

    //ID of Course Degree (polish inz. , mgr. , etc)
    @JsonIgnore
    @Basic
    @Column(name = "id_tytulu_zawodowego")
    public Integer getIdTytuluZawodowego() {
        return idTytuluZawodowego;
    }

    public void setIdTytuluZawodowego(Integer idTytuluZawodowego) {
        this.idTytuluZawodowego = idTytuluZawodowego;
    }

    /*
    RELATIONS
     */

    //ManyToOne realation with CourseDegreeEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_tytulu_zawodowego", referencedColumnName = "id_tytulu_zawodowego", insertable = false, updatable = false)
    public TytulyZawodoweEntity getTytul() {
        return tytul;
    }

    public void setTytul(TytulyZawodoweEntity tytul) {
        this.tytul = tytul;
    }

    //ManyToOne relation with CourseGradeEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_stopnia_studiow", referencedColumnName = "id_stopnia_studiow", insertable = false, updatable = false)
    public StopnieStudiowEntity getStopien() {
        return stopien;
    }

    public void setStopien(StopnieStudiowEntity stopien) {
        this.stopien = stopien;
    }

    //ManyToOne relation with CourseModeEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_trybu_studiow", referencedColumnName = "id_trybu_studiow", insertable = false, updatable = false)
    public TrybyStudiowEntity getTryb() {
        return tryb;
    }

    public void setTryb(TrybyStudiowEntity tryb) {
        this.tryb = tryb;
    }

    //OneToMany relation with CourseCoordinatorsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "kierunekByIdKoordynatora", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KoordynatorzyKierunkowiEntity> getKoordynatorByIdKierunku() {
        return koordynatorByIdKierunku;
    }

    public void setKoordynatorByIdKierunku(Collection<KoordynatorzyKierunkowiEntity> koordynatorByIdKierunku) {
        this.koordynatorByIdKierunku = koordynatorByIdKierunku;
    }

    //OneToMany relation with CourseYearbookEntity
    @JsonIgnore
    @OneToMany(mappedBy = "kierunek", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<RocznikiStudiowEntity> getRocznikiByIdKierunku() {
        return rocznikiByIdKierunku;
    }

    public void setRocznikiByIdKierunku(Collection<RocznikiStudiowEntity> rocznikiByIdKierunku) {
        this.rocznikiByIdKierunku = rocznikiByIdKierunku;
    }

    //OneToMany relation with CourseTutorEntity
    @JsonIgnore
    @OneToMany(mappedBy = "kierunek", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<OpiekunowieKierunkowiEntity> getOpiekunPraktykodawcy() {
        return opiekunPraktykodawcy;
    }

    public void setOpiekunPraktykodawcy(Collection<OpiekunowieKierunkowiEntity> opiekunPraktykodawcy) {
        this.opiekunPraktykodawcy = opiekunPraktykodawcy;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KierunkiStudiowEntity)) return false;

        KierunkiStudiowEntity that = (KierunkiStudiowEntity) o;

        if (idKierunku != that.idKierunku) return false;
        if (liczbaSemestrow != that.liczbaSemestrow) return false;
        if (!nazwaKierunku.equals(that.nazwaKierunku)) return false;
        if (!nazwaSpecjalnosci.equals(that.nazwaSpecjalnosci)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku;
        result = 31 * result + nazwaKierunku.hashCode();
        result = 31 * result + nazwaSpecjalnosci.hashCode();
        result = 31 * result + liczbaSemestrow;
        return result;
    }
}
