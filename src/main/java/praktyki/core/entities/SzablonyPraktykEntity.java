package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 28.02.15.
 */
/**
TemplatesEntity<br>
ENTITY CONTAINING TEMPLATES OF PRACTICES<br><br>
 ATTRIBUTES:<br>
 Integer idSzablonu;     //ID of Template<br>
 String szablon;         //Name<br>
 String opisSzablonu;    //Description<br>
 RELATIONS:<br>
 Collection<WymaganeKwalifikacjeEntity> kwalifikacjeWymaganeByIdSzablonu;    //1:M relation with RequiredQualificationsEntity<br>
 Collection<OferowaneKwalifikacjeEntity> kwalifikacjeOferowaneByIdSzablonu;  //1:M relation with OfferedQualificationsEntity<br>
 Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity<br>
 */
@Entity
@Table(name = "szbalony_praktyk", schema = "public", catalog = "praktykidb")
public class SzablonyPraktykEntity {
    private Integer idSzablonu;     //ID of Template
    private String szablon;         //Name
    private String opisSzablonu;    //Description

    private Collection<WymaganeKwalifikacjeEntity> kwalifikacjeWymaganeByIdSzablonu;    //1:M relation with RequiredQualificationsEntity
    private Collection<OferowaneKwalifikacjeEntity> kwalifikacjeOferowaneByIdSzablonu;  //1:M relation with OfferedQualificationsEntity
    private Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity

    /*
    ATTRIBUTES
    */

    //ID of Template
    @Id
    @GeneratedValue
    @Column(name = "id_szablonu", nullable = false)
    public Integer getIdSzablonu() {
        return idSzablonu;
    }

    public void setIdSzablonu(Integer idSzablonu) {
        this.idSzablonu = idSzablonu;
    }

    //Name
    @Basic
    @Column(name = "szablon", nullable = false)
    public String getSzablon() {
        return szablon;
    }

    public void setSzablon(String szablon) {
        this.szablon = szablon;
    }

    //Description
    @Basic
    @Column(name = "opis_szablonu")
    public String getOpisSzablonu() {
        return opisSzablonu;
    }

    public void setOpisSzablonu(String opisSzablonu) {
        this.opisSzablonu = opisSzablonu;
    }

    /*
    RELATIONS
    */

    //1:M relation with RequiredQualificationsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "szablon", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<WymaganeKwalifikacjeEntity> getKwalifikacjeWymaganeByIdSzablonu() {
        return kwalifikacjeWymaganeByIdSzablonu;
    }

    public void setKwalifikacjeWymaganeByIdSzablonu(Collection<WymaganeKwalifikacjeEntity> kwalifikacjeWymaganeByIdSzablonu) {
        this.kwalifikacjeWymaganeByIdSzablonu = kwalifikacjeWymaganeByIdSzablonu;
    }

    //1:M relation with OfferedQualificationsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "szablon", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<OferowaneKwalifikacjeEntity> getKwalifikacjeOferowaneByIdSzablonu() {
        return kwalifikacjeOferowaneByIdSzablonu;
    }

    public void setKwalifikacjeOferowaneByIdSzablonu(Collection<OferowaneKwalifikacjeEntity> kwalifikacjeOferowaneByIdSzablonu) {
        this.kwalifikacjeOferowaneByIdSzablonu = kwalifikacjeOferowaneByIdSzablonu;
    }

    //1:M relation with PracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "szablon")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiEntity> getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(Collection<PraktykiEntity> praktyka) {
        this.praktyka = praktyka;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SzablonyPraktykEntity)) return false;

        SzablonyPraktykEntity that = (SzablonyPraktykEntity) o;

        if (!idSzablonu.equals(that.idSzablonu)) return false;
        if (opisSzablonu != null ? !opisSzablonu.equals(that.opisSzablonu) : that.opisSzablonu != null) return false;
        if (!szablon.equals(that.szablon)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idSzablonu.hashCode();
        result = 31 * result + szablon.hashCode();
        result = 31 * result + (opisSzablonu != null ? opisSzablonu.hashCode() : 0);
        return result;
    }
}
