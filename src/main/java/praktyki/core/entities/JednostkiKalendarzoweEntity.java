package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 05.02.15.
 */
/**
CalendarUnitsEntity<br
ENTITY CONTAINING CALENDAR UNITS FOR COURSES<br><br>
 ATTRIBUTES:<br>
 int idJednostkiKalendarzowej;   //ID of Calendar unit<br>
 String jednostkaKalendarzowa;   //name of Calendar Unit<br><br>
 RELATIONS:<br>
 Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdJednostki;   //OneToMany realtion with CoursePracticesEntity
 */
@Entity
@Table(name = "jednostki_kalendarzowe", schema = "public", catalog = "praktykidb")
public class JednostkiKalendarzoweEntity {
    private int idJednostkiKalendarzowej;   //ID of Calendar unit
    private String jednostkaKalendarzowa;   //name of Calendar Unit

    private Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdJednostki;   //OneToMany realtion with CoursePracticesEntity

    /*
    ATTRIBUTES
     */

    //ID of Calendar unit
    @Id
    @GeneratedValue
    @Column(name = "id_jednostki_kalendarzowej", nullable = false)
    public int getIdJednostkiKalendarzowej() {
        return idJednostkiKalendarzowej;
    }

    public void setIdJednostkiKalendarzowej(int idJednostkiKalendarzowej) {
        this.idJednostkiKalendarzowej = idJednostkiKalendarzowej;
    }

    //name of Calendar Unit
    @Basic
    @Column(name = "jednostka_kalendarzowa", nullable = false)
    public String getJednostkaKalendarzowa() {
        return jednostkaKalendarzowa;
    }

    public void setJednostkaKalendarzowa(String jednostkaKalendarzowa) {
        this.jednostkaKalendarzowa = jednostkaKalendarzowa;
    }

    /*
    RELATIONS
    */

    //OneToMany realtion with CoursePracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "jednostkiKalendarzowe", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiKierunkoweEntity> getPraktykiKierunkoweByIdJednostki() {
        return praktykiKierunkoweByIdJednostki;
    }

    public void setPraktykiKierunkoweByIdJednostki(Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdJednostki) {
        this.praktykiKierunkoweByIdJednostki = praktykiKierunkoweByIdJednostki;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JednostkiKalendarzoweEntity)) return false;

        JednostkiKalendarzoweEntity that = (JednostkiKalendarzoweEntity) o;

        if (idJednostkiKalendarzowej != that.idJednostkiKalendarzowej) return false;
        if (!jednostkaKalendarzowa.equals(that.jednostkaKalendarzowa)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idJednostkiKalendarzowej;
        result = 31 * result + jednostkaKalendarzowa.hashCode();
        return result;
    }
}
