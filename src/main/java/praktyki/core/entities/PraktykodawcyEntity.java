package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 08.12.14.
 */
/**
EmployersEntity<br>
ENTITY CONTAINING INFO ABOUT EMPLOYER<br><br>
 ATTRIBUTES:<br>
 int idPraktykodawcy;    //ID of Employer<br>
 String nazwa;           //Name<br>
 String pelnaNazwa;      //Full Name<br>
 boolean zaufany;        //Trusted Status<br><br>
 RELATIONS:<br>
 Collection<AdresyPraktykodawcyEntity> adresyByIdPraktykodawcy;  //1:M relation with EmployerAddressesEntity<br>
 Collection<ProfilePraktykodawcowEntity> profileByIdPraktykodawcy;   //1:M relation with EmployersProfilesEntity<br>
 Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> koordynatorzyByIdPraktykodawcy; //1:M relation with EmployersCourseCoordinatorsEntity<br>
 Collection<OpiekunowieKierunkowiEntity> opiekunKierunku;    //1:M relation with CourseTutorsEntity<br>
 Collection<PraktykiEntity> praktyki;    //1:M relation with PracticesEntity<br>
 Collection<ZgloszeniaPraktykodawcowEntity> zgloszenie;  //1:M relation with EmployerApplicationsEntity<br>
 */
@Entity
@Table(name = "praktykodawcy", schema = "public", catalog = "praktykidb")
public class PraktykodawcyEntity {
    private int idPraktykodawcy;    //ID of Employer
    private String nazwa;           //Name
    private String pelnaNazwa;      //Full Name
    private boolean zaufany;        //Trusted Status

    private Collection<AdresyPraktykodawcyEntity> adresyByIdPraktykodawcy;  //1:M relation with EmployerAddressesEntity
    private Collection<ProfilePraktykodawcowEntity> profileByIdPraktykodawcy;   //1:M relation with EmployersProfilesEntity
    private Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> koordynatorzyByIdPraktykodawcy; //1:M relation with EmployersCourseCoordinatorsEntity
    private Collection<OpiekunowieKierunkowiEntity> opiekunKierunku;    //1:M relation with CourseTutorsEntity
    private Collection<PraktykiEntity> praktyki;    //1:M relation with PracticesEntity
    private Collection<ZgloszeniaPraktykodawcowEntity> zgloszenie;  //1:M relation with EmployerApplicationsEntity

    /*
    ATTRIBUTES
    */

    //ID of Employer
    @Id
    @Column(name = "id_praktykodawcy", nullable = false)
    @GeneratedValue
    public int getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(int idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //Name
    @Basic
    @Column(name = "nazwa", nullable = false)
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    //Full Name
    @Basic
    @Column(name = "pelna_nazwa", nullable = false)
    public String getPelnaNazwa() {
        return pelnaNazwa;
    }

    public void setPelnaNazwa(String pelnaNazwa) {
        this.pelnaNazwa = pelnaNazwa;
    }

    //Trusted Status
    @Basic
    @Column(name = "zaufany")
    public boolean isZaufany() {
        return zaufany;
    }

    public void setZaufany(boolean zaufany) {
        this.zaufany = zaufany;
    }

    /*
    RELATIONS
    */

    //1:M relation with EmployerAddressesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "praktykodawca", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<AdresyPraktykodawcyEntity> getAdresyByIdPraktykodawcy() {
        return adresyByIdPraktykodawcy;
    }

    public void setAdresyByIdPraktykodawcy(Collection<AdresyPraktykodawcyEntity> adresyByIdPraktykodawcy) {
        this.adresyByIdPraktykodawcy = adresyByIdPraktykodawcy;
    }

    //1:M relation with EmployerProfilesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "praktykodawca", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<ProfilePraktykodawcowEntity> getProfileByIdPraktykodawcy() {
        return profileByIdPraktykodawcy;
    }

    public void setProfileByIdPraktykodawcy(Collection<ProfilePraktykodawcowEntity> profileByIdPraktykodawcy) {
        this.profileByIdPraktykodawcy = profileByIdPraktykodawcy;
    }

    //1:M relation with EmployersCourseCoordinatorsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "praktykodawca", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> getKoordynatorzyByIdPraktykodawcy() {
        return koordynatorzyByIdPraktykodawcy;
    }

    public void setKoordynatorzyByIdPraktykodawcy(Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> koordynatorzyByIdPraktykodawcy) {
        this.koordynatorzyByIdPraktykodawcy = koordynatorzyByIdPraktykodawcy;
    }

    //1:M relation with CourseTutorsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "praktykodawca", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<OpiekunowieKierunkowiEntity> getOpiekunKierunku() {
        return opiekunKierunku;
    }

    public void setOpiekunKierunku(Collection<OpiekunowieKierunkowiEntity> opiekunKierunku) {
        this.opiekunKierunku = opiekunKierunku;
    }

    //1:M relation with PracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "praktykodawca")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiEntity> getPraktyki() {
        return praktyki;
    }

    public void setPraktyki(Collection<PraktykiEntity> praktyki) {
        this.praktyki = praktyki;
    }

    //1:M relation with EmployerApplicationsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "praktykodawca")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<ZgloszeniaPraktykodawcowEntity> getZgloszenie() {
        return zgloszenie;
    }

    public void setZgloszenie(Collection<ZgloszeniaPraktykodawcowEntity> zgloszenie) {
        this.zgloszenie = zgloszenie;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PraktykodawcyEntity)) return false;

        PraktykodawcyEntity that = (PraktykodawcyEntity) o;

        if (idPraktykodawcy != that.idPraktykodawcy) return false;
        if (zaufany != that.zaufany) return false;
        if (!nazwa.equals(that.nazwa)) return false;
        if (!pelnaNazwa.equals(that.pelnaNazwa)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPraktykodawcy;
        result = 31 * result + nazwa.hashCode();
        result = 31 * result + pelnaNazwa.hashCode();
        result = 31 * result + (zaufany ? 1 : 0);
        return result;
    }
}
