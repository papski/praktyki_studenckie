package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 08.03.15.
 */
/**
PRIMARY KEYS FOR CoursePracticesEntity ("PraktykKierunkoweEntity")
 */
public class PraktykiKierunkoweEntityPK implements Serializable{
    private int idKierunku;             //ID of Course
    private int idTypuPraktyki;         //ID of Practice's Type
    private int idRokuAkademickiego;    //ID of Academic Year

    //ID of Course
    @Id
    @Column(name = "id_kierunku")
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Practice's Type
    @Id
    @Column(name = "id_typu_praktyki")
    public int getIdTypuPraktyki() {
        return idTypuPraktyki;
    }

    public void setIdTypuPraktyki(int idTypuPraktyki) {
        this.idTypuPraktyki = idTypuPraktyki;
    }

    //ID of Academic Year
    @Id
    @Column(name = "id_roku_akademickiego")
    public int getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(int idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }
}
