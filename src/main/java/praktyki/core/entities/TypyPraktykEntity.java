package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 05.02.15.
 */
/**
PracticeTypesEntity<br>
ENTITY CONTAINING TYPES OF PRACTICES (Mandatory, Voluntary etc.)<br><br>
 ATTRIBUTES:<br>
 Integer idTypuPraktyki;     //ID of Type<br>
 String typPraktyki;         //Name<br><br>
 RELATIONS:<br>
 Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdTypu;    //1:M relation with CoursePracticesEntity<br>
 */
@Entity
@Table(name = "typy_praktyk", schema = "public", catalog = "praktykidb")
public class TypyPraktykEntity {
    private Integer idTypuPraktyki;     //ID of Type
    private String typPraktyki;         //Name

    private Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdTypu;    //1:M relation with CoursePracticesEntity

    /*
    ATTRIBUTES
    */

    //ID of Type
    @Id
    @GeneratedValue
    @Column(name = "id_typu_praktyki", nullable = false)
    public Integer getIdTypuPraktyki() {
        return idTypuPraktyki;
    }

    public void setIdTypuPraktyki(Integer idTypuPraktyki) {
        this.idTypuPraktyki = idTypuPraktyki;
    }

    //Name
    @Basic
    @Column(name = "typ_praktyki", nullable = false)
    public String getTypPraktyki() {
        return typPraktyki;
    }

    public void setTypPraktyki(String typPraktyki) {
        this.typPraktyki = typPraktyki;
    }

    /*
    RELATIONS
    */

    //1:M relation with CoursePracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "typPraktyki", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiKierunkoweEntity> getPraktykiKierunkoweByIdTypu() {
        return praktykiKierunkoweByIdTypu;
    }

    public void setPraktykiKierunkoweByIdTypu(Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdTypu) {
        this.praktykiKierunkoweByIdTypu = praktykiKierunkoweByIdTypu;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TypyPraktykEntity)) return false;

        TypyPraktykEntity that = (TypyPraktykEntity) o;

        if (!idTypuPraktyki.equals(that.idTypuPraktyki)) return false;
        if (!typPraktyki.equals(that.typPraktyki)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTypuPraktyki.hashCode();
        result = 31 * result + typPraktyki.hashCode();
        return result;
    }
}
