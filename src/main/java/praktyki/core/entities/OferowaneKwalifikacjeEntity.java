package praktyki.core.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by dawid on 28.02.15.
 */
/**
OfferedQualificationsEntity<br>
JOIN TABLE BETWEEN PRACTICE'S TEMPLATE AND QUALIFICATION OF TEMPALTE<br><br>
ATTRIBUTES:<br>
 int idSzablonu;     //ID of Template<br>
 int idKwalifikacji; //ID of Qualification<br><br>
RELATIONS:<br>
 SzablonyPraktykEntity szablon; //1:M relation with PracticeTemplatesEntity<br>
 KwalifikacjeEntity kwalifikacja;    //1:M relation with QualificationsEntity<br>
 */
@Entity
@IdClass(OferowaneKwalifikacjeEntityPK.class)
@Table(name = "oferowane_kwalifikacje", schema = "public", catalog = "praktykidb")
public class OferowaneKwalifikacjeEntity {
    private int idSzablonu;     //ID of Template
    private int idKwalifikacji; //ID of Qualification

    private SzablonyPraktykEntity szablon; //1:M relation with PracticeTemplatesEntity
    private KwalifikacjeEntity kwalifikacja;    //1:M relation with QualificationsEntity

    /*
    ATTRIBUTES
    */

    //ID of Template
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_szablonu")
    public int getIdSzablonu() {
        return idSzablonu;
    }

    public void setIdSzablonu(int idSzablonu) {
        this.idSzablonu = idSzablonu;
    }

    //ID of Qualification
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_kwalifikacji")
    public int getIdKwalifikacji() {
        return idKwalifikacji;
    }

    public void setIdKwalifikacji(int idKwalifikacji) {
        this.idKwalifikacji = idKwalifikacji;
    }

    /*
    RELATIONS
     */

    //1:M relation with PracticeTemplatesEntity
    @ManyToOne
    @JoinColumn(name = "id_szablonu", referencedColumnName = "id_szablonu", insertable = false, updatable = false)
    public SzablonyPraktykEntity getSzablon() {
        return szablon;
    }

    public void setSzablon(SzablonyPraktykEntity szablon) {
        this.szablon = szablon;
    }

    //1:M relation with QualificationsEntity
    @ManyToOne
    @JoinColumn(name = "id_kwalifikacji", referencedColumnName = "id_kwalifikacji", insertable = false, updatable = false)
    public KwalifikacjeEntity getKwalifikacja() {
        return kwalifikacja;
    }

    public void setKwalifikacja(KwalifikacjeEntity kwalifikacja) {
        this.kwalifikacja = kwalifikacja;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OferowaneKwalifikacjeEntity)) return false;

        OferowaneKwalifikacjeEntity that = (OferowaneKwalifikacjeEntity) o;

        if (idKwalifikacji != that.idKwalifikacji) return false;
        if (idSzablonu != that.idSzablonu) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idSzablonu;
        result = 31 * result + idKwalifikacji;
        return result;
    }
}
