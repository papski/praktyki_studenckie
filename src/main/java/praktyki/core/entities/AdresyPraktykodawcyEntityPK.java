package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 10.02.15.
 */
/**
PRIMARY KEYS FOR EmployersAddressesEntity ("AdresyPraktykodawcyEntityPK")
 */
public class AdresyPraktykodawcyEntityPK implements Serializable {
    private Integer idAdresu;   //ID of address
    private Integer idPraktykodawcy; //ID of employer

    //ID of address
    @Column(name = "id_adresu")
    @Id
    public Integer getIdAdresu() {
        return idAdresu;
    }

    public void setIdAdresu(Integer idAdresu) {
        this.idAdresu = idAdresu;
    }

    //ID of employer
    @Column(name = "id_praktykodawcy")
    @Id
    public Integer getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(Integer idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdresyPraktykodawcyEntityPK that = (AdresyPraktykodawcyEntityPK) o;

        if (idAdresu != null ? !idAdresu.equals(that.idAdresu) : that.idAdresu != null) return false;
        if (idPraktykodawcy != null ? !idPraktykodawcy.equals(that.idPraktykodawcy) : that.idPraktykodawcy != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAdresu != null ? idAdresu.hashCode() : 0;
        result = 31 * result + (idPraktykodawcy != null ? idPraktykodawcy.hashCode() : 0);
        return result;
    }
}