package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;


/**
 * Created by dawid on 13.03.15.
 */
/**
PracticesEntity<br>
ENTITY CONTAINING INFO ABOUT PRACTICES<br><br>
ATTRIBUTES:<br>
 Long idPraktykiStudenckiej;         //ID of Practice<br>
 int idSzablonu;                     //ID of Template<br>
 Integer idPraktykodawcy;            //ID of Employer<br>
 int idStatusu;                      //ID of Status<br>
 Date dataRozpoczecia;               //Beginning Date of Practice<br>
 Date dataZakonczenia;               //End Date of Practice<br>
 int idKierunku;                     //ID of Course<br>
 int idTypuPraktyki;                 //ID of Practice's Type<br>
 int idRokuAkademickiego;            //ID of Academic Year<br>
 Long nrAlbumu;                      //(ID)Student Number<br>
 Integer idOpiekunaPraktyk;          //ID of Tutor<br>
 Integer idKoordynatoraPraktyk;      //ID of Coordinator<br>
 Integer idAdresu;                   //ID of Employer's Address<br>
 Integer idPorozumienia;             //ID of Practice's Agreement<br>
 Timestamp dataZapisu;               //Timestamp of Taking Practice by Student<br>
 int ocena;                          //Student's Grade from Practice<br>
 String skierowanie;                 //String containing URL to Practice's Referral<br>
 String kartaOceny;                  //String containing URL to Practice's Grade Chart<br><br>
RELATIONS:<br>
 PraktykiKierunkoweEntity praktyka;  //M:1 relation with CoursePracticesEntity<br>
 StudenciKierunkowEntity student;    //1:1 relation with CourseStudentsEntity<br>
 KoordynatorzyKierunkowiPraktykodawcowEntity koordynator;    //M:1 relation with EmployersCourseCoordinatorsEntity<br>
 AdresyPraktykodawcyEntity adresPraktykodawcy;   //M:1 relation with EmployersAddressesEntity<br>
 PraktykodawcyEntity praktykodawca;  //M:1   relation with EmployersEntity<br>
 OpiekunowieKierunkowiEntity opiekun;    //M:1 relation with CourseTutorsEntity<br>
 StatusyEntity status;   //M:1 relation with StatusEntity<br>
 PorozumieniaEntity porozumienie;    //1:1 relation with AgreementsEntity<br>
 SzablonyPraktykEntity szablon;  //M:1 relation with TemplatesEntity<br>
 */
@Entity
@Table(name = "praktyki", schema = "public", catalog = "praktykidb")
public class PraktykiEntity {
    private Long idPraktykiStudenckiej;         //ID of Practice
    private int idSzablonu;                     //ID of Template
    private Integer idPraktykodawcy;            //ID of Employer
    private int idStatusu;                      //ID of Status
    private Date dataRozpoczecia;               //Beginning Date of Practice
    private Date dataZakonczenia;               //End Date of Practice
    private int idKierunku;                     //ID of Course
    private int idTypuPraktyki;                 //ID of Practice's Type
    private int idRokuAkademickiego;            //ID of Academic Year
    private Long nrAlbumu;                      //(ID)Student Number
    private Integer idOpiekunaPraktyk;          //ID of Tutor
    private Integer idKoordynatoraPraktyk;      //ID of Coordinator
    private Integer idAdresu;                   //ID of Employer's Address
    private Integer idPorozumienia;             //ID of Practice's Agreement
    private Timestamp dataZapisu;               //Timestamp of Taking Practice by Student
    private int ocena;                          //Student's Grade from Practice
    private String skierowanie;                 //String containing URL to Practice's Referral
    private String kartaOceny;                  //String containing URL to Practice's Grade Chart

    private PraktykiKierunkoweEntity praktyka;  //M:1 relation with CoursePracticesEntity
    private StudenciKierunkowEntity student;    //1:1 relation with CourseStudentsEntity
    private KoordynatorzyKierunkowiPraktykodawcowEntity koordynator;    //M:1 relation with EmployersCourseCoordinatorsEntity
    private AdresyPraktykodawcyEntity adresPraktykodawcy;   //M:1 relation with EmployersAddressesEntity
    private PraktykodawcyEntity praktykodawca;  //M:1   relation with EmployersEntity
    private OpiekunowieKierunkowiEntity opiekun;    //M:1 relation with CourseTutorsEntity
    private StatusyEntity status;   //M:1 relation with StatusEntity
    private PorozumieniaEntity porozumienie;    //1:1 relation with AgreementsEntity
    private SzablonyPraktykEntity szablon;  //M:1 relation with TemplatesEntity

    /*
    ATTRIBUTES
    */

    //ID of Practice
    @Id
    @GeneratedValue
    @Column(name = "id_praktyki_studenckiej", nullable = false)
    public Long getIdPraktykiStudenckiej() {
        return idPraktykiStudenckiej;
    }

    public void setIdPraktykiStudenckiej(Long idPraktykiStudenckiej) {
        this.idPraktykiStudenckiej = idPraktykiStudenckiej;
    }

    //ID of Template
    @Basic
    @Column(name = "id_szablonu", nullable = false)
    public int getIdSzablonu() {
        return idSzablonu;
    }

    public void setIdSzablonu(int idSzablonu) {
        this.idSzablonu = idSzablonu;
    }

    //ID of Employer
    @Basic
    @Column(name = "id_praktykodawcy", nullable = false)
    public Integer getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(Integer idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //ID of Status
    @Basic
    @Column(name = "id_statusu", nullable = false)
    public int getIdStatusu() {
        return idStatusu;
    }

    public void setIdStatusu(int idStatusu) {
        this.idStatusu = idStatusu;
    }

    //Beginning Date of Practice
    @Basic
    @Column(name = "data_rozpoczecia", nullable = false)
    public Date getDataRozpoczecia() {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(Date dataRozpoczecia) {
        this.dataRozpoczecia = dataRozpoczecia;
    }

    //End Date of Practice
    @Basic
    @Column(name = "data_zakonczenia", nullable = false)
    public Date getDataZakonczenia() {
        return dataZakonczenia;
    }

    public void setDataZakonczenia(Date dataZakonczenia) {
        this.dataZakonczenia = dataZakonczenia;
    }

    //ID of Course
    @Basic
    @Column(name = "id_kierunku", nullable = false)
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Practice's Type
    @Basic
    @Column(name = "id_typu_praktyki", nullable = false)
    public int getIdTypuPraktyki() {
        return idTypuPraktyki;
    }

    public void setIdTypuPraktyki(int idTypuPraktyki) {
        this.idTypuPraktyki = idTypuPraktyki;
    }

    //ID of Academic Year
    @Basic
    @Column(name = "id_roku_akademickiego", nullable = false)
    public int getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(int idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    //(ID) Student Number
    @Basic
    @Column(name = "nr_albumu")
    public Long getNrAlbumu() {
        return nrAlbumu;
    }

    public void setNrAlbumu(Long nrAlbumu) {
        this.nrAlbumu = nrAlbumu;
    }

    //ID of Tutor
    @Basic
    @Column(name = "id_opiekuna_praktyk", nullable = false)
    public Integer getIdOpiekunaPraktyk() {
        return idOpiekunaPraktyk;
    }

    public void setIdOpiekunaPraktyk(Integer idOpiekunaPraktyk) {
        this.idOpiekunaPraktyk = idOpiekunaPraktyk;
    }

    //ID of Coordinator
    @Basic
    @Column(name = "id_koordynatora_praktyk", nullable = false)
    public Integer getIdKoordynatoraPraktyk() {
        return idKoordynatoraPraktyk;
    }

    public void setIdKoordynatoraPraktyk(Integer idKoordynatoraPraktyk) {
        this.idKoordynatoraPraktyk = idKoordynatoraPraktyk;
    }

    //ID of Employer's Address
    @Basic
    @Column(name = "id_adresu", nullable = false)
    public Integer getIdAdresu() {
        return idAdresu;
    }

    public void setIdAdresu(Integer idAdresu) {
        this.idAdresu = idAdresu;
    }

    //ID of Practice's Agreement
    @Basic
    @Column(name = "id_porozumienia")
    public Integer getIdPorozumienia() {
        return idPorozumienia;
    }

    public void setIdPorozumienia(Integer idPorozumienia) {
        this.idPorozumienia = idPorozumienia;
    }

    //Timestamp of Taking Practice by Student
    @Basic
    @Column(name = "data_zapisu")
    public Timestamp getDataZapisu() {
        return dataZapisu;
    }

    public void setDataZapisu(Timestamp dataZapisu) {
        this.dataZapisu = dataZapisu;
    }

    //Student's Grade from Practice
    @Basic
    @Column(name = "ocena")
    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    //String containing URL to Practice's Referral
    @Basic
    @Column(name = "skierowanie")
    public String getSkierowanie() {
        return skierowanie;
    }

    public void setSkierowanie(String skierowanie) {
        this.skierowanie = skierowanie;
    }

    //String containing URL to Practice's Grade Chart
    @Basic
    @Column(name = "karta_oceny")
    public String getKartaOceny() {
        return kartaOceny;
    }

    public void setKartaOceny(String kartaOceny) {
        this.kartaOceny = kartaOceny;
    }

    /*
    RELATIONS
    */

    //M:1 relation with CoursePracticesEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false), @JoinColumn(name = "id_typu_praktyki", referencedColumnName = "id_typu_praktyki", insertable = false, updatable = false), @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)})
    public PraktykiKierunkoweEntity getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(PraktykiKierunkoweEntity praktyka) {
        this.praktyka = praktyka;
    }

    //1:1 relation with CourseStudentsEntity
    @JsonIgnore
    @OneToOne
    @JoinColumns({@JoinColumn(name = "nr_albumu", referencedColumnName = "nr_albumu", insertable = false, updatable = false), @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false), @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)})
    public StudenciKierunkowEntity getStudent() {
        return student;
    }

    public void setStudent(StudenciKierunkowEntity student) {
        this.student = student;
    }

    //M:1 relation with EmployersCourseCoordinatorsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false), @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false), @JoinColumn(name = "id_koordynatora_praktyk", referencedColumnName = "id_koordynatora_praktyk", insertable = false, updatable = false)})
    public KoordynatorzyKierunkowiPraktykodawcowEntity getKoordynator() {
        return koordynator;
    }

    public void setKoordynator(KoordynatorzyKierunkowiPraktykodawcowEntity koordynator) {
        this.koordynator = koordynator;
    }

    //M:1 relation with EmployersAddressesEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "id_adresu", referencedColumnName = "id_adresu", insertable = false, updatable = false), @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)})
    public AdresyPraktykodawcyEntity getAdresPraktykodawcy() {
        return adresPraktykodawcy;
    }

    public void setAdresPraktykodawcy(AdresyPraktykodawcyEntity adresPraktykodawcy) {
        this.adresPraktykodawcy = adresPraktykodawcy;
    }

    //M:1   relation with EmployersEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    //M:1 relation with CourseTutorsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false), @JoinColumn(name = "id_opiekuna_praktyk", referencedColumnName = "id_opiekuna_praktyk", insertable = false, updatable = false), @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)})
    public OpiekunowieKierunkowiEntity getOpiekun() {
        return opiekun;
    }

    public void setOpiekun(OpiekunowieKierunkowiEntity opiekun) {
        this.opiekun = opiekun;
    }

    //M:1 relation with StatusEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_statusu", referencedColumnName = "id_statusu", insertable = false, updatable = false)
    public StatusyEntity getStatus() {
        return status;
    }

    public void setStatus(StatusyEntity status) {
        this.status = status;
    }

    //1:1 relation with AgreementsEntity
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "id_porozumienia", referencedColumnName = "id_porozumienia", insertable = false, updatable = false)
    public PorozumieniaEntity getPorozumienie() {
        return porozumienie;
    }

    public void setPorozumienie(PorozumieniaEntity porozumienie) {
        this.porozumienie = porozumienie;
    }

    //M:1 relation with TemplatesEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_szablonu", referencedColumnName = "id_szablonu", insertable = false, updatable = false)
    public SzablonyPraktykEntity getSzablon() {
        return szablon;
    }

    public void setSzablon(SzablonyPraktykEntity szablon) {
        this.szablon = szablon;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PraktykiEntity)) return false;

        PraktykiEntity that = (PraktykiEntity) o;

        if (idKierunku != that.idKierunku) return false;
        if (idRokuAkademickiego != that.idRokuAkademickiego) return false;
        if (idStatusu != that.idStatusu) return false;
        if (idSzablonu != that.idSzablonu) return false;
        if (idTypuPraktyki != that.idTypuPraktyki) return false;
        if (ocena != that.ocena) return false;
        if (!dataRozpoczecia.equals(that.dataRozpoczecia)) return false;
        if (!dataZakonczenia.equals(that.dataZakonczenia)) return false;
        if (dataZapisu != null ? !dataZapisu.equals(that.dataZapisu) : that.dataZapisu != null) return false;
        if (!idAdresu.equals(that.idAdresu)) return false;
        if (!idKoordynatoraPraktyk.equals(that.idKoordynatoraPraktyk)) return false;
        if (!idOpiekunaPraktyk.equals(that.idOpiekunaPraktyk)) return false;
        if (idPorozumienia != null ? !idPorozumienia.equals(that.idPorozumienia) : that.idPorozumienia != null)
            return false;
        if (!idPraktykiStudenckiej.equals(that.idPraktykiStudenckiej)) return false;
        if (!idPraktykodawcy.equals(that.idPraktykodawcy)) return false;
        if (kartaOceny != null ? !kartaOceny.equals(that.kartaOceny) : that.kartaOceny != null) return false;
        if (nrAlbumu != null ? !nrAlbumu.equals(that.nrAlbumu) : that.nrAlbumu != null) return false;
        if (skierowanie != null ? !skierowanie.equals(that.skierowanie) : that.skierowanie != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPraktykiStudenckiej.hashCode();
        result = 31 * result + idSzablonu;
        result = 31 * result + idPraktykodawcy.hashCode();
        result = 31 * result + idStatusu;
        result = 31 * result + dataRozpoczecia.hashCode();
        result = 31 * result + dataZakonczenia.hashCode();
        result = 31 * result + idKierunku;
        result = 31 * result + idTypuPraktyki;
        result = 31 * result + idRokuAkademickiego;
        result = 31 * result + (nrAlbumu != null ? nrAlbumu.hashCode() : 0);
        result = 31 * result + idOpiekunaPraktyk.hashCode();
        result = 31 * result + idKoordynatoraPraktyk.hashCode();
        result = 31 * result + idAdresu.hashCode();
        result = 31 * result + (idPorozumienia != null ? idPorozumienia.hashCode() : 0);
        result = 31 * result + (dataZapisu != null ? dataZapisu.hashCode() : 0);
        result = 31 * result + ocena;
        result = 31 * result + (skierowanie != null ? skierowanie.hashCode() : 0);
        result = 31 * result + (kartaOceny != null ? kartaOceny.hashCode() : 0);
        return result;
    }
}
