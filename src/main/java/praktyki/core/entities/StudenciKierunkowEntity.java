package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by dawid on 08.12.14.
 */
/**
CourseStudentsEntity<br>
JOINT TABLE BETWEEN FIELD OF STUDY (COURSE) AND ACADEMIC YEAR<br><br>
 ATTRIBUTES:<br>
 Long nrAlbumu;              //(ID) Student Number<br>
 int idKierunku;             //ID of Course<br>
 int idRokuAkademickiego;    //ID of Academic Year<br><br>
 RELATIONS:<br>
 RocznikiStudiowEntity kierunek; //M:1 relation with CourseYearbookEntity<br>
 StudenciEntity studenciByNrAlbumu; //M:1 relation with StudentsEntity<br>
 ZgloszeniaPraktykodawcowEntity zgloszenie;  //1:1 relation with EmployerApplicationsEntity<br>
 PraktykiEntity praktyka;    //1:1 relation with PracticesEntity<br>
 */
@Entity
@Table(name = "studenci_kierunkow", schema = "public", catalog = "praktykidb")
@IdClass(StudenciKierunkowEntityPK.class)
public class StudenciKierunkowEntity {
    private Long nrAlbumu;              //(ID) Student Number
    private int idKierunku;             //ID of Course
    private int idRokuAkademickiego;    //ID of Academic Year

    private RocznikiStudiowEntity kierunek; //M:1 relation with CourseYearbookEntity
    private StudenciEntity studenciByNrAlbumu; //M:1 relation with StudentsEntity
    private ZgloszeniaPraktykodawcowEntity zgloszenie;  //1:1 relation with EmployerApplicationsEntity
    private PraktykiEntity praktyka;    //1:1 relation with PracticesEntity

    /*
    ATTRIBUTES
    */

    //(ID) Student Number
    @Id
    @GenericGenerator(name="assigned", strategy = "assigned")
    @Column(name = "nr_albumu", nullable = false)
    public Long getNrAlbumu() {
        return nrAlbumu;
    }

    public void setNrAlbumu(Long nrAlbumu) {
        this.nrAlbumu = nrAlbumu;
    }

    //ID of Course
    @Id
    @GenericGenerator(name="assigned", strategy = "assigned")
    @Column(name = "id_kierunku", nullable = false)
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Academic Year
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_roku_akademickiego", nullable = false)
    public int getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(int idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    /*
    RELATIONS
    */

    //M:1 relation with CourseYearbookEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false), @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)})
    public RocznikiStudiowEntity getKierunek() {
        return kierunek;
    }

    public void setKierunek(RocznikiStudiowEntity kierunek) {
        this.kierunek = kierunek;
    }

    //M:1 relation with StudentsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "nr_albumu", referencedColumnName = "nr_albumu", insertable = false , updatable = false)
    public StudenciEntity getStudenciByNrAlbumu() {
        return studenciByNrAlbumu;
    }

    public void setStudenciByNrAlbumu(StudenciEntity studenciByNrAlbumu) {
        this.studenciByNrAlbumu = studenciByNrAlbumu;
    }

    //1:1 relation with EmployerApplicationsEntity
    @JsonIgnore
    @OneToOne(mappedBy = "studentKierunku", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public ZgloszeniaPraktykodawcowEntity getZgloszenie() {
        return zgloszenie;
    }

    public void setZgloszenie(ZgloszeniaPraktykodawcowEntity zgloszenie) {
        this.zgloszenie = zgloszenie;
    }

    //1:1 relation with PracticesEntity
    @JsonIgnore
    @OneToOne(mappedBy = "student", fetch = FetchType.LAZY)
    public PraktykiEntity getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(PraktykiEntity praktyka) {
        this.praktyka = praktyka;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudenciKierunkowEntity)) return false;

        StudenciKierunkowEntity that = (StudenciKierunkowEntity) o;

        if (idKierunku != that.idKierunku) return false;
        if (idRokuAkademickiego != that.idRokuAkademickiego) return false;
        if (!nrAlbumu.equals(that.nrAlbumu)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nrAlbumu.hashCode();
        result = 31 * result + idKierunku;
        result = 31 * result + idRokuAkademickiego;
        return result;
    }
}
