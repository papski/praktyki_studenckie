package praktyki.core.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by dawid on 28.02.15.
 */
/**
PRIMARY KEYS FOR RequiredQualificationsEntity ("WymaganeKwalifikacjeEntity")
 */
public class WymaganeKwalifikacjeEntityPK implements Serializable {
    private int idSzablonu;         //ID of Template
    private int idKwalifikacji;     //ID of Qualification

    //ID of Template
    @Id
    @Column(name = "id_szablonu")
    public int getIdSzablonu() {
        return idSzablonu;
    }

    public void setIdSzablonu(int idSzablonu) {
        this.idSzablonu = idSzablonu;
    }

    //ID of Qualification
    @Id
    @Column(name = "id_kwalifikacji")
    public int getIdKwalifikacji() {
        return idKwalifikacji;
    }

    public void setIdKwalifikacji(int idKwalifikacji) {
        this.idKwalifikacji = idKwalifikacji;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WymaganeKwalifikacjeEntityPK)) return false;

        WymaganeKwalifikacjeEntityPK that = (WymaganeKwalifikacjeEntityPK) o;

        if (idKwalifikacji != that.idKwalifikacji) return false;
        if (idSzablonu != that.idSzablonu) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idSzablonu;
        result = 31 * result + idKwalifikacji;
        return result;
    }
}
