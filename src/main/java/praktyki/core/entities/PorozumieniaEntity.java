package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by dawid on 17.02.15.
 */
/**
AgreementsEntity<br>
ENTITY CONTAINING AGREEMENTS OF PRACTICES<br><br>
ATTRIBUTES:<br>
 Integer idPorozumienia; //ID of Agreement<br>
 String nrPorozumienia;  //Number of Agreement<br>
 Date dataZawarcia;      //Date of Conclusion<br>
 String porozumienie;    //Name of Agreement<br><br>
RELATIONS:<br>
 PraktykiEntity praktyka;    //1:1 relation with PracticesEntity<br>
 */
@Entity
@Table(name = "porozumienia", schema = "public", catalog = "praktykidb")
public class PorozumieniaEntity {
    private Integer idPorozumienia; //ID of Agreement
    private String nrPorozumienia;  //Number of Agreement
    private Date dataZawarcia;      //Date of Conclusion
    private String porozumienie;    //Name of Agreement

    private PraktykiEntity praktyka;    //1:1 relation with PracticesEntity

    /*
    ATTRIBUTES
    */

    //ID of Agreement
    @Id
    @GeneratedValue
    @Column(name = "id_porozumienia", nullable = false)
    public Integer getIdPorozumienia() {
        return idPorozumienia;
    }

    public void setIdPorozumienia(Integer idPorozumienia) {
        this.idPorozumienia = idPorozumienia;
    }

    //Number of Agreement
    @Basic
    @Column(name = "nr_porozumienia", nullable = false)
    public String getNrPorozumienia() {
        return nrPorozumienia;
    }

    public void setNrPorozumienia(String nrPorozumienia) {
        this.nrPorozumienia = nrPorozumienia;
    }

    //Date of Conclusion
    @Basic
    @Column(name = "data_zawarcia")
    public Date getDataZawarcia() {
        return dataZawarcia;
    }

    public void setDataZawarcia(Date dataZawarcia) {
        this.dataZawarcia = dataZawarcia;
    }

    //Name of Agreement
    @Basic
    @Column(name = "porozumienie")
    public String getPorozumienie() {
        return porozumienie;
    }

    public void setPorozumienie(String porozumienie) {
        this.porozumienie = porozumienie;
    }

    /*
    RELATIONS
     */

    //1:1 relation with PracticesEntity
    @JsonIgnore
    @OneToOne(mappedBy = "porozumienie", fetch = FetchType.LAZY)
    public PraktykiEntity getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(PraktykiEntity praktyka) {
        this.praktyka = praktyka;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PorozumieniaEntity)) return false;

        PorozumieniaEntity that = (PorozumieniaEntity) o;

        if (dataZawarcia != null ? !dataZawarcia.equals(that.dataZawarcia) : that.dataZawarcia != null) return false;
        if (!idPorozumienia.equals(that.idPorozumienia)) return false;
        if (!nrPorozumienia.equals(that.nrPorozumienia)) return false;
        if (porozumienie != null ? !porozumienie.equals(that.porozumienie) : that.porozumienie != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPorozumienia.hashCode();
        result = 31 * result + nrPorozumienia.hashCode();
        result = 31 * result + (dataZawarcia != null ? dataZawarcia.hashCode() : 0);
        result = 31 * result + (porozumienie != null ? porozumienie.hashCode() : 0);
        return result;
    }
}
