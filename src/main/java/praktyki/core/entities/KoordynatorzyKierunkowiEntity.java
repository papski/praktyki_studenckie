package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

/**
 * Created by dawid on 18.01.15.
 */
/**
CourseCoordinatorsEntity<br>
JOINT TABLE BETWEEN COORDINATORS AND FIELD OF STUDY (COURSE)<br><br>
ATTRIBUTES:<br>
 int idKierunku;             //ID of Course<br>
 int idKoordynatoraPraktyk;  //ID of Coordinator<br>
 Date DataPowolania;         //Coordinator's Appointed Date<br><br>
RELATIONS:<br>
 KierunkiStudiowEntity kierunekByIdKoordynatora; //M:1 relation with FieldOfStudyEntity<br>
 KoordynatorzyPraktykEntity koordynatorzyByIdKierunku;   //M:1 relation with  CourseCoordinatorsEntity<br>
 Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> praktykodawcaByKoordynator; //1:M relation with EmployersCourseCoordinatorsEntity<br>
 */
@Entity
@Table(name = "koordynatorzy_kierunkowi", schema = "public", catalog = "praktykidb")
@IdClass(KoordynatorzyKierunkowiEntityPK.class)
public class KoordynatorzyKierunkowiEntity {
    private int idKierunku;             //ID of Course
    private int idKoordynatoraPraktyk;  //ID of Coordinator
    private Date DataPowolania;         //Coordinator's Appointed Date

    private KierunkiStudiowEntity kierunekByIdKoordynatora; //M:1 relation with FieldOfStudyEntity
    private KoordynatorzyPraktykEntity koordynatorzyByIdKierunku;   //M:1 relation with  CourseCoordinatorsEntity
    private Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> praktykodawcaByKoordynator; //1:M relation with EmployersCourseCoordinatorsEntity

    /*
    ATTRIBUTES
     */

    //ID of Course
    @Id
    @GenericGenerator(name="assigned", strategy = "assigned")
    @Column(name = "id_kierunku")
    public int getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(int idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Coordinator
    @Id
    @GenericGenerator(name="assigned", strategy = "assigned")
    @Column(name = "id_koordynatora_praktyk")
    public int getIdKoordynatoraPraktyk() {
        return idKoordynatoraPraktyk;
    }

    public void setIdKoordynatoraPraktyk(int idKoordynatoraPraktyk) {
        this.idKoordynatoraPraktyk = idKoordynatoraPraktyk;
    }

    //Coordinator's Appointed Date
    @Basic
    @Column(name = "data_powolania")
    public Date getDataPowolania() {
        return DataPowolania;
    }

    public void setDataPowolania(Date dataPowolania) {
        DataPowolania = dataPowolania;
    }

    /*
    RELATIONS
    */

    //M:1 relation with FieldOfStudyEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
    public KierunkiStudiowEntity getKierunekByIdKoordynatora() {
        return kierunekByIdKoordynatora;
    }

    public void setKierunekByIdKoordynatora(KierunkiStudiowEntity kierunekByIdKoordynatora) {
        this.kierunekByIdKoordynatora = kierunekByIdKoordynatora;
    }

    //M:1 relation with  CourseCoordinatorsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_koordynatora_praktyk", referencedColumnName = "id_koordynatora_praktyk", insertable = false, updatable = false)
    public KoordynatorzyPraktykEntity getKoordynatorzyByIdKierunku() {
        return koordynatorzyByIdKierunku;
    }

    public void setKoordynatorzyByIdKierunku(KoordynatorzyPraktykEntity koordynatorzyByIdKierunku) {
        this.koordynatorzyByIdKierunku = koordynatorzyByIdKierunku;
    }

    //1:M relation with EmployersCourseCoordinatorsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "koordynatorKierunku", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> getPraktykodawcaByKoordynator() {
        return praktykodawcaByKoordynator;
    }

    public void setPraktykodawcaByKoordynator(Collection<KoordynatorzyKierunkowiPraktykodawcowEntity> praktykodawcaByKoordynator) {
        this.praktykodawcaByKoordynator = praktykodawcaByKoordynator;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KoordynatorzyKierunkowiEntity)) return false;

        KoordynatorzyKierunkowiEntity that = (KoordynatorzyKierunkowiEntity) o;

        if (idKierunku != that.idKierunku) return false;
        if (idKoordynatoraPraktyk != that.idKoordynatoraPraktyk) return false;
        if (DataPowolania != null ? !DataPowolania.equals(that.DataPowolania) : that.DataPowolania != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku;
        result = 31 * result + idKoordynatoraPraktyk;
        result = 31 * result + (DataPowolania != null ? DataPowolania.hashCode() : 0);
        return result;
    }
}
