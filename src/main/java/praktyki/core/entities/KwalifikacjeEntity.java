package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 28.02.15.
 */
/**
QualificationsEntity<br>
ENTITY CONTAINING QUALIFICATIONS OF PRACTICE'S TEMPLATE<br><br>
 ATTRIBUTES:<br>
 Integer idKwalifikacji; //ID of Qualification<br>
 String kwalifikacja;    //name of Qualification<br><br>
 RELATIONS:<br>
 Collection<WymaganeKwalifikacjeEntity> szablonyByIdKwalifikacjiWymaganej;   //1:M relation with RequiredQualificationsEntity<br>
 Collection<OferowaneKwalifikacjeEntity> szablonyByIdKwalifikacjiOferowanej; //1:M relation with OfferedQualificationsEntity<br>
 Collection<KategorieKwalifikacjiEntity> kwalifikacje;   //1:M relation with QualificationsCategoriesEntity<br>
 */
@Entity
@Table(name = "kwalifikacje", schema = "public", catalog = "praktykidb")
public class KwalifikacjeEntity {
    private Integer idKwalifikacji; //ID of Qualification
    private String kwalifikacja;    //name of Qualification

    private Collection<WymaganeKwalifikacjeEntity> szablonyByIdKwalifikacjiWymaganej;   //1:M relation with RequiredQualificationsEntity
    private Collection<OferowaneKwalifikacjeEntity> szablonyByIdKwalifikacjiOferowanej; //1:M relation with OfferedQualificationsEntity
    private Collection<KategorieKwalifikacjiEntity> kwalifikacje;   //1:M relation with QualificationsCategoriesEntity

    /*
    ATTRIBUTES
    */

    @Id
    @GeneratedValue
    @Column(name = "id_kwalifikacji")
    public Integer getIdKwalifikacji() {
        return idKwalifikacji;
    }

    public void setIdKwalifikacji(Integer idKwalifikacji) {
        this.idKwalifikacji = idKwalifikacji;
    }

    @Basic
    @Column(name = "kwalifikacja")
    public String getKwalifikacja() {
        return kwalifikacja;
    }

    public void setKwalifikacja(String kwalifikacja) {
        this.kwalifikacja = kwalifikacja;
    }

    /*
    RELATIONS
     */

    //1:M relation with RequiredQualificationsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "kwalifikacja", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<WymaganeKwalifikacjeEntity> getSzablonyByIdKwalifikacjiWymaganej() {
        return szablonyByIdKwalifikacjiWymaganej;
    }

    public void setSzablonyByIdKwalifikacjiWymaganej(Collection<WymaganeKwalifikacjeEntity> szablonyByIdKwalifikacjiWymaganej) {
        this.szablonyByIdKwalifikacjiWymaganej = szablonyByIdKwalifikacjiWymaganej;
    }

    //1:M relation with OfferedQualificationsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "kwalifikacja", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<OferowaneKwalifikacjeEntity> getSzablonyByIdKwalifikacjiOferowanej() {
        return szablonyByIdKwalifikacjiOferowanej;
    }

    public void setSzablonyByIdKwalifikacjiOferowanej(Collection<OferowaneKwalifikacjeEntity> szablonyByIdKwalifikacjiOferowanej) {
        this.szablonyByIdKwalifikacjiOferowanej = szablonyByIdKwalifikacjiOferowanej;
    }

    //1:M relation with QualificationsCategoriesEntity
    @OneToMany(mappedBy = "kwalifikacja" , cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KategorieKwalifikacjiEntity> getKwalifikacje() {
        return kwalifikacje;
    }

    public void setKwalifikacje(Collection<KategorieKwalifikacjiEntity> kwalifikacje) {
        this.kwalifikacje = kwalifikacje;
    }

    /*
    EQUALS AND HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KwalifikacjeEntity)) return false;

        KwalifikacjeEntity that = (KwalifikacjeEntity) o;

        if (!idKwalifikacji.equals(that.idKwalifikacji)) return false;
        if (!kwalifikacja.equals(that.kwalifikacja)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKwalifikacji.hashCode();
        result = 31 * result + kwalifikacja.hashCode();
        return result;
    }
}
