package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 15.02.15.
 */
/**
StatusEntity<br>
ENTITY CONTAINING STATUS OF PRACTICE<br><br>
 ATTRIBUTES:<br>
 int idStatusu;      //ID of Status<br>
 String status;      //Name<br><br>
 RELATIONS:<br>
 Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity<br>
 */
@Entity
@Table(name = "statusy", schema = "public", catalog = "praktykidb")
public class StatusyEntity {
    private int idStatusu;      //ID of Status
    private String status;      //Name

    private Collection<PraktykiEntity> praktyka;    //1:M relation with PracticesEntity

    /*
    ATTRIBUTES
    */

    //ID of Status
    @Id
    @GeneratedValue
    @Column(name = "id_statusu", nullable = false)
    public int getIdStatusu() {
        return idStatusu;
    }

    public void setIdStatusu(int idStatusu) {
        this.idStatusu = idStatusu;
    }

    //Name
    @Basic
    @Column(name = "status", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /*
    RELATIONS
    */

    //1:M relation with PracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "status")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiEntity> getPraktyka() {
        return praktyka;
    }

    public void setPraktyka(Collection<PraktykiEntity> praktyka) {
        this.praktyka = praktyka;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StatusyEntity)) return false;

        StatusyEntity that = (StatusyEntity) o;

        if (idStatusu != that.idStatusu) return false;
        if (!status.equals(that.status)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStatusu;
        result = 31 * result + status.hashCode();
        return result;
    }
}
