package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by dawid on 25.01.15.
 */
/**
EmployersProfilesEntity<br>
JOINT TABLE BETWEEN EMPLOYER AND EMPLOYER's PROFILE<br><br>
 ATTRIBUTES:<br>
 Integer idPraktykodawcy;    //ID of Employer<br>
 Integer idProfilu;          //ID of Profile<br><br>
 RELATIONS:<br>
 ProfilEntity profil;    //M:1 relation with ProfilesEntity<br>
 PraktykodawcyEntity praktykodawca;  //M:1 relation with EmployersEntity<br>
 */
@Entity
@Table(name = "profile_praktykodawcow", schema = "public", catalog = "praktykidb")
@IdClass(ProfilePraktykodawcowEntityPK.class)
public class ProfilePraktykodawcowEntity {
    private Integer idPraktykodawcy;    //ID of Employer
    private Integer idProfilu;          //ID of Profile

    private ProfilEntity profil;    //M:1 relation with ProfilesEntity
    private PraktykodawcyEntity praktykodawca;  //M:1 relation with EmployersEntity

    /*
    ATTRIBUTES
    */

    //ID of Employer
    @Id
    @Column(name = "id_praktykodawcy")
    public Integer getIdPraktykodawcy() {
        return idPraktykodawcy;
    }

    public void setIdPraktykodawcy(Integer idPraktykodawcy) {
        this.idPraktykodawcy = idPraktykodawcy;
    }

    //ID of Profile
    @Id
    @Column(name = "id_profilu")
    public Integer getIdProfilu() {
        return idProfilu;
    }

    public void setIdProfilu(Integer idProfilu) {
        this.idProfilu = idProfilu;
    }

    /*
    RELATIONS
    */

    //M:1 relation with ProfilesEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_profilu", referencedColumnName = "id_profilu", insertable = false, updatable = false)
    public ProfilEntity getProfil() {
        return profil;
    }

    public void setProfil(ProfilEntity profil) {
        this.profil = profil;
    }

    //M:1 relation with EmployersEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_praktykodawcy", referencedColumnName = "id_praktykodawcy", insertable = false, updatable = false)
    public PraktykodawcyEntity getPraktykodawca() {
        return praktykodawca;
    }

    public void setPraktykodawca(PraktykodawcyEntity praktykodawca) {
        this.praktykodawca = praktykodawca;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfilePraktykodawcowEntity)) return false;

        ProfilePraktykodawcowEntity that = (ProfilePraktykodawcowEntity) o;

        if (idPraktykodawcy != null ? !idPraktykodawcy.equals(that.idPraktykodawcy) : that.idPraktykodawcy != null)
            return false;
        if (idProfilu != null ? !idProfilu.equals(that.idProfilu) : that.idProfilu != null) return false;
        if (praktykodawca != null ? !praktykodawca.equals(that.praktykodawca) : that.praktykodawca != null)
            return false;
        if (profil != null ? !profil.equals(that.profil) : that.profil != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPraktykodawcy != null ? idPraktykodawcy.hashCode() : 0;
        result = 31 * result + (idProfilu != null ? idProfilu.hashCode() : 0);
        result = 31 * result + (profil != null ? profil.hashCode() : 0);
        result = 31 * result + (praktykodawca != null ? praktykodawca.hashCode() : 0);
        return result;
    }
}
