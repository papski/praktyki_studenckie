package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 04.02.15.
 */
/**
ApplicationTypesEntity<br>
ENTITY CONTAINING TYPES OF EMPLOYER's APPLICATIONS<br><br>
 ATTRIBUTES:<br>
 Integer idTypuZgloszenia;   //ID of Type<br>
 String typZgloszenia;       //Name<br><br>
 RELATIONS:<br>
 Collection<ZgloszeniaPraktykodawcowEntity> zgloszenie;  //1:M relation with EmployerApplicationsEntity<br>
 */
@Entity
@Table(name = "typy_zgloszen", schema = "public", catalog = "praktykidb")
public class TypyZgloszenEntity {
    private Integer idTypuZgloszenia;   //ID of Type
    private String typZgloszenia;       //Name

    private Collection<ZgloszeniaPraktykodawcowEntity> zgloszenie;  //1:M relation with EmployerApplicationsEntity

    /*
    ATTRIBUTES
    */

    //ID of Type
    @Id
    @GeneratedValue
    @Column(name = "id_typu_zgloszenia", nullable = false)
    public Integer getIdTypuZgloszenia() {
        return idTypuZgloszenia;
    }

    public void setIdTypuZgloszenia(Integer idTypuZgloszenia) {
        this.idTypuZgloszenia = idTypuZgloszenia;
    }

    //Name
    @Basic
    @Column(name = "typ_zgloszenia", nullable = false)
    public String getTypZgloszenia() {
        return typZgloszenia;
    }

    public void setTypZgloszenia(String typZgloszenia) {
        this.typZgloszenia = typZgloszenia;
    }

    /*
    RELATIONS
    */

    //1:M relation with EmployerApplicationsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "typZgloszenia")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<ZgloszeniaPraktykodawcowEntity> getZgloszenie() {
        return zgloszenie;
    }

    public void setZgloszenie(Collection<ZgloszeniaPraktykodawcowEntity> zgloszenie) {
        this.zgloszenie = zgloszenie;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TypyZgloszenEntity)) return false;

        TypyZgloszenEntity that = (TypyZgloszenEntity) o;

        if (!idTypuZgloszenia.equals(that.idTypuZgloszenia)) return false;
        if (!typZgloszenia.equals(that.typZgloszenia)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTypuZgloszenia.hashCode();
        result = 31 * result + typZgloszenia.hashCode();
        return result;
    }
}
