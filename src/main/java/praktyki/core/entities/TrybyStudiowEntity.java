package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 14.02.15.
 */
/**
CourseModeEntity<br>
ENTITY CONTAINING MODES OF COURSE (Full Time Studies or External)<br><br>
 ATTRIBUTES:<br>
 int idTrybuStudiow;     //ID of Mode<br>
 String trybStudiow;     //Name<br><br>
 RELATIONS:<br>
 Collection<KierunkiStudiowEntity> kierunkiByIdTrybu;    //1:M relation with FieldOfStudiesEntity<br>
 */
@Entity
@Table(name = "tryby_studiow", schema = "public", catalog = "praktykidb")
public class TrybyStudiowEntity {
    private int idTrybuStudiow;     //ID of Mode
    private String trybStudiow;     //Name

    private Collection<KierunkiStudiowEntity> kierunkiByIdTrybu;    //1:M relation with FieldOfStudiesEntity

    /*
    ATTRIBUTES
    */

    //ID of Mode
    @JsonIgnore
    @Id
    @GeneratedValue
    @Column(name = "id_trybu_studiow")
    public int getIdTrybuStudiow() {
        return idTrybuStudiow;
    }

    public void setIdTrybuStudiow(int idTrybuStudiow) {
        this.idTrybuStudiow = idTrybuStudiow;
    }

    //Name
    @Basic
    @Column(name = "tryb_studiow", nullable = false)
    public String getTrybStudiow() {
        return trybStudiow;
    }

    public void setTrybStudiow(String trybStudiow) {
        this.trybStudiow = trybStudiow;
    }

    /*
    RELATIONS
    */

    //1:M relation with FieldOfStudiesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "tryb")
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<KierunkiStudiowEntity> getKierunkiByIdTrybu() {
        return kierunkiByIdTrybu;
    }

    public void setKierunkiByIdTrybu(Collection<KierunkiStudiowEntity> kierunkiByIdTrybu) {
        this.kierunkiByIdTrybu = kierunkiByIdTrybu;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrybyStudiowEntity)) return false;

        TrybyStudiowEntity that = (TrybyStudiowEntity) o;

        if (idTrybuStudiow != that.idTrybuStudiow) return false;
        if (!trybStudiow.equals(that.trybStudiow)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTrybuStudiow;
        result = 31 * result + trybStudiow.hashCode();
        return result;
    }
}
