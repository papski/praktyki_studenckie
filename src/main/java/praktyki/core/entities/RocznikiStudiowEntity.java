package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 17.02.15.
 */
/**
CourseYearbookEntity<br>
JOINT TABLE BETWEEN FIELD OF STUDY (COURSE) AND ACADEMIC YEAR<br><br>
 ATTRIBUTES:<br>
 Integer idKierunku;             //ID of Course<br>
 Integer idRokuAkademickiego;    //ID of Academic Year<br><br>
 RELATIONS:<br>
 Collection<StudenciKierunkowEntity> studenci;   //1:M relation with CourseStudentsEntity<br>
 Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdKierunku;    //1:M relation with CoursePracticesEntity<br>
 KierunkiStudiowEntity kierunek; //M:1 relation with FieldOfStudyEntity<br>
 LataAkademickieEntity rok;  //M:1 relation with AcademicYearsEntity<br>
 */
@Entity
@Table(name = "roczniki_studiow", schema = "public", catalog = "praktykidb")
@IdClass(RocznikiStudiowEntityPK.class)
public class RocznikiStudiowEntity {
    private Integer idKierunku;             //ID of Course
    private Integer idRokuAkademickiego;    //ID of Academic Year

    private Collection<StudenciKierunkowEntity> studenci;   //1:M relation with CourseStudentsEntity
    private Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdKierunku;    //1:M relation with CoursePracticesEntity
    private KierunkiStudiowEntity kierunek; //M:1 relation with FieldOfStudyEntity
    private LataAkademickieEntity rok;  //M:1 relation with AcademicYearsEntity

    /*
    ATTRIBUTES
    */

    //ID of Course
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_kierunku")
    public Integer getIdKierunku() {
        return idKierunku;
    }

    public void setIdKierunku(Integer idKierunku) {
        this.idKierunku = idKierunku;
    }

    //ID of Academic Year
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_roku_akademickiego")
    public Integer getIdRokuAkademickiego() {
        return idRokuAkademickiego;
    }

    public void setIdRokuAkademickiego(Integer idRokuAkademickiego) {
        this.idRokuAkademickiego = idRokuAkademickiego;
    }

    /*
    RELATIONS
    */

    //1:M relation with CourseStudentsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "kierunek", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<StudenciKierunkowEntity> getStudenci() {
        return studenci;
    }

    public void setStudenci(Collection<StudenciKierunkowEntity> studenci) {
        this.studenci = studenci;
    }

    //1:M relation with CoursePracticesEntity
    @JsonIgnore
    @OneToMany(mappedBy = "kierunek", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<PraktykiKierunkoweEntity> getPraktykiKierunkoweByIdKierunku() {
        return praktykiKierunkoweByIdKierunku;
    }

    public void setPraktykiKierunkoweByIdKierunku(Collection<PraktykiKierunkoweEntity> praktykiKierunkoweByIdKierunku) {
        this.praktykiKierunkoweByIdKierunku = praktykiKierunkoweByIdKierunku;
    }

    //M:1 relation with FieldOfStudyEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_kierunku", referencedColumnName = "id_kierunku", insertable = false, updatable = false)
    public KierunkiStudiowEntity getKierunek() {
        return kierunek;
    }

    public void setKierunek(KierunkiStudiowEntity kierunek) {
        this.kierunek = kierunek;
    }

    //M:1 relation with AcademicYearsEntity
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_roku_akademickiego", referencedColumnName = "id_roku_akademickiego", insertable = false, updatable = false)
    public LataAkademickieEntity getRok() {
        return rok;
    }

    public void setRok(LataAkademickieEntity rok) {
        this.rok = rok;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RocznikiStudiowEntity)) return false;

        RocznikiStudiowEntity that = (RocznikiStudiowEntity) o;

        if (idKierunku != null ? !idKierunku.equals(that.idKierunku) : that.idKierunku != null) return false;
        if (idRokuAkademickiego != null ? !idRokuAkademickiego.equals(that.idRokuAkademickiego) : that.idRokuAkademickiego != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idKierunku != null ? idKierunku.hashCode() : 0;
        result = 31 * result + (idRokuAkademickiego != null ? idRokuAkademickiego.hashCode() : 0);
        return result;
    }
}
