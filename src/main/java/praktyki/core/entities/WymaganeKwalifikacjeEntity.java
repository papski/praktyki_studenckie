package praktyki.core.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by dawid on 28.02.15.
 */
/**
RequiredQualificationsEntity<br>
JOINT TABLE BETWEEN TEMPLATES AND QUALIFICATIONS<br><br>
 ATTRIBUTES:<br>
 int idSzablonu;         //ID of Tempplate<br>
 int idKwalifikacji;     //ID of Qulification<br><br>
 RELATIONS:<br>
 SzablonyPraktykEntity szablon;      //M:1 relation with TemplatesEntity<br>
 KwalifikacjeEntity kwalifikacja;    //M:1 relation with QualificationsEntity<br>
*/
@Entity
@IdClass(WymaganeKwalifikacjeEntityPK.class)
@Table(name = "wymagane_kwalifikacje", schema = "public", catalog = "praktykidb")
public class WymaganeKwalifikacjeEntity {
    private int idSzablonu;         //ID of Tempplate
    private int idKwalifikacji;     //ID of Qulification

    private SzablonyPraktykEntity szablon;      //M:1 relation with TemplatesEntity
    private KwalifikacjeEntity kwalifikacja;    //M:1 relation with QualificationsEntity

    /*
    ATTRIBUTES
    */

    //ID of Tempplate
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_szablonu")
    public int getIdSzablonu() {
        return idSzablonu;
    }

    public void setIdSzablonu(int idSzablonu) {
        this.idSzablonu = idSzablonu;
    }

    //ID of Qulification
    @Id
    @GenericGenerator(name = "assigned", strategy = "assigned")
    @Column(name = "id_kwalifikacji")
    public int getIdKwalifikacji() {
        return idKwalifikacji;
    }

    public void setIdKwalifikacji(int idKwalifikacji) {
        this.idKwalifikacji = idKwalifikacji;
    }

    /*
    RELATIONS
    */

    //M:1 relation with TemplatesEntity
    @ManyToOne
    @JoinColumn(name = "id_szablonu", referencedColumnName = "id_szablonu", insertable = false, updatable = false)
    public SzablonyPraktykEntity getSzablon() {
        return szablon;
    }

    public void setSzablon(SzablonyPraktykEntity szablon) {
        this.szablon = szablon;
    }

    //M:1 relation with QualificationsEntity
    @ManyToOne
    @JoinColumn(name = "id_kwalifikacji", referencedColumnName = "id_kwalifikacji", insertable = false, updatable = false)
    public KwalifikacjeEntity getKwalifikacja() {
        return kwalifikacja;
    }

    public void setKwalifikacja(KwalifikacjeEntity kwalifikacja) {
        this.kwalifikacja = kwalifikacja;
    }

    /*
    EQUALS AND HASHCODE
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WymaganeKwalifikacjeEntity)) return false;

        WymaganeKwalifikacjeEntity that = (WymaganeKwalifikacjeEntity) o;

        if (idKwalifikacji != that.idKwalifikacji) return false;
        if (idSzablonu != that.idSzablonu) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idSzablonu;
        result = 31 * result + idKwalifikacji;
        return result;
    }
}
