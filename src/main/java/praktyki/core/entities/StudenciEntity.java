package praktyki.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by dawid on 08.12.14.
 */
/**
StudentsEntity<br>
ENTITY CONTAINING INFO ABOUT STUDENTS<br><br>
 ATTRIBUTES:<br>
 Long nrAlbumu;      //(ID) Student Number<br>
 Integer idAdresu;   //ID of Address<br>
 Integer idOsoby;    //ID of Person<br><br>
 RELATIONS:<br>
 AdresyEntity adresyByIdAdresu;  //1:1 relation with AddressesEntity<br>
 OsobyEntity studentByIdOsoby;   //1:1 relation with PersonsEntity<br>
 Collection<StudenciKierunkowEntity> studenciByIdKierunku;   //1:M relation with CourseStudentsEntity<br>
 */
@Entity
@Table(name = "studenci", schema = "public", catalog = "praktykidb")
public class StudenciEntity {
    private Long nrAlbumu;      //(ID) Student Number
    private Integer idAdresu;   //ID of Address
    private Integer idOsoby;    //ID of Person

    private AdresyEntity adresyByIdAdresu;  //1:1 relation with AddressesEntity
    private OsobyEntity studentByIdOsoby;   //1:1 relation with PersonsEntity
    private Collection<StudenciKierunkowEntity> studenciByIdKierunku;   //1:M relation with CourseStudentsEntity

    /*
    ATRYBUTY
     */

    //(ID) Student Number
    @Id
    @GenericGenerator(name="assigned", strategy = "assigned")
    @Column(name = "nr_albumu", nullable = false)
    public Long getNrAlbumu() {
        return nrAlbumu;
    }

    public void setNrAlbumu(Long nrAlbumu) {
        this.nrAlbumu = nrAlbumu;
    }

    //ID of Address
    @JsonIgnore
    @Basic
    @Column(name = "id_adresu")
    public Integer getIdAdresu() {
        return idAdresu;
    }

    public void setIdAdresu(Integer idAdresu) {
        this.idAdresu = idAdresu;
    }

    //ID of Person
    @JsonIgnore
    @Basic
    @Column(name = "id_osoby")
    public Integer getIdOsoby() {
        return idOsoby;
    }

    public void setIdOsoby(Integer idOsoby) {
        this.idOsoby = idOsoby;
    }

    /*
    RELACJE
     */

    //1:1 relation with AddressesEntity
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_adresu", referencedColumnName = "id_adresu", insertable = false , updatable = false)
    public AdresyEntity getAdresyByIdAdresu() {
        return adresyByIdAdresu;
    }

    public void setAdresyByIdAdresu(AdresyEntity adresyByIdAdresu) {
        this.adresyByIdAdresu = adresyByIdAdresu;
    }

    //1:1 relation with PersonsEntity
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_osoby", referencedColumnName = "id_osoby", insertable = false, updatable = false)
    public OsobyEntity getStudentByIdOsoby() {
        return studentByIdOsoby;
    }

    public void setStudentByIdOsoby(OsobyEntity studentByIdOsoby) {
        this.studentByIdOsoby = studentByIdOsoby;
    }

    //1:M relation with CourseStudentsEntity
    @JsonIgnore
    @OneToMany(mappedBy = "studenciByNrAlbumu", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Collection<StudenciKierunkowEntity> getstudenciByIdKierunku() {
        return studenciByIdKierunku;
    }

    public void setstudenciByIdKierunku(Collection<StudenciKierunkowEntity> studenciByIdKierunku) {
        this.studenciByIdKierunku = studenciByIdKierunku;
    }


    /*
    EQUALS I HASHCODE
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudenciEntity)) return false;

        StudenciEntity that = (StudenciEntity) o;

        if (idAdresu != null ? !idAdresu.equals(that.idAdresu) : that.idAdresu != null) return false;
        if (idOsoby != null ? !idOsoby.equals(that.idOsoby) : that.idOsoby != null) return false;
        if (nrAlbumu != null ? !nrAlbumu.equals(that.nrAlbumu) : that.nrAlbumu != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nrAlbumu != null ? nrAlbumu.hashCode() : 0;
        result = 31 * result + (idAdresu != null ? idAdresu.hashCode() : 0);
        result = 31 * result + (idOsoby != null ? idOsoby.hashCode() : 0);
        return result;
    }
}


