## STUDENT PRACTICES MANAGEMENT PLATFORM ##

This project was created as my Final Thesis for my Engineer's Degree in Computer Science. With limited amount of time that i got, platform should contain pretty much all basic functionalities for this kind of platform. For now only things that are missing :

1. No uploading files on server
2. No JasperReports
3. No SpringSecurity/User authorization

MailController with all methods needed fot it to work _WAS NOT_ tested

### Download ###

In Download section You will find :

1. Database image
2. INSERT.sql file with couple of inserts into entities that don't have/shouldn't use controller (for example You add there something once a year)
3. Javadoc in .zip file
4. Collection backup for Postman  (RESTful client for Chrome) - testing purpose

### Comments ###

Every Controller and Entity has been commented with JavaDoc comments. Every method in project is written in english just like a lot of things in DAOs and Services - no need for comments right now.

### Database ###

Here is a link to DB http://images69.fotosik.pl/971/66a7f476fe267a29.png 


Before we launch our Application we should change value of `hibernate.hbm2ddl.auto` to `create` to create our DB on server. After that we should use value `validate`. Default catalog of DB is `praktykidb`, to change it we need to rename `catalog` value of each Entity manually.


### Exporting SQL Schema ###
Type this in commane line 
`mvn hibernate4:export` and schema.sql will appear in `/target` folder.

### Entities/DAOs List ###

Polish Name           |  English Name
----------------------|---------------------------
1. Stopnie studiów     |Course's Grades
2. Tytuły zawodowe	|Course's Degrees
3. Tryb studiów	|Course's Mode
4. Kierunki studiów	|Field of Studies (Courses)
5. Roczniki studiów	|Course's Yearbook
6. Koordynatorzy kierunkowi|	Course's Coordinators
7. Studenci kierunków	|Course's Students
8. Koordynatorzy praktyk|	Coordinators
9. Studenci|	Students
10. Koordynatorzy Kierunkowi Praktykodawcy|	Employer's Courses Coordinators
11. Adresy|	Addresses
12. Adresy Praktykodawców|	Employer's Addresses
13. Osoby|	Persons
14. Użytkownicy|	Users
15. Role|	Roles
16. Lata akademickie|	Academic Year
17. Jednostki kalendarzowe|	Calendar Units
18. Praktyki kierunkowe|	Course's Practices
19. Typy praktyk	|Practice's Types
20. Szablony praktyk|	Templates
21. Wymagane kwalifikacje|	Required Qualifications
22. Oferowane kwalifikacje|	Offered Qualifications
23. Kwalifikacje|	Qualifications
24. Kategorie kwalifikacji|	Qualifications' Categories
25. Statusy	|Statuses
26. Porozumienia|	Agreements
27. Typy zgłoszeń|	Application Types
28. Zgłoszenia praktykodawców.	|Employer's Applications
29.Opiekunowie kierunkowi|	Course's Tutors
30. Opiekunowie praktyk	|Tutors
31. Praktykodawcy	|Employers
32. Profil	|Profiles
33. Profile praktykodawcy|	Employer's Profiles
34. Praktyki	|Practices


### Controller List ###

Polish Name             |      English Name
-----------------------|---------------------------------
AdresyController |AddressesControler
KategorieKwalifikacjiController |QualificationCategoriesController
KierunkiStudiowController |FieldOfStudiesController (CoursesController)
KoordynatorzyKierunkowiController |CourseCoordinatorsController
KoordynatorzyPraktykController |CoordinatorsController
MailController | MailController
OpiekunowiePraktykController |TutorsController
OsobyController |PersonsController
PorozumieniaController |AgreementsController
PraktykiController |PracticesController
PraktykiKierunkoweController |CoursePracticesController
PraktykodawcyController |EmployersController
ProfilController |ProfilesController
StudenciController |StudentsController
SzablonyPraktykController |TemplatesController
TrybyStudiowController |CourseModeController
TypyPraktykController |PracticeTypesController
TypyZgloszenController |ApplicationTypesController
UzytkownicyController |UsersController
ZgloszeniaPraktykodawcowController |EmployerApplicationsController